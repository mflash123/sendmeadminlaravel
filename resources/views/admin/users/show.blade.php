@extends('layouts.admin.app')

@section('content')
<div class="container">
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row justify-content">
            <div class="col-md-6">
                <form method="POST" action="/admin/users/{{ $user->id }}">
                    @csrf
                    @method('PUT')
        
                    <div class="form-group">
                        <label>Username / {{ $user->created_on }}</label>
                        <div class="d-flex" style="font-size:10px">last login: {{$user->last_login}}</div>
                        <input autocomplete="off" type="text" name="username" class="form-control" value="{{ $user->username }}">
                        <a class="d-flex" target="_blank" href="https://sendme.cc/{{ $user->username }}">https://sendme.cc/{{ $user->username }}</a>
                        <a class="d-flex" target="_blank" href="https://instagram.com/{{ $user->username }}">https://instagram.com/{{ $user->username }}</a>
        
                    </div>
        
                    <div class="form-group">
                        <label>Email address</label>
                        <input autocomplete="off" type="email" name="email" class="form-control" value="{{ $user->email }}">
                    </div>
        
                    <div class="form-group">
                        <select class="form-control" name="tarif_id">
                            @foreach ($tarif as $item)
                                <option value=" {{$item->id}} " {{ $item->selected ? 'selected':''}} > {{$item->name}} </option>
                            @endforeach
                        </select>
                    </div>
        
                    <div class="form-group">
                        <label>Active</label>
                        <input autocomplete="off" type="text" name="active" class="form-control" value="{{ $user->active }}">
                    </div>
        
                    <div class="form-group">
                        <label>Comment</label>
                    <textarea name="comment" class="form-control">{{ $user->comment }}</textarea>
                    </div>
        
                    <button type="submit">Save</button>
                    
                    
                </form>
                <form class="float-right" method="POST" action="/admin/users/{{ $user->id }}">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Destroy</button>
                </form>
            </div>

  <div class="col-md-6">
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">tarif</th>
            <th scope="col">status</th>
            <th scope="col">date</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($user->order as $item)
            <tr>
                <th scope="row">
                    <div class="d-flex"><a href="/admin/orders/{{ $item->id }}">{{ $item->id }}</a></div>
                </th>
                <td>
                    <div class="d-flex">{{$item->tarif->name}}</div>
                </td>
                <td>{{$item->statusorder->name}}</td>
                <td style="font-size:10px">{{$item->date}} - {{$item->subscribe_until}}</td>
            </tr>
            @endforeach
        </tbody>
      </table>
  </div>


          
        <table class="table table-hover">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">type</th>
                <th scope="col">data</th>
                <th scope="col">date</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($user->content as $item)
                <tr>
                    <th scope="row">
                        <div class="d-flex"><a href="/admin/content/{{ $item->id }}/edit">{{ $item->id }}</a></div>
                    </th>
                    <td>
                        <div class="d-flex">{{$item->type->name}} id:{{$item->type_id}}</div>
                    </td>
                    <td>{{$item->data}}</td>
                    <td>{{$item->date}}</td>
                </tr>
                @endforeach
            </tbody>
          </table>
    </div>
</div>
@endsection
