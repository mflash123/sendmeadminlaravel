@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Users count: {{$users_count}}</h1>

        <div style="width: 100%">
            {!! $Chart->container() !!}
        </div>
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">username</th>
                    <th scope="col">email</th>
                    <th scope="col">utm</th>
                    <th scope="col">active</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($users as $user)
                    <tr>
                        <th scope="row">
                            <div class="d-flex"><a href="users/{{ $user->id }}/edit">{{ $user->id }}</a></div>
                            <div class="d-flex" style="font-size:10px">{{ $user->created_on }}</div>
                            <div class="d-flex" style="font-size:10px">{{ $user->is_mobile?'Mobile':'Desktop' }}</div>
                            <div class="d-flex" style="font-size:10px">last: {{$user->last_login}}</div>
                            <div class="d-flex" style="font-size:10px">content: {{ isset($user->content) ? count($user->content) : 0}}</div>
                            <div class="d-flex" style="font-size:10px">tarif: {{ $user->tarif->name }}</div>
                        </th>
                        <td>
                            <div class="d-flex">{{$user->username}}</div>
                            <div class="d-flex">{{$user->tarifName}}</div>
                        </td>
                        <td>{{$user->email}}</td>
                        <td>
                            @if ( !is_null($user->utm) )
                                @dump(json_decode($user->utm))
                            @endif
                        </td>
                        <td>{{$user->active}}</td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            @if($users instanceof \Illuminate\Pagination\LengthAwarePaginator )
                {{$users->links()}}
            @endif

              
        
    </div>
</div>
@endsection
