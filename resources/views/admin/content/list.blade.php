@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Count: {{$count}}</h1>

        <div style="width: 100%">
            {{-- {!! $Chart->container() !!} --}}
        </div>
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">username</th>
                    <th scope="col">data</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($contents as $content)
                    @php
                       // dd($content);
                    @endphp
                    <tr>
                        <th scope="row">
                            <div class="d-flex"><a href="content/{{ $content->id }}/edit">{{ $content->id }}</a></div>
                            <div class="d-flex" style="font-size:10px">{{ $content->date }}</div>
                        </th>
                        <td>
                            <div class="d-flex">
                                {{$content->user->username }}
                            </div>
                            <div class="d-flex">{{$content->user->email}}</div>
                        </td>
                        <td>
                            {{$content->data}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            @if($contents instanceof \Illuminate\Pagination\LengthAwarePaginator )
                {{$contents->links()}}
            @endif

              
        
    </div>
</div>
@endsection
