@extends('layouts.admin.app')

@section('content')
<div class="container">
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row justify-content">
        
        <form method="POST" action="/admin/content/{{ $content->id }}">
            @csrf
            @method('PUT')

            <div class="form-group">
                <ul class="list-unstyled">
                    <li>User: {{$content->user->username}} / {{$content->user->email}}<li>
                    <li>Tarif: {{$content->user->tarif->name}}</li>
                    <li>Type: {{$content->type->name}} {{$content->type->placeholder}}<li>
                    <li>Date: {{$content->date}} <li>
                </ul>
                <textarea type="text" name="data" class="form-control">{{ $content->data }}</textarea>
            </div>



            <button type="submit">Save</button>

        </form>
        
        <form method="POST" action="/admin/content/{{ $content->id }}">
            @csrf
            @method('DELETE')
            <button type="submit">Delete</button>
        </form>
        
    </div>
</div>
@endsection
