@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <h1>Count: {{$count}}</h1>

        <div style="width: 100%">
            {!! $Chart->container() !!}
        </div>
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">username</th>
                    <th scope="col">summ</th>
                    <th scope="col">status</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                    <tr>
                        <th scope="row">
                            <div class="d-flex"><a href="orders/{{ $order->id }}">{{ $order->id }}</a></div>
                            <div class="d-flex" style="font-size:10px">{{ $order->date }}</div>
                            <div class="d-flex" style="font-size:10px">Ends: {{ $order->subscribe_until }}</div>
                        </th>
                        <td>
                            @if ($order->user) 
                                <div class="d-flex">
                                    {{$order->user->username }}
                                </div>
                                <div class="d-flex">{{$order->user->email}}</div>
                            @endif
                        </td>
                        <td>@if ($order->user) {{$order->user->tarif->name}} @endif</td>
                        <td>{{$order->statusorder->name}}</td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
            @if($orders instanceof \Illuminate\Pagination\LengthAwarePaginator )
                {{$orders->links()}}
            @endif

              
        
    </div>
</div>
@endsection
