@extends('layouts.admin.app')

@section('content')
<div class="container">
    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif
    <div class="row justify-content">
        <form method="POST" action="/admin/orders/{{ $order->id }}">
            @csrf
            @method('PUT')

            <div class="form-group">
                <label>Order / {{ $order->date }}</label>
                <label>ID: {{ $order->id }}</label>
                <label class="d-flex">Ends: {{ $order->subscribe_until }}</label>
            </div>

            <div class="form-group">
                <label>Tarif: {{ $order->tarif->name }} {{ $order->tarif->priceRUB }}RUB</label>
                <label class="d-flex">Status: {{ $order->statusorder->name }}</label>

                <label class="d-flex"><a href="/admin/users/{{ $order->user->id }}/edit">{{ $order->user->email }}</a></label>
                <label class="d-flex"><a target="_blank" href="https://sendme.cc/{{ $order->user->username }}">https://sendme.cc/{{ $order->user->username }}</a></label>
            </div>

            <input hidden value="2" name="status" />
            <button type="submit">Pay order</button>


        </form>
  
        
    </div>
</div>
@endsection
