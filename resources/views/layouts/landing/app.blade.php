<!doctype html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge" /><![endif]-->
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="/land_assets/img/FAVICON-01.ico">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,300,400,700,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet">
    <title><?=isset($title)?$title:'Sendme - умные мультиссылки ссылки для продаж через социальные сети'?></title>
    <link rel="stylesheet" href="/land_assets/css/vendor.min.css">
    <link rel="stylesheet" href="/land_assets/css/dashcore.min.css">
    <link rel="stylesheet" href="/land_assets/vendors/themify-icon/themify-icons.css">
    <link rel="stylesheet" href="/land_assets/vendors/flaticon/flaticon.css">
    <script src="https://kit.fontawesome.com/c96d33c528.js"></script>
    <link rel="stylesheet" href="/land_assets/css/style.css">
    <link rel="stylesheet" href="/land_assets/css/responsive.css">
    <?php if(isset($description)):?>
        <meta name="description" content="<?=$description?>" />
    <?php else:?>
        <meta name="description" content="Sendme - умные мультссылки для продаж через социальные сети" />
    <?php endif?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MQ3TTSX');</script>
    <!-- End Google Tag Manager -->
    <script src="//code.jivosite.com/widget.js" data-jv-id="Pcv2LhpIM3" async></script>
    <script charset="UTF-8" src="//cdn.sendpulse.com/js/push/319e71b5c091d364c90a56d2e14e90d5_1.js" async></script>
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQ3TTSX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Making stripe menu -->

<nav class="st-nav navbar main-nav navigation" id="main-nav" style="background-color: #081b37;">
    <div class="container">
        <ul class="st-nav-menu nav navbar-nav">
        <li class="st-nav-section nav-item">
            <a href="/" class="navbar-brand"><img src="/land_assets/img/logo-stick.svg"  style="height: 40px" alt="Dashcore" class="logo logo-sticky d-block d-md-none"> <img src="/land_assets/img/logo-stick.svg" style="height: 40px" alt="Dashcore" class="logo d-none d-md-block"></a>
        </li>
        <li class="st-nav-section st-nav-primary nav-item"><a class="st-root-link nav-link" href="/tarif" style="color: white;">Тарифы</a></li>
        <!-- <li class="st-nav-section st-nav-primary nav-item"><a class="st-root-link nav-link" href="/blog">Возможности</a></li> -->
        <li class="st-nav-section st-nav-primary nav-item"><a class="st-root-link nav-link" href="/blog" style="color: white;">Блог</a></li>
        <li class="st-nav-section st-nav-secondary nav-item"><a class="btn btn-rounded btn-outline mr-3 px-3" href="/signin"><i class="fas fa-sign-in-alt d-none d-md-inline mr-md-0 mr-lg-2"></i> <span class="d-md-none d-lg-inline">Войти</span> </a></li>

    {{-- <li class="st-nav-section st-nav-mobile nav-item">
        <button class="st-root-link navbar-toggler" type="button"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
        <div class="st-popup">
        <div class="st-popup-container"><a class="st-popup-close-button">Close</a>
        <div class="st-dropdown-content-group">
        <a class="regular text-primary" href="/tarif">Тарифы </a>
        <a class="regular text-primary" href="/blog">Блог </a>
        </div>

        </div>
        </div>
    </li> --}}
        </ul>
    </div>
    <div class="st-dropdown-root">
        <div class="st-dropdown-bg">
            <div class="st-alt-bg"></div>
        </div>
        <div class="st-dropdown-arrow"></div>
        {{-- <div class="st-dropdown-container">
            <div class="st-dropdown-section" data-dropdown="blocks">
                <div class="st-dropdown-content">
                    <div class="st-dropdown-content-group">
                        <div class="row">
                            <div class="col mr-4"><a class="dropdown-item" target="_blank" href="blocks/call-to-action.html">Call to actions</a> <a class="dropdown-item" target="_blank" href="blocks/contact.html">Contact</a> <a class="dropdown-item" target="_blank" href="blocks/counter.html">Counters</a> <a class="dropdown-item" target="_blank" href="blocks/faqs.html">FAQs</a></div>
                            <div class="col mr-4"><a class="dropdown-item" target="_blank" href="blocks/footer.html">Footers</a> <a class="dropdown-item" target="_blank" href="blocks/form.html">Forms</a> <a class="dropdown-item" target="_blank" href="blocks/navbar.html">Navbar</a> <a class="dropdown-item" target="_blank" href="blocks/navigation.html">Navigation</a></div>
                            <div class="col"><a class="dropdown-item" target="_blank" href="blocks/pricing.html">Pricing</a> <a class="dropdown-item" target="_blank" href="blocks/slider.html">Sliders</a> <a class="dropdown-item" target="_blank" href="blocks/team.html">Team</a> <a class="dropdown-item" target="_blank" href="blocks/testimonial.html">Testimonials</a></div>
                        </div>
                    </div>
                    <div class="st-dropdown-content-group">
                        <h3 class="link-title"><i class="fas fa-long-arrow-alt-right icon"></i> Coming soon</h3>
                        <div class="ml-5"><span class="dropdown-item text-secondary">Dividers </span><span class="dropdown-item text-secondary">Gallery </span><span class="dropdown-item text-secondary">Screenshots</span></div>
                    </div>
                </div>
            </div>

            <div class="st-dropdown-section" data-dropdown="components">
                <div class="st-dropdown-content">
                    <div class="st-dropdown-content-group">
                        <a class="dropdown-item" target="_blank" href="components/color.html">
                            <div class="media mb-4"><i class="fas fa-palette icon fa-2x"></i>
                                <div class="media-body">
                                    <h3 class="link-title m-0">Colors</h3>
                                    <p class="m-0 text-secondary">Get to know DashCore color options</p>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item" target="_blank" href="components/accordion.html">
                            <div class="media mb-4"><i class="fas fa-bars icon fa-2x"></i>
                                <div class="media-body">
                                    <h3 class="link-title m-0">Accordion</h3>
                                    <p class="m-0 text-secondary">Useful accordion elements</p>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item" target="_blank" href="components/cookie-law.html">
                            <div class="media mb-4"><i class="fas fa-cookie-bite icon fa-2x"></i>
                                <div class="media-body">
                                    <h3 class="link-title m-0">CookieLaw</h3>
                                    <p class="m-0 text-secondary">Comply with the hideous EU Cookie Law</p>
                                </div>
                            </div>
                        </a>
                        <h4 class="text-uppercase regular">Huge components list</h4>
                        <div class="row">
                            <div class="col mr-4"><a class="dropdown-item" target="_blank" href="components/alert.html">Alerts</a> <a class="dropdown-item" target="_blank" href="components/badge.html">Badges</a> <a class="dropdown-item" target="_blank" href="components/button.html">Buttons</a></div>
                            <div class="col mr-4"><a class="dropdown-item" target="_blank" href="components/overlay.html">Overlay</a> <a class="dropdown-item" target="_blank" href="components/progress.html">Progress</a> <a class="dropdown-item" target="_blank" href="components/lightbox.html">Lightbox</a></div>
                            <div class="col mr-4"><a class="dropdown-item" target="_blank" href="components/tab.html">Tabs</a> <a class="dropdown-item" target="_blank" href="components/tables.html">Tables</a> <a class="dropdown-item" target="_blank" href="components/typography.html">Typography</a></div>
                        </div>
                    </div>
                    <div class="st-dropdown-content-group"><a class="dropdown-item" target="_blank" href="components/wizard.html">Wizard <span class="badge badge-pill badge-primary">New</span></a> <span class="dropdown-item d-flex align-items-center text-muted">Timeline <i class="fas fa-ban ml-auto"></i></span> <span class="dropdown-item d-flex align-items-center text-muted">Process <i class="fas fa-ban ml-auto"></i></span></div>
                </div>
            </div>
        </div> --}}
    </div>
</nav>




@yield('content');




<footer class="footer_area h_footer_dark pos_footer_area" style="margin-top: 200pt;">
    <div class="top_shap"><img src="/land_assets/img/pos/fotter_shap.png" alt="" data-pagespeed-url-hash="1069738004" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <div class="f_widget dark_widget company_widget">
                    <a href="/" class="f-logo"><img src="/land_assets/img/logo-stick.svg" style="height: 40px" alt="" data-pagespeed-url-hash="651001205" onload="pagespeed.CriticalImages.checkImageForCriticality(this);"></a>
                    <p>© 2019-<?php echo date("Y"); ?> Sendme, All rights reserved.</p>
                    <p style="margin-top:0">ИП "Марков П.Д." ( ИНН: 540201317871 )</p>
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fsendmecc%2F&tabs=timeline&width=340&height=500&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=true&appId=2522856534684017" width="340" height="350" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
                    <div class="f_social_icon">
                        <a target="_blank" href="https://www.facebook.com/sendmecc"><i class="fab fa-facebook"></i></a>
                        <a target="_blank" href="https://www.youtube.com/channel/UCt4UPSRenth0havIeTwndLA" ><i class="fab fa-youtube"></i></a>
                        <a target="_blank" href="https://instagram.com/sendmecc/"><i class="fab fa-instagram"></i></a>
                        <a target="_blank" href="https://t.me/sendmecc"><i class="fab fa-telegram"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <script async src="https://telegram.org/js/telegram-widget.js?15" data-telegram-post="sendmecc/{{env('TELEGRAM_POST')}}" data-width="100%"></script>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="f_widget dark_widget about-widget pl_70">
                    <h3 class="f-title f_500 t_color f_size_18 mb_40">Информация</h3>
                    <ul class="list-unstyled f_list">
                        <li><a href="/blog/p/privacypolicy">Политика конфиденциальности</a></li>
                        <li><a href="/blog/p/usloviya-ispolzovaniya">Условия использования</a></li>
                        <li><a href="/blog/p/term">Договор-оферта</a></li>
                        <li><a target="_blank" href="https://sendmecc.sendmecc">Пример использования</a></li>
<!--                             <li><a href="">Тарифы</a></li>
                        <li><a href="">Блог</a></li> -->
                    </ul>
                </div>
            </div>
            {{-- <div class="col-lg-2 col-md-2">
                <div class="f_widget dark_widget about-widget pl_20">
                    <h3 class="f-title f_500 t_color f_size_18 mb_40">Помощь</h3>
                    <ul class="list-unstyled f_list">
                        <li><a target="_blank" href="https://sendmecc.sendmecc">Пример использования</a></li>
                    </ul>
                </div>
            </div> --}}

        </div>

    </div>

</footer>

</main>

<script src="/land_assets/js/dashcore.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
<script>
$(function() {
    $('.price_btn').on('click',function(){
        ym(51755045, 'reachGoal', 'clpfromlk');
        return true;
    })
    
});
</script>
</body>

</html>
