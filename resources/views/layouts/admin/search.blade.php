@section('search')
    @auth
        <form class="form-inline my-2 my-lg-0" method="GET" action="/admin/{{$search}}">
            <input name="{{$search}}" class="form-control mr-sm-2" type="search" placeholder="Search" autocomplete="off">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search {{$search}}</button>
        </form>
    @endauth
@show