@extends('layouts.landing.app')

    @section('content')
    <div class="row">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="/">Главная</a></li>
                  <li class="breadcrumb-item active">Блог</li>
                </ol>
              </nav>
            <div class="row">
                @foreach ($posts as $post)
                <div class="col-md-4 mb-4" style="height: 250px">
                    <h5 class="card-title">{{$post->title}}</h5>
                    <p class="card-text">{{$post->description}}</p>
                    <a href="/blog/p/{{$post->url}}" class="btn btn-primary" style="position: absolute;bottom: 0;">Читать</a>
                </div>
                @endforeach
            </div> 
        </div>
    </div>
            @endsection