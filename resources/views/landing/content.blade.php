@extends('layouts.landing.app',[
'title'=>$title,
'description'=>$description,
])


@section('content')
{{-- <style>
    h1,
    h2,
    h3,
    h4 {
        color: black
    }

</style> --}}
<div class="row">
    <div class="container" >
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="/">Главная</a></li>
              <li class="breadcrumb-item"><a href="/blog">Блог</a></li>
              <li class="breadcrumb-item active" aria-current="page">{{ $title }}</li>
            </ol>
          </nav>
        {!! $content !!}
    </div>
</div>
@endsection
