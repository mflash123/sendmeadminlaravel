@extends('layouts.landing.app')

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js" crossorigin="anonymous"></script>
<style>


    /* Embed google fonts for demo layout
    -----------------------------------------*/
    @import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900&display=swap');


    /* Main Page Layout 
    -- NOTE: Just for the demo page, and not
    required for the video modal... Video Modal
    styles are down below --
    -----------------------------------------*/
    * {
    box-sizing: border-box;
    }
    html {
    background: #fcfcfc;
    }
    @media (min-width: 641px) {
    html {
        background: #4b4b4b;
    }
    }
    body {
    font-family: 'Fira Sans', sans-serif;
    }
    .noscroll { 
        overflow: hidden;
    }


    main {
    font-size: 0;
    }
    main .column.left,
    main .column.right {
    background: #fcfcfc;
    display: block;
    position: relative;
    font-size: 1rem;
    width: 100%;
    min-height: 0;
    vertical-align: top;
    }
    @media (min-width: 641px) {
        main .column.left,
        main .column.right {
        display: inline-block;
        width: 50%;
        min-height: 100vh;
        }
        main .column.left {
        position: fixed;  
        }
        main .column.right {
        margin-left: 50%;
        }
    }

    .video-banner {
    display: block;
    }
    @media (min-width: 641px) {
    .video-banner {
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        z-index: 0;
    }
    }

    .video-banner .video-banner-img {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: 50% 50%;
    }
    .video-banner-headline {
    position: absolute;
    left: 1.5rem;
    bottom: 1.5rem;
    
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 1.25em;
    font-weight: 900;
    line-height: 1.0em;
    color: #fff;
    text-transform: uppercase;
    letter-spacing: 0.045em;
    }
    @media (min-width: 641px) {
    .video-banner-headline {
        left: 2em;
        bottom: 2.75em;
        font-size: 1.5em;
        line-height: 1.5em;
    }
    }
    .video-banner-icon-play {
    position: absolute;
    left: 50%;
    top: 50%;
    
    display: block;
    width: 2.5em;
    height: 2.5em;
    
    -webkit-transform: translateX(-50%) translateY(-50%) scale(1.0);
        -moz-transform: translateX(-50%) translateY(-50%) scale(1.0);
        -ms-transform: translateX(-50%) translateY(-50%) scale(1.0);
        -o-transform: translateX(-50%) translateY(-50%) scale(1.0);
        transform: translateX(-50%) translateY(-50%) scale(1.0);
    
    -webkit-transition: 
            all 0.2s ease-out 0.05s;
        transition: 
            all 0.2s ease-out 0.05s;
    }
    @media (min-width: 641px) {
    .video-banner-icon-play {
        width: 4.5em;
        height: 4.5em;
    }
    }
    .video-banner-icon-play:hover {
    -webkit-transform: translateX(-50%) translateY(-50%) scale(1.2);
        -moz-transform: translateX(-50%) translateY(-50%) scale(1.2);
        -ms-transform: translateX(-50%) translateY(-50%) scale(1.2);
        -o-transform: translateX(-50%) translateY(-50%) scale(1.2);
        transform: translateX(-50%) translateY(-50%) scale(1.2);
    
    backgound: #330099;
    }

    .content {
    display: block;
    min-height: 100vh;
    max-width: 40rem;
    margin: 2.5rem auto;
    padding: 1.5rem;
    }
    @media (min-width: 641px) {
    .content {
        margin: 5.75rem auto;
        padding: 2.5rem;
    }
    }

    .content h1,
    .content h2,
    .content p {
    font-family: 'Source Sans Pro', sans-serif;
    font-size: 1em;
    font-weight: 300;
    line-height: 1.5em;
    color: #0D013D;
    }
    .content h1 {
    font-size: 3.25em;
    font-weight: 200;
    line-height: 1.0em;
    
    margin: 0 0 0.5rem 0;
    }
    @media (min-width: 641px) {
    .content h1 {
        font-size: 4em;
    }
    }
    .content h2 {
    font-size: 1em;
    font-weight: 600;
    text-transform: uppercase;
    letter-spacing: 0.155em;
    
    margin-top: 3em;
    }

    /* buttons */
    .content .button-group {
    margin-top: 1em;
    }
    .content .button {
    display: inline-block;
    padding: 1em 2em;
    min-width: 8em;
    text-align: center;
    font-weight: 900;
    text-decoration: none;
    border-radius: 1.5em;
    box-shadow: 0 1px 1px rgba(0,0,0,0.25);
    
    /* button theme - secondary is default */
    background: #f0f0f0;
    color: #2B2B2B;
    
    -webkit-transition: 
            all 0.2s ease-out 0.05s;
        transition: 
            all 0.2s ease-out 0.05s;
    }
    .content .button ~ .button {
    margin-left: 0.5em;
    }
    .content .button:hover {
    background: #e5e5e5;
    }
    /* primary button overrides */
    .content .button.primary {
    background: #E50055;
    color: #fff;
    }
    .content .button.primary:hover {
    background: #9D00E5;
    }


    /* video thumbnail grid */
    .content .video-thumb-grid {
    position: relative;
    font-size: 0; /* collapse the white-space */
    width: calc(100% + 2rem);
    margin: 0 -1rem;
    }
    .content .video-thumb {
    display: inline-block;
    vertical-align: top;
    
    text-decoration: none;
    
    /* calculate the width, gutters, and aspect ratio */
    width: calc((100% - 4rem) / 2);
    height: 0;
    padding-top: calc(((100% - 4rem) / 2) * 0.5625); /* 16:9 calc */
    
    margin: 1rem 1rem; /* gutters */
    
    overflow: hidden; /* clipping */
    
    position: relative; /* enable positioning for inner content */
    
    font-size: 1rem; /* reset the font size */
    
    -webkit-transform: scale(1.0);
        transform: scale(1.0);
    box-shadow: 0 0 0 rgba(0,0,0,0.0);
    
    -webkit-transition: 
            all 0.2s ease-out 0.05s;
        transition: 
            all 0.2s ease-out 0.05s;
    }
    .content .video-thumb:hover {
        -webkit-transform: scale(1.05);
        transform: scale(1.05);
    box-shadow: 0 2px 4px rgba(13,1,61,0.35);
    }
    .content .video-thumb img {
    display: block;
    width: 100%;
    height: 100%;
    
    /* position inside the 16:9 container */
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    
    /* clipping for source images that are letterboxed */
    object-fit: cover;
    object-position: 50% 50%;
    
    background: #2b2b2b;
    }








    /* Video Modal
    -----------------------------------------*/
    .video-modal,
    .video-modal .overlay {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 3000;
    }
    .video-modal {
        overflow: hidden;
        position: fixed;
        opacity: 0.0;

    -webkit-transform: translate(500%,0%);
    transform: translate(500%,0%);

    -webkit-transition: -webkit-transform 0s linear 0s;
    transition: transform 0s linear 0s;


    /* using flexbox for vertical centering */

    /* Flexbox display */
    display: -webkit-box;
    display: -webkit-flex;
    display: flex;

    /* Vertical alignment */
    -webkit-box-align: center;
    -moz-box-align: center;
    -ms-flex-align: center;
    -webkit-align-items: center;
    align-items: center;

    -webkit-transform-style: preserve-3d;
    -moz-transform-style: preserve-3d;
    transform-style: preserve-3d;
    }
    .video-modal .overlay {
    z-index: 0;
    background: rgba(13,1,61,0.82); /* overlay color */

    opacity: 0.0;

    -webkit-transition: opacity 0.2s ease-out 0.05s;
    transition: opacity 0.2s ease-out 0.05s;
    }


    .video-modal-content {
        position: relative;
        top: auto;
        right: auto;
        bottom: auto;
        left: auto;
        z-index: 1;
    
        margin: 0 auto;

        overflow-y: visible;

        background: #000;
    
    width: calc(60% - 12em);
    height: 0;
    padding-top: calc((100% - 12em) * 0.5625); /* 16:9 calc */
    }

    /* Scaling to fit within the current Viewport size:
    When viewport aspect ratio is greater than 16:9
    work off the height instead of the width for calc */
    @media (min-aspect-ratio: 16/9) {
    .video-modal-content {
        width: 0;
        height: calc(100vh - 10em);
        padding-top: 0;
        padding-left: calc((100vh - 10em) * 1.7778); /* 16:9 calc */
    }
    }

    /* Mobile Layout Tweaks - side margins reduced */
    @media (max-width: 640px) {
        .video-modal-content {
            width: calc(100% - 1em);
        padding-top: calc((100% - 1em) * 0.5625); /* 16:9 calc */
        
        }
    }

    .containermedia {
            padding-bottom: 25rem;
        }

    @media only screen and (max-width: 600px) {
        .biglead {
            font-size: 2rem !important
        }
        .containermedia {
            padding-bottom: 30rem !important
        }
        .lead {
            font-size: 1rem !important
        }
    }




    /* modal close button */
    .close-video-modal {
        display: block;
        position: absolute;
        left: 0;
        top: -40px;

        text-decoration: none;
        font-size: 20px;
        font-weight: bold;
        color: #fff;
    }

    /* set the iframe element to stretch to fit its parent element */
    iframe#youtube {
        position: absolute;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: 1;

        background: #000;
        box-shadow: 0px 2px 16px rgba(0,0,0,0.5);
    }

    /* show the modal: 
    add class to the body to reveal */
    .show-video-modal .video-modal {
        opacity: 1.0;

        transform: translate(0%,0%);
        -webkit-transform: translate(0%,0%);
    }
    .show-video-modal .video-modal .overlay {
        opacity: 1.0;
    }
    .show-video-modal .video-modal-content {
        transform: translate(0%,0%);
        -webkit-transform: translate(0%,0%);
    }


</style>
<script>
    $(document).ready(function(){


        /* Toggle Video Modal
        -----------------------------------------*/
        function toggle_video_modal() {
            
            // Click on video thumbnail or link
            $(".js-trigger-video-modal").on("click", function(e){
                console.log(1)
            
            // prevent default behavior for a-tags, button tags, etc. 
                e.preventDefault();
            
            // Grab the video ID from the element clicked
            var id = $(this).attr('data-youtube-id');

            // Autoplay when the modal appears
            // Note: this is intetnionally disabled on most mobile devices
            // If critical on mobile, then some alternate method is needed
            var autoplay = '?autoplay=1';

            // Don't show the 'Related Videos' view when the video ends
            var related_no = '&rel=0';

            // String the ID and param variables together
            var src = '//www.youtube.com/embed/'+id+autoplay+related_no;
            
            // Pass the YouTube video ID into the iframe template...
            // Set the source on the iframe to match the video ID
            $("#youtube").attr('src', src);
            
            // Add class to the body to visually reveal the modal
            $("body").addClass("show-video-modal noscroll");
            
        });

            // Close and Reset the Video Modal
        function close_video_modal() {
            
            event.preventDefault();

            // re-hide the video modal
            $("body").removeClass("show-video-modal noscroll");

            // reset the source attribute for the iframe template, kills the video
            $("#youtube").attr('src', '');
            
        }
        // if the 'close' button/element, or the overlay are clicked 
            $('body').on('click', '.close-video-modal, .video-modal .overlay', function(event) {
                
            // call the close and reset function
            close_video_modal();
                
        });
        // if the ESC key is tapped
        $('body').keyup(function(e) {
            // ESC key maps to keycode `27`
            if (e.keyCode == 27) { 
                
                // call the close and reset function
                close_video_modal();
                
            }
        });
        }
        toggle_video_modal();



        });
</script>


<main>
    <!-- ./App Landing - page header -->
    <header class="section header text-contrast app-landing-header">
        <div class="shape-wrapper">
            <div class="shape shape-background shape-main gradient gradient-purple-navy"></div>
            <div class="shape shape-background shape-top gradient gradient-navy-purple"></div>
        </div>
        <div class="container overflow-hidden containermedia" style="padding-bottom: 25rem;">
            <div class="row gap-y align-items-center" style="margin-top:70pt">
                <div class="col-md-7 col-lg-7">
                    <h1 class="bold text-contrast display-lg-2 font-lg mb-5 biglead">Создайте мини сайт бесплатно</h1>
                    <p class="lead" style="font-size: 35px;font-family: 'Fira Sans';line-height: normal;font-weight: bold;">Если вы только начали продажу товара или услуг в интернете и вам нужен простой бесплатный сайт!</p>
                    
                    <div class="nav mt-5 align-items-center"><a href="/signup" class="btn btn-rounded btn-lg btn-info shadow mr-3 px-4 text-capitalize">Создать сайт бесплатно</a></div>
                </div>
                <div class="col-md-5 col-lg-5 ml-lg-auto">
                    {{-- <iframe src="https://www.facebook.com/plugins/video.php?height=367&href=https%3A%2F%2Fwww.facebook.com%2Fsendmecc%2Fvideos%2F3630786496998043%2F&show_text=true&width=560" width="560" height="482" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true"></iframe> --}}
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAS4AAAJYCAYAAADc0wsCAAAAAXNSR0IArs4c6QAAALBlWElmTU0AKgAAAAgABQEaAAUAAAABAAAASgEbAAUAAAABAAAAUgExAAIAAAAYAAAAWgEyAAIAAAAUAAAAcodpAAQAAAABAAAAhgAAAAAAAABIAAAAAQAAAEgAAAABRmx5aW5nIE1lYXQgQWNvcm4gNi4zLjMAMjAyMTowNToyNCAxNzowMToyMwAAA6ABAAMAAAABAAEAAKACAAQAAAABAAABLqADAAQAAAABAAACWAAAAADEZM0VAAAACXBIWXMAAAsTAAALEwEAmpwYAAACcmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp0aWZmPSJodHRwOi8vbnMuYWRvYmUuY29tL3RpZmYvMS4wLyIKICAgICAgICAgICAgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIj4KICAgICAgICAgPHRpZmY6Q29tcHJlc3Npb24+NTwvdGlmZjpDb21wcmVzc2lvbj4KICAgICAgICAgPHRpZmY6WFJlc29sdXRpb24+NzI8L3RpZmY6WFJlc29sdXRpb24+CiAgICAgICAgIDx0aWZmOllSZXNvbHV0aW9uPjcyPC90aWZmOllSZXNvbHV0aW9uPgogICAgICAgICA8eG1wOk1vZGlmeURhdGU+MjAyMS0wNS0yNFQxNzowMToyMzwveG1wOk1vZGlmeURhdGU+CiAgICAgICAgIDx4bXA6Q3JlYXRvclRvb2w+Rmx5aW5nIE1lYXQgQWNvcm4gNi4zLjM8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+CuJ3aGQAAEAASURBVHgB7J0JYF1VtffXnW9yM6dNOs+lA7S0CAUFGQQEFBFR8SkoouLwqehz+ESf03Men6I+J0RFxOkTBxARZVBEZC6FAh3pPCVNmvnO936//05PSUPSNmnSJu3Z7c6555x99tlnnb3/Z62111rbzE8+BXwK+BQYZRQIjLL2es0N8WMseQF5IfkY8lRyGblI9pNPAZ8Cz6dAG4fWkleSl5KfIbeSC+RRlUYLcI2Dqv+X/DZyCVnApbbndmc2jvhB/fCTTwGfAn1SQB91b8xH+a3xItDKk1vIXyD/iNxBHtHJe4iR1Ei1qZb8EvJPyAIqEVwEFqF9cIIIfvIpMIQU8DgubcUUKD1Jfi9ZnFk7eUSlkQRcAqiLyR8nzybrK+B9FfjpJ58CPgUOIQWyu+8ljLiD/E3yPWSNy8OeRgJwjYcKnyC/iyxiibsSYPnJp4BPgZFBgczuZkhH9j2yRMrU7mOHZXM4gauSJ/4d+XSy2qHsi4EQwU8+BUYoBcRtibGQbvln5PeQPc6Mn4cuHS7g+jOPeC7ZU7Ifuif27+RTwKfAUFBAICbQ+jD5O0NR4UDqOJTAJW7qFWRxWWIzS8l+8ingU2B0U0CK+wR5DnnNoXqUQyWaSY/1MPk3ZIGlD1oQwU8+BY4ACpTzDBrTsg27iSwQG/Z0KDiuN/IUPyVLNvamWvnpJ58CPgWOMApIfGwkX0B+fDifbbiB5GYa/wFyhHyouLvhpJdft08BnwL9U0BjXNLUW8gCsfvIw5KGi+OSOYPcCSaTBVrDmgKB4XqMYW22X7lPgcNCgWJRws+wJ+m+niK/cDjuNBwjXj6EDWQh7pBzdAIpEd4Dq2AwaPF43KLRqEUiw46Rw/EO/Dp9Cgw7BTRmMpmMy+l02o2hQwBgaR5sPflYsvBgyNJQA5dmFsRpCdKHTDQUSCkLnCoqKlyOxWJDRgS/Ip8CRysFurq6rLW11To6OiybzTpAG2JayOZrM3kRWQ7dQ5KGEriOoUViDYfENsvjqBKJhFVXV1t5ebkDL+8r4Z0fEir4lfgUOEop0FN6yefz1tbWZrt27bJkMrnXeDtI8sjeayd5NrnzIOtylw8VcM2gtlVkcVkHVacASeLfmDFjHGDptw9S7l35f3wKDDsFPCCTWNnU1GTNzc1DdU9xXrvI9eSDVrIdFMjsfqJqtlvJUsgflHgogJowYYITBfXbB6zdFPY3PgUOAwUEYoVCwXbu3OmyxqMn8QyyOQKvLeRpg7x+z2UHC1wCqhaypkAHpYj3iCFxUKDlIf6eFvo/fAr4FDisFPDG5Jo1a0yK/YNMAq9HyAc12zgosNndcF37N/Iscnj3sQFtBFrhcNhmz57tc1kDopxf2KfAoaOAJ/nU1NSYdM7Sgx1EErMzhlxH/stg6zkY4Po0N30jedCgJS5r2rRpTiT0iOM9iPal35LCMBQKuTL6LaDTcS+JldV5zYhoq6xjKqOySl5dHuur4yqjfU+Hprp71uvV7299ChwuCuRyuT39Xn1TfVQmPx4HpHZ5v9XHvbHgtVdlvfGj63VefV9jRMp3/db1Spqx98aN9lVfX1l1CsA0G6kxN8ikgXkC+QHys4OpY7Ci4lxutpw8aOCbMmWKmynsq9EeAUU4Eaq9vd1tRXAdE5F1TESXDZeOeS/AAzftp1IpKysrcy9EncB7UTrnvWT99q7R1k8+BUYKBdQ31be9D63ENI0BmQLJfKGkpMSdV9/Wb4GJfmtMqC8LWHStkq5Tfd5v94M/KqdzGg/e732NA51Tnco7duxw2atrgFs1RgNOdp9SNw0oDRa4JKfq2udYnwO4rQcwEg0FSNrvK4ngAp0zzjjDGcw9+uijDqyWLFmyhysSARsbG+3ZZ5919aguZY9r6uzstFNPPdUB1/333+9eil6szCp0fNKkSfb444/b0qVL3UvQCxcg+smnwEihgPqysmb45syZY1dffbX98Y9/tL/85S/uuMBDYCTgqaurs4svvtjOPPNMNyOvsSCl+gMPPGB/+MMf7Mknn3T9W+NOICVwk8h37LHH2qxZs9wxfeQFjjrfX5LNl65Vfdu3b3dguXHjxv6K7++4WLZ1ZNl/DigNhmN6jDvUkAdspi5izp071wGFfveXRMDrrrvOvahx48bZX//6V0esG2+80S655BKX9ZJEwD//+c9WWlrqdGQCJr1o1T1v3jxT+QsvvNDuvfde27x5swOxm266yS6//HIbO3asvfOd73RfKAGjXqiffAqMJAp4ICMwufLKK+1Vr3qV+6DfcsstTtekj63SW9/6Vrv22mvt5JNPtqqqKtf/NTYk0p144on2mte8xvX9hx9+2I0PffR17fTp091xfeTr6+sdYAkENYa07SuvWLHC/v3vfzumQmNK4CmVj0wnBpGEP3GylPVrB3L9QPVTx1G5xETdbEBJxDjmmGMcMfq6UFyWAEdczxe+8AV76qmnHFGlDNRL0JdFv1/60pfa1q1bXT0qr3Mf+chH7NJLLzVxZKpHX4z/+Z//sa9//et2zTXXuC+WWOnjjjvOcVyvfvWrXf1XXHGFveENb7Cf/vSne3RkfbXtSD22r49Hz2f2RIyexwbz+1DfbzBtPNTX9KSJwES09jipD33oQ7ZgwQL75je/6Y5pDAkkPHDStRdddJG9//3vty1bttgnP/lJu/vuu90YEugIoHT9Zz/7WbvqqqucTZY+3Kpf1wrYVq5cafpwq6zXFt2nvySOS2DY0NDgxqBAT23S2F69evUecbS/6/s4rjA4CiwqTOlG4j4K9T7Ufwt7l+wWCxUEcFCgNXPmzH5BS7cS0QRaYou//OUv25e+9CXHSYmIsuQVcaRQXLhwoQOld73rXQ7YRPAf/OAH7ovkKQwFRgKw//3f/3VPoZeo+iUyCvjEQqueV7ziFfbPf/7Tld3Xy3KVHIF/NEgOJA/Vox/IvVTmaEo9aaI+qn6ofi4QO+mkk/Z8bHWsZ1K/V1mJjp/+9KfttNNOc3quW2+91QGR1CASKRctWmSvfe1r7b/+67/sF7/4hQOtnvXotz70aoe2+xsH0q+prMap3O9e9KIXWUtLi6t38uTJe8Cv9z32s6+XLvA64DQQ4HoLtc4g9y/j9XFbvYxp06Y5UOrj9J5DYovFEkvsk+5KHJL0XCKkgEesrLirs88+2wHQBRdc4ABO5aUk1Ivy7iXWWVyYvig6JkKrI4joAjR9oX74wx86cVEv2vsC7WmM/8OnwCGiQE/gUv+84YYbnGpD/V9jQufV7zUGeieNDyXpsF73utfZt7/9bfeBf/rpp+2+++5zEspHP/pRe/Ob32x/+tOf3HhS/+8reVye7revpPNql9qj8SqGxJOIpOKRrm0QSZLfS8izD/TaAxUVpbX+CnlAoKVGjB8/3j2Yfu8rCe314vQyPFTXV0WEElGlDBRoifsS4SQuShwUlyaAkvLyzjvvdCzx2rVrHWHnz5/v6pMIKXlc7gsCss9//vP28Y9/3C677DL7KWLiueee62Zp9tU+/5xPgeGggD6oH/jABxzXor6ofqwJJPV5j6vyAEpjRIChfY0BfbR1vcBIei9JJbfddpvr51KrqNyb3vQm++Uvf7nnw61n8D7iql+/xT2JQVCdOqZ795e8ceqVu+uuu/aImRpbctUTd+jN+vdXTx/HxUR9n3x2H+eed0iFDyRdQqEK8oGWdwAh4omYeqD9JZXxWFURUteKOEJ1JSker7/+eveSBFoCRJ3TS3vve99r3/nOd9wLr62ttWlweD/+8Y+dCKmvgTgw6bf0EgVuEimVxKnpyyadgcr5yafAoabA1KlT3cf27W9/+x5OxgMWgYk3FvQB1750Skra1/jQuFEWiEll8uIXv9iJilKBvP71r3djRvXpWl2jrI+9N5ElkJLqRMeUBIxeub62MsMQMIm70v2llPfa4Crgj2bslXR8AEmFteKX9Oj7Tf1D63OXikf9JXnARk56KYNJIqa4IwGKvhoCp2XLljlirVq1yikU9WWS8l3gJfldop9kbYGTCKY6lDUL8mZYZXFsYmOlpNQxmVHMmDHDfve739mGDRvcS/Be3mDa7F/jU2AwFBAQKKnPqp8rC4TEsVRWVjowE1gJwAQkAiF94D3uy7unlObidn7zm9+4D7Xq02+VU50Crp5JH2xJIeedd56b2JIk440ZbftLAjaNG7XRA9G+ysp64JlnFOFqQEly6ifJl+7vqgOBxBOp5N/k/p+mj7tMg+sR1zSYJGKLeJoR0QsT0OgLIWWgZjQmTpxojz32mDvuydveV0X30wtWHQI9KQ9FQAGc6pIYKtFRnJnAS+f0YgVaA/xCDObR/Gt8CuxFAX2gFy9e7IBAeloNePVzzfTpw61xpJk/9VsZbUtKkP2UgEgg4iX91jFlgaHGj46pb/csp/IaH6pP4CjOSTOCuk5gJi5KY6q/JG5LY1F+ixpzvev2rtM9NPMojky/B5C0+OwU8o59XXMgwKVoprXkvSF7H7UKsERwP/kU8Clw9FJAgCXQFRgOIEk39Lndud/L9gdcEnwH5A4urkUzDUJ0P/kU8ClwdFNA1vmbNm0aCBHEnimL7evXhH9/XNS1XHzARmECLbGePmhBNT/5FPAp4CbMBqg7FjMlwBq/L/LtD7hez8UHrNsSa6jZPj/5FPAp4FPAo4Dc6waoPxZ4/cG7vq/tvoBLeq3Kvi7q75g4rX0p9vq7zj/uU8CnwJFJAQGWZkf7U+L389SaddCkYL9pX8D1bq4akH7L57b6pbN/wqfAUUsBgZdmSgeYpOea0N81+wKuC7nogMVE3WCw5g/9Nc4/7lPAp8CRQQGJiwNM8k26pr9r+gMusWqKkdPf+b3qGySi7lWHv+NTwKfAkUsBz65sAE8oDLqov/L9AdM0LpA9w/7MJVy9UsorbpaffAr4FPAp0BcFhBFeOJ6+zvdxTMAlPXufGNQfcJ3NBQOyGvOV8n2Q3j/kU8CngKOApDK5JAnABpDkeqN1GJ+X+gOuxZQ8oDjGatAA7TSe1wj/gE8BnwJHPgX25QPZz9MrCNkL+zrXn/J9Zl+F+zomBO1vxkDnPD9A/famROVHOEDk7evW/jGfAj4FDjMF5OMo5kXj+UDGtDBggOP/TB7x970fsy/gkkw5j9ynbNm7Au1rNlGN1gP0TrLt0nE1tmfqq2zP8/5vnwI+BUY+BbxxLH9Egdj+kjxrBrAuo6S+PpmovoBLx2Q/8XwU6qdV/UVW0IMI0Dwvcj2kdGE6fiAP2c/t/MM+BXwKjAAKCKw0vrXtzZj01TyVFXARgkeKrgPBFyGhFPTPS30BVymlpJjvP7ZFr2r6AyE9kMBKsXs0oyDuS2UP5CF73cLf9SngU2CEUUBj2xvPUgkp3I43tvclgfEYEr80a7i/JIDr0wCsL+Aqp/ABA5fX+L5aoHOaSVB0UT2gh859lfWP+RTwKTC6KKDx7AGVdFfynFG8LgUl9ACt9xPtnsg70KlFcWXCI233uqYvoVQc116F2O83CZyU+0oKTCbF/b7K9HWdf8yngE+B0UcBjXONd+m8BVy9Uw8c6Bswel/QvS88el5lzztAoT4L9l0nNfbRQK+sFrEQC6kGC5395FPAp8CRSwGNcemzFV1Y0Yb3kw4EEARwfeJRX8C1n/v1f9pjDwVU0meJfdSxgYKWynvXeKxo/3f1z/RFAY9u3vRzX2WG+5h0nAqZ7bVluO/n1z/0FNC7U9Y41vv0xrikKTElPZPGvVdGTMtwpiEFLjXaAxxv1Rzt64EGmnSNlwd6rV+emRVmb/Xx0Ds5XEmdXBMz3gfscLXDv+/gKSAOSiAlDkr6K/UnGZIqrr2O90zeeBXQHYCx6cBBocfNhhS41GA1XmDlAVePex3wT48A3vaAL/QL7qGAOB1vCarD5Y7lcdvqD/rtp9FHAYGTFr24+uqr7XOf+5wb3xrneqcCtd5JHymdOwDg6n3pgPaHtDep0Z5oItA5mORd720Ppq6j8VqB1YUXXmiXXHLJYFcXPmiy6QutmSatzKTffhp9FJA4KFMmLb6ssag1TAVc+7PdFA4MZxpS4FJDD/Trqi+wsoighxRRvKz9888/326++ebnydHDSYzRXLdHO+9r+IUvfMGOPfZYJy5+6lOfcgAiuur99E7qnJ4IoC+l3ov2tVW9OqascqpfX1rvvOpTGe++XjtUXvebPXu2nXHGGa6MuHCV8+r17uuJHLrWq9u7r7Zeed3L4x51f6+v6bzu5bVBz6d9ZZ1TnTrnpwOngN6N0rRp0+yDH/yg3XjjjTZr1ix72cteZldeeaWjp95N7+TR2Xtn3n7vcge7P+TAdaAN0pqHSiKQxJqeSfLza17zGtPir5dffnnPU/7vfiigTqSswSzdlhbt/OxnP2s33HCD/fWvf3UdTlyPtwBpz2oEMgICgYI6nMpoStt7N6pXYqcHDiqja7QgqO4n/Ye2XtJxgZE6rZel61I5gYjaoTrUTp3XvurWPWRZ7d3Xq1NlVF5tVB0qq/q9tTLVFrmReOoJtVfPoOu1SLDKem332uhv+6aA6Kx3pHfz8pe/3L70pS+5NUzf8Y53mD6GWrFHiyvrXXnvp++ahvdoXwaow3vH3bWrI9fX19sFF1xg3/ve9/a657x581xH1cC77rrr7Fe/+tVe5/2d51NAnU0DV+CjLPDQYqOampZFswBBA1qDv3fSdT/+8Y9NM0H6oNx7772uw2rh3W984xum6JW67rbbbnPvo7q62r75zW/ali1bbNGiRa4Df/7zn7elS5e6+33ta19ztxAg6RolAcivf/1re+ihh9yAEAB961vfcpy1OEOB1Yc//GH3sdL7lz5F95Ex41e/+lV74IEH9jyDBoyA6r//+7/t1FNPdXXr+re85S2uLRKPr7jiCrfgqUSaD33oQ7Zt27Y9nKFrkP+nTwqojwjkJ02aZG984xsdTfXujj/+ePviF79oH//4x+0f//iHTZgwYQ99+6xomA8eFuAScdTRXve617mOd9ddd9mGDRtcp1OnPOuss+zWW291q/bq+TWwNKC8L+8w02RUVq8vpMBLA1Vcqj4KDz74oPsyin4Cit/+9reO+1C5nulVr3qVPf300/bRj37UgcU111zjwEqD/4477rAf/OAHDpAkLvzlL3+Rr5mbLRTYPfXUU3buuec68eG+++6zz3zmM/b73//eAaHuKXF/7dq1DnTEBWkQaHVxAZ5A8f3vf7979+985zudPu7aa691APaVr3zF7r//fps6daoDLq04rnarf6gvCJwEtKqrvLzcLTv/5je/2XGXr3/9693S8gJrrdL8nve8x/7zP/9z2BXGPWk6Wn9rXCpLvfDpT3/accPqW/Pnz7e3v/3ttnHjRps8ebLrA+pXeh8al0oa14cqHTJRUSiuB9ODKuuh9WXV11lcl5YDVxkRacmSJe6rr9+33367Q30RRx33UBLnUL2EobiPx0mJhRf9xPJLRBLHJe5W3Iui1IruvZM+Gqeffrob/PrS6suq9yE9o+p77Wtfay984QttxYoV7surji1uSoAkS+mHH37YuXapXomoEk2l0BWwCOj07gRiAhKdU5vEMenY1q1bXXMEUnr/ap+ASPWqX6iP6LiAWM+o3wJnKfxvueUWd1/t/+QnP7Hrr7/eTjrpJLvzzjtdPxLACfDEyXn9rvez+/vPp4DegbjsRx99dA/d9AHS7KL6lN6/xuE09F/6rfcocf1QpkMGXHo4dWA9oAaVOp7kZQ0acVgex6COOmXKFPflloh4zjnn2CmnnOKuUwcVofw0tBT45z//ae973/scIGiQS4TTQNd70jsT3QVCEtcEXgIPj/vxPkZqkTq8xFRxZLpevwVw+u0Bq8ftaV91e1sdVx9QVh/RtdpK1yY9nTg1lVU5bVVO4Kc6dF/pu1RebZWYrDKqw2ur1++GlnJHXm2ipd673mtf+lDRXbTVO/3Yxz7m+oVH/wFS4zml6AAvVPFDBlzqRCKIHlidT/oIgZDEERHohBNOcMj96le/2r773e861l7n3va2t7mvv0QCdUoRyU9DSwFxaOqMN910k9MHiSPSF1c6q6amJvvjH//oRPfGxkb3rjzFuN6j907VInE4+hjpo+QpzvVevXIeyAlM9NvLulbAousFRuojTzzxhEmFIB2Zrhfnp/LSrWi7fPlye+lLX+r6kDgBiY4XXXSRE3ml99I9VE9dXZ0TRwVwfto/BfTelEXvuXPnPu8CMSACL33IxEF7kyDPK7j/AwclVw4pf6eOoux1mp5t9zqvOpAiRqjzSQGr41LOfuADH3Bgpt9f/vKX3Zdag0Lgps575plnOn2LBo2O++n5FBCtBECiac+kYwKGvs6pnDrhJz7xCXvkkUccYK1bt84p9qXDkmJc+xI19V6l59BHSB8RbfUe9c69pGs+8pGPOMW9bLg8cUOgpOt1Tc9+ooGgj5HqF3clZfxPf/pT+/73v+9mr2ROsXLlSrvnnnucXkx9401vepPTnWmW64c//KH74Olen0YnI12dQE8TAbpOg+/rX/+6a576ntrrp/4pIPro3f7yl790Y1L6Tu3r/XmMw86dO02zjFIDiKPW+9N1Xl/w+kPPj1r/dxzcmb7e4iKqeoCsVX72mzQgpAD1OoQ6hxqsL52Q2Ut6GJXVoNIAky5CA0XHVP7kk092ymRtNWvR81oNuAULFtiyZctcWQ0AP+1NAdFftBUQHHfccTZnjlaX6046J53V3Xff7egqevZM2heg6Bp1SumuBCT66krs0zvRV1j6In04VJ90YeLAvLrVB6SvUjl9iadPn+50Ijrm6bR0zebNm907Vx+Q4l2zfbpG/UArHovD03NIL6c6pUqQmKj+oOOa6Vy/fv2ewSLAkj5M4KrnUN9Qed1LKge1Wc+g59M5tddP+6aA6KzxqokSjTlNzoh+GqcCME3+vOIVr9ije9a788BKW++33m3vviZ84J2oAx7IINYXUVli1l46or7e4kEBlxqqziFRQ53PS3poHVfDvayOpN86rgEhouihVdZ7eF2vfSWV02DqCWruhP/H0VGdS/RXR+rZYbzO5H05ewO/6Kpr1GFVh8rren1NRWtdp/ckgFFZvQ+V174HBjomMFLyyqs+/VadAlQlXaM6vferrdfxVUb72gr89Fv16xq1RxyZygqI1A7Vq32VV//x9Gk6p7I6rt+6Xu1XWT/tmwJ6j6K16CZO6qqrrnIGxOJeRUNxsLKvlKmS6Kvk9TVdq3emrLQf4NKg7gt/3LW7/xwa4NLDqrMqSRxU5/OSHkYdR2UEUN4XVMe8ASPC6LfKKHtJv0UcEUZZ5fy0NwU8ungDuTf9RH/RrTdoqRadU3md82ir+kRzrxMKlAQU3vXevrY65t1P1+mY2uHdz6tTW5XTOdXtvXevP3h16D4CIZVXWdWvdnjt0TnNPHrn9QwaRF79AkuBlQae7uFdr/N+2jcFPJp671f00zgW96pzsg0UF+29X70r733r/ek9eX1m1ACXN3i0Vdb0ux7QezC/4+y70/hnfQqMZgpofAu8NPbFFSv3TiozFKLikPLO3hfRQ1w9hBoq7spj23s/iL/vU8CnwJFBAQGWl2SSMpzpuTsN0V080NJWSlIlse2eiDFEt/Gr8SngU2CEUUBjXlkmLRIx95O6FWH7KdTf6SEFLo9N1FZZ8rD0FwItcWN+8ingU+DIooDGuZLHsAiwBFzDzagMqaioB/AeRCKiFHdy8NU0t5Sp0ncpCcSG+8Hcjfw/PgV8CgwrBTwmRWNaJjcySBV4eWN9Hzd/bvZtH4X6OzXkwNXzRkJhPYAQWFmzPdJ3SRb2ELpnef+3TwGfAqOHAmJOvKwJOAGWGBKNb/3uqfMa6qfqC7gOSvbs2UBvRlHHxH0pSXT0xUZHCv+PT4FRTQEBkzgujWcPtLQvpmS4GZOewPUyqPgtsqxUo0NBUT2Qh7r67QPWUFDVr8OnwMigQM/x7Kl+xIEpefvD1VIPuK7kBor+liBL9jwo+ZPr/eRTwKeAT4F9UUCS3aBxxgOu91JJ9cFUtK8W+ud8CvgU8CkwlBToaQ4xZLqtoWygX5dPAZ8CRyQFBs1tiRoexzXslJGuS/KvtrKi1wyjn3wK+BQYvRTwzB6kkNdvbQeQhkRUHMD99i6q2QPPnWd/Mwk6r5C+noPs3jX5ez4FfAqMJgrIakBWAjI0F2jtD7h03lPeH+xzDgnH5aHtvoBL5xSjS7GTlHSNn3wK+BQYvRTQmFasNQGYYrPta0x7oNYDIwbEnvWm0pAAl9cYb9v7JjouOy5xWno4/fbMJHqX9fd9CvgUGB0U8DguMSMyMO8do94Dq55P0x9G9CxzIL+HBLi8G/XVUB3z7Ln0W7otT9/lXedvfQr4FBh9FBATIvASGMluq/f4773f6wkPSsfVc1axV73+rk8BnwI+BYaNAgclKvrANWzvxa/Yp4BPgX1Q4KDMr4ZFVOw9czBUcu0+iOCfGjIKqD8916eKAb5tRdY1KKqrsA0QV74ov1N983DvCKStGEy6bSAvG+Z+UrADM2nFKGdmCY+yAPXl5VmmuqkrHGhj691XH2Py7nPs+GkEUkCioMZ27/HuNVXnVMYr5x3fvT0ojmtIgatXw/zdUUkBAZL6lHzOCMWrxVUAKwdgOiNsCQBAHngVcbQtsJqTOnEAAOsnCahwtQea6Mz8y1PeisS0D+6eXS5wPtAjJjz1cqCf2vzDRzsFfOA62nvA855fgAJoOMzwOCCt+pKmpDgsuC0HQrqQ7qOy2lcWN9ZPKjqgUxmc7QVQxZwVHGixmjX7kQJusu52PcBrDwfWT6X+4dFMAfcJHOwD0JP85FOgJwUETgIP+CIHSiz6CkAFXcAQVmFywUNURv1OW0W2FcqxSpA4L13HX5eK/KKcShYpVxSnxl6I4yxCx+USOd0vFeC3uCyJju4KrvVt/SCGn/qgwJACl6fL8rbe/bTf+5h3zt+OLAoUHVjgvlGUSxZrIvJPoBLgHaZSSQtGCAQZYsHQQJflCp1AV9pyeabFQ2FLhKMWYno8GmHL9LiSvCoymbSlsl2WyWew4Su1YICVj4OsSJ4HEEMx7H+SFirNYDYDdAW0HJnWdvRCfQtEBWh+GmkU0JjuqcOSrsvbV1v3M+b3fN8G81xDClyDaYB/zUikgPqUuB6AJ5uyCLgRCspdK2RNuzZYSSkW02NjNn/BTFu48FhWzl7IUnQTkf4anXFxWH6oAJkDHMAq5xZk5dqmDlu1cp0tf2qNLXt8lW3c2GC5ZNAqozFLA5hhAK+Q1/qJcGGh3f1aHJ3Td7nm+H98CjgK+MDld4S9KCAletGJhQAXeq1YLMu6mE0WiWZt9vyZ9t6zzgGsjrHKmrjlCyjt8yyEku2wQtcWKylTd0IcJIyvZREHnePtbuEvm7da1gc+87Tj7IxTF1oxlrB0awcgtsIefPBB+/O/nrLmpmarLKuzYkFGy+K0lP10hFJAX8ZBc13ehY9RyfHkAfPkEgkmTZrkWESPwL2nR7WvJdK1Gq5YSVnO78uvyavH3w4TBQolTvdUkDIdjiZQYKVoGJsYrJW4nSJmCKFgFwDSaFOnJOzccxbbGWcdbzU1iHld2y2T67JIWF1FQFVK76tA9CsDjGqpj+PkADmYkbIe8FHl5HyBiLhwUgFnAaE+oFWwFTWkaO35MvvXfQ/bn269x9as2UofqbQCCn3iifToW3RXxwhqK12aZjLRne3mDoeJWn61/VDAExU1nrUojrcArCcueue17yUd27Bhg/QA3boE70TfW71tZUVl3mvmZ9g5rp5yrvcgfbfRP3rIKBDE9qoIx6QZQn5nMp1WFqqyqDpYSBxWq82dUW2vueQ1tnjRBIvGk4hwzZbvaGCJJgCOPhfIABvhOL0qCHOVRfeVBK9QpjuFPueFV/wLFNXfMhzmg+X0XjrKSUBM4mcBUVIlS4tP2Xln1dl5Z1xpTz652W761d322LItcHo16MZoFqJnkPZKXxYO4+tKdxZ05p4bE4eMfP6N+qZAz/Hdc9z3XZqXzieun3P7PTzswLXfFvgFDjkFipENjPjxgEoZEFK0BJYIoWybZdIZdFUhu+LKl9kppy6yYvtWOCtAB2yJwAFJhOvMt1ppRCDi2CbH9QQBp0IaARPOqbs7qk8CSiBLjmJcZmF2BDaurwJuAbej+Gxwe0XMIYJwToUkv/M2+5ha+/wX32ePLV1t3//hjbZ1Ox9cygQB1ni0jEmBNHXTLmYpu80sDuTjfcjJ7N9w3xQYNGipWh+49k3cI/JsMDOR55JIx3JSOcAL5Xmx0GbnnbvYXnvpyYhw62zl0lusuqwExXrSSiLlAFgCkS5osTHlzABWwX21AUSImWBGMIgVPHUVAKMAABOQaQMigbivAmJnnvMCnaAzeRBJ4bjgwLQRwDlJQlbyiI5WSFmiNGHtbc/YogVV9r/ffr/d+IvH7NY//s2SXcxWBsrh3LpFyILE0sF/tHVzPx0+CugzNmjwGlLg8thDT6b19g8fbfw790WBUKbaIpgfdGXbAYISwg1F7LI3nGcXnDMfEa8FkbHGCijfpaYv5DKUoZsEmSkMRiyJiLl+1RqU6Oi7mAUsSYStpavdwiUJa8m3WU28FrVZ0bIdXVZbWWWhNLOFAJ64KgdWrkEAGvKf9GCBkNAL0AKQpORH5YUY2mXlJYiUwTbAMGRvetMSe+lL5tknPv5Na+8IWzIlXVnCUukQ0UYEfVKZ+GmUUWDQoKXnHFLgGmWEO2qbWyxttpbOvEViQRs3OWjv/89LbOG8Sku2brCyKF0CQAgV8CdkVjEkrka6q6CAJ6P/Nm3mdC1HbmlmDTfvarFVW7fZhh3bre6YxYBP0XY1tNrpL3iRleYrLCa7LC4t6nogpjtpK25JXJlAS8BTzv2k/0qzm0KUVHmBntyCttqEuphd98NP2pe/fIPdd99q7MamWBwuMJvvBMQo6qejigLqPX46yijQGWiyYnSchUom2yvf+FKrnzoWhTiuN4UZzBgKZbpgjkAosT9SqIvjEoAh+sVK2KY7LJXsQOWOgj5RadtTOZt50uk2p36SnTR3oV38sovs3gfutw7MJbISCRHtkCWhcj8fWcCrGEKPFS6FM4PPk98iZhaBPPdixpP5Aww0CEBZbLJPfuqtiLOngZs7AK8UYupR9vL8x3UU8F/7UdgRCtExVl6x0KbMWmKzF86zn//yn/bDb22wG657EvGvBKV3BxiTgjLitOgi6KhAOrKs6FlqPdnObB8AEw7Z8rVr7cQXn2O/v+seizR3WAmiYQf2WGeddbat3rjeMpRJAmABot4GpK13yQMwb1+4CFDJYh6xEUmT+2DqAGgJuALZOEAaQ3eWsq72TfbWt7/K3vD68wDaXXBpvpi4m6ijbeOx34Nq93M9Z1CX+xeNVAqksK3KIm4VsccKanADOqFwzLry7ZaMzLGymbNtS6jVvnz9cnvwieNt/inj7HWvn2TRVAHoULQHARUAI92U7LCcGQ1cGFgWjpdzDL0V9WZSO21yScBeccJM22ZJi44da8GaKouUV9hTK562ljYWUkAsLObSloRzaw8m0MFjXkFFu/idjrAGARycw0hcgULxagAMG64wdmFwa3lFpgi3c30LzF8WURZlfts2u+KSU+2q15yAc3ajlcS7gTWADq5AiJ0Cz6znDeb2EWZnpL64o6dd3tdrUE/saQfewdXjyAOuTMZnijntKeT7a4XizFdWVrpyKtvbSLW/6/zjg6OAOBcWjerWJCF6FXOATLrBTloy1SbWLbJUc5dNGTfWWnekrJqB//b/M9FqK55BMa5r9DFUV+grRzmL8SgcUgHlfGN7h+XQlc2aM89WrnnWbvnbHba1YadNGD/BqirLLQzmhbCCj0fh1vBzLDDbGAUIMxiubsskrKuIzyKqVvk35hEnVXcQm62iXIboW0U4rWARhX+gtJvzo++EEyWWSbbZcYvm2rbGDlv25AorS1RZJz6PYQxju7/GKP4FrsG97BYHR0z/qn4poLGsrHjzWvHH2/cu6L2v4xiq9uxgXtF9bT/HSSk99yQfuPaQ4sj6AbR0T+Ih4gXgdIrFdjv+uDH2iY++0U5dMtHqK7vs/POmW8uOdbZxRYNNqSsD0BQHAhMJx2H1TY88deXFCQFcXXhEJLHdagO8IoiT046ZbQsWLLKJE6fa7bf/2U476RQbx8xiEODa1bzLosw8Bigf5S4PLt9iP7jtcesMj+HDV2/jqioQU+Gy4PQCOF4Xg3LEBqxCpSj3a/iNsSu2FwpDmM1wvzggh+h46pkvxf9xta1btwnvjAqOqUsDWurnchgf8Ke47+f2j/ZNAQ+YBgFcA5H2fODqm/xH3tGgRCyiL4SJ2GAM8LqavH3sw/9hE8bmbdPaO+ykY4/F7KHVjl9YZVMnlVtXyxabPW0csSCwx8pjUMo3MQAYIZghenEUX8NdLRipCjiyAAJK+wAc1NOrVtiO7Y22btVq27x+rW3esNnaGnfZhee83BKAWwz8iCJyJsrKbTucWAxOKgxndfvD62zJa99ryWCVPfrAg7ZkxkQAi7bSAkWMCJEVQUIgFkB01Sdac46yfwhHS+HYuT+iZWcyZS885WR77LGl1tKCWIx3SACAKxa0mhTmHHBdfho+Chwu4IIfd2kvNmz4HtOv+VBRQC4yYpwC+bTFgy32tjddZDMmwcGkGq0sNwF4yFtVJGsZFPFLTtDrB1CKzS7UDEYIRIXAjYcZxjwmD51dLRigRrDZqkBxnmWbsM7OFMe7bNbEyVZ/0mQ4LgEOMIcfZDGQkGUW1+PWIwAEThRLdRLiY8PaZ23NlkarHjPdOoulVlIes9NOfTGRIYAlx+mxVXPglASaoCb6eZT7KO6DmEwwM0DFxP4SIRE7yyukZ8vau9/9H/apT30PcRGxM17lrPWLmFX46cikgPc5+g8eT91FbxoNrJ9GOwWymBPEAJt0Z4Od+aIZdtbpsy2Lg7TeckUVTvHxnYDSSrihjMVyQQvJ+iEHyiD+pXH96Whvt3ZyKUr2+klTrKauHi4ogrsP5gnZDLG3Qja+stqm1ddZbtcu62jYZjGcqgOdnVYC4CHJYb7QzSehbIJ7QvcECNZXo4tq7bTZc451hqRduxpt/nh0n0REDWr2EU5pN2RxTBovVkrHIbGgCQO4KWrmNPoyzijn2ruIV5+24xaMt1e88lQOya8SbkvipmYl/XREUsADrjU8nfj0c8lXk9V7/DSKKZCXtgrWZdL4CrvispdhUrAdKQvUItxMSV0jynpU4qH5vOkSRKosAKbgfwT1a2+0ZDJN2RKrm0A0D3ibPCYOeSzdY+iowihiQ3A8EZTtcvmJcLwiFrOqcAmK/u0YnIIdRIDII8LxFwrCtcF25QkoiC+RtQJy2WwafVSCyYK05Tt3Wml2F5wZVmFwcw7AADF1QQGXvLpjsRpmJ+EO4RNDMXF0PEcI465Qp4UFiOx3de20173u5TZtWr0YMUArDIB5AsUofpFHbtP1cgedPOBSBaroPvKDZPU4P41iCoQBlVxuvb3hshOtuhqLd9lQ5dEZofPKJ8utDX1QOtfE7GCKgR7HAt2sAV1VgAgMNZVhqyqnfJr9vEAFVxz0ZIGMxDR93yTCEQU1T0TUQCdiYifiGW469bW2q7MZn8IWC8SkH2PmEcPVgv5FidmFyLdia8SeLJxgy/KVls5stRdMiFgC6/gU+rQMBq/FMGXxVyzind2VLrP1DRWWzE60r1z7O/vHw1usJVhjbeFKa8U4NUUbkpHttD1vCWYeE7gcve+t52KCtoNn6rJohT+jOIK7MJ1o8Mn/JA2ediP6ykIec4cJtfaiU5fAeewEa+BSZJcFxxTDfKGpaZeVlU92z1CAGWpsaLRKxDjZRAUZ9N3fMX3L9G3rr4/puDJ1kxQ4MI+jdDgubgxg07XOpUfha1jFnLDOuQ7suloLtuZff7VgImATzz6FGnAtctyVq8a1Mc8EQRYr+l/85tdWPfFEW7MjYxt+c7fd//hKmzdzjM2fWmmTJ1QjujIJAXclUZSb2Ky5M2zJiQvsgSc3WKYDP8sQscf8NBIpoM7VX8fab3t94NoviUZngUx6u7364jcYJk/4F2KICgcGZjm2OoCoN2HiRNu2GXurCdNsy9ZNVjtmHIMfAEGHZVnpkPpiunWsr74mcBMTBgKKE0t1WLEUswY4LmdVJfAidlfnrjabM7HGXvTChbYRkTVYlbCxcqYmEqpmCB24SrSUUp92ZBA34/GQ/eW+BywXqbSuVNAeeaLdHnl4Pc/UYHVVRfvaF6+0kpD8GZlMkE9lLmUXvvJMe+jJ6wC1uKW7MdW1z/8zoijQV0c64AZ297gDLu4XHC0UmDw+Yhdc8CIU8oh6WLjLtPM5LoqFKVCwj6mbAAg8YaVYq0fwFYzFZAQK+DBDiAKJ3KN7iGPbK6nfUQZQUrjnAApz/lpLE3Hno4BOxy7OyZuROFq6NfqpOLOSYeZ/Ap3bbUKxzSZG4IiIplpgRjGEPk4hcWTyEIrHXMDAELqxuXOm2bTKrC2ejr2XkDeLAWywzrJlM21FS8L+fOcTFi2vc7HxeVD0a5Q9abYdM3Oca89eTfZ3RhIFXK8YbIN69MzBVuFfNxIoEEYM82xq1J5XvmyJZTq3MXiJtYUyXGKiRDcF9ZO3g8wlUl2dcFo1DizC5eUMfnRhioqq2ThsqphghAvitwBjT+4JYN3gJV1WEXuptpZ2q8Y7oqu1gQtZBSjdQl1ERpUoyY1VX0UN7jztzRbuarMwynkuZFoIP0bNPLrbuIDMgFyJVSbi9qKTFtnH33Caveflx9vXPvwmC7Ztou6Mrd/eZMu2tdr1v7jVcnCUsUQp9ysyk8nEATq3Cy88lX3A008jlQLqPINOPnANmnQj68I8CmpvSTDF8z/9tIWEhOmECUEH5AyjACAgRBxSkfUPkyw1ViCS6PRZEwCypLU3Eu20hNUsBFyYPeQxiZDD9V6pF9elanOIdTK/ktGnZh7HjhmDnoz1rwGNfL6Fe7VzT2b/ED3DgJOYtAyW9EXC3xS6CGEjtCKialEhbdQ8cg4ABUUpmrOH7r3bWlcts5KOLbbyX7+z911xDqC33lK71ltZpGAbN6Owx91EM6hG3PtgMca9m4mRvwDbMO7tpyOSAj5wHUGvVYEblWfPno2lfAmiH0CAc7PWKnQrTSPSieMqAiOtrW1WUV/J03dgDFrCrCCx42VUikK7iD2XTCOKjuXqg0AOwBR+GaNVWbuz3bZ1h40dO566uQEgVZpAdCTsTDpFrPoss4y5TlRYMmGA85NbTzBmeRy6WZQReMJaH/ecPE7hap8U7UWU83lswo6ZPMmmTjveOtswnUi12sLZY2xKVdYWTS61OuqtgTMrOlmUZ8wKaEtwJsetKJ5hYmJhH433Dx0JFPCBa9S+RdmiS1mOjRTR/dLok9JYkCMf2kXnHIcVfCf4IYU8js1O9KMkJg1BDFM3rVljE8ePYfWxnUiCLNiazFi8tNpipRUWBVg62lutGf/DTDqPy4ycquVyA9Cw8nQKZihHfTJz0Ko97V2t1tS8zWrriEmPviqAjiyIjswFpKB5YUCukOuAC8JEwRAP4ZLCLuop8SGw6hc3mIO7K4RkWsG17GepF9gCONNWj1nFloZNVjW+zpYsOdmiiJgvwjbtxs99yE4/dpx97P1XWhV+RQXsxCJRUI9r5ZCd7yzYqy66kGfn3kwEpGXvIeU9rXBc56h970dMww9Kx+XPKo7SfiCNlbL+FlhoIouMlcAINIHSe+EsWK1CKxmDTYWnAUVymVaAAbun9rRzvdHqOkEtUJFuZ1my7tAyqi+KYj2ICUEEcW7t2jU2BtEvgn9gEB2aIjvIjxGDfECtw6IxwshgNFpVg2EqXI5chFzS0juInHKo1r0FSHtCcQGMEcTDIhxYnnNFxMKITBkA2LBwBT9IBYYQ5xViJjKQ7bT6WvwTs7gjIeaGcbp+5cWvtGUrnrH6+gp76dkvYA6A+PdcmseCXlURzwfgjtr0iXWIrXnrStJgZlK1+na3s5D0doC878foXtdh+sMbHnzygWvwtDvMV/LepRCSGMiwjSpaKaYIY8ckULiXADBwGEUMRjkXgJspYqxVJL58Fv+9RGRst24JIMvlm7AplRKbugrouJyTM0puAGbqtEkWZz1MBflTGKI8oJJHjJRaqpyYWwFCyOQzeIlJdJSOSXW41P0xVTlXGP1VCBchiYWwWDSJfYqHAcFAB8arxOXKs66iwbGxCgeGroo9z8UZFO6IoYEU9yKwYTScti4uXNuWsv/66iftV7/9EYDdzvMxmyg3IN0WMGROkx8KccMzTKm31asRRXEK1/qNii/RnbrbuHvH34wyCuj75KdRSgGZIXTrrlBjp+EgAKfJk6pwzcHkALumAFbxmq0rwAlFohNs5ZOAThZ7LbTpK5/cZEv/xexfBLefPOuTScPOYM8DHkHAIYQFe6I6wW8tTIFiHe4szDYA2JVUEkOLxWNBMgtjQqFYX9mkQJJqwAVll0Aup3yXzkoslzgw4rIpckNRPpKAa6YB6/0t2zFK3Qbn1GyZYId1BtoBLBTroWas31stnWi1VDxFcEFMKwhMWE0ki+uuea9Vcd94oQutVhKuDXZNRq/u/qAi7Q2EU3bC8XM5LtooQOJz3V1BEP00einw3Jscvc9wVLZc/ELBsTQKmMeQlIyE+Hfs/BkwQCjDNXAxKNVKOqFold1312r76qfvtG988Q58lLVGYYmteKLFPvf+66ypcYyLqpBDRxTEn7GAQr+ACUUhKT0Zs5JZXHAAxSDKc2UUVtiHEbWUe4qL0fJkYaKrFtlmsgXESGeNxXn0YgrmB2AIsyRqunj1mGhobcScIj1gEpFpbbcG4ml1MSvYEJ9q22KzrNHqMdkqtyImEeVYyjcAVkkMZeMtHTY+Umrja8bQVoARx+wQeq0Ixq9hgEuLzOaRNZPMjCaJ4XXcsdO5MXSAIyyCaqIVSIaBhjhEPx1GCqgLDzr5ouKgSXe4L9R7l1IIHRbgIBEoCHc1Bn0TUd7dMRdIT6Ii7jY7NmPomZxoa1dtcqJaHpGxsXG7TZs6235x/e/tvR97LaLZBjgWRUClzn6SgLAokQtFVxFOTr+DxMcKwNFkUeYHMWTVLF8Wc4dYSbejdxhAKwIuAq5QAn1VCmV5O7otTCgKmD3kWGyjPTrd1jdWWlNyDAvUVlglnFQXyvwIM5NxnL5jgQaL41uZDlfZtpqptgNfykQkbxNC44huwb3QWUXwnRRHyB9AHQMMgKqqMoaISXugj/SBPDzPCO16mXb087j+4RFKAR+4RuiL2X+zZPckLqL7w4XaGsDKEjK5HoDYxm9MAxC5YDfgsDI2fpKcmpvs3PNOAHACNn1uvR23bQuO1TssgNuN9ENp1iuMlUr87P/ukdJSrNplEY/uSSIguqjOtk5LVLC8GYr+TLrTqbGisVKLVZVjbEobJB4KNqg6hUgZxycyAJikW+C24JQkTm6unGotzGy2E8+rozlgbdiaBbGIf7Zlp9Xv2GwT42NsJ5MPuyIT7OnQVGtOY1Lx9CZWFiq3GrjLWZUZq480AWItcF6IgZr1RJSuqy1zXJhbvUj6MsmSEp994Or/JR+aM/qKDDr5wDVo0h3mC/XaEX9klS7hR4Nf/yqrCKJX3NINXIaox/l0coctOftFdvytAbvvgV/ZlZn/y2xjp518Jsp3FnAN5eOsnsOgr6pGROyAc+upQdi7f0lJr3sJLxt3NBAJImWTiNdVjOIyFEXnFGdlbJ0EIHIEI5SB6gaio06dzjqIgF6axV7F8UTgvCKIfGna0drUZiuJCCEdW2kMLgxwC7Ps2dLmRluBr+KJiI3N8lUMU1/VBFvfia0WM6bjS+chWpbZMxtXWNOuVnvxRLN6RFkFTww7LjRnpWVl4JSiusINSpx2aR/IvLuEvxl2Cugl7N25BnBL700O4BK/6EiggObH8oh03RbnGvQELWbqP1a6nXnBWvgbZhJzuPMwaKMRxMfiOrvqg3OsomShvfvtP7It66tZMGO+fey9f7B7795spWMrUPBjLkC4ZGekCieVISBfTiKews2w39bJgghtDQQSTGBGMQYj12k2c9oEFmYlmF9XA37UzPwlihYvD7MuBv0S6/wEVrBzjlvofCA3bljpfArTgFQW0wXNCcSmVlvNAoCPmc/VgM+2ZnRp8Up7ljJN2F5NaSnYptIp9kTpDNtYMsPaO8ud83QwkrSZVTEbi41YRfVUW5VdYI83jGHWkWcnZlcW0TEQxhcSkZWo0ejg1NUx6dCEBSJ2JFc1El7j0dyGQYOWiOYD1xHUdaR/CjLt76RHXF/k+CzzBn3Y0iwjVjs9YP/1mYuJBb/QPvTun9sH3/Fba9k0x77wyVts7TLsueCajJA3CkKoWO9RXIBCuP10daRsJ3HkU8mslZYea0EM7qPlzwJmW+CssBnLj0HdxrJiXZiNYt6Qw+I9UFJKW1QPXYzVruNwSBMAmLaGDvRbzEZij6W2yng2T0DAl08O2EvrM1a2ay32YEnb3LjTOtGdNQfKMXVAzESXLk5OorFMK3LYmVWUx531fSWL1FZVhW1De95aQ9WAIzHCQui6tKgsTy8/Tp/HGnEd/aBeiQ9cI+59Dq5BGtBBQCKEzRVoQtZ0PwOeWTRMPi1ehtI8u97GTGqyj3/pZfaeD77EZs1DjV9cZa++9MU249gpLDyxHdFvJ58zrO0xIGWuzhoArBymDInKWhvDStWBGPUSsC/PzGOUtQ8DBcBO98J0QSuIFbCdyscqrbmNFYAApywAY4iQAZytBYl1Y+GI0JEpCkUui9jI8TBrmE2mbYtKm+zsY8czOQrnp5A0iJspAFSzhs7UQeInT6P5QD0dEisBEmM4VqcsXJK3naEqe7adpcsQF1Wv3H9UxvPh9PSBnPDTKKeAr+Ma5S/Qa77TO6F/kmjnPmWhNk5hec66hbBRgBazi9guBSJdKMdX2AmnhW3u4kVYxk+1EGGUCd6OfmkWMa/k7oP9FLOGJSVxq6quRRcl404U2tRvKP6DhHsORsZg3wqMIHopjLIW0UiVLLBNW0J2x9822WPLGmzyxHp78anj7cRFZczurbN8F1EhopUWLauznTuarW5yPdYWzVSNuUKhirA1gB31taZiAFSIWUfAkYU3Ioip3XAMhwYSpWlLUtFPZWUPuBZxEk+h18qV1tvmjm22qLLUioTVKQDaBULkSPfndHNwgAf1mfeI7W+HggIHJSr6wDUUr2CE1JFFGa4ZP4l7AdxsAgCB6x0FGZhqsQkMMbEqjzI7F0X3FIm0WHPr/RZqx9C0OM6yBOoLI3ZVEuImRk4zIxiRxbuzioe7AUAsz9qFOQEd3FUMsJDrTGSK7dxZYTf8vtMeemQFwfvqEBGPt+1biO5w43qb8fe4vfy0MjvrhS9AhEvi1dOCYWka0RNfyWwMpf1Yy0ofBR8lP8V2ZgNzcI9lWP6H84idnJHeTVxWhm070S92Jbussa3VppXW2C4BKmJyHC6tqxUj2vFxokVgHgLXlZO3AFFZu6FvhLwovxmigL4hrnsOhhw+cA2GaiPxGrgQDdBkFyJZHG00K0WLw3I+eXKB0chXX5GdFuYROWypJFrWYk4QQHkfSKIbQ3GQw65KfoMBDEPjrPDD8sR7PW06mQDUKizdhZlDeCIAUmX33NNqv711mbVGZqMQn2WZMG0IswajnL4DlbZyR6mt+slO+8PtuzDHmEDInYkwg5ssSTSKujJ+p1H2xxsQCTuZ/WOFIUTVJJxVKZZcVEDTZUrBij40MItx6/rWViYEAra1cYedNHeMNTWksf0qtSpxaISGRgK1CoVg74KeAABAAElEQVRslgV+Ert6ZimDIdyKoNHgh8peZPB3Dp4CAwGt55X1gevgX8CIqUELtabTMsIEcIo1tAtxkcHMNODu75uApN3SeQIMBmeggidCRK4CMQ3lFCJlPoNNFxFEQwxy9zlkVrB3ipUR5bRtK3Zbx9jf7+20P/99gy3firRYs8CSuWbAB7hpKkMEDDPLGbVEeRHmrM0ChIZeg1J+9Y0Nds8/Ou0NFy20xceil4uvQaRbaaEOADTbhtjYaRlANoPluxaCDcr5Gs4pja6smdWGMh2IwsTFb4Mz68CiP45ndglhntMYw9aKQ6Ns+85WGzOevp5qIoIFIJsmKgamEDBq0Kb3E/n7o4ACU2kjUSlxqNidfODyKDHKtjL+hkfCHILIC9JdYa0eK5lkW7bX2ozJGGGitEZTTvbmX6TglthXDVgknOmDQsgo2mkwiDJc7j4o9osZuC8MOHM4NGtx11COKUS58GDqkA01WWdwvq1qC9n3f/C4bd7JuXA9K5wh5sGhbWeZsQTxtyJJ7hkvZb9gUbrajFiFW4SjE11ZEofqZ+HavvbHHVbzt2a78NxpdtLxp9ik/APWVFJnjamQlWca0UnN4F64S4d3oWzP2M5d1fYM/o2VzAXMIwDhCyLL7eKFYRa3xfAUH8rk6g24A6FDM5yuZb9WXW0E67JdzxIlI8dFrE5UgEZFxdVHjJTtWsGFuRllL/7Iaa77Nu7ncfSZUV5JlqbgU+QvkHnPfhqlFNB7fy4V4EBSuNxsWb/Z7IWTUAbpPWv+TYmyzlKcPiDDTBT1oUgZ+rAgUU8JxEefkIhWYGWJEOIjhxnccbgnLg22sXZGzNKxSbahuc5+97sd9tBT66wDs4MiphJpwtuk2tosBtglALFSVsquJcZXV1sL4hv+hrgAbW9F2U/s+ygGVQEMRvO0BbtVDEXj9otfP2r33PmkXXLWeBu/YLalwvgnMiuYR9keRH8WB5TXpYq2GdeiBLG5xhH99OTKop0/u9LK83B4fIi3bd9kJVks8sGnCACVkdsPnF+uM2ubNu1w9mh57Nl4OEcNjQW3NuPuPX9zWCgwEN5XZYVVnyP7wHVYXteQ3lTmDlpOjHeKDCRvvBUr1zEoifhQhNNwXUOgxaAVeMlsT9xZCjEsilEqJgttO9JWWVNB6C4s2XF+zqOMly4onOueScwDWjsLY+w3tzfarXdtRFc0De5rtrV2pC3dijEnEwLl6J+KKMsj+aW26Li4veU/TrLy8rF27Y/ut0fWFjEKjRLtIYBOi0gPrM1YQWSJ0pJq68yVYkxaY5sak/a1mzfZ5KejduaF52FN30obNLkAJ8Vkw0omAxJRVsFObrDXTsvbuWM6rDz5lJXE6lDBNduMCXHrwN2oFPFU5h85AhkW4PoKiL/Ln6bNPAMoSJ2YSQB1SvLJ9NNhpYA65EDAy2usrlGP99NopIBzFOa9B7SmoJTWit6A0/KGrY2s6hyzcrdSj3Ra4rw0UAVy+oVoGK61HOZa//jZ31iBugq3mBKbf84SszEYhSqwH3ZVKpdk4dW/P9ZpP73lQWtsVWyuU6yxfZu1tDRYNcrwSBYlebCZ8DQNNn/BWLv0ddOIxjDXtuzYaas2rLYlZy3GtmqjdclKncB+mgzIRGmHDF1RwGsZNEVoLR2jZcSm2BrC25Q/ucOihNPJuCgPUdvaDtiyOOwk9GCvnt5q59SFrLpzHaYShMTBMDYewe8RzjIhP0QCHsr6P899OomqWlpeY6ue3YEIjJgojpP/IYnPDrTETg5m3IiGfhoCChwU8XsClypSJDnmuv008ingUIhmYvoAlxFHTpIlejNhXxp2IlaNQY/DzJsrxZvtDueiWFhogWLlVlYasbnTFlvX0k2W2Jq1DZvus0wCEY2oC9lYl009aY6tC0yy3/y5xZL5JRZMEa2hcSVB/4pWx+rW46I7bPELEnbu6cfYnNnzWb+RGTzqvvX2J+wPf0chUb4Ig1ZZyI+xUjiyLIaqMnHAtZrGYIAqFJXzNSFnsgBgkPOliK8PPbjcKiZU2TgWdo2VV1kKt5+FTDicOWm7nVffZFWt2I4xY5jjGRTFIpArsyIr/uQy3UuahUsRaxExY6wcVMAObfWzKNkIb6Pl2IKAJbDpaFaUS5DzLBj5b9pv4fMp4AGX/EIIsCRZYvebfX5Z/8gIogBqZveqgijXi0QALWIKkSN8jcy4nli+2WaeA5dD4L0ix4LM0BVwfwnKkpxB3Iw+Sg7Vk04/2R5f2WiBXRmrTsctzIIU0daIteZaLTGhaBPqAIEtm6yxq8Uqw522YHrQ5i+eYiedsMhmTo5bCYpzHB4JXIoyHKX6pq5T7R+PLrWO0gXWlEpYHCPYOOsoOkEVoAIWmdmT7Tu6NdZSLGrGk+dwsbEQDUO0rSxUjvjaYjubHrUUi8SGsM6vD2y2U09MW3knLkbJ8RasqcE+K0zI6YjlNjdYmugULtwOtmltKOsrp8206LgaW7FqjbV3EMcCy/0oonCO8DegnthPFPPo8xyqj6CX6jflgCngAdf/cIVCCTAV46fRQoHucSeOS5bhspgPYVhaZn//5yN28TnnYPMEEBFtQVEawohLAazQDSfqCRhqatXpfH2VLfrQZbb13sft2XuWWqCly2p24WKD8j5HkMHO0ofs/5w/2VLTxrByULWNKcHUgpnFfOdyi2UAIKxBtfhGWAp1RMGbH0jZGqze22PMapbA9TBZgOk6X0RAk5m/iMCCf1p8tkBoHfGDip8lUS8JuKTR2Jei8C8FsMpRtqfbd2GlT2jn+DNWF59u2XYW9CiZyVVJvIgyltrWBmhth5MCoOE6i9ESq506jxlNzDkQgX/7+xsRixEixOERcFDir8RqyEUWnMoo10+HiQLdX95B3twDLrS5vog4SBoelstkCe6GvlxuGIiydyqioM/jY/jM6iacokNWWUFAQKIohBCJAnmMUgkNIyV5CJuoMIM9F2yxAmYQE86darUnTzTb1mVLf43IuKrdVjz5iM09b5addibxtyqYWbQm6gF8UqzME0NHhaI7pOlHfiaJB79uR50tfWqnhWJTABFI0smaiizMkcMUoRNuxyLNVobuLJUs4lpU43RbYdlq0Y44Il0XqxOVFcZaDFMJIMg6Wcw2hj4syfHx47dyuxMI6Yz5QwJzDUA3vWWb5XfisB3tsgyq2rrxUyxaNVbrg1gqx7JobV3Yiz1G/diqyS0ItFIYay+kYEjmIn4atRTwgEuCv59GEQXEObgklxuSVpIWgDkdDt7Of7truV12xWmIcatwm6GAlubRij9wPrLvEuhFAB9FZigS3z0X7bCSYypsyfvOsa33PMFK0UWb/rITARx0Q5hWxNxiFNh1UUeG5b6KGHRK3x0gzlU+McH+9giahgLrKsKtBRFbDfFySq3hk7gW8JJB6ziOlVnUxcwpRXmOz2RpFpEtxSo8WceVhYubAUc4InRaZYRs7uok/j3/5k5jjcgcoBWvsWzrDsvtwg8RxbwW/giWJQC26QCmDG5pC14CYSJT3H3nQ4BeEX9Lde3dM4m0otsciHb781LQYvQmD7hG7xP4Le+TArf/5RG7/O2XWnvDSqspY+AGFM65BBADwOS36D5VmDNg35QjMmqiAv0YtlYsF2STLq63SXA/zu4JEAxrkItDQSwEeVCJ4+4jw9bSECYNedvYOM4eemYZM4GTsGBnlR5CLVdk2+0Dbz0Pv8j1tnxlg91+3zZbtyWF/RhGq4EapLkyZhKJj5Utw/6M0MylKPBDrXCBzAYSjyvZmrVKDETHVmTtktOXWASn6WwrdWMFnwXY0oBjzZSJFqqvBywRkwExGZQWANRouNR+f/NtVl01kbq7gb1PIvkHDycFDopZ8oHrcL66Ybz3ztag3fK7u+2CcycTHlmhnFl7UHolTByYwpNsCYBl2cC1aBVrlOzo+FEYBKyLJb/yuShcTo1Fyoi1hf5KFvbSazsLGqzqtdy9ggsG4bZuuQ2/w/AU68ijG0PvlSB4Xw2r9dSWbsC84iE7b3GdvfjkeriqgD35VKOtW7fNlq/otO2NMfwryzFlkF5NgQsJPk38rSIcXg2K/Tnjd9kVlx5rgaa1lmxptyAW/GE4tlA0bCVViJTVOHwzGRFg/cUinKT4qSCi8oMPLLdnn2XlIMw+8L3208ikwJDouEbmo/mtGjQFAjha3/Tr23BqvgbOCU6KKAuyo8ri+BxkeXrZRml2TwryAvGvQlrAVWIa3EucbRBgYmULjDi5hvjwrN3DBACW7JkqpE3QAGV4F1zYpo5SW7oyDQ9WAweFGJoEHDvb7bxXzEa0w6Kemb9ADh9IIqTKEv/0WQk7dRZRVi8YR51ltnVHO6FwGm3HxoA1EaUimQkRrDBri+eV2ylz0IV1rrJda7ZaOUEF4xiyyoQiT6ia2PgJ4GnUOvF/TEgMpnKZR8Cn2S9/+T2L4WZUAGm12rafjjwK+BzXkfdO3RNlYTW27Ejan2572C56+VxEKUI5C7zgsnLE3pLCWkEANSMZoqzTmUmJL/9FBf+jhBFqRhFMiyjRhVUFwKEAtxXK4iYkVob4V3+6A27IZGAaslTrTqsBJ04GdC55+UKMRJ/kHoiccHARRNRkM2sg1sp2C5EQh24Zxc6uDtkxY6MWWhy1lFb8YeGLINxWuNBGuJ2wZXcxy1hSTjRWghLSqjgK+HAFEw2AVhHOLpFQ2BoteEs45uhYe/iBFfb0U1spi21bWdTwHPLTEUgBn5E+Al+qHqkDXVBt/XT7xa/utA4WlwjjW5hHHxVCox6GMwlhCuC2KNu7lQ2YByA2FgCOvFgjlqsn5jJo1YFeTIu3sm5QXroxZjHRlWlNx61NWVu2qotwMwT+I3RMHQavs8a22buvXIBj9jpm8ZgfDBOpIgx3FKpBHwZXRCwwEAYlvEwpYJSYmQwz01hMsfwYvrRlgScswWIfceqMlhDPftxUFtYox4cxaiWzZrJYBlwg0SE0cxkELHN5HMSZfZCBbRYO7sYbbgHE0MsBbM0sMuunEUuB7m43yOb5wDVIwo30y6qIjrCjkZhYWJT/+Ee/xXIdPZLcbBSTnqgPcnDGFsLprkAiHkdglbJ8JGWZOMuGRTDrI2qEsb5hCEV4JIv9VUbRVAE4QAVtkz3xzHqs4zXLKJsu7La6uuw9bzvfqhLbrYTwN4GcuDWYetxyjMgPwTK4rzj3RkeVBRwDLhOFgogNxcIEwLSae41Fl3UMQLSQttZbV6wTl6B6C+GgnY9QVwXmGcSQl8gopZscxLmRRfAcuPkXN9vGdY3ouTCmxVq/CpDz05FJAV9UPDLfK2scduEyg+U7SvW//3u9LV7caC85fQKuL88y2FFqBxEbEfJcVFRm97LYgeWkfQcHwuiFIi4cDuAiMQwuLc/CrMZq1XlFXYi0ceU8u/eRZcAdtlMdDTYOo9OvXl1i48auBdiwEUOR7uynMJcoYCGvRWOl2Fd8rBD6p0gBDhDRVStay58yTM4WK+EIcV0iggS2EzQlbKXBWktG0laKPVgAg9YQXFuOlbgzPEUMtiuCKCtbsccRD3/1h7vQf+FaBJCxQBHPAoCx8dOIpMBBKed9jmtEvtODb5Ss6JW1onMOcfBHN/w/W7cB3VIA4ypNxSncscwb8nBRcFBhLDdDgFUUZVYkDyem62HmBQK5Iqtg51sBMQL9JbdYGD/He55ebctb221LFsNU22DvuWyB1Y8bix4N7svNPqpfPpcKyQw+iYihAA9hGjArQxTEgj/GwrFxgCwcKQeE4JCwfk8TPYIWAmwK1SzOjAqFQHIYp+1hTDiI/cC+ovcQdSIdsB/+6GfWSZwuqeh1wk08ID76acRSwBcVR+yrOZwNk+0VWU422KZbEyFsrv3hb62xgwVbEfdwkkFPJOBC5IIzkXI+ApgFBWhgjuxEZRifw24rT1TUADqvIEr1CKGck8wCrntsqVXkCByYXmVXvPIYe+Fcud3AXTHzmM+gwAd8pOiXL7VyAf2a8ycUbmUo6wxRmQyQo7QMY+WORLDmAu5AaUAthzhYiGAiQdYqPWpqkUiuBTRZ4giDoKq8BIrBKvvGt39qz6zciDsQnKQLcQpsaWaU7KcjkwL+mz0y36sb2JoxFNclkasQqrQ1G7rsO9+9haGPCIk5RBG/RuCkG1mAhABnAjhm5+GyugCgIPqoPHWEmUEMwSUJ0HI7Smz7XY/b22ZOs3dNCtq7F8TtDWcfY/nkVgAGmMRzOYz4VgC8hCEyuFCOYLKghTzcajvsF50oSvcDXOWO49qi0DcBVhBKFXAHjxtBV62cdobZz8PJ4XFIE9RO9GpplPEo/r/+7Z/Zvf9abmWV4zGl6Lbl8l7pHu8C74C/PWIo4PHSV/BEU8je/gE/oGJ5V1QQt9x96fq/TEtcVVZWunIq65a66r+4f+YgKUD0GWgNZ6KhDpcTJ5QyOnTC1zRi+7TVlrzwLNwJd2GxHgHkMDSQ6xBimeJgpTAtiGvBVoLxRcNjMEuAKyJc884VO2zN3TutaiuLXKRylmhYb7OmIOqVo3uqojwcl+xaA+ie9EXkJ1n/8B8kPlYZCvYkAQcFp1Kea+sSwNVEmOeQrN5pY5SFPG77xvfsb9f/3NKrttqU4+ahXiPCRRwQpB9lsRULx+vtxv93u9162wMs9lHO5AMASKjnAiDr7u2q5pfESz8NGwU0lpU7OoiECzfu7Xs37L2v462trXo73d3DK3jg28+oqN6xn45ACqDZ4uUq8gJrD2ISr3A3efRZAcwibrtjk339W3+0YOl46wBI0ijCc/JbhJPJwwHF4cTCSUTINRnbcNNjtuFXz9gTNzxp62/fYfVtUYuuS9vmPzxsTU+ss3QTjtUlxIZHyS5R0WXHGUkfRf90GeCMYc5QjpkCImQMECpyPy3Q6uUox4NEssBx0X73xa8Rwz5v7/7oJyyzq8Nu/9HPMa9ADOVpOtCRheNEfrj5b/bzX96JfVklx6WQR/GvocC9u7NGBojtpyOSAj5wHZGvFV02CmysnBi83WCSQ8cUxocvEmVWjyXE/nX/Wvvmd67D4JNSAEgO7ieHzkihXoJaPLY5Zw/e/E8LrkKw3BC38V1TCZ08w1LPrEPHBdilCfIXHW+hsVPlDcTVOGtzz25A0j2fAyX3GxDRCtXZbBqRLsX53SC3G8BKQdZsChMMWtzU0GBnXfEG66gM28lvfb1tWP40kwNEiiA8T5LY89/5/vV2ww2/pays4wkSCOB283i0A66x2/tboOUDF0Q4IpM/W3xEvlZEM0IZe0mMSBj9U4F1C10CaHL5Grv33122Zet99v53X2RTJxA1K7cWXRSmCKk67KQSNvHYxdbwFJEYEPOCWN6XVxCl4R1vs5rpZSx60WRTa2SUAAixYEWc+PFa5ELA4yKLSjGu6UX0ZdzN2XNJKV9dVWMtzawEVEUdaO1zxIpXrC7F7kqWs/oQx8ehZN90w59s2usuthU3/MZK59QiIjKp0Bazb3zzJ/bIU+stH2Mlbf6FmPF0jJaED088dA8JR+dOuB3/z8ijwEG9He/iu3muU8maBx9Q0ozPpEmTnGyrL2vP1POrW8JqMpMnT8YHTn5wsr1Rh/bT4aCAzBLkqZgoRcmdbLSxlV125eVn2pmnzrQdqZUWqWU1npYaqy4eg1J+jAtEGGQBixB2pHg7O5ehYoxFZdEfBbBoDxYRAQlQaMwIStneDVzcQXpPxwHlCEGTtghxvPKo0gqwaK0tbYScIVwz8e6lhNN6iln1DULRpB5eaz/78GfQnZbhv9hlb7n5Z7Z+6077LlzWys2YZMA1FlzYZXRdcG57dGWHg5hH8T01vvWONZ63b9/OWgQtbt+999106a3j0jUbNmyQPM9XbcBJAKPr9hi6XMHOlN0H2Rx48pXzB06rkVKS4W4xooQmu0ARfucQDZc++rRt2txhpyw+xRI5VoamMxYzzTBNLRYqBSxirbgD0TGLG9jHLKGIIha9FDIcoCOrK6KjBjEyVXKsjrQQ+i4CShJZCTlTVDQKtPeKHRYtJZwNMbXyAJ6WRWujvnIBHxb48UmEZ97RZlseWWGnXv4fdteGZvveT35tTTLkx/m74CKXdtevyQcfuET0w5M8YPKV84eH/kfVXSsqiYWFL6Obb0M0TBeqWPN1gv3r4WZ7x9uut8f+hQFoCtuu6HZ48CcsEHsWi3bC3sjEIVjNEvelTEJi5Z4qwVg1TJYoSYwuJxZmwS1x04iNMuByd0EDha1WDoByh2TYkGm1qqlEmmBl7ZauHazDuIuAFYQBhBvUYrIveMtV1lg3w378r6fst7f/CxehKtyLAlZWzn2ploAVZIGWn45GCjynCDkan/4ofeaudmJzYWvlVq6WuQD+hDjlOGfpPLqkT3zrD7bg2DJ71SuOtyWLZ1ocO4pCCl1UoJN1FxHt0GUpEqrsxJxeSZEisMJPowdzLjsyDMUmzJ3jj/5pNxbFbgyRURJkKJq1FEudhUoiVk2YmgKLazQQcz6OtuKJZevtu9f9xhqw6SriItS9tFgALjGCKUcHoqlnSoGph49co7UXH9Sb84FrtL72g2i3hn0QbkXuPErdg19CF2YNHAtUj7UnN2Rs+TceslmTVttZp8+zU5aMs/rxMhBF1wV25YlwGpYC3oV0lm4LUwqtPs2CssiQ3IGtZg67WSyn+yii19SsY0gO0theFVDIF3E1KrIGZBshav79yFL7+x332+qVWyyVqLFU9RgW5cC520V/oKHgn8TC4G4uTnv6x2E/HWUU8IHrKHvhetwI3JHWNQwqhA254MAF400BkeRHxL8AYCJn6JVb0/bUzx6wn/++aPPmTbYzTpxmC+dPsjGVXE8kCdmJgSRcJqNiODNyECNWcVXPLXMPtDCriTINI1McuhUjCzejrq6gLV/2rD3wIIC1ei0TAhnr5P4lmFnEFU2VeGCJFKv54AoUlJLfQZSLB8Hvbu2WD1uQYnQmfW8GzXX5wDU6X/pBtTqtBTNIkvTkFtO9vqD60O5ZIoEQHs6ygNfR0tJaS8MRPfp4my19/AEriclbImwTx1XZwmNn29z5x9g4Yr8nSjcxy5ggxEyZM1/IofwXGOaxim9PRW3HrrQ9s2K1LXt0ua1bv9Xa2wAl3HnkM5nDSDYigAL00oobRqMi3DMPB+fAUQ3ew1t5/X308VrRHGtBAvQl4H0S96qmeNHq092+oztLidvRjidDlMivthP6d1oXBrZx3J7yBFeMEOa6PZazGjjVNB+CLpjaSiZW2hH1t0e22jGdU7m2wtKskBTFyLcZsb881wVnTKgit7K5I+IR8ccHriPiNQ7sIbo5LO8aCVs9k7wBAQTN/vFPs8Z5+QyRAhirEnMUQ1BMFpry1tzcBJhttmTnLSjNFZhQRq4Roj4ksBsjjDLlcthupQgXnWIRjK4CYXSQSysrawFGiZKMYBm9yheSmnVfcWr6UwA0Q9iByRq+P65q73a7Jo74PxnEXj1TDJIWAGmJ0mFATM8Y5GMRwQOgDdu2YpzA1vJi4BsTJyJtK0EaQ3DHOa4poJdk4yYo5CIVo77aKCYsOzosUw7wERWWSEJu1XHpB3OOqCOeNANqoA9cAyKXXzighTN2Jwlt0SgrVsdqnQM14RlQ8GOrrwCC5BCcQAHOqYAivpjvsHJCMEeITJrVYrIM0G6UArxcGn3c0+6GD2iTDBPfDDE3LDpBgxjApMkGzZKWY1VSQwSMCJzWLuKeZfEMqGCNyFIczFv4gMgtK0gW8IlaMGyOYyaAhyVYHGUGnPCGbIO1hsqcSF5CyOw0H6AMEynR3R+fATV2eAsf1HfHB67hfTlHYO0aMho5CHBwBHnccGJR9GEYonYQ5ka90RkmqhQjTM71sveKEfY5zypDqSTcAmKLZjR7JhisoyKFMQUpIBRn0fU5AIMDTYNaOVyZSohucdrksZZs3GSPd2Vsa6zcuohEG9Gcb541KaFnEOPd7phj4oeJRUbYH/HDCSYxzppUYw9vWWErEEeTAFaeCLYRABD5nRKytRtRaXdHGlybvM/d4K4eAVe5WE30eoVMkTFclC+6fudhuyXmeFmDSREp3ECiXE/rXu8xdEzldE3v89rvnb3rVF73DRNDSvdWHWpXkjjsOn4gSdcMNPVsz0CvPdDyPdvlaKKZSCJJyFZLJg/hKKsAMXSkpYkGquAIiDGfT7A8YzVG8wlLdQJwaQaOFuJAmAyH8IVkICkel+r2smiYZn1F0ct7p3qHOq/31rMdvdvuRRrpSQ/vd++yPfe9Mt7WO6f9oUh99aNElqiu6Kzgl3gmZkyhQwYapiJ5DH9TNr6lwd46c6KdjwRdhZdCG2HKOgB+1uW1Eq4tz8gzAXoC/hH6W6eWZYP1SuTbbGY0ZZfPm2gnMqlRhntXSq5UPEuCVzYC00ERedQDl0JpqONWVVU5NyJ1uiuvvNI+8IEP2Pnnn+9ASGCiFIsRoI4peQ2QvlLvgeINqr62WTgNHfc6vfaV58yZY2eccYbcGuyyyy5jlWVWd+4xQPv6rbYI9NTRB5J61jWQ6wZSVvfwgKPbTUvfd8kojAbsuTIsxFrUQqyAWRS9VgSd1Wsufo017Gi0+rHj7GXnXwiAIToKfOSzuDt3h5vZXZfjGeAaEglra2tjtrGLCYFSO+88FpTd/UHwwMTbes+gfQGd97HqSZOB/tZz6h2qH/ROvevqfb7nvt6j6lI/693eIACuRXkLgI/Ew1rExECmDc1hl00PZWwS8f5rakP2ypmldmYVoNTRaRlEvwRgD8ZpPV628FzM0MaI7JEo4uWQYtXwYoPVlKZtPmGGLpleYfM4DltHKKMc8dRGJnL1pNlAfw9spAy09kNQPk6McXXcZhx3Fy9ebDfffLOVl5fbY489ZieeeKLddNNNxIEqIx4VfnJ0SA0+gUTvDqWmqnOKI1PyBmvvDuvtCwRVnzqnOrvX6bdu3WqXXHKJ3X///fbKV77SHn/88X0Cl9qujq7BOlD/Ta8t2g5XEp2U9YyiWwi7q27bLWbHhF/osbK4/YjjSmYaUdw32Mxjxtlf7rjZ/uebX7Tmlq1wZ8xqZXEbcg7ZigyhgSRwEHB1J9Ut0LrmmmvsnnvusZ///Oc2d+5cEz09INFWXGzP5LVNdFTqSZOB/Na1eg/K/fWNnvWpfH9J7VR/EOj2fjdJDH9dZFm061HoNpe+OaskbBM6d9klc6bbgkXTzCYxu1i/086ryNuxBfSCmQQ6QtyiYkxyEI02jx4sxFoAJ4yrswnpTjsOe7mrX7LY6mYS4mdMl81J7LSzS8qsllnITkTJriARGY+wNOp1XBrscuAWgLzvfe+zz3/+83bnnXfaxIkT7fbbb7drr73WLr/8cvv2t7/tOtHs2bOtnqn71atXu4GiAaM6ampq3HENlF270NUwWNXpqlktp2eSI6k6t87NmzfPdc5169a5usQl6NxVV13luAd1Xjmga0Aq2KIGl46pjJKOe+1XXQ2Ec2lsbHTneg4eXSduhABs7nrtqz49swBP3GZnZ6c7p3ZpX3UrqU3yI/N+6xoNLF3vlRFYi4aq37uvt/XKzpgxwzZu3GhtTUSRgGbin6ZOmmxVtdW2ZdtWVhTabgXMHurrx9t3vvM96mYtRRbXGDt2LLqtELOOBBzESl7Pj36ZuFwR68CB2gNrcUxnnnmm41DPPvtsB5LipvUsaovOi5Z6d88++6x7JtFRZcaNG2dTpkyxVatWOTqIPvp46bzaL5qITgJf1aU6lXRcWXRQu/SBmzVrlrW3s0jtpk3unqKf99HTtcqqS/1GdXvcvIJkin5KKiPOW9ctX77c3cN75ykAS0KzZgdj2LzNgj5blz5uJ06otfmVpVZCd2tlId3KsiYrb0va+NxC2ynRkO9Fe4S+TnifHDORtWVxm8I7bFjVZidCl3Eo4oOVFZapYBk5a7L6DAr5FEa+zDJmcsTpH3lM10HpuDwN6RXQewrZ2xf9DyjphRzOCKjqQGqDvm4CqK997WuOa1InVcf561//ak899ZTrZJ/4xCfsggsucCDwrne9y3XAZcuW2Yc//GG79NJLrba21oHOihUrXCcUSN1xxx0msDvllFPs6quvdhzd/2fvPuD0Kqo+AE/KpodUCISS0Ls0EelFEAEVULAgCIgVwYoowoeICIoNUbEhgiAgItJULBQFFGyAIE06BAiQEEgvu/nOM3HwZdmEJBuSLTP7u3vfO3fmzJkzc/73zLkzc5X1k5/8JHfQsWPHpiOOOCJdeumluTP/7Gc/S7/61a8y7eOPPz69973vTVdccUVSHkvswx/+cAIC2267beZrq622SvjS8d/5zndm5bnpppsy4KgXy26LLbZI3/ve99Lf/va39Mwzz+Q0559/flbAW265JfamOieDNQXcbbfd0qmnnprQkPdLX/pSuvLKK7PS4oeijxs3Lp177rnpoosuyrI76qijsgzQoZTyUWSAtummm+aHgc4AkE0q/dcdt6ZjjzsmbbnlFjF3q3867NBD0sDY5vnemER6zDHHhuJPTU8/MyFtv8P26Yzvfjfi70l77fXG9Na3vi3k8YGwpDZMO+/0ugCHx+IhMSkAONY6Rl3f/va3p5EjR+Y67LPPPun1r399+uMf/5h5//jHP57e8Y535L5Gluqq7eTRLsDigx/8YAZ+fJ999tlpzJgxac8998xyB5D33HNPWnPNNdMZZ5yRrXNt8J73vCe3qbTfDV6Bz9Zbb53L1va77rpr7lfvfve7084775y0FxnqR0D5X//6VwavX/7yl+m6667LDxNy5SLQzkceeWS6+uqrs6UI6Po28w/GR0JCbZefE8PAkNUTAUabjOiXthwVoDcyhuYD+qam58JpP7Fn+k0MFfvEROAZ8VWmvrNjjlxMhYiPHKV+8RWnZ56Nhe3NA9Keo2ekUZuG2yLmz9mzv/f4Oen+4PGvsVRrRGweaXKw45UIBcyX9iLrTm9xeWJSMhYW4ekcrAdPQsoAEMSzqLbZZpu0//775yfqhhtumE488cR01VVX5ac+v5i8FN1T15OyPEUpBovn4ovj+4QBiBQbcBiWyrPDDjvkJ/5jjz2W+wYl2SmsB9aBJz8wOO2003I5v/71r9NJJ52UeUDnAx/4QDr88MPThAkTMtCiqxw0KAjwZUEAKLzLyzq7//77c1ksLZ2nWGHAj8WALzyzsBzkhF4BQ/UQXvWqV+V0rEx5AId0HkbqY+h29NFHZysHb8cee2xSBz48QCePLU34Ew3L8as8bXLQQQflfOoCeMlBHnT+/ve/Z160lQeBe+uss0668cYb01lnnZV5uOCCCzLAAxzy3HfffXNdgQ+errnmmgwqHg6ADEBsv/32WZYsZ3LFz2c/+9lclnI86MjOw0b7AHD8AqETTjghgyGLrIC6/nHZZZelz33uc7kNlKlvbbDBBi/IFU1DWHLn18SHByhZ6ztvfOMb049+9KP8IGgKYJkTMusbDvXhU6elXQMwp9x8U1p+vTVSGhlf6F4lpuH2i7Z5wsqF/mn7PrF9UFhaV8U6zukxudcbwr4xPWJMDBc3HDY73T3+P2nI0LVTj8Gx7GqoqRS2r462j/t7xzcox02ek66P8mbGVtkdLLQLSbsEcBVzXyelMOVtHmV2zxDDMICC6eAU+qGHHsrK6bfhJOBjqQAaT15Pbb8pJhpo6fgU3rVOLx1FN5zQmeVxT1mUiSXnyS+uAIXOgw6wobQU5phjjsk0lKXMFVZYIQ9V3FcX+SkbpWQFsg6BB0uwAIU0rBIKxxogB3GUnHVIifBKqSgqHpR13HHHZWsQWKgDa8bLBfyxZtTFEFZ+1gggU1/DNWA8evToLFO8F+sXUH3sYx/LlidrUV68KNMBpMiqtJU6KAfdP/3pT7lsDxp1ZOGMHz8+txnwE/BmeEg+ykQPfcN/IAcA1QUA4cU9POBRWWXoXNrWA+TOO+9Mn/70p/NLHcBd2lK7yaMcdJStDvrDAQcckA4++OD8W1nkSh7AjqyVw8cJ1OVXnreBPUwknT0lrTKoV9p2ZJ80bPPRqc/wp9PcsPh7DLcN9lOpT+xTZsrcHsOb0vgnn0p/jMm7PniChxT7/W+8Yr+071p90q2xKH3Q8MCA+HZAWik2i4xPxvVcvn/4zGakVZ6YmG6eNST92YaOMTG4K4VOD1w6pM4ElHQmvgW+Dp2Rwn3729/OndlwTWfUEVkTAMFvnQtQOFN8wwRP6m984xtZgQ25lKHjKYdCsLBWX3313Mkp3w9+8IMXlFPHkt/QkcK5xotOW5SHlSROYGmxYvDmBYMnOb7xB1TxSjEFVqDNGJUtDec1ZZFXPDBlrbFM8AVonnjiiTwckoY1iQ/30DRE/OEPf5j5VD9AAIxOP/30rJz4FMiV9cn/BpDPPPPM9MlPfjJbLXhmtYnPihnpN95443ywOgzHiqVHfmiVa7/JB7iQK/oFjMQBAzL025l16j6+lEGegmtAp46bbLJJTuu3egIdtNGQl8zuu+++zIf8eFCHCy+8MFuMhp/qwXoEpHgtAWC5RtOZW8AwXN+5+eabMz/igSoLWf9Sh+IzJeOWAJGW2O+sd8yg7x9lz3747rTZGtFWYwJ8Vh6YWlaIh+Ws8Emu2D8N22hg6nnDzWlKNH/vAcPjZUhMO4k9zXqHHPvPjWVRT/85vXqdlVJTvEmMTpxahscb9njD2zt2jB1uf7S7/pmWa1knFr7P8+mVenSQc7t8XB3OflxUoeqclFynMLSgnFtuuWXuRG9961uzkrNCKLo0/CT8RBz5fFk69I9//OPcwSizN5OUBF2WiycxpXCw1nR8nZ3lI703l+4BDSACkFgyOjWFohw6tk6vLDw44xkflOj9739/VlrlATFgBijkVSYllIdysYwcxcqjlNKdcsopeTijfLxTGvmU45pCFZADIKxQadGkiOhLo27yAhQHheSbM/TCJ6UEptLhH58sM7JQnmBIy7eDHt7QVzYFxgsZCOgL+EEH2B922GH5vmvtx89kWCrstddemR4r8ZBDDsnt5IElnTNrkYWpzqVsdUEff4CLz+v222/PPInHj7P64BGIScMCLPJyj5y1BRrSqIM4IKd+6Et3ySWX5CEyGZExH5q3pNI7Zs8J6z0c8716NaWnY0Q4OeZv9RzVlIautWrqEdba3BFrxT7+G8WQcWTquUJMsRk8Oz05IJZZxfy3lljP2SdE1xx0noy9zWbGG8M+o2NvtLEAb9XUPGz9GG6um0Gvx7B4Czk0Huj9YsZ9vMnsgKFdQ8XijD84KtYpnfM6jg6h8/Cb6OR8K5RN5+GbAESUlKOek5UT31CHRQAAxLMg9ttvv2ydcd4aNsjD9N9ll12yVUFROYBPOOGEPMdIOcq//PLL87UpGJyyrBgdmUKwhDzFAYNObwhhSEOxgJmhEaBlpeEFAFEmYCfggRIBhRtuuCE7nPnWxFN8b7+8+WRRGpZQHjwbMlEsAAB8AYh0lI488MXvgi984otF5xoN/JMpn476s3ZYcGQmjYOfaKONNsrObhaQlyBA+y9/+Uuus7IdLGBgon7qYtjLH9QYlKftPATImGPe0F2d8MfyNTTjtNd2J598cpYnsPPSQNuhS/aG32QrD7l56cKaMy9M3fnY8KKe0nqA8a2ho9/IR06GnNqU7MhHHB5Zwh5UrNkCYiwrjnoAL54PTh3ODlfB9ddfn/lQjznhiO8b9GbH+sye8RZ2eCyeXnHdeMist3oA2Ppp7pAAn/j0Wq94Sxiwlh56pDldM75/umNuAGmf+NZkjwDl2Kesx4xpaaWBz6dR6yyfeq03KvVY6TVp7nJrxj5nsUC7+ZmYwzU53frgnHTtpEHp4XgZMDd8Y69EUCeH/qpPl+tSVutr8dGPirW1OOB1Ihol4zXxe9s4Fm6at5z/DRrfkwWD5Qla7rkuB6WlLDqBQ8dfEkG5aOmoFB4/lKSUoeP6XZ6MjcOfkpeyUXLKg1/X3v4x9ymhpzba4j1RKQB6pQxlOgRPaR0bPb+VgQeH35Tfb/JAEx1lAl8NT6EE8epUOoNr+fCCLl78lg9dNPDjd7EEdCb08CMoT7nq2dqaQFsdWvMkDp1SD/nRdyjPIZS8eMIrgMI/gCpp8AH88exeY5APryU/PuRD16G8Apjyok82aLKmSxsCdGmVgR4rD50iO3KSH3305HefTAR53ceHOrtPVkUuHobq59r90k6sMXwqX36A5hqf8herrGefeBkR+5ZNj7wjYjrEbgPHpR12fjqt/oY90qAxr0tzBmyU13f2ejZeAtz++/SbC8eln98yMD05cOVYdzg3doN4Mk3vMzqtGAux9xl1f9r2dSukVffYOCyut6fZfVeJN44xpeXxy9Lkf96Uzv3po+m340akqf1HNk6ZaxR7u36TJRmQH4vXKMS1o4TW1/IE+JvEV4ymknRhzgBPvlhwNi90WuD6L/9L/NTYuRHXQYvC6YjdNfSLV/Kzek+NbVViOBn7lfaOfbViSmXsnTUlfs/z27Ulm76zpYs3m32eM6E7FCm+JpQXbM+JLVtm5cXGbeXranHTYsJu75jEa6ua/gFgg3s/k7bYuCnttfvqaY2wvNOILQN8pqRn77glXfu769PVf3kgPTQlPqTbY2R6vndYZrGrxoBYwD5w1oy06nLT0nabD0pvfsNaafqmb00jBvaNPc3Gpwlh4V5+xbXphtufSo9MjekQPQbm1QtLWpbLErheGftxSUtoGdDzxC1PWU8NoAW8POU1mKdMdwzPxev53jGXy3IVk1C9eu8V8hgUaDRjASKZabFd5LBsxZeAmj2Z54YFGLPH+8QU/Okd73X9K9K8/VvC7xd17hlLo+bEzhlTYzvZa297NN014cm09q3T0siVJ6bnxj2c7vzrTfElpOmxs8OQSDcwrLdwGcz577bVtvuJ9Y0PTY6pKDfcn2557Im0ymsHpqFh7T394H3ptr/+NU0OT8OcphEh5/CBhT9t3kqFV6RKy4RoBa75iL2YuMx9IOW6DDe6K2gR1ZS+MTwLoBk8e97wdEpMyZ4bADRgViDSgoCrNys1HNlhaZiLNNPuhQF6gwxf44HQXYCrT+wCYQN+6w1tOTPDEqqm1dJd42en+8L/1jQ3vgwe8f36jUlTY7Mt2/8w8HvFl5QGAP74M3N+es94I9t/RJrWPCRNHDcr3faLG+Nt5Tynf9++66Tp/cMNEbtxWNhuQ0ildrCApTLiW2TWKnDNR2SsK5aV4Mz3URzi7gG07hgGhOL1jm2VDVd6tcyObVdmxho6uxzwWcWU7vmFkGHfmBwJuOaGY9p+ErHZQbwts0lL9+mG9sbKIUA7elZY9eFzi0XUgWVhiYXPKJbnzO7bJ3acjflusYC9b7xJ7Bu7Ds6NeV8B8QF480DP2tBYqR0kYuJpzI73KbkUS6xmh+N++pzYISXapikWV8enM2Or7LB2O94IYbFBi/y6T4/JvWXh/xWLq+RgbRk6dlfAKnIYHPs+NQcIDQ9LYETMJZo94/l42zXvdX3/kM/8Qt/YR6pf7BLRJ7ZqHjW0b5o0Y3x6LpzjM+LLP+NjmDmk4cvb86PRFeJnGroFaPfsEROWY05Wk6kRs2IqSQyfe9jqJzSyOSxYu270jNn1FqTPCWurKXyKs5qHBuibVxbzwAL0eob8faKtZ+SzftTwe3oMQ+OREJNcAwSDfnNs8G+EEEu+O5r42mVxLcC472j1rPx0BAk0xVbMzWElDYsFc/uutVrars/cNHr65DQ4lqEsKNiDqzksrr7hmxkdr/QPXGfVtMWsyWnIc5PS0P7z3nouKH9Xudc7QKh3gFEeKYc8esQh2O6nJZbpNAfgML8yqEXaXgFOAUFx11vpALwMWOG6AHQBWuJS7P4wh98rgCtDFGtL3hia2y21JeK7WqjA1dVa9BWuj28ntgyMyZrTnkrrxEZ1+626Utp24IA0cmqMdRYQfOd1Wvh1msNJPyR2St1ucK900Bpj0sZ9Yq/65+fNWVtA9i5zq//c51K/udNiS5sAHC8p4qvcs3r2TdPCYppuiyC2VABNrwC3JkPHgJ+Wuf0jPrZmirldfVuAWQBR7Os1O+ZnTQ8n/7SYYMoDFpOCIl5eL08MI+MFQAwdO6C1pT3bhaYVuLqMSiyhivChxIct+syxmDee702xVi8+QzY0PnixecwfGjFtYHr1Mw+kt/R9Jg1fM9b1rpXSIUMHpLXmTgy1mZqei9ncc2PDqR5NMVFyQChnLCoeNW1OWrulX1ru+SfTjrMeSO8IA2vQmj3TiLWb00ExbNzg2VibNyOshen/3aaYdRb+nNlxnhVTJbpSACRzws/FQprLz5U3+Yu3jIZ8YU3N22AxMCcsrLwBTt6PHhDFQulY5zgrnPIsK/lidlu2qnqx2nqGBz/aTv4XvmWJQoAf2l0tVB9XV2vRdtbHM54KNcebLQoxM15ExHThNCZmnw+KL0n3GDc17bjuWmmbVWMngyGxVGdurMMc0JKWHxxzhWJKQ4+Y3d0n9pWfG66YvuG38iJj+dVWTHOmzEpjYj7RlmuMSCusHBMi+8eC8lhk3COAa8Uhs9K9sUh4TuyVPjMc9z7z2DPKpqyGUvP3nLWzsssgO/m+OMwDldaxL04TeBStMr+889J2PXBqLYPG62pxNUqj/g4JhArFUz5eGoZVEOoS84d8PmxQbNP87FP3p1GxpfxafQLUYguVuYNiWVNYRM1zJsVbxhjGzIw99uMDrvGBsjSgpW/qFU732Ech9R/QKz098ZHYXrglrTUsVgaMDKsuJrH24WiOvdEHhkO5bzij5wRQNcdwiTVibljfeJ3fb3b3fHvbDbpiu5C2Alc36CGLUkWWDksrFrQE5ARm8bnPjnWWkycEKI1LI3tPTmPic2PxWjHNHBDDuMChlvC9LDdjSnp1TBFZsTk+FhKZmmNIMzMAaVD8bnrqsbDMnktrxjBm+LDY939UWFVhsNlAvbllcho4+bnwdcXOpDHpt2e8mTSEmhvDnnl0FoX7mra7SKACV3dp6YWsZ8+YstAUvq14Nx9fTuYjifV68WzccuSQtN7QWWlkr6di19OwlGKY2G/08hl8WgbOTluOXSXt0X9AWsebrBgnmp8Vbq3Uf9qz6XVhpq3T67k0bM6TERGW1YhYJBxr+GJvltQ0aFZ67Vqj0k4xDF05ZloqqyUc0C09p8fuBzNiz/SuNFBcyEboHslebnS8QClUH9cCxdP9bpprzTXcHFZPrBlI/cNyGhQO3tXDctpko5ViWU8seRo9PTWvFEPBeCOYRsai6+XnpuWnxD5nT01JI2OIODf8Yy2GmzGZcqWYGB4bzaR1t9sw3FqxJ/sq4bsaEfORpj4f27CEdTXi2bRmy3Kp77jJaWTYeQ9FWTHlK37FByGAWLb7ul871BovWALV4lqwfLrdXW/arSec1csYMeDDioHwN/V77um0YtPktMKI+Dz8OtFtRveP74nFq/jle6flNl4xDVghhpgtE2OoF3ujRXovFAfHJKLhAX5pauyTn+5Og1Z7Ps0YC7jCUTZ8SPwelIa8akjqPfD+1DLzufCnxUz6KLU5Xu9nH1cYW31jUXENVQKtJVCBq7VEuvl12EDhn5q3HMV3XHv3tdVMcxo36Yl4QzglLbfmCqnHmOXT3GFDUvMKI1Ov0fER2NWGxwcepqanwrHuI2QtgX79mmNr6PiK8jNhWY2bMiE1Bcj1jr2jWkYE0aHLpR6rjk39Yw5YzzEj09OzIm9Yes+HdRf2XLQA4Ap7LxC0T+xfVUOXlEC7GrYAl13L9NYaurkEOMbN0x4yOxbyhrN9ekyOfL5pcLo2dhi4Z1IATr/Yc2nA2NS3aYc0p//2qWm5dVOPWEs3M8Dsql6j0r/ijeD0frFbZ9OgNDt8ZffERNM/zxiYxk+LeUn9eqQBQ9cMS23L8F9tHkPN2DRv0Mg0e51B6ffx/cDHLGiPbSdsgJNimkVLTK6cFX6uGqoEWkugANeH4gYENNvPGKGGbiqBedMgYzJoDPcsT0nxCa3mWKf5dOyseV/PUemx58Ov1TteCc4ZEY77NQPkRqdZcwemu8ZNTA+ElTWlX2ycF0PNKbEoeFqsx5vR1C+Nj/SPThuUps+M4WVLTMBsXiHeGK4abxQHpinPz07/eXhOejLKmBFLf/Kky9h5omdLzAELOnNiKkYNVQKtJVCc84/HjdXj2DqO9eI4KY5yL37W0H0kwD0/Dyx6xyLdXrHJ+cx4u/hwGpwuf2BqmjpkpfTqlaelMZs9mfoP6psevP8/6abf35vuurFHuj8mkD4fABQr0cNvFaAT0yFmxI4F90yNWd1390tPrtQrbT02vgG43MPhcm9Kdz/wQLr2ykfT7TeNSA/FG8Zp4dDvEw7/XjFEtGdVcywOfumky+7TErWm85dAPNNeEjaNmJvimP92lg1Z7JawLLdubmCl/lwiEjBzPXYsiMmhPiPK4olNo8P6iedY+J2Wj0mms3tPSrNjnWKzWe5WyMXOp31nrpAm9A1nfXz8wTq7wfFG0deap8Vs+sExEXXgzBgqxkdNm5smzpunlYeDAZAtw2KVSnyIJPb56gm4mmOHUCthlB07fs6JhcfW3NXQ8SRQd0DteG3SjTkyAbRHTEbw7cd5/i5v+PLKuJ794qs08d5v7soxzJtnDfWIfbjmRLpp/WOqQwzv+gXolJ0PvJU0J8zECjugxsfCIt9aAUYx8TTK4IDvhXLMoo9cMfSMPfXsgxZXdgfldO0dcyNiAn0NXU8CmnmxW7YOB7teh2hXjWyLYu7U7B6W+sRSnJjJ3hQWVN7pjt8pQCelSbmM2BM2HOixL2eka+4V3xsMIOodwzzr6lhqefZ9gB6rrDkWAftlA8GWmOc1b9oF6w5Qxez6sKqaoiu32Okg7s+MWRBNyoZei929M5v1XxeUQAWuLtio7a1STGiID1rYqi6+3m0HggCuHnHYPCW+MxMb003Mc71idXWAio+cxi6mzYPDmT8z0sbE03izaBjZHKulm2L7FvtCAULpTG/oO4sTPj7R1Ttmx8cUiDkBfoObJwc+xQAx0kyPXjkjAKxfmFrxwrKGKoGXSKAC10tE0r0jbKTMwBnYPO9zXYZw03vO+2SaoV9ASvyPa4Y+/5MdpPz2K6YvxAvBHNhb2TiL/KDLjg+Ght4UNuf95+el62OuVgCifamsNBL6h4HnEAq9eVf1fxeSQLvs6PquuQv1hFqVKoHuIoEKXN2lpWs9qwS6kAQqcHWhxqxVqRLoRBL4r4Nh8TiuwLV4cqu5qgSqBNongerjap/8au4qgSqBZSCBanEtA6HXIqsEqgSWoQTqUHEZCr8WXSXQjSXQrqFi4zwuX+XcJo514qiA1o17VK16lUBHl0ABrmHB6BNxlO90twsNO3qlK39VAlUCnVsCBbi+F9Woq8I6d1tW7qsEuo0EypAwvtBZ99/qNq1eK1ol0MklUICrDg07eUNW9qsEOpkE6nSITtZgld0qgSqBdkqgWFztJFOzVwlUCVQJLJIE2jXKq8C1SLKuiasEqgSWkATqUHEJCbKSqRKoElh6EqgW19KTdS2pSqBKYAlJYHEtrgx4XWKo2CM+vDBz5sz4uEPsptkn9tOMz8b37t07n5eQkCuZpSgB7Sg0xafOfEmmZ3ymzNekZsyYkdt5KbJSi3rlJLCoFpftd801zZt5d3rg0qmBVr9+/XInnz59ehowYECaNm1aGjjQKqYaOpsEZs+endsSgLXEBzOc58yJD2oEkAGwGrqEBBbF4rKq57Y4xpaad3rgAloAylmn7tu3b5o6dWo+6+w1dD4JACiANWzYsAxgyy23XLagm+PTZe7V0G0kANxYWGPi2DyOcXHkUJb8lOtOdwZUrK6TTjoprbXWWpn/f//7pNOJ4gAAQABJREFU3+lb3/pWBjOdvYbOJYHykeHvfOc76bzzzkvvfve705FHHpnuu+++PPyv4NW52nM+3C7qUPFFZDq9xcWfdfrpp6cddtgh3XrrremZZ55Ju+22W45jcVEC1pghpMMQkq+EL8x99yZPnpwmTZr3rUA+FWmef/75bLmhb+gCHN1zlDBlypRM3z00gaT7pUy/0RcMX/1WDnqDBw/OluJTTz2VJk6cmOkbEqGBTxaHIM61PPhlTeKdD0/wu9QJr/3798/KrWz38OI3eo7nnnsu02TFCK6l8QBwv/iRyjDNEBz91nWRHk/lkL7IAB31QNeBD/cEslYP+chEOnyrI5mLJ9ebb745HXHEEWnChAkZtP7+97+/YE1LKw2ahX/19lt+9JSjXHwrQ3r53MOf9DUsUwn8T5EWg41Ob3FtttlmiRJ+/etfT7/85S+zQu+///7pwx/+cFp//fXTuuuum1772te+4KzXsSnB+eefn3beeef0vve9L+f/wx/+kM4555ys1J/+9Kezz6wAiY7/5S9/+QXxiv/Qhz6UaX71q19NG2+8cS7v+uuvzzxQFhbC7rvvnsHqjDPOSNdcc01W1K985Su5fLyuuOKK6cc//nH6wQ9+kG677ba0yy67ZOti+PDh6Te/+U36+c9/nsaMGZPe8573ZGVXLv4ppfwf/OAH0+9///ucB2CfddZZ6R//+Ec66qijMmB88YtfzAoKAIAF6+WUU05Jl156aebnbW97W9ppp53ShRdemLbffvs0YsSIXH8KT7mlv+mmm9Jhhx2W9tlnn0yLjC677LI8ZDv66KPTY489luW8+uqrZ5o/+9nPXgBAIDt06NAMfHh43etel5599tlc33vvvTeDMB7Qxp86K3Ps2LGJnNRf/fbbb7/Mw+c///n0jW98IwOSh4B28QA488wz0/33358OPfTQdMghh2QAvOiii9LZZ5+dhgwZktt4jTXWyA8HIKxOV155ZW7jFxq1/ljaEujeFtff/va39KY3vSn94he/SFtttVW2vDyJdVCKTcn9BgBAzFPd05dVRomfeOKJ/HR/+9vfno455pisdBTXwYpr6+0kYNp0000zTcD5ta99Laf73e9+l62AY489NvP0pz/9KT300EPpU5/6VNpyyy2zom2xxRZpo402yoCGtmvnTTbZJH3hC1/Iik2xDjrooESp8e5YaaWVkrIofvHhAcwPfOAD6Y477kiA47Of/WwGF/T22GOPDESsGsDAskPnNa95Ta7bAQcckD7+8Y/n4RfAJRMWCz6VBRRcAxzABVjJGiDjiyW04YYbpve///0ZAAAn0HAPLcM5Vhl5A14PkxtuuCHf8xBwn8zR/tWvfpX++Mc/5rq8+tWvzuCsLVl7hv+HH354Es96U38WqofRaqutlq/VS33IguzuvPPOzKd8AG7zzTdP66yzTr6Hn9IeS1tTa3kvkkD3trhYWzquYYAn+tZbb52VrgxRPLE9Xb///e/npzNrapVVVskWFNDiG6N0rBlgxkI44YQTMjBRHvkefvjhrITSAC2BUnmaf+5zn8vgSKFZQ6NHj86Ax4piAS2//PLZotl3333TP//5zwyMnv4f+chHMmABCCDJ+mN1sDSefvrp9MADD+S0znh2ABRg62UEwFNHIAAw995773TcccelV73qVdlqef3rX5923HHHXDc8s8SUowzgst5662WL5nvf+14GO/WUjtVnmHbaaadl/lg7+Aaq6ox3+VlGeMcfACQ3lg/5//SnP820yEuZu+66a7rlllvSl770pVwHoCr91VdfnZ588skMqoCI/MgLyAralWWrbZUN7E499dTcjuSFpocGvs8O64qr4MQTT8yWFT7f8Y53pAsuuCDnA/aG6ePHj89yNWSsofNKoNMPFYtlRLkovQ4+cuTIrEQ6tiANJdfBR40a9cK5+JtYBnfffXe2UvxGw1BJek9scX4LlFEQv/LKK2clo4RAjKIBUgpI+fymMCwQaQod8SxAAMQ6ks99oCItXn/729/msvjPpAMSAuXFC8XDy0Nh0bE8WF2Cuo4bNy4rqWHg448/nodRjzzySB6qKs8QWv3wgJYyHKXe6BQfkLIBu3LlwesKK6yQyxH36KOPZn5Ztg70pcObA11D39tvvz3nIZcrrrgiy+Utb3lLtoqBF4ur5FVXeYET6/RHP/pRtqbwih801VOaQYMG5fLJDCgJ0rEIAT2aLHB1BZjaSX7xNXReCXR657xOaDhjmFd8NAcffHDumL/+9a9f1DI6twNAUHQKCHwogU4NNChRAYkXZW51QTmAhmEHRWUJASn5gYohGUtBGY577rnnBVC68cYbs0+MfwwdCogW4DCkYXV4K8qacw996RqDegNIw1ngxAqSBmhRSj47QySW17XXXpvrrF5offe7380gz8qRjzzQA8bOZISWerA2DQnJCF38AUF1cwBB/HpYrLnmmnnoXeighQY/2Nprr51Bh/XFp8YyY93dddddWfYsQmXiET/OXAB8eR4waJGFOjcGefABQA2dAa4Hg/LIBS/4LsPHT3ziExnItttuu0Yy9ffSl0D39nF5ynJ+8/9cfPHF2V+yzTbbZIuLgjUGHZ8C8PsYGunwnM1XXXVV2mCDDdIPf/jDrBjSOeYX3CuKbeqF4RZfjCETq8NbTsNWQzjOao5jZxaKQClZCs7oAAUga4j4zW9+Mw/DgIBhIB6BTWsLoeR717velX1HhnReAABAdUQPAPnN6V0sJrQosyH0n//85wyg/EeCMtyXB30gwYoFvCxAB1DBo0CO6mE4RvYsG75GdPCtfOWymFg/XgqcEMNwPPKX/ec//8m+PY50siNXFlGRC3+WvMrED7ruNYbSnmQOQLUlsBN/8skn5/rIw4dIJoa4+PLwqKHzSqBo5zVRhW3jKHvOL3SNdAI+o6LMjRl1tnJ4Eq666qq5Q+lUOuOSCpRNh2fl4IOPhvVD8YoSGq7owBSqKAbePaUppo4sTwmGdawIT21BPRoDJRFYWOi4BkCGQqwMkyf5oZTHKV2UrjiUy7CKlei3dNIAYGX/9a9/zfxQ/KLQfhu2qRdryhs2b0XJlbXIMa2+0gEnCgwkPvnJT2Ye0ceX8gz5lKPu2kI95cMPAAIa6qVs6cjWmfXmPlp8eKwpwO3FBzDChzxkLH8JZAk8lAPs3VOuh4y+gXfDQHk53w3p8cmZ7r5rZZV2AITqYEiIlj4gPysSf94ckxM+xaOBJ6G4CApvnfGsvuqtTmSm3cU5tJP2Z8HrV65L3ZdkXbVFKZsekKtrRwmtr+UJK15D/K9zlMQvPVM6R784XmRqlxI6LXARhM5ZGs1ZI5bOTHDlN7mUdOKlc+jgGhngNAaKBYycpWsMpSPoFGihgbb0fkuPnmvxQKGkwa+AbvldFLLw77rwJ125LnUBAhzT3tg9+OCDmT6ayjBFgDN/2223TaZEeDnBOlKW+0VmhQdnQXnqUn6XcuVTF3XAH1kBqJ/85CcZZLxZBAxk4X7hMRP67z/xZEYeyqFY8hSZo+1+qaeyBenFl/Tiyr1SDnroC/JLjxd1lcZ98dL47X6pZ87USf+pizqqK5m4Vi8yLXXVdqXOS7qaZKo89BcDuChUwZ/5sTZf4Cqod3DkXC2Ocj0/Qi+JxzSfwst1BB2TVSSdo3S0lxBcxAgdU0cs9EpDEapO71wUEGnpisDxRCFKwzbWQZ6iqIXnRtZKOUWJCk1p3FNO4Q0d6cRLh19n8TpeCcqRxn0BX+hI5554R0knL8tCGlZGoSkNHw8/l+EZuSu/gEThq/CunFKuvOgU/gtwSCM9UJTGtWC4V4ATX+LxhUZJkxPGPwpW6Ptd+JG2pEdD2UKjfAoQSYdXeaUVimxdl9/SyVNoKVdwVv+SN0d20n/k66UO/6qXG6wrdRPUjz9P2xcL95WopnIcHmT6YLkuZbW+Fh/TbADSiy2BkqHt80kRPa9T/Pf+omRum+QyjiWYooA6usYsT5wSXxpTZ5ZeGgfQKqBQ0pTquC4KKl/rUBQCHb+d5ZG25BNPySiycp3dKzyU+4VWSYsWvtDDYwECPIhznxOen85QybVDfunV39SESy65JPugCjjLq+xyuHaU8tEovKElNAIEEKMI6ElnKMrvxQJTL/J2oMfCawzkoh6FhyITaUq5aDTKR5rGe4VXachHULYgbWnvIguK5HcJeC59oMR11rP6k4N6cxUYrjMgyF6dTULea6+9sruB3DpgeKlSLQKTxVS7JvJ0Wh/XItS3Jq0S6BISKA8C/juHB5UHikm3JuOadOzlTplWUx5ES7Ly5SEAKJf2UPF/j6MlWaNKq0qgSuAVlwCrlkXMKW6lhWDyr2kkho9WJLBIWcodMBSjabFYq8C1WGKrmaoElq0EWDmGxoaBwKtYW97afvvb387+zTJcL0PoZcvxki29SwAXk1XjaMzSoEzj4ssx7i+/i/jkKUeJk6ekLb6W4jOQRidRjgO9Up4nmmtpHeiiVXgpZTu7J+hs6JV74tyTp/zOPxr+lfLkK/4N5RRaxd/jvuBc+EdbnkK/gWz92QkloF31HW2q3fVJU0c++tGPvjCPTbWKn7ATVnGBLHd64CoA40yBBZNBPYHEOSircwENaTS6eE5O8aXxxQEDYKQzFAAqoONNVQGcAlIFzNyTzrnEuW4MJmwqz9sgZ4e0jbxIX8CnMW/prOrJSW4Ol3ldZsebj6aToq/u+DbXicPWtXvqqLwaOr8ESn90bnw4lXlb+pO2du6goV0dcd6row5as4VhC0gInJLmLZnoaKLiIYcc8iLQ0cAAAoD5rUEpsoZ2XSaE+l2Um9OTwpc0ynIPOIjXYcorYGa5a0BRJrICj2L1lLq4h47Jk8DRNXrFair8AVSA0xjkw4t7ylJfi7elM8kUT15LlzWAxQdilrwlRGjXUCXQFSTQ6YELcFh7Z80hZ6QZ53YJoNgCpWZ5AC1LagpoFYCSzxsRwEHxAU9R8AIi5i4BGiBjBj6aAAmQoCMfBykAAogmVioLwJhR3whA8pldLy0QlEZae3OVmcdmtzP7Wz8t1RXYAic82bbH1jPigCx+gdVhsVWMZUZ2dLB1jC1l7MTwr3/9q6M/hbuCTtU6LAUJdFrgKsBjka+FvxySrKyddtop/7ZujzJbxzh27NgMTADKNsBAzfYoZQscCm/YZYsWr5Bt+VJAQzmWudguxXIdc6eAD8BhMZngae0eWtb1AUBAAkBtbGcSKMtLmcpB14JuoGLHAgvCpT/33HPTQ7E8x2Jxa/+sucO7fMoDhtYB2s7Y/CyHjfbKYmxb2Jgtb3tjS3EsKjYx1GJywIWOuii/WJRLoX/VIqoE5ieBF/tQ5pdqPvEddgA8H35fEm2NHEW0sZ/lLdbLHX/88RnMLNy1xg04UGrWlUXJFhUbZtl91H5PLB2vj1krrCdvZsp8GNZSUXhnlo59pUzwkw8oyQOkgCULxytpG/JZB2jfLftkSQP00GUxuZbXwXIT1INlB1wAjXvKlLY12JRrIGuHCOAmHavQEh+7LtjVlAxs72OGO1o1VAl0EAl0bx8Xa4WC20XAbHJDJBsCUtqydYm1dBTdcBFoAQdKbnKeoRZry7WjBNaP4RplBwbys7IEy2zEAwvAxvJiQR144IEZ1Mxkdh9fAAN4spjsZMGqK3QMCU0cFDjrAZp7+DTr2Q4TrDRWm3WBjQF94GYDQ9sgA3BlqAPgw5OtjllegFNaAGm4ie8aqgSWsQTa9RTttEPFInT+IkpM6VlMAIZiUlwABXxYIIK90t2zmwVwkM/hDR0QKZvdiXO/KDgwAQjSACoHUCpBetaboZkhK+sPGBZflLzoCQAEwAAkFtsJJ5yQ+f7MZz6T+XAf73i25Q5Hu8mFdrxoHYAz35idPu1xpRw848fZEPfyyy/PQCbOfeBVeGlNr15XCXQWCXTaoWIZKnE4eyNo0z0b3rFAAA2lve666/LwkFXGqW6bZqDCQgFyh4RPzDCLTwjQUG50+anQKGWwUjj499xzzxwPDAX30QFowIg/iYXF3wQkAJB4YAHogBW63gwKAJCVyIIDQMXiYuHJA1DxLh2e5EdTnLJtE803hj9WVgEsoKW+Y8O3ZxcJtEpdKmhl0dd/nVwC/zMbOmlFKL7tXeyCyrdF+e1TZcdRe8pzqPsaDUvGnlu+7gPoOLrtxglkDKc43ym84ZwpFcXJzqIDHIafHPCAgp8LgCiLw5+lhZ7hnTlVHPOGfl4a4AO4lW2FARqQwYMyyvQGO3gCJkBn+MpH5gA0gJXT3R5ZeAFQD8VQVn7l4rfxjSk66gvYpAW0ALOGKoGuIoEyzuy0i6yBgCGhN4jAhGIDA9MagIMAaCgwQABO0gIBgOJe+a4hkGi0TuRBk7UCCFg6aMijzGJFoclKYjUBEGmLhcPyAXDABH1l4hFAyYO+4Fo6+aRhRSlf/YBdsbKUKV0ZqkqPtgMteQRlqEsZHpb4fLP+qxJYAhLQ90ofXYxF1gvjaOXAd7xkI8FOb3FRTEACLCh7UXzgRXGBRTkAlt9AxzwqAbgBAfkLGDhLV0IBHQAFYFgwaMuj4Vg+wAaoAQ/p8YEOP1YBlHJPXmnkKRZYsfDEN4INAJIOP0DMfXQE18rBB2ATSkdyz+/SufLN+q9KoONIACAVw2mRuer0wAVMKHpjoNxFqd13TYkLGImj7CW4J66Ekt41QBHQK78Nuxrzt6YnrQA0CsgUECl8uY9GKVda5QrSlt/uF75Lme4L8giljHwR/xrBqoBxuVfPVQJdQQKd1jnfFYRf61AlUCWweBKowLV4cqu5qgSqBNongcUeJiq2Alf7hF9zVwlUCSyeBOb5ORYvbwWuxZRbzVYlUCWwDCVQLa5lKPxadJVAN5ZAHSp248avVa8S6JYSqBZXt2z2Wukqgc4tgQpcnbv9KvdVAt1SAhW4umWz10pXCXRuCVTg6tztV7mvEuisEqjTITpry1W+qwS6sQTqW8Vu3Pi16lUC3VICdajYLZu9VrpKoHNLoABXu8abnVsElfsqgSqBziaBAlyPBeMv/vpoZ6tJ5bdKoEqg20igANcHosb942B5laPbCKFWtEqgSmCpS6Bdo7wCXDOCbZuSD45jpzhevDNfRNRQJVAlUCXQUSRQgKvwMzV+2Ki9XWhYiNVzlUCVQJXAKyGB1sCljApar4SkK80qgSqBRgnUeVyN0qi/qwSqBDqFBNplILVlcXWKWvvohMOHJHzRx5d7fEyifGTCByR8RMOHJcrRumKtP1bR+n7jdaFRzo33lOmjFvgpH7RovL+4v5VVPnxRPowxP1rkgAd52uKh8O3cOqBdylGHBYWSVhmteWoso/XvBdF8uXtolXLb4r91/sJXe3hoT97W/NTrNiXw0o7YZrK2IxfcS9vO06FiKZDvDAIuX98pX4oW5xNkpcOXztzIfAE+57buN6Zd0G9f0gGSaCyMYi2IVuO9Aojq+HKAot7la0dt8bAgOShzUev/cmUsKr3Gerf+XeRQ2qv1/cbrwlcp/+Xq3Zi3/l6qEuieFhdldrzuda9LV199dVpzzTVfsLa+9a1vpfPOO++FT9Pr8I7SqUvzuC4duzUwlPj5nQuNxrO0rek03l/U3+ipIz5b8z4/WvK0Tuu6yKAt/sRJU2Q6vzqLFwo958a0jWX43XhvfvwuTHz5ziX+Wn+KrXX+UhdlS9/IU+u0C7pu5N3vGpa4BLqfxUVhBB1TGDFiRB4yii9fiR42bFhWMEOocpT0OVP8Y6H4wKvvJZbvG5Z7rTtu6+uSztmHaH20Ff0l2ckpKdrOrXlvLN9vw2U8sE6KfEoaeYsMnFuHkr51Hed3LX0jQJR0jWX4XeKd2xO0DxkAMPVcUCh1kUa5jTwtKF/re428t5f/1rTrdfsl0Kk/CKtDFcAZO3Zsuu222/JQUScvHfj73/9+VnoduADBIYcckrbZZpt0xBFHpNGjR6dx48al0047Ld11111Z+cvn69E/44wzMsCdeeaZ6Z///Gc6/fTTswL9/ve/T7/4xS/Sqquumo488si0ySabJJ8hZ+n95je/SZtttlk6/vjj09lnn50uvfTS9M1vfjP9+9//zvQ++9nPplVWWSWdcMIJ6dhjj83K+MUvfjF/jfrUU09Nl1xySbYifXn7uOOOS1tssUWaOnVq+tGPfpSUq87qU6wagHXiiSdmHiZOnJjU+dprr80y+NSnPpU22mijXAf1V6dDDz00AzY57Lbbbllm11xzTfrBD36QZUWWRx99dAbNAuyXX355+u1vf5s23HDD9P73vz+tvvrq+Svd559/fvrVr36VfvzjH2eeyM5XtYHNJz7xifS5z30uf/lbvQTgI42hbQkeIOp24YUXJnyst9566WMf+1g66qijMt+f/vSn01ZbbZV5u/nmm1ORVXngqNPmm2+e28FD7P77708nn3xy5ke55YFCXlwI2l27KUN9fIFc+/7pT39KZH7KKadk1r7xjW+khx9+OLeB9tIu+CtD8sJ/PS+WBDzNFtvq6pQ+Lh1V0BFLJ1prrbXS4MGD0/Dhw5NP3FNm6QwjAdPaa6+dO93f/va3tPzyy6evfOUrWYG+853vZCtFJ11ppZUyvQIInu6GoCussELafvvtM2Css846iWKPGjUq5wM8q622WvrCF76Q7rjjjqzwlOyWW25Jf/7zn7MyjRkzJjnw94Y3vCG98Y1vTFdeeWV6+umnM3CiVZRwjTXWyBakegG7V73qVRlQKCxFRptVhUcKSRGBg3gg+9hjj2UQAyyCM6CjlKw3clKvPffcM+23337p3HPPTT/96U/Tvvvum3beeedMe7nllkv4+N3vfpcBcP31188KrbzPf/7zGRDV+6mnnkrve9/7Mh/S/utf/0rk849//COXhzd0yBtYqZN64r11kK6AJN+k9lLeAQcckLbddtv0ta99LV100UVp9913zwAtPxAEkgD5pJNOygAJtMkaaJIvoH/iiSdyOwLzv/zlLxk0PRCAF5lpt//7v/9LO+64Y+Zz3XXXzQ+WN7/5zZnXV7/61fl65MiRme3yUGxdh3q9SBJYbNBSSqe1uMpwhZLcc889GSA++MEPZrACXJRGmssuuyx39l122SVdf/316fHHH0/veMc7MrgBK3lZWj/5yU/yU/uqq656wYqj8MDPfQpJuR566KGsBCwgCs06+MxnPpMV4q9//WtW/j322CNbZ6w4lp2nPzDYdNNN05ve9KasTJ7c+MQjJf3ud7+bFbvEAUug873vfS9bdoAYOO2zzz7pxhtvzPlYLdKhq54sO4B5xRVXJJYXxaZsgBtAoYdnyg6kpAGCzzzzTAYUceioM3BhZQGFww47LF+Le+9735tp7LDDDhkg8FDSAgJgSM6sS/eUxeIDOl6gAA6/gWhjwMfBBx+cARXfAI4szjrrrGzB7rTTTum1r31trjerqDywACIePTj+85//ZDACRFtvvXWu/wUXXJD233//tN1226Vf/vKXmaa2ZGkBLdbxr3/96/xAAZB4x9sf/vCHHAfUx48fn+WM32Lp4reGZSeBlz76lh0vi1QyhXc8//zz+akPLHRQ57vvvjsrDMvCU1s6Z2kpmSe7DmhoB1BYDgIQ0CGLUhSGKOHYsLI8kQEDQEOHBSUtkKQ8FNM1ZVUmxVWWMlz7TSmV4XCNjlDKBRoO6QTggp66TJ48OdNSBtqCdJQXHb8pmSEt5UOfkgMu5UvjUHdDKpYNED/ooIMy0JAPgGQhSYOGuhYrCY+U3RAYjyysArR4kEc8UC/849vDwhD6vvvuy2Wx7loHeaSdNGlSpoGOaxadvIAI6AmlrmRQeLvpppuy/D/ykY/kBxC+8YQOvsgMr+pY/J9kVR5O6LK49RN0PRy06U4BmLfffntuH7QE8TW0WwLtcnx2WuDSwSiSTqRDFgDRER06KIUvICK9oNPr5NIYhshLkXRKikHhpNHp/aa8lIkfZO+9985PZHSAAkAQ//a3vz0DBOsKUNx5551ZWT760Y9mGoZV0t1www0vPN2BIIVysBT4olgNRTmU73j961+fy2JtGAKxCAoQ449yU/oNNtggDRkyJFtHrMaVV145W0bA9dZbb81ghLb68OuxVMkMjw4AiW+0WZZoAjHXZKycoUOHphVXXDF99atfzVakuuIBn9qC3MhZmcpB3zWLjgXDH4YHNJzRLmfg/vOf/zxbrz/84Q8zXXTU+957781+uQkTJhB95ke58irTUJ1LAAgDLgBpeIhv6QpoSS+OPNDykFMnZbjHYtYf0NS+eGdBkpV20mfwTDY1tFsC3XOoOD+xFctFB/M0Zg3pcAJFdO0Jalj1zne+M1tp0nK0G2KwQtAAeM8++2zOq1PLw8/1wAMP5Ps6Mn+SoQznvCFJeVJzMpumwY9kWISuexTdsJGznYJ9+MMfzkqCP8qlbHQpF2XncOdA5hQWKJbflE3dpFWfs+MFAB4oPloXX3xxeiiGtBzm0vFLqZMDH4DH8JZDWx50WB+AxbDwbW97Wy4HbeABmPDG4gMKQPY973lPzocmS8WDAt+UHj35gB3Z8RHx6QERgAJYWwdp5QEWeNQmaKuzB4M6iWPVKg9PytNOgEX7eRBpU6DpJQqe8E3u8uEFb+5zxpO/tlIe/xcaAEtbKIuPUlux4NGQ16GONbRbAu1yzreFepsGSzfF0XdhWKMo3rhoaB2jMbguhw7BB6LRS+duTLukfqNdOuujjz6ay8Kj4QFnrc5OISgKn48hk6GiN1HyuYdX93Vg1gmLC+hREm+ZOLgpNeWRztBK3VhVgI0sWBDKZE3Ji6YgHx5YRA8++GBOIy3rTXloeaJTLqDDcc/Swjs+5UeLguMVT3hQDzzgVb0FyuhNobd+QEO5X//61zPflBx/fGCUknLiW33JwfDYtTLISdmUn1XHIlMO/+DYGEIDWfTxa7jNmil5WUMARLugBfwE/UJcqQf+1b/IGg/qjI4XJOoMxLbccsssK2UAHEDCSkKbrMhVWwJiNAvAq2cZGspHvoAS/36rv3bCHxryka3yga2XPtIpR5niu3vQhuSgHbldtJ3rRtm0vpYndIiDcJ41sWAhAhRHvzhetGNNW9Lv1MBVhElgfpdr8vG7KItz433X5Wla8lAq8aUhXANB6ZylE9zXoRvTii/pnVuHwkuJR4/iolHSl0YvtOWRzrVzSYdG6/oCEtaHoSUfk/uA1ls5Cvqzn/3sBYsF8JVQ6JSyxSsHX0UuhR88FFmU/NI20ih5xJV75VzySFPqVspzbk2nyAjwSC+UvNIKhVf8lvYUX+43pi88ud9W/UpbyFPylzh5lmXAj/7ioQMwgGwjb9paO3uoeKC9EsPbIhflLgZwMVvbwp9GsXYf4GqsdXf+rTOxkIqlRxYUn9VAuXVqAKTz1dD5JKANy9Qfc+AOiTl5rHaWKtDy4DJNRpy35wBuSYd2Ale7LK46WF/SrdlB6HkaAycdvACU4Q/w0uGKZaKT19D5JGB4bAjL2jIE5s9cPYb0huXa2Jtfk6D5/1hjXS1U4OpqLfrf+ui8AEswrGj87akMvMQBtxo6nwQM71nTzl7geFFkMvXGG2+cX8bwBR5zzDH5Tbm+0NVCBa6u1qIN9QFY5dB5DR8F1lgZIhZrjAUmbUnTQKb+7IASMBQUtJcXCh/60IfyCxTg9ZrXvCa/+fXCoQO3Z7t8FBW4OmCnXNosFXADZgXQljYPtbz2SYAVbQ2muXif/OQn89nDyouMDhpezjG/QLY77ZKfBdaq3lwkCXgqF8trkTLWxB1GAt4ecsCbmwbEvITxECpTRjoMo/9jhMW12ODVJSwuilesBdaD0PrsfokrVkWxNKRvjGvM73frUOiU/GWYVWg0phfnkLbxfqMJ73cjjZK+kU4pU9z87pf4xnIaabT+jaZ5WeZelWUw+Kih80mAs970D2egZY6aa787aFhs0FKfTg9czOECIBqK4mksTxpv0QT3NWiZ6yLefWkbndbFiVnetLl2XzoHgJEPfTQLjTLlgN9BOveAB37EFRp4QRudUi5epMWb3/LKI6/gNz7KHC/3TSR1FudQprzq6OxQhiew39Kgoxzx6KF/SLxCN7PfEhsz6G0fg1ZJp4waOocEtJWjtJ021z+1ewcN3dvHZSZ52TKG4jrstkC5mcyCa7OvvUEzt4l1QUHLDGlKvHq8SjZRr4CMvHY1sAaQskvPMjEb28x5ef02sc9sd+CFvuUtzuiIdwAlnUq8vMorlg4+BbPn8SCYAW7GuTLwigYeBOnsBmH2uiA/cLLjgTdMyrc3mA7sWrkAjAxs10I+yrdsxk4Tlt/YZsfOCbvEDhrS1FAlsBQk0K6nYoeF44UVHDCxfYxtWyy0tYcVZbbfkl0SytYygARoWaNm1jhfgMXVdpQAfieccEIu0kJdim/BM/Ci6Ja2WIhsDy+gglaxisRbf4hGASkLljlIBWsZ7RGFPzPZy1pAW7jYGsY2N3ZAOPDAA/O6ObsSWJZTgMrZMiOb91lwffjhh2eLSfn2FjNfx3Y3eDB7GSCxqK677rocZ0Gzraytb5THhERvoCynkcc8H2+lOvCQIsux/utyEuh+Pi5WBIVmVVA6JjFlpaDOFgHbfgZ4UWp7SFnlD4AAlvSsEflZQ34DI/EWVgMja9ve8pa3JHt8sULsVWWxrx0FpLPVCovFWxyAaI3fu971rgxC/AtoKE8Z5tSwimzNwokqHii6B4jk8xrbnlzWCFqcDfDUhRVkQTaLztk+Xiwk9bCrAR5Zg+RhdwsLpC3zAYqsM3nUEUDb3YIVp0z1tTkh60sdrV20N1VZs1iGjF1OXWqFuoQEOr2Pi3UFOCg/sLHrg32iWBg77bRTtoQABd8Si4zVYUhGcQ2h7DrAcjHcBIjFUV32tLKzA/CwqR6wUZZ0QIcfAWAYAgIBawKV5QAQAM6OC4aHhmqGopZnoGOICViAIUvJ7hLi0UYXoCjDNWvOBoDqaidTZVrMbda0jQwBr3rZQBDg2PFAWkNZQ0rDUoutz45dJOS1K0QpxzXrz8xrS0TIpIYqgaUgge49VCRgwznbrQAnSshqYXEZFhk6sYAM3ygyhQVaFBwgGGq550yJgQYgoMDABQCZ0GcnBzuhsl7kLTScAYwhGbCRDyAZivnNh8SaMSQEcKwh/BY+bFPz1re+NVuJ9jpvpF/SsLws5UCf5cV6UxfgxOJ0XzBUBZr8XHhSD+CmTvZeB7r4VE8BsAI84IoP+dVZfA1VAh1ZAp3e4qJkFPe68Ol8+ctfzqBib3ZWBt8SEHHNj2RYxYIqAATQKDpQko5C8/UAPsNDQyr+IP6qAiLAQzoHAHMWx/elHOlZVqw4oGA7GKBqWKmskh89ZfFn2Rfd/uZ8bo100S+gJC9fFx8ZcDZURIMlWN5qGvKpp/vKUS8WGvDltzO05LtjAQI99PG9U1im9rYHWmjWUCXQ0SXQKYGLNUHpKBkF5WeyFxYLg2XBOuGLAlKGP/YL49+xGaCv1xhS8jX5KhDrhxIDBotWDde+/e1vZzBBw1ALIAIfFhSQAGwACciwuICLtWLAzr7lhmyGoCwdfNr5lCVlGCvOoXzDPeXzbbEQd9111/wWEeCxpNSLleZa4Luz1xYA9pYU6NnvCz2yUI5ta8jHrqv4Peecc/LLCB/pkM8e6l4UkFUBXZYqGuRQyuroHbfy170l0NbjtVPtx0VJgQDfFOsLkFF6ikxxKWd5YwZkWFbSAR55pQFWVthLywqTl2I7Ax4KDUDQlcYZHaBj+AWkgB5rSBz6pjEAU+e99torDwf54YAEHtFHm2UoDm9AwzCwxAFg9IAlXpStPGd5BfwDMS8mOOANUdVXndCUnyWFtnLlRQvY4lPZhszKUEcyka+GKoGXk4C+UvrlYuzHtTArv715dLxkI8FOPx2CcIEWZXVQaGABGCg5xaWslNS9AmrAwbWDjwsN6eUTxFPmosh23ESLogMLyi0tS8Vv6QrIAB80gYW3eqwwzncAK40AOPxmiQEO4IheKQNN5aCrXjqItGi6h7+SB0gp0311dY2+Q1rp8Am41NtZ/sKzNA7X7imrhiqBjiyBtnpop7K4CJeCUkzKSPH9Lk+CcgY4FJNyuw8ABKAAICisg+JLJ5+jxEkLHORzSEfZCziywPwugAAs5MebwPpizaFXwAQ9ZQHVAlp+o1P4lFe6Qk/Z6uIsrzOQBLLSOaNR6ogHPBW+XAvy4UW8tOirk/vo1lAl8HISKHqi31SL6+Wk1cZ9QEB4BUycBYJ1UM4CIJSVcsojUHIKL5R8RbnlLfncL9aS+JLf8EowHBPkdShHAAqCvKUc98WXtAXI0JWmgIr7JW/+Ef/QxZO0gmugKaiLUOSh3iWUssq1NOLQaqxjoVvS1XOVQEeUQKd0zndEQVaeqgSqBBZJAu0y6ytwLZKsa+IqgSqBJSSBttxUC026AtdCi6omrBKoEugoEqjA1VFaovJRJVAlsNASqMC10KKqCasEqgQ6igQqcHWUlqh8VAlUCSy0BCpwLbSoasIqgSqBjiKBClwdpSUqH1UCVQILLYEKXAstqpqwSqBKYAlKoM7jWoLCrKSqBKoEOoEEqsXVCRqpslgl0AUlUCegdsFGrVWqEujqEqhDxa7ewrV+VQJdUALV4uqCjVqrVCXQ1SVQLa6u3sK1flUCXVAC1eLqgo1aq1QlUCWwAAnUt4oLEE69VSVQJdAxJVCBq2O2S+WqSqCrS6BdPq4u8bGMxhZuvfVw2f64MU39XSVQJdC5JVCBq3O3X+W+SqBbSqDTDxV96KF8wcbHInwdx5dunMuXfUrLssZYYOWjGOLLhyfcE18+MFGupWm04uT3oQlxjfE+cNGYXz6h0eKTpsQV+uKkaTzcW3/99dOIESNyGVtttdULPPvIRuFRnkYeMvH//ivxhW4p19kHNgoN14XvxrTi2wr4LR/XUEZbPKBdaDWeleNa8LuE8rucxZd8aJV84sp1OeOh1LXkc13q2EinlOc+mg51cRZXeCvlKEMo98vvHFn/LVMJdHqLy9d1dtxxx/zpeYquwwrOP/jBD9LVV1+dr3VGHdDnyMr3EgvoOQMjHdWnyoCDOGeHT3c5pPEVHl/sQV9ndwBM6ctv6UrwW7kUHpAqo5QvTeOnxvCIrvRbbrlletOb3pS/xejr1TfffHOmL49y1AM4o4umuMZQysMbuuULQPj3mTTfePSFIny7jxY66MpbAv7dF5RR0vld5Im234WP8kWjkq/kLR/VLfJ2JldllnYrcnatLco9PIlDWzmlDOX6tJuzevntfmkD6YpsnPHkXNpVWnF4KfVxrTxx0inXdcmnHtqthnZJoF3TIf7XQ9vFw7LLrKMfddRRudP9/Oc/z98vXHHFFdM73/nOF5ShcFc6eOmIPsTqA6k6uU5Z4kvHFw9kKI+gY1Nc1wDAPZ1bPgcF1qGlK8F930n0kVaKCGiV65Nizu6rA3rulQ/VAt3LLrss33v88cezclEiZSgfH/IUfihoY5CWAqqLujnjCw/kMGrUqPw9xhKP7qRJk3L58paAt1IfafEsrXjXAj4K4LsmN3JQtxKkL1/mlr7wq87KK8CkPuS1/PLLp4cffjitssoq+b4P+QIm99CXDk2/1ROPynRd2kL7qKv648+3LclXWuVKL43rAnj4VS/8oOWMX3XGA9B69tln87nUrZ4XSwKetIsNXp0euHy12vGb3/wm/exnP8sdTGd/xzve8YLCFbEacn3hC19It912Wwa7yy+/PD300EPp+OOPz8rx8Y9/PO21115ZEaTxWXsd/dRTT02f/vSn05133pnOPPPMrDDvec970mmnnZZ/H3bYYbmsc889Nz3yyCPp6KOPLkVmJXrjG9+YLcKhQ4dmcPjKV76S/vSnP6Vdd90106WElORXv/pV+trXvpYOP/zw9Na3vjXtscceebj405/+NCvd//3f/6Uf/ehHL1IaCvfud787K/QLhcYPyggQgPqrX/3qhF/X5513Xv545/ve974c70vb5KX8Sy65JP3whz98kdwo/cknn5zjPvWpT2XAufTSSzOv3//+99NOO+2UPvnJT2aeACr53n777el3v/vdi4DLvbe//e2ZB7w5vv71r6c11lgjA4tybrnllnTSSSeljTfeOH31q19Nf/3rX/NXwNULiH/zm9/MNLXFG97whtxOt956a/ryl7+cq37OOedkWvi66qqrkmsAKe8ZZ5yRtBNZAVRy+8xnPpN5lU4bHHLIITn9d77znXTDDTekn/zkJ+m73/1uflDsv//+6YADDkgf+9jH0nXXXZfbKRda/y0TCfzPNFgmxbe/UB3wgQceyEpuaLX11lvnzu6p6YlahgFKMty64oor0mte85r0uc99LoNC6ZhA681vfnMCKhRj3XXXzUrvC706NXoUn4JRBk9+HZhlADg9/UeOHJkVurFWrARABNA+9KEPZVrKBhRHHnlk+sc//pF5v+mmm9Luu++e6bACHPj/xCc+kVZYYYUcr0wgi0c8UVhghif8UUgHUGAlqLsgLeAoFg/a8nz0ox9NwJQyAt23ve1tCcjKV4ZG+Cz1Y3EUa0Z9lUNuAB3Q/vvf/86yU+axxx6bTj/99Fz25z//+XTccce9AFroqwsahsEeMh4Or33ta9N6662XrTr8aSd07rnnnrT33nunDTbYIAOWhwswJVcgd8QRRyRWKasIXekEbUP+6rvSSiulQw89NP3617/O9WTNAVnluE9G+GJRss6KBei3OsuvvciXbMlb3hoWWwKLbW0psdNbXIY3LCcKRPHKsEPH08kag07IYgEQr3/969Mvf/nLdO+99+ZOSDHuvvvubLlR1r/97W9pl112yZaETkqpKCoaOqxOzmp617velZVHeYDh73//e2OR2WJjtV100UXpvvvuS4fEU92wES2KxwoETltssUXmvQx/EFlttdXy/YkTJ+b0+MBjURgKDVgLQItXZ7w0BjyzOCige+istdZaacyYMVl50WG57LPPPmmHHXbIMpDGsMiDQZD+xz/+cVbaIluWmrrxl7GAHnzwwcwvwMCntnB4sDz22GMvDMvwgT6gMWRmBaFlKAfIAAnAAXba4amnnsqWIH61HRCWXjlkzh+IloOlTJbuA1q0lLXOOuvk+/jZbbfdMkDxiRoyk5t6sLTIEk+ADJ8CoNLOrvFVZJJv1n+LK4HuPVTU4d7//vfnp70nok66+uqr546uwzUGYCMApmKRsJ5KOh2zKL8OLFC43//+9/kFgJcAOq6nsnQPxTDT/T333DNbYXfddVeaMGFC7vQ5c/yTTnnKBkrKoBisGENNvhZDRGCw/fbb53TyOAwbgSMl8sQXJ5R64OWJJ57I1+otvrE+OXH8U5c//vGPGdTIRijghmaxVAqNnCD+oecgH8pq+CTNhhtumIG0WClAmFUozfnnn5/Bqig32sWiUZb641FwjTfgQ46bbrpptpZYTwIgK0CuXKADTNFwVv4f/vCHzBNaePUg2m677fKQ33DRA4mM8SCNsvEKZB1oCdrU8FY6oKc86clp9OjRmS+WLou39QMxE6j/FlUC7bK4Ov1QUed1sBh0XJ2KslAmlk1j0PHf+9735uHR9ddfn4eGrBr5+cg8lYGTocY222yTh0AcsYYUBx10UHb4P/3007mMAl78MIYrlJmPiEI1BgpMAVh4LAQWnyGeMimQocsvfvGLNHbs2AwQrIlCgwJ98YtfzCAnnpKpF3oOgKKuQM2wjDViqAQoG4Nr/hoH+aDxn//8J4MeC5DVse+++6bhw4cnQ1ZBGrQoMOVlCRlOXnjhhZkGMDDcAjxofu9738tpN9lkk8wXwHEI+CwggBc0Sz2AE9/ktddem0GFjNCWx9Dag8kQT9sCmmuuuSbLhyV25ZVX5iE9SwyP8mhjFhq3wW9/+9tcvn6gvoXu2WefneX2qle9Kt/HC/l6ucMHKJSHDZra6f77789+M/eAWw3tlkC7xtkvHlO0m5elTwDIUByWR7FocKHjAQcKQ1EcLBrDIdYHRTf846B1sG7k5zsRWE9+66TFigEoAIulg7ZrjmpKYljCKU3BGoOhLOfvgQcemHYKRzbFOvHEE7OTnjKxEjmNWQrAa+21185p8O/lgPSUivJQTvHFYlEOZWS98cmNGzcuW2/FyqF0fqt78dWQhyBenTnczzrrrBzHeuHIlqZYIkWmzgVQ0XMNlPEIYLxoEI+WoW0po5FfNNVBOuXL72EB8F0bXrLqCsgBRWAiPRBi4bLGNtpoo+zYlw54GgbjDQCRv6EpMONzA77aRhzL1guCt7zlLTmtlwMsZPIFbupU+MKb8rW3t9RkRc5kWh4guZLL6B+54kNfLMDbyIp6kAVrVr2k6UrhxVo2r2abxsljd549/zK1BRosA0IqSlGyuC4HAFh11VWzAAmxKEZJu7hnnU4nAyQaER86q7PGLY2GD2mdxenowK484V3rqJzV+AM4Oqlr+eRxTfHKEEYZymUxsfi8TaQAFK0EaXQedKRlwQEeZVAKgIR/5bmPv2I94A1wASc8kKH7gEFaPEvLCuS3oVhAWfnKLTJ2rZ0caChPPnSd8Swtfxmrq5F/5TkENOVRNnkIZOxQP3WTV/2UNX78+MyjtMppDMoTV4AAbXU0jAbC3gzyWaInTeFNWdIVHshQQEv7kavyta1+iXe8CPgD4Mpwv9AgD+nwgL6zvgTUyFT7yYcPtMmAHJd1wA9++er0+cZAbu6Ri7qRy5IORWZoax99mOwcJbS+liceNoYEGPpfwpLhxWcdz9Evjv/N0YmLJV+bILo0gw6mIwqerAXEXBehlXMBD4LW0J6knkw6sHw6pzNloGwU2rVO4Fwan3JTBk91byl1jrNj+AFIdJbGIK/OLw26aFIESqbzO0tDIRzi8OsMkJQpHm3ppPf2Ul2AmvIMUw0T+WgAgs7hnt/y47WAR6kHHtxzUETpKYAyGgNe0CudVNnqIh26lAdtZZEfeaKJL8Bb6iOuMciLjqGgfNKpkzh1VB5QJwO8krl4QVr5pSn84h8t5QO2sWG9qiv+3ZO3WCEATL8RpFW+PoCuukinL5Uy3XeNN/WUblkH9dInBPy1Durhvvq1ln3rtMvo+uVAa4Fsvfi127ykK8bpvXG8+BE5HzKEAjgIckFBZ9CxpHOUDregPAtzT6csioQX5aCt4XRGvwtv5Ymp8xUe3C9pyhkddKXxm5K4ForC+E0x+H4uuOCCPNRrLN99oZQjP94oUOlw0lPwAgrulTLF4x+vfhce1NVvwW/pKaIhlvSu56WPKQ29YnJlGM5zo47NzaH0LQFqzTE7Puj17t0U5lKOSv16909zZgcfgVk9g3Z4oeIxF+0UuecE3317h8XUoyk1z44nd+95LxmUQZ6A2Jns1EOdnN134Mm1ozGoQ5F9aTO8kxMgveOOO/JbWDTUU0C7gBG5lDi0gYk4NJVJYdErvChDfkGc39IWGYorR5GhtKVsZ/SlQct5WQZywQPr1JQe02rKm1vx2sVbUv5PftjSZ5Y0z2Tl8FAuMnddQrlfrp1jxEJ4L36SNSZ46e+TIupFT9ROD1ylM2qY0jiEpfE0rlA6mXidtVGY7rlGx7mkL7/LNdrSSF/yoM93wkQuCoB+WwE9+QqNRvrSuy40ynUpy7XfhYdy7SyIl1/ZhbeAgPgNtAKgeoQcegYoprDA5vZOM5riKdwjhqQ9eqY+6owvaXrFCoA4N/fQLaK+/kedo9apZ0s8FHrEcDn+GkGDDEpdGvlVT8f85CGt4FzayXXJw18nXt1KvaQtMlJmoVHyuC5ycC58lTj0BdclretG+iVfKaeUIR4vjhIn77IKBTwBhpdD5sLx9wF8Fr4XSl66eGnCtYDvVyIUeS0GcFG2/yHcgpl7CXAtlFW1YJrd927pzB1RAkAnBpZpbs94o9cjQKxHAOrcAIEwpAPmUo/4PXduLJmJ657ANtKFLRa5Qjlb+kqRKcwFZAFcc4NgYN1SCeQKjISOABJLpdKLWIiHBzBi5ZiiAaicAZoXQdwgVjR44cH6Kg+bRSzmlUy+sKDVJg/zekebt2rky0mgUcFeLu3Svt+zJZ5JAVgtPaenOT1jCBWABKjifWLqN3PF1GfOoNQ7LJRePSZHuucCIWalns0xJJzTJ/ViucyNfgWtwt5qCdBrDtSa07Nta3JJ1w1olSf5kqbdVeiRD4uUZQjcvRgy+fakk07K886OOeaY/FaVewa4dcDQrsdgBa5WLWrYUJSmDHPaMrN1FkdJ04rMMr/sMTeGiCX0MI2id2rq1TfAamYa3hRzr2Y9FEOJmOm+3ZT0prcMSjvsODStMiqGYi3PpqY4BvQMn5z8MbRMMeScE8NKx9IIRbblvDTK7Ixl6KdkxC/oBY6lZHxdH/zgB7PPldOe3xS4dcDQLourQ9ZoWQm5OOA98Tn3PdE6oIm9kOKZBzLhnQonfFMa2BRvoGY/n/r3npx69f5X+sD7Dkw77vr6NGz0yjGmjBcP0Y2mTJqQ/v6Xv6fLLvljDDEeSX0HrprmzmkKz1j4R8LiatcjciG5rskWTwL6LoCyzratB+3iUX1Fc+lOiw1eS+cR+orWf8kR5x8AVIDL63K7M5i02ug8XnKlvbKU5vaIN2AZbGIvsJ6xqeKUmOuWpqQRg59Jnz1u7/Tmd26dho0M38fk8GDN6puaZ06L1+rT0w67jkkfOnyHtMG68ZZu2lMxbJz3giOe7dHN5v1+ZTmv1BdHAvqsvqsPd9Ch4eJUa755qsXVIBpmNbD6wAc+kKduML+tJ9QR2por05C14/0M/9a8N8jxTjBmDgzoF7Onm6ek3XfdIq2z9vphZD2RJs+6L/VbbkC8f4x5US1D0qSJs9KAPpPTWqv3idnlO6dvnnZ1mto8O2y28KfEX6HX8SpbOQJcHrD6annj2MGlstjWlnp1WuAq/g+Ntfnmm+eZ69tuu22ez3PjjTfmSYuGeyYxmqBZln3YycD0BUtNrKszC97SEK9z11xzzbyVjfVq1sF5jYw+Gt7MmBPDASqt+TPmY/Ep2FHBtjVeP6+88sqZFztBeC3NB+Z1tYmb5tlYb2dGNp6Y9jpZMfPNvrezgXlMlhyZEGqOlLryZ+DBEifWoLk56qGjmkeHN/Ut/rl77onlRzE3q0evmBzaK+ZwNU1JY0aGP2v92Ptr0iPpR1+7OB1wxEfCjnoy9WialPqkmNk/q2e65KLfpM1WGZG2HbtuunaNSen3D/dPzeHjGjwnHPnNPdL0BtdZB1eMbsVeo5tDf+kEoV1DxU4LXKVhgAWryMJaCk15LefgoAQ4JuFpVADiqWQZyWabbZbXnpmDBRwou7247DLh9bHlMwcffHCmxeF53XXX5XxlvSFwsakdsDrssMPynlEAiG+h+Be81bFttPJtXWOSKCCzqNqEVUtamPYsOZMq8Vj2pbJv2AknnJDzAEd8O1vgLOiYAM3rbiBqt1STLlmM+PB73/3flJ59KtYMNoe/LiaQArGxo4alvj0np99d+qv0gfe8P50Xayh3fmPsbT+yZ3p+diwyPv836YADD0t3/um8NHdi37TNxpumGx94ImZuzZuQOquX7jJv4mdmpP6rElhGEugSPi6gZOKdzeyAGOvDFik77bRTXpxraxMLaik1q4pF47Ux0LFjAsADDIDOjgF2Ad1vv/3y3lpeM5clRQADiFiUrUz0gA/AAD52kLD7hDgLedFjsZljYwdNoAggLa62ROcb3/hGvi+fAw3AB+BYgzbVMyPc8iR8KN+W1MqwDMYHNaTxyttmgDvvvHPeM0u6mQGETX1iaUpLzNqaHeAVMxlGDoiFtzPGpTfsuEH4tJ6P5TRPpYkPP5QGhB9r9nMT033/npUmj78jbbz+gNSjZULq3zOWlURd+lefM/4AAEAASURBVMSM++YA9+kZuJZRT63FVgk0SKDTW1xAA5iwZCjsn//856zYhmKGe3ZfsJ4QKBiaGW7ZIpi1ZdcG6w0Nu8q6QEM5Q01gZucHAGiYB4xYRQDJmjrDPvTQFSz9sXMBoEHDAl9WoF0fbMvibQ8+8cjKQkt+1p4zy8kZ0AGjU045JVtOLDVAiW88WszK98Y6Y1mVeMDlPtDGaxSTD0t7WlrmzU6fPj32ZJ8V11PmpBn9BqRd9t4/rTEs5nFNejIN7jMkffDQjdLQPlO9Xoy8K6SJz07NE1X7NEf+3vHaPfjvU/3zDepTfy4rCXR6i4vVY9gEKPh8KDMAsDPBl770pQxM9gq3xTFrBhBQbpvDsVzsd2VPdtYK/xVrBzAApmIxARPlCIag9p1HR7mGpsosVhtLjAUEUO2nbq93W7LYbNAeUsUHpYwS0BEP1ByGil4SyAs0gSdflrKAn/I+/OEP5zeeyhEPtPjIDF8BV7+mfrFcJ2bK88zH46m5ZwyhJzSnpyctl6b32Szd9ES/9GjLmmlun7WCjVGpKaZMLD9oudQ0K5BpWshpUp90//iZaVaPWGsYf9mTalJqDVUCS0YC7epMnd7iKuABhICRDQABlw3xWDqCbYcN1YCKw9ARkAAt+19JD4jsNirecJOl5DcwAILAAHix1OwR5UVAARqgooyyfxcQZTHZXQAgARblA0h0AKx76BWfmHqwxPCCFt8agMIvK8vGh/YPA8Zo+g2M7cAANPnNAKMthoHwnOkxxIuZ7jNMi4jHU+9e/dL9T85J9z4WOyAMWTtNHbtt6jVkePr7P85LfR+6Pq2+/kZppVHLpZanJqQ5M2Jvr4kD078enZym9h6R+rbEmsEA1DC6aqgSWFISaJdzvq2Vl51qdwhDM/4oVpCPK7BA7DJqUz+WC4CxJ7mhJMsJMPBrAQkOeGu6bCrIKjJcs7iX895wzTYxnPB8TEAR0Jx99tn5raD7AMswk9/JDpmuva28+OKL8zAV+AkAENgARkDFsgN6XgwYdqIhAClAik/3vGSw15c9trylBG6AGRDasA9/QI4Pzv75gM7LCnmv/d01aeq0GPIF2syJOvcK/1TzlBj6zZyVHh8Ymy9usneaEFMgbjjnB2nlXlPTP269Ow3vGx+KSIPT+CdTuvqf49JNj/dLU5tGpD5htfUO3ubGDhEpgLCGKoEiAf3aQXf0/3Ld+n65dv7v7hAsroW1uk6KtC9ab9ZWxk61kaAhF1DibOffAlyNAUCUQKiNgZ/J0MvR+l5jurZ+F2tLPh/CMETzVhIQNZYpb+P1opbTWHYjHfGLS6tl473TKjvtk1qmPpje0uvKtGvvS2NjiMlpysx106PTh6bf3fFk+tudE9L0HqPT7LmxOWMMFXvFUqHePWammT3+N8Rt5K3+7n4S0B/1QfqztDcS7PRDRUOuMpQzDFuUUPxW8miA1sDwcrRYT4DSXDDzxAzZllZYXNDC36x/fj89N/vfaaVhfdK0prvSAwFIs5tHpkefmJhueezBdNe0cMT3iJ1Qe8W+Uy8446OrzI2H3ouxf2lVt5bT9STQrp7UVuZOZXExTw2dDOdYX41gpK0bFbw1MHlSCOJb38s3FvBPOcW6czadAnAairamtSAeFlDES24tKTppUHOaOC32ow/f1YotIb/mAOBeAVSxJrFHzK6f2ys2xAnH/pzwizXHdIpe4ZRvaom3n2G8zur14i8nvYTJGtFtJKCf65OLaXG15aZqLTvDJcdLtm7u9BaX6QAAg/Vj6NcauFpLovU14fMtORcga52mrWtlKVPZQAtwOrcGrbbyLok4HWZxy2p+fkAaETuhDuoV0xxahqXnwo81I3xgvXvEhNWYPd+7Z2wLHb6sWc2zoo7RbwK0WmJzwZjCuyRYrzSqBNotgU4PXGV4yEnfVliQcgMsAQg0WjNt0WkrDm3g5az8+ZU1v/i2aC4obknRCYROg1pmpX5zpqdp4bNqCcurX0t8ACI9HfPi+6VpYXkFpMU2zjHvzJpHdnlsjfPfSRELYrHeqxJYKhLo9MC1VKTUxQqZ1Ce2ZQ7AmhZ7dj3Xc1jss9U/LTenOc2cu2L8jj1Q58bWzgFTvWO5UEts19wr+7ZsRijUxYpdrDssq+oYArblqloofipwLZSYulai5WbHLhCxaHpOTG/gz+qXJsQ0iFhQHlMhYsV1aoo3iHNb+gR0xSz7vHngf3eaMBVCd6uhSmAZS6AC1zJugGVRfL+YJ2bveZsM9g/nfOxcHtexdU3ebyv8WfHbXl4+A+TKzPm8IaHdUGuoElgyElhsa0vxtScumUboVFSae9pdS78xG94UkviWYY//TeWY95UfwNU4vUT6hXkRFMlqqBJ4eQm0a6hYXxO9vIBriiqBKoEOJoEKXB2sQSo7VQJVAi8vgQpcLy+jmqJKoEpgyUugXT6uClxLvkEqxSqBKoGXl0C73k9X4Hp5AdcUVQJVAkteAtXiWvIyrRSrBKoEOrIEqsXVkVun8lYlUCXQpgQqcLUplhpZJVAl0JElUIGrI7dO5a1KoOtKoDrnu27b1ppVCXRZCVTnfJdt2lqxKoGuK4FqcXXdtq01qxKoEmhLAtXH1ZZUalyVQJXAKy2BOlR8pSVc6VcJVAl0LAlUi6tjtUflpkqgSmAhJFCBayGEVJNUCVQJdCwJVODqWO1RuakSqBJYCAl0eeDyyTHfXvTNRcHXeMq3D93z2Xpf+JGmfJ5sYb74U74MVPI0yrrcK+fGe74KVPIs6ld7Cr1ybqSLpnih0C/3xTfeL/Gv9LnwWfhqLG9JyaGR5rL63Sjb1rJvD0+N8mtLho3321NOZ8zb5YHL58sGDhyYv3v4mte8Jn9xWkPNmhUfjIjvIG644YZZqaUp31dcWECRrjn2b28dxDcejfcLbZ1uUTt5I81Cp5H2/PhvzNeY/pX+vaByC//tlcMrXYeFpb+gui4sjdbpGmkWeTWmabzfGN9Jftd5XAtqKOAAXJZbbrn0xS9+MYOFj7eussoq6fvf/37aaKONMohNnz59vmAzP/o6TgGLxjSNHap1hwOkjrbyNdJo6/eC6BZ6JU1jfnFk0JqXxjSvxO/CS1vlLik5vBJ8LypNsm+U/6Lmn1/6Rvm1JcPG+/Oj0YHj63SIBTVOozltOOhYaaWV0umnn54uv/zydOaZZ+bho6HLCiuskLbaaqs0atSo/FVsX8k2rHS4z0obNmxYev755zMIzJw5M62//vppzTXXzPeUxYobOnRozuOMJvCUD3hsscUWOQ2gNGwdPHhw7vQ6vvujR4/OPK622mp56EfB0QW8ynM2vFWOY/jw4fn+lClT0hprrJEtSF/ZNjR+6qmncvl+b7bZZpmHqVOn5vR4KhbfiBEjskzcw8fkyZPT8ssvn5577rmcFu/i8YLfIUOG5PqSO+UhH7JRH/fwq8wVV1wxbbzxxvnr4tK5j/aYMWPSlltuma/RxG9jIBvlN9ZRuylHIJsNNtggy0tZ7qkD/gW8FrnIM3LkyGx1S0N+6Ks7XpVP7ugo09k9bVfukbcvpONTn2CdoyNOPSdOnJj7jX6AVp8+fdKkSZNyPyI7PGg7dFoHciMndZUPT8pBGx94RoPsPGzJ0YPXPXF4Ub8CYq3pd9XrtlBv06jsTXHMcwq9TM0pNIFqcMJrDEWYzoS86qqrZoETuk6xtALeNPDFF1+c9tlnn3TWWWeliy66KF1wwQW5o1D6XXfdNX3qU59Kt912WwajX/ziFxnUjjvuuHTvvfemK664IivEZZddlvbbb7+sgGeffXaOUx+d6cgjj0znnHNOjlt77bXTww8/nDv1ySefnMsaN25cuueee9LOO++cjj322DRhwoR06qmnpt122y0rxBve8Ia0yy675HtXXXVV2mabbTJ/p512WrrlllvSddddl0455ZQsZ4r5yCOPZN6+9rWvpUsuuSQ99thjWa4U+8ADD8yd/8ILL8x8jB8/Pm266abpxBNPTP/85z9zO5C/thEHSHbfffesZEBOOe9973uzwpHBoYcempXrW9/6VgbDvfbaKx1zzDHp8ccfT+edd17aY4890hFHHJGOOuqoDFbvfOc70xNPPJEVGxgfcsghGUjl9+C47777kqH7Jz7xifwb2JTgNznjmdKrz9FHH53rf/zxx6dNNtkk1wmfhx9++P+z9x5Qdl/V/e++vU3vmhmVUbNkY9mWbbCxwaKDMRgMGP4kOCaUhJf1EpKslMVaSZzAf/1DkpXyVtoKf0qAQAj/PHihxlQb3HHBHcnqXaPpd2Zuv+/7OXeOfDWeGckjyXM1vkf6zf2VU/Zp37PPPvvsY7t27XLfXvOa17goyOcf/uEfGjRS79Q1gMIFaEILYT7/+c+7PJDOFVdcYX/+539uv/Ebv+EAhzqBNtKHU7/77rvtvvvus69//esubUCIsuUddU67YcA7fvy4ffSjH7X169fbpz71KXvwwQcdMNFGKJPZ4EXb+8lPfmI7d+50wMXgQ93t37/f1cdv/dZvuTgBNRy0b9u2zbU12hxtiMGTdoMjnRfK0XY80B85csSBNc9c3s1+Joz6BfKVkPezwC+AwgXinzS6vaiOJ2N0+8pXvuK4AwCXUQ4OABClQwBIcCmMpv/2b/9mABMNhhGdCqCRMBLS2GhAu3fvtj/6oz9ynR//NFzeMwoDPIAkHYeRnA5Ag6ax0fgJ9973vteN/nTAJ5980oX9+Mc/7kAMWkkbjpC46HzEC8ACMsT1kY98xKVNQ3/qqadcR6Ph0th/9Vd/1f7pn/7JNWTS2rNnj1177bUOpG644QaXH9/ASIfO85a3vMXRRqcFkOCuKBtoobyuv/56l3fKwHMhcC9vf/vb7R3veIfdcsstLn/IDQl70003ufKCnl/5lV+xr371q47zg6tlIAMoATuu6g4HxwF4AZaU2yc/+UnXtl/5ylc6GgE8/JMP/ACWOOqTuoHb4TtlT3hopawB0ttvv92VGXngG364/vRP/9SBNJwR5UFZ08nw4y/PmREX98TxiU98whg4vve977my+od/+AfbJmABwADHX/u1X3NlB8hdcMEFDvQcsTN/iJuyvVXthjKhjDxnR/zf+c537G/+5m9s48aN7ndwcNCV86//+q+7OoXGu+66qzrKF8X9iwq4aIw0sjvvvNNxQD/72c9c5dNA6KCMvDR4Gj+ABhABWnBSH/7wh12npHHRKRn1f/CDH7gpCWEAERydgLho2HQAGiX3dBqmMoDAY4895sCRb9///vcdGDEtwz8jL/HRoL/2ta85APmXf/kXNx0gXjgGOjUX/nFMx374wx+e6GzkC3rohIAtaTP1efzxx10n9h2Q9AEv8gP3CdgBkkx5AC46H9/p+J5ToYN5rpIOBpcDh8N7nn1+n376aTfFAdjgVgDXgYEBRyd0k8d77rnHgQ554bnakaYHB+ijHphmkyem+TiAimkS/ggPUOOX+Chr6oqLb9BGffJMXP4ddQ1o/fVf/7W9733vc2VKOQOOcH3kn+kbgEe83lHPpAsYwUXijzjJNxwi5Uc5wnVBJ+X/xBNPuEHJx8Ev73GEJX7iwXlAI7+kxbeRkRE3CCLiYHYATYSjTb3Y3MmtZRnmno7E5dlaKp1GzHSRKRCgRWPnOx2Kho1/fgEvGgxTITgGwItGjwNoaJB8p+HAqsOpcc97Gj+/NEQu0qCRES/+6DA46IEz2bJli+N6COcBCa6KKSEdyjdq0uceGnHc01noXDjSoDMw2gMaPj/QQBh+8YMjz1zEQSckLej46U9/6soFP3QgOg9c0Z/8yZ+cAAbigVbkfXAEcEW883GRX76RF/JLp+OZTk35M4hAG+89Tfxy8Z40fTn6vAIGTEmY0pEm3CkcD/4pd6aT1BNTO+gmTzgAGFrJqy8n4ictBiDK69vf/rYDaDhiwjLFu/nmmx1HxzQdGnx8fPcX9UH6OOIHcMkT98i+EAkwpYYjhvsiXWglbfwwIOCf+MgvafAN4PXfiJv0iZswlB/lTJ7gLnnHRb0S9sXglj1wUYlUKhVPg6EjUbkAhu+QyIZ27NjhAIJGwRSPKSMNnkYJANCgABviIh5YeKZONLY1a9acmAYSP/4BRD+yEwejJlwHacPmAzakT0dkysl0CoCETt7TkJFh0Rnf/e53u9VPwvKNBsoFTfiDg6PDIuQFJJjK/OhHP3KjOx31l37pl1y8yGzuuOOOE+GJj/zQCcg3HOTv/u7vurDQDLjQkTdt2uQ6Ep2P9KHRA+yjjz5qyAOJF8Ag/9AFJ8KUkHwzBQcM92i6ipwTGR/x/+Zv/qZLkw5JnOQJOm688UbX0Slv/JEmZQmYAHzETYeFE966dav7TnnT2aHLlwtxAj5Mr+B+kMXihzA4/MKZUi4IwSkHaKB++UbZALakj6O8vOM7dAN4ADr5ZIWavD3yyCOOBvwTLxdATtqUD47wACbTc/wTtq+vz32jjLiQ0d1///0n8kMYQG7btm32hS98wX4smSccMvX0YnOVYfvkXPfo8YO6TmsaSUXTWaor9eToKk80LDoR/rhoJC+E8x2JRkzDuPfeex2o0EHoCFdffbUThDItQLbD6IiDgwCAmF7QSQARQIJREIEswlM6FR0dLuWv/uqvHPiRTxoxadFZWYUivzQ2pqKea2PKStlRDpQfAmwaowcMwiNEp+MgyEaeA+3ERTnSEaAfmgAEgBZZD6ALkHzrW99yoPr617/e0f7bv/3bDiTJF+ly4UgfwCM8cjbkcYAyq6FwOHRaaIHDIW06CWVCh8IvHQmAYapKOZB3/NDJEDQj92JKyOIBcQGyvENQDRAiwwMoABviB2CQmbEgQd3B2SLoZqEDeij7D33oQ24QoMMD9gAAHR1ZD3FwMYWkvOFa+WWhAxkfU/H3vOc9brGFRQKmsUytoZc6YCEGWnCAMvkhvyyswEEBnNSDrzsGIMqP8gUQmXJSX9QP+QfIKBcGEsoAsMPRHgEmZJbkH/qYCpPHWzXtph1Qbgwm0IBjdZHpJukg7iAc8VA+lD80kfcX0vnypg0wuPhnT8PsZ96rThG4Px+m6RPyfxJgzJXLZbWqSCdipOOXgqVBw0nR4bnoTFQ+jYDG4jkQCpiOA3DQCOng+KOCAA7ipKHgn/dUEM+8Jx7SoqHxix8WBZgScg+4QQ/gSbxwS9CEgNe/gy5oIg7CwPkQjjQ8RwH9xE/6dAho8/nCD7TDEcB10DHxQ1w46CVt/PEegABASQcgoKOST+KGKwQYAHIuwgJaXnZGnL58oRkuAsE73AzpwpVSJnwjPHliGs478ks58+sbOd/gUvAPfdDJN+gjj8RJWfi4+U4Y4qF8eKZcAAwGFsqXAYT4iAfZEAMBaXKRFuHJB+XHL3GTHvXJM2F9PvGD8+9Il/ryZe/bFGVKXvFH+RAn6UMjF8DFdwYaBgC+4WgHrDBDG3VBPRI33ylL6gOaKUvKwbcN3w6h84VwpOPzA00M8Dz7fEDD7GfCnI1VxWUPXC9EBZ4qDWRHqBcwtaERV7u/+7u/c40SzoAGT4M+G47ORKNGcMwUledz7cgbHQwuD6G9517PdboLxQ89dHyAg04EjYAuHc1zMguFP9vfPHgB2gAPHZl6ou4BVwYwOD9AANpr2Z0hcNHQ58Kf6iyDwFzPUYeYK+Cy4riqS2Gp7uksjKxwC9WjEfQwSjIyw93AXZwtR7xwC3ROGhhcyLl2npMgn6RJ51xqB4cC9wUwwFlyDyfFLzS+0I62QLkAYJ4LA8Cof8qNAYY28kLV2ZnkHxqhlXp/oTmuszO8n0nul3lY34E9Wz87u3QoQA12fzaozfb7fJ5Jl47KqE1neSEcaTG94Ze0a8H5qR4gQR3A1QBaZ7Osn08+GUgoIwYWBhMuyoupKpwX3wCCpaLv+eRlKf3WNi/6ApdMdWOh458NR5xwUlxzNUgaqx+F8VNNw6nSh0bixM2m13NYdFLSmP39VHEv5rsHSDoi6S+Ul4W+nW1afXyAKeDlQWMxeTzTMJQR6VPXvo4oL2j0IObB60zTqvHwdLC5ZnynRXYduKqKyTfwqldn7ZbGOpcjTTrxqTr6XGEJ5wFgPtr5Pt+3ueI803eeHg+o88X3QtHk6SE9Luh6odKeK+++Pjxd+PH0+G+886DG/TJ1iwYtyqMOXFWtorqznQ8Np7qhV2XjvLitLuvZBHuQmf2+/lwvAV8CdeDyJaFfBKbeLdSxvJ+l/gVc6eT8Mt04n1x1Wc+mG0Cu5khmf68/L4sSqE8Vz6QaPWeFgJztGcgekDHwHm1wdH6QEeHoTAjRWZVCmOqX2AE5/DMdRIYBiPBbLQimo/IeP14IWy3n8HIP0uA9cVa/A6AQ7LK07+lhszV6aewVZPsL70nXr1ZVP0MfNPiwxE+cvEP2w8om6bHixXvu+UY4aKJM/IoX97488MPKHf547wGURQeflvM86w/Kn+hYkRZyJ+LmnvBo4FO+xE3aXJ6marqhkXTJs9etIxmff+6hCX/EgcM/Dtr4Rt5Jx8ePP+SCPh/UBbQRDj+eRsLgiBuHH9oGztNIGL77Z58GZYN/3hOPp4l73hPGx48fLp++S6D+pz5V9G2ABgt4oW9Fo0bxDzBAo5zNyTQoOhOdlGVrGjwNjEZIo0JXi85f3UHoAACB7wg0XDopDZHVLjoYjjB0Iq8+QPx0GJwHK8IQlg6KBjebd9GyRhPda2RXr0zinw4NjXRC4odOfskf38kP34mXd9BHHOSNezqPp438V3ce6OM7ZUacPt/ERbq8953PZWTWH9JD4xzLFdBA3AwIt9122wl6SZN4KT9fVsSPf775sveKoj6fnnbyyDueiZ97wjOg8Exd+n2ClDN1wjviJg840vVlR3jCUj/QRf58vH6w4ZmwpI0jbRzvuOe9X1kkPHnxZU7a+CG/0ODbgI/DRbR8/tRlXGejLmmINCLsZdGwaNBoL2PriK0lNFq2dWCyhYbLFhh0VzC1QqNkCwx+2SrDVh/MkKC5jeY6DZNtLFxsKWG7D42UDb5YIeAe0ILToEPD+bEfDw1qtgEBAqRButBBPGxHYWsJgMn2IzoBaaP4iVUJQAna4MYwtYL2/8c+9jG3ZeWb3/yms3wAOLP9hW0zhAdI4OKgmfzfqq0nbElhk/FnP/tZBxTs+2PvJPsP2SL0z//8z+49Sp3st2R7DFuRyCMgO58jH3RM9ml6ICIfON6zzYitLeSPPY/YQwOg6dBojaMNzx5KtrqwbYh9iAA6NsTIA1uDqCO2cbEBnjrAAXI/1h4/rHIQL/HAxf7jP/6jq3uADeD27QHa2FOJfTXSIr+UE+XLnlO26VBP2Hrbtm2bq0/opzzJP+2C7WSY36E+ACXioP6oEzbQwzXjn3JjKw95ZEM29QYQAnb8QlPdVUqgrsdV1RIADzoAAECDp2FjH4mGTEOis7Fhme07gAtgApCxv45vAMw111zjGhmNkU4PF8beOraYoB0PcAEUOMzRAB5slKVT0Hjh8mikGMhj/xocCaZp6Ayeg6Bj0mFo4ITB1hPp0xHQWodzQPsacy3edhVWFNiiA/hiLA9QBgjZ0AxY4gBs6Kdzse+RDk++8YdFBuhiOw/7LfkON0q8cD7s1cQ0Dh2RTo41iYUcnZA8wc0ADuSDZ/JBuZIXNj+TF2/IkDohz9CChjnfAQvKio3y0ES+oQ/AxoAjZfff//3fDkQYIABAyoG42MlA3OzN/Pu//3sHltBCfuG0yBfgAaAyrcVYImnxnXC0FTbBUyYAJgMJIAbYEp46hSNm/yh0Qq83oYMf0rruuusc6AFutBHS4D3bgXBwW5T7MnRnpG90fkl0z3Ht0diwrECnoWPRcNmYS8OnU9IJGOUBOBoeHZ5GRoejs9MxcLzz7wEDOBQaPJ0Og4C3ipNh1AaI9miDNBt1aaCAHSDIVhk6Mh2LkRxA9BwDIz37BuGKADz8wZ3BOWEMkHRp6H/wB39gbCfCxhZxf05GEelEbBImDgCBfEIDv+QJuRPAAVCzORiA45m06FBwOgAS4Ep4jCeSPn5u0xSPzcxsLOaX/OEoG8oSGqDNO94RjvQBKuLxU1TKjDzwnvxTLwAL5YalUaZr2AQjPJYZODuA+gFoMWhIeCygAsKADuDobXkRHge3xEZupttwzgAdA5W3cUXcgCC/5JO6pUzh7hjMiJvN4rSRD3zgA85SCIAI9waocrEZmzxTL2zmhtuDk6QsoIl80Y4wQYQ/BgLKlfeUB1ZZeY/lEp6pN1+GxPFidnXgqqp99of95V/+pXtDg8UyKaM4O/u3acSkU2BdgIZEx6LxnGo0pGPS2OB0iB9ujvB0XICMqYl3dFwaPI2XjuIbMenQmHEAGB0ASwiAAmGwsgAXhkUI0oBW0mTEZ4pCXFgVID9wBuSNqSCdDsdmaOgBiEnLgxncJUAEKNPBiZMpIWUA0NH5AAzCEgdTIzZnQx+dmLgIwy9+Ttfhl4uOCngRB+VCXik/8sh7yp50ADJohxOCJi7yCAhRxjjyhH9AjfwQF2F5xv8eDSCACvd8p14YpEiXuAnv0yJOyg5umnvigmNigKHOZjvs4zPdBJjguMkb9AFGxA0nBh2UNWIJ0qbOeKYsGcBol0wv665SAnXgqmoJNEAcDYuGSuOhM/BLJ2ZkpWPCFTElw3kAcw9z/KFx0siZ3iCDoVHSUAEOOnS1Iy24GrgVRm4aLR2UC5DCARgPPPCAm0Yy7aGjIAtiRId+Oh/cBBYy4eDIB50LWc7nxHUBZExDMCsD50QHYkoFSNBR4QzpvNAJKNFZSZspowcKOiKmYAAqzORgeZQyIU46PVwZdBK3Lz/KAQA6HYdf0oIThCPhGZkhnAnmheAMKRvcmjVrHJAB6IA7eYVeuFC459vECXogxD95gw5AhGk+cVJGTKnhYKGZuAEUOFS4LvwTDv/UIWmRDkBFuWFLi6knMjX8Uq/+Ih9ww1ixhQPjPVZCSJN2Qf0xzabcvDULpq44OD3o4YJjrAOXKxb350UNXDQe36hpRDRGhNA0LjoegmYEznxjmkgDBFAALTrtgEwRE55pFHa9kFcBcqxMAkCEoWEzlWEVkGkici84FuIhLI0SR4MHzGjcTPEQ0gIwyNPwQ2f0/pGR0RHoqKRJpyZNaIJmRmeE6HRw4oUOOiaAg40nrEUgPPedy3c2Rn0cQEn8ABKLA5ioYdGCOBFiI+THfDKLBHAIcCsAzDZxesjl4BQoB+JgSgWXh5yPdLwjbcqfX/IH6AII5BGwg7Mif6QDd8l0G84DvywWMHXkPXIhbGARF1N67pleAp7IB3lP2QF+gBG/1DN1QdwAEyAHaN8mkIMeQJsw/JI38szAQH4pNxzgwmIKgwcDBVNWwuCgkbR4piwZ7Pz5AwwA5HGbygpOHj+IBghDuSEjA7Bx+GVqzzTW5wX66k7MxRyF8KKyDuE7EA0cDoXOReMGuBiNGQnphIy4NFw6FfaTAAI6GtMFAANH48MRxndSOATiY8URmQWjOJ0dYKMB4w/AoqEzOgOSdH4MFiKPYYpHQyYO6ACESI9wrFLR8JkS0UHo6BiVY7WPexyg5k0PI7eCS2DFjdU8plh0WmRspE2HRn4GsMIJMLWEZkZ64iBdaCUN8s8UlbwQDkE1oIzxQwCF1U2E1QASZUd85KHaARaspAGYlD/+yDfpQzdyKgCKDg2QM20j376MSBPuE3CBS4I2OBM6OoDCoEG+iBvaqR/o8MBEuQLKPDNAMAjgl4t65hnODD8MAoA+Uz3KkOkq9cTgBW2AOmVOfQC6xMngRJrkm4GN+mPFmXThqilX4qUdcBEnNBOG9kJ+qFdoIc++fVWX4VLe03egm/J6oa1DvKiBi0ZdDVx+qsh7nAcgOhRAxi8VBcgAWnQiGqUHFiqQhkucPl4aLw2Wzk2nIR7uaYy+URMO/zR8n0Z1mv6eRkzahKWj8p5wvIMO0iYu4uU9tPKe9MgTjZ80uOcb/glLvOSFd4TzeaAMCMszjm/EXZ1P4uedLx/iJj78EB+ONEjbP/OOsuYdZUOHJR0c4fgGaMA5YgSRcMRPvOSZ8qZu8Otp4ZfvvgwBZaZ10EbeiBP68YPzeeGXi/DkhV+ecdBHPOSJdPlGfJQHF/Hi+MUPjvfklzh4xy95IzxhSYPvABuOb9xTDvjlgkY/SOCX/PvwLlCN/IFWaKNcFgFcQWVjLvypzh0VwfUce1zP8u7Pel9Wppufzdb8dxQ8Fw2Nhkdl0Hh8I6Uz0OBw/jsNCcc3KpCwhMP5b7yj4fPMPWnQyD2AEJd/D9fhO3d1GjRs/PhGTfzQhfMNmnihz4fjPfeefvKCH+LwYYmXd3Qa7gECn0eeiYNvpA3NPo/44Z1PA7/EiV/yyXc6Gw5/XKTjaXMf9If4eEe6dFrC4o+0GBR4B91MUXE+HtIlDfxy8d6XjaeTZw8GPk7CeQ7I084v6eIffzxz7+P3IA8dOPLJN3+PXw+g5AVaoAE/nl78UK+E5Z7yIk0cfrkoL8Jz8QwdDHKALXTx3tPnAtbQH2jlYjClLPyzJ3H2M+81SwGMAK7TdZ+Qx5N0QuZCvBfVVPF0S67ur14C9RI4uQQAYoAJUF0ExzUX03RyAhVua06O6/mg3uxI68/1EqiXQL0EFlsCANKiXR24Fl109YD1EqiXwBmUwFyzvdOOrg5cp11UdY+1UgLIj7zshOkKUxV+z9QRJ87Hyb1/x33d1U4J1IGrduqiTslplgBCawTgCLn9QgBgcybgRVgvQAesiBtBOoL5OnidZsU8P29nNNLUgev5FXbddw2UAEqjAAorsX6bDsBzJgDDah6rmcQLaK1Zs8atQgKSflWxBrK+nEg4o6liZV19ORVHPS/LvgRQBMYKBUvwgBea5+h2ATBwTYt1bEhH4RYdKjTrUR5GreJM4lwsLfVwC5fAsuO4GDm9Pg1ayF4PxuvKoB/DiIpuDA0ffSH0T7hwvKMDEI5f758wvKODcI/jmfTQcOaeX9Lx+jdeC9p51h+4AjTt6QwokKLfg39+CYOeEfdMg6r1heAkuEgLuvlGHD48z6QNx4DjGdq9kip5QMmT8J5m/BEH34gHulDyxA9huSgL/HPv9ZU8rT7PlAVxEA4aoQ9HOPxCE/pQPt98Zwrm68OXP2kQD+nwy3voIy++vuC0iPfP/uzPTmxJYk8mdOPwR5o4/BEPbQHg8TpcPEMD70iTMKTJlhw09NmryVYbNk3jFy15ygH/0IN/3pNXaCNfOOKADtLnHr+EwT/+KAPC8B4/vMdBJ3VPGoTjPRe0448wPj7oBVR55/NBmyJ+whMPbY78kV/qBBpIowbdGU0VFz881WBJQBIVedttt7mtHzx//etfdyZQ6AiYQqHT0CjYUPuZz3zGaVejoY11BSqcSsbfl770Jdd4aXC4PdpWwmZY7D6xl/DTn/60247DFpM//uM/dvaZGPnZaEynZDsLtpp4Jg4fN/vT6Hhs/YFbYG8klgPYzkKabDsCZFC8ZAMz+fGOBsy+SPb90XDZAI2dKOKvNjiIGZrPf/7zrlOxuZs9ivhnjyPblSgDbIOxLYfOgJIse/z+67/+y7Zt2+YsYtDB2JYEbWwJIh3267H1hw7FvkA6OaZgsL/FHj5AnY3b0MU+R+hivyJbYAhDXuFk6Hjki+9slcE8DOUEXWwX+ou/+AsHXNgZ8+CCEUG25aBJD5iwzxC6qd8fyzAgezxJm84LgEA/nRj/mKOhnqhb0mPDOWWJX+qF8mbzOfknX3BalBvgzMVp4Gykxi/0027IA2DH/klAY4/aByDKPk5MIfHdg8att97q4uEbW3/4BtjQBtmYTZljQ8wPGuyBpJ3RHgAxBiCACJCETja/sxcTugFz2i/7QSlPbzSSvGKChz2VlCv1v5xcTULxmRQwVgqoNDZCYyQPS500PiwFUKm8p5HwjYbLfjz2m71atpy2qQEBWnRAOAW+0zjZyMt+NPasATDsL6Ph00DZhM0UA0CiM7LHjrCkSyOn0fGexorjF3oITwejExIn9LDvDysD0HfVVVe5jkKD8+BFR8XMDh0PutjSwn45QIg9gtCDFQv2DLLnj7yRHgBFB+MbQEAHZEsNG4OJB9v6vAfc2RSNCZ63vvWtDnjZLwgN+KcTEZaOyshPHvjmp2mUH4BNB6Rjss+QTcMYLaRjYV2DcIAY4E6+yCfABR2U7xrJligT8shAQXkAmkwPSYsy85wsdUpdExegitkhygfLFRjoA7SoK+oRY3+UB/ngl3DQCK3UEfFiuoeyA3QZNAAwypZyJjztgDTZq8nmZ+h+17ve5eiGNtIkTux8UZ8MMpQV+aXOoQWwv/766x2gM4Di328gp5zYkE/e2ZhNm2SQYn8keaNdEA95oOx9XfAMsDFQMmgRD+Goc0DLt58z6VfnIGxdxlVdqGwwhgOi8jATwqZWOBtAi9EOkzJscGV0ovGi8Uujw4wyjYFGwGjNNzo9HZhGTQdiJKSR02B4ZpOzN1EDYGIymY3McEvEB2dFHMRPA/KgAdABVrwnHS44CujD0gAbnQEpvnMRBxff2ZhLWECGzgWYQAOghIUGaMdcMMAFmLKJmQ4GR4nJZjoxcZIPOgF0ARR0fsJSTuSLzsXUCeenHHQwOidp0hnhKIiLzkeZ0qnhROHA+E7nJh8AFmBOHuAoKXPKkAuggDMgDcoBroWwdH5Ajc6HORm+kxZ+iANOA5D9wQ9+4MqCOqaDQgv1Rb6IHw4Lrg+AYeMy34jHp+fvoQ+Oi3KFq6ScABo2k7ORm3ZAmwEgiIfyYrAgDThKuHzopo6gkbxCD+WM4x00AWS+7fGe9gZXy6ADeNNmqRf8EAdtBrCnzKEZ/6QDuBEf7/APHZQT3CV+eAfwsokeOvG7nNyy47ioQACIimPkZSpDRVKhgAlTNDonDYJGjrUFGi82khhRqXwaN42G71iIIAwjOo2FbzRIGjVh4eCIi04Pd0EDpGHv3bvX+Zs92jHKAqbEh7UB0gAAEDaTPlOEz8luVvUGYRoc+aLT8AsY+IZMh4B+OhIXdPMOMKKBEz+0AHhwADxDL1wmozr0wcFAP50fwICDgjbsQRE3YShTRn64Mzoy6dMZ6GDQBPfGdBF/nhZ+6TyUIZZI+U46hKHDEZ6wvOMXeikLQOQTMgnN9AwQYdoLnXyDFvwxLSc+8gKNXMRBvFz4pxyYJkMbgMHggFUMTzNh8Ev74B46+CVv/BIHfrEQQVmRF8wTUTb4gRacL3PyA22+HgjvARs/lK+XQREOf9D8OdU39cVAw/SY98SPgz78cFG2xM83uPMfa4qMhQ7iJm38Qi/p4of33FNuNeielYEsgrhlB1ywyMz/ARGmd4ycjKJMD+CIkIlg651GTUNAjgMQMOVDloGZXRobjYAL0IKDgQVn9AI8aAjIGQBDOBPAC3PFyBxo5HANyCdogPglLToYvzQsQIQLS6i8gytgask0FRoBDrgnQM6HpW7pdNBOh6Fj0SEZ4aEZG1vQRvyYdQEYyQ9gxT30QguNGRUCuDaAl45EXJQRnQMOAb/ImgBg3/mgCW4WkzN8wz8yF8LCoRE3nYk8cxEO2RjhvO11pqB0PMqCjoUf4mJA8B0Nmv1UDDkW5c8gwXfCEoYyB0SIn/xwUVc4yot84B+akG1hHoj6gBNmKgvdhMFRltQZv7QRAA5Q5zv1Qtlh44w4aE/kAQ6LuIibOmdggG5AkjSJi/D8MtARJ+UFcFFHlJnnfAEs2iDhvSVb6gRHGMCUPJE/4iRflC/lwoCAiSLPhXEalZ+eEu8aTbupM/zXoKtPFasrBQEuDR5uiwpDALpHglPshDNyw8nQCGlM2FQHQBhNse1Ex8DR8GkkdDCmDzQaQARLoQCD7/B0IkZ+hPU0IsCL02MwY0z6NFAaHXF5RxgaMGnSyImLKRVcIBegSQOmESIw51AOGjadDbAB2MgLcdJ4GaXxD/dBx6JhA87Ej/E58szUFEBC5gb9/JJXwJ1naGIKCJASJ0BDOREfU0DkNaRNOUAfQM8zZpwJSzkheCavOPwwvUE+xqIB8i3CUk7QR/n6jgv3wnSRtEiT8mN6yxSQvJFPbGtRlkylCEvnRw7FN+IBPOjkPPPdAwW0wTWyILFt2zYH0twz5Wf6ysBFvNQDgOQXFAALBgUAAUBBgI5ZbOKnPAEq8gzNCPO5px1Btz/kAjoALRYqmNbSdmgTlAMOGmkHACBhaZMAJdw4si1vu41pN3UPR0V+GAhoi9xDPzQyu6BOEE3AMTNl5xm6CYc/aK8xB8e1aKLmCnheW4egEdJxaMi+gqlsOiqVR+diROMdDZQOzTP+aYD4oTHTMPhl1CROGhydnAZJvIxoNAieCY/chbA0OjgeVqX4RsPxjjihgzhJn/R83KSF8/TT8PhOuvx6+qCfeMkjwOw7Lu/xg/N54BvACPcGjcRJZ0GmhCCcZ+Ihzc9puoIVVaZ0pEfcgEx1fKTB5emn8/n80XkoE+IjXeKETuKgjAEb75d3+OGXdPjGM/6JH8cv5UtYfik7/FIePPOdZ9IlT+SFDo4jHeLGDxfvqR/qm7IhLi7igk7ucb7cqAvi8N+gj7KlDKEHfzg/0OHXly9tg/CAFuHxwy8OPwxClK/npPgGreTB1yntDDAG/H39kA+cLyPCk2dfRgwktClohUbooHwoVx/O3ZzFP5QbZQFtL7R1iGUHXGexXk47KiqOhgJ3hByN1R2mH7ynk/uGftoRnmWPvpMSLR0JoTcjPDTT6aATITTcD42dzlF39RI4VQmcIXCBxHPhT3WyjChczzEkOFfA85rjqs71C3UPODHKIkxmSZ1Rj1GTd1Quv0vpAE7owEEr4MQzF6DFM1xA9bulpLee9vlRArSXpeK4Kvzn+VFONUslHAoViLAYtt8DGVORWuBePCBRgNADfdDmGx3TEgDMT2lqtqDrhNVLYKYE6sB1FpoC3BVcC52fqRjTLwACLgZwOBeuGJBt95C4JP2GS9IRKkoVoix5jUlmIjqCkbw15LSylgvZRETHrMVj1pPWknxA+k5RLfcXpZagGWE4G7JkQrpBZfmTddyxqGzmm8IXFU8ka9mShMy5FdZbardMbEi8veRTJa2WFZVSSZyc6eiugPzqmopmrK0kYX1WAvRwwoaCWu0rayVVaeQByzJhtaKmdHlfnAlH2HRAag7BuGlDjJULGWvMdFo812bZhLawBCWvUroRhY2IeQ0oLPnkmgxJjqP8ThfGLBvRQkihwVqyjdZditporLKN61yUfz3OpS2BmlTwWNoiOT9SBw4DldmfOrA4qZmHkoAMl89Jt8mS1qX7hslRO9oYsrEY6hCVQGUBqnBHQMQzsQFXWoIPSNEzL+FyLG7tmXFxZzkbjgwJwHSYalHKm/JDWvgtO0xWeD0TbbiohQQBXlc4YLGptB0L52w8lrBmAWo8qNW0csGlRroVKRooJEE8UQgAIwLD5qJWCk2nNgeHbCqilbRsZWyFShxhK+lKMVfP8VDYMpOy7R9tto6sVCxK4zYkkE4LXJvKNakG4PJR/3Oi4S2qKOoc16KKbekD0WmpPACjqJtiUEAl0JJETW+1KiYOLBONCQgm7X+sbLK1U8etXMxaKSFui86PL/3mXTiexDuVBU7ZksXCcRsrZW2DQOeGDnEwpT0WiE4LRKQ6oQSBjIKArzgDfkGFC+tqnJbiYyhuocK0vW9tp10X1apoNm2FkFQgSlI9UHrCJf0KLmdoIBzptoSk2FsoWqc4thsGuq0veFCcqzYKF6VBLtoA2kJAyrbKdQWaXQ4skZmycjgpTjdk1zYn7LpGrablD1sprLKYqqi3KHN1t8xKoA5c52mFAlgh9X46NNNFurExbRQIRMTZaAONPEijOjhqVzZO2YdX9dllUvQO5mUxo+LZcU4ASEmRic+yqKaFcaFgSL8mgAtNHba39Tfb+1Y12pqcFhxKFQ6mJABxoOUgBToqYaPirDJKOxBO24r8Pru1r81eH2+0ZHZC71yiDrgAPeIAgsLlouItWLwgeiNSD8kP2dU6l/ZXN3XZBt23F5QTgkKoo7UCfAHlNaKw0aKmy5qWR+JhS07vt3eujtlN3U3WNSl9M00d6255lkC9Zs/neqUzyzHt0uFk4JQ4FMmfxLWkA1mBx5itjqatp1ubmbti9t6eDluTlQ6UZESVkPBZrAzpKuYl89LqqGZ0geKErc0P24aWgLW0Fe1V3TF7a2ebrRQHE5hJUyEceCFtiihsMiDFz2TA2gJT1hcYtc7WjK3sCtnNqzrsqkTMokoXRzhACywKOMArWEoAlC+lraM8ZusSWetJ5u3S1qL9jwvW2MaRSYEbwFoJWwTwJDuzAvTK/IvU5FLlSdtQGrM1LSXrUJpv6knam5oarHeicqwY6dZdzZWAb4KLIqwOXIsqtqUPRK1TefBaYEkypsUBrWg2ByO2pX+1BcN5e11z2X7p8k0W62mz8OqAbWwYtcslqA9pzgbHhmP6FhDHI2mS9TQ0Wnt/hzWUJuyXe1vthktWm/WnLNUWtK0tUds4PeY4pIpMSmZbtDIZUjzl6YxdqJ0HuVDBLo5O2vu3rrcVq/vN1qQU5xFxfDFrFUcFAAF8pA3tEf0m9XyZrC2YwOf1zUH75cs3WKovYfGOqK2P5+3lDTEtAgi4ND8MklFohsNSXCs72i0kJc2u3JD98rpGe+lFA2Y9zdbWmrVru+PWOzjq/Nf/LL8SqAPXeVun6sR0ZNeXS5VtLhJUr+vusdzxERvQyt7ljeJi2rVipxXPfEJ7LFN7JSTXTgABBoDjAES/JmDQEbC2rq/fpg4fs7VaursiVbDmRvFyWlkMdcs6QXnQmhIjLqyb9SntfCZnUYHXCgFIg4TvvUMZuywyYZs6pU3d0KzppoT8zUc0Azyq1cwKyME5QXIALkrC+P5OaZIPH9eqZcFeGpOViTatSsbFnbU1iZOTtn3DmFYS5Rc6FZLpMfcJaZyv1Jaj5FDOXhLN2iVtg5Zs1Ine0hQPdYr/zO+05khlCYCQdbe8SqAOXOdpfSIgLwoMggIAOJKGFm1hymVsao/20U0O2hrJhnqaxUe1aMtJtzZ5i6MphbJWzE7a6qyOqodTE9gwXcsjK5JqwZFndllE22PWxVPW2CDrGR3HLSNhd1GCeqaUTOf6JP8K6AIzG6T2MS3wsuFpGzy411ZmI7auOWWh+KiVW2W5tCwLqpaWsF6cYCZrrUoHmRygFSxKvqX0R44dteGDOxVv1PpSmkK2DFmxkz2E01pdDNiUpq29uaw1iMsqhcS1SYAfzCseyesGd++21tGcbWhMWkOz8tchHToZQy1qClksiG5NoNuCqKQorKaYALVKTX80rXZAeJ5W/vIg+4xqoA5c52kjyEvOg+QH0EoWAjY2Ke4nHrUrpVPWqhW5tvCgRTTNCrQLnBonLNXcJ/Dqs5cki3ZLMmR9rLoJSEJaVbSkLDrkxKFJqN1rR62hIH2tJsmSejVN01QxotXFpPSwNjYE7N3NsraRZ4VQYbPaL6k0V2VCtqVT+zmDR7Q3TqDUJr6qa9oiK1osYmutT6ubb2oxuyqGnpVWNR3XpxVH6Zn1i5a1wcPS2zpk4UZNJTsiFu2RrbS2BseRDYhrurkpbBcWpLcmWpMC2uZwVKoZAXt5T4t1l/dYwjQlTDSatasMuqJSkei05kCTbemQJQnFmdKChIpJYKX0AzKDI50xAL/ulrQE3FxhsRTU9bgWW3JLHC6kjhcR5+NWD6U82iDeJpkZtY2drXb1ugttcuwpi3WJu2hos1hUnTo8IS5o0i5cfdzG9jVZn/StDmoFMSOJfkRXoziyl3V229bWbsmuxCl1xC0YXyFZWZuVE8cs0TlsWwUQo6MSvoe0sVyqFQEpfoYEChe0SW1iQ4sNNDxlTT0NApB+qU8oTWFiMXXUenoHLTYkCxsjYevMRWyfOLWCQCw1mbaXtXfalZu32O70TouulKS9sVccm7TP4rK53jpmGweO2MiRVts3WraWvGxRxcSxaRFhbbhoV3dHrO+l0jcTyFpnhwUbZa1CfFoplbZEz6RtjR6z3uE+uz9fsCmAtqRN8kEZdJTALK8FUhRw627JSoCRY9HgVee4lqzezixhgCus+ZKbMgbD0l0yk21WqRbITM70YVvTKnDoU+/sE/fTKZWBLlltuLjbkj0Dlpg8bs05ac5rvlfWVRAX1NGkaeWhJ2xdQOZqeiQrWqN9rV2y8BAesWxX3hovWmUtyaglJo5Zk/yHpKSKYD+kaVmbZFKxAw/ZhuYxi61qtnKP9Bm6NVVMSXdsXYulBlZYU15yrpwsVDBdVJOdkmJrKhGxxPigdaaP2epOpb9G87w+2fhvyFq6RRY+XtJtDV0rrGFaYaVNH9cUMaepZk46Ye1xpb/vQbsgmRZYNlpB6Vq38tkyasVVMWvUimSDNPXbhwetsaQdBJqWCmkFXlrEENCzOll3S1oCiwYtqK5zXEtad2eSODIqKYNKn0nd1VLa9iNr8nYgO2abCmFr3txvE6sl9+pukYmVJgm8JVOKKsz2QzYhzfJpHQgkaZWVNI0rBrM2PD5hYwGZ9xkQOL2kz8ZWt1qwvVvTP3FB08e0kpe3sfuCNiywzAl4AkHFIdhEOH8sPWFT4+KstrRaflO3TXU2W6opbpGWZss1DJv23thYTnbMNN0bF2cYC2gKq+nnSGHUBjNSbE0XbcUrV9rEStmxb2+zeKrDIk1Y19BpR9sP2/h02oZjsmOv7T2FKS0uxKJ2SLKvCSnV9rV0WHFDj2X6Wi3UqimmppKFuMzXSJF28EcBae9rGp2XfE46XXxD/0u7mgTakqeJC6u787ME6sPO+VlvTjiOFnpBcp+cpm5otI+rMz4iudFwSdO7pr0W623UlO9yTbuuk9zopVbUCmN4c4c9nmy13SX2ImoLj4TkZU0ZhyTo356J2DDC7JbjFl2x0gpNrzRr3qZrpbgZybsu7LKHg0k7FmoQ6ImbE6eXC0TtyWzBdpdXWrFBsrFO6Vd1rFOY11sxLkMjAhMT9zbW02WP5qM2pbSLOQGSFgiGBUQ7pPF+3NpF43aLr5Dt+45rNF18tUWbNon2pAXXd9jOln57QpPiKSm4NkRTbl/kLoHR0/k2K0YHLdQh8zw9q6yceq2G4mst0tqjVcmMtV6+yu4W8I4kBdzi1pBt5bXvEZfMiwOru/O2BOrAdZ5WHStkYrTk4BrUKSU8z8Qa7JlYuz2ZabJ8MW7RSIM053vVYdeJ02jXSp7Z7qNpu12cz15tzYGDiUvIH5WsalQbnHcEm2xPIaWpo474CktGFehTGr0Wj7Qb+qNPDabszmLUDkhXjG07To00HLPDiTZ7XGA5lm/W5m6BY6RF08G1For2izsK2cHBKbsz3WCPa4N3XpxaROkFtDgwrenbvmDCHo10akVU6g+RRgtEegSmPeLIOpStoO0eDNjt2gO5WwAZ1JVXJvJhycu0N/GJYLvty4hO7WmMKK85E2DGNltAaiHTmoreuS9n92nT9Xi5yWJaZQ0HpLAq9Y4pFiZEfd2dvyVQr73ztu4kpBbtyLpkrtBN2dKSV2mHnz20b9R2/0JTq4OyXa6pGJNIS4/awae22z13D9njU+rAjbIsihqEpn4h6VDlIjHbmwnYA09n7cB2gcPxMYGP5pNahSsMHbZf/Gyv3Xl31vZpijcuoAghmNfigBg2GxUIPjw0bU890WFjh6c1tRyXKoQOldViwdi+p+3++w7YPYek2tAknS2mepq2yeq86Nc0Tnsqbz94yHbtkoXRY+PijLQCKOVZ05R3z/b9ducD0/awEpmOa5VROZmUrAtOMyv1iacni/bgjpQd3Sc1kLHjbvdAQSuemaH99sB9P7e7fy6zzgLAXFbbgcStUU7T2lYWL2wKAABAAElEQVSUlp5aQfTX3ZKWgBt2F0vBXAIy8fd2ry7x+Kd22HXCXjvmW7D7VO28HSh+MWXLsVuYfuHCJtRineM25gmsvlt3KoFERkqo67ttYKM4LnEpz+zYbwf2pWUvLGmF8PydNiSdrVQsa+skN1q1ulOKrZP2zPYjdnjflDgemQqOzh+Wzc1d3e228YK1Mh+cklHFI7b96T2WmZKyqjirUmj+JhUvDFv/6jZbu2GFNleL09p51HY/MyxLFSmlOf+0Lqg9lE2NYdt04UrrlpB+dGTUnn5yv00MyxCOlg7zCcno6s7psM1XDIvtM/Rrb9NtEaabaUin6q0ACtfyt4C6EKjNV3HL8X1IewYlghJoiXvRdLCoTYhFqSIUyjpSbIEMhyUQDwY0L5Q8yLh3Sqo6AUjyqbwYt7KE3fO5WBhrDLLfIJlbSdwYhgkz05qWiTuSDQgnE5svLNbAQqjkazpHLwtZSlPGmLglLShoajifY/oXFDCHpB7Bjs2YOMJpcXVlqT4UJAcradpYd6q3+att0cVzhsA1f6U+S9G8wFWv1WcLaVndFbXNR/JvabtL5hSSqgGqABG1FThdtDHncUXUBiIphZWVByw2CEJgpGn34YSM90knaj6Xw0ghq446yCEswAhINoXyakmyKpeu4Gs+F4gnJZcTaeyjlGHBCvMuAEvpXna25nN5adOj5lDQNDcorm48I7M80iErC8wK0tc6nd4xX9zL5f25AK2zUDY0wkXD6XkJXDVaEWehLs9eFFhgDUpwHhXXIqvy4ri08iiWKaK9LhVQmDutnHS0JDqXHwngpaQa1KpcQCAAB5bXv6BEAvO5rHS7wmGtNgotiqhqaC9iQe8IEgJgFrC9PylwCkj4HigzKyBdhQN8pIu1QJKSiUlHK0qaSeExnF3JspKJBcQthqSsKlHbi965YUp1gEyRsizNagBLNEuZvyGdRo2dl8DFLpX5XB3UKiUjtS41UGzha9rkxjZN4aTDVAYIUGWYx4XguAQwtCpnPkbTLyGQi0f8m97OLzMKq1cEBJjitZxkglllmVVP0XGquUrEKYSSKit+pAmtcHcL0wtnh7Te2exSWiFuCKcFhzJ0O/GIonkRO0pVVQ9jqvLQ0yyOe4n6zIuP40IUQq6pA+98Kfg6qfpU8Tvj0YWbufd+/C+v/Xf/yzv/3f/yDuf9+F/eeT/+91T+qsOcym91nKfyG9I0DQoDsuEekG0uKGN/Hg0Y3Pdx+V/iw4WlC6Z5lu5o6Voh9D5Rk2faOBPA59n/6qNWKAVQJzl9JUHRwaStWgXBp3vi1xkpxL/ka96BvuL/XNK683757NMNu3B8IW0BllcqdR5I81m/PoxenYhrrjj5frp+T+WvOq5T+fXf/S9hcZ5G/8s778f/zueP9zjClukcUsINVbOweqWtrkvhzijV85LjOrGzv7rWlqLoazjNUDkmqBDXIdCSvVJNwWQORqoHknTLvPIChKM2QPuWIqoDAgdz2qgtAbvU7CXshgua2zmM0qeTWiQPcH16Ww1cs2MIST+MNLHm6hYGRLVO9XBpIq+azwWwKw9Lqakhqht0TA4N0aqEgoQrP/MFfrG8pw5URP64PDQBqt38NVrtq7buaxK4wpp054VOkxKuRgqyfjBRtIZYyo5I+2eyMWqXykBcUvamBhqa7MLuXuvXib5tDSmLavWprJNvzifn5N+LIBhwqUyPFhG4HuRFVQIl9SHEAxNSyt2pE6+fGhq0X4xLxy6utVz1tfFUq7VOFXW0SsBGtIc1L4u1nMTUnAnqsBM45tpzNQlcjK90SrZlFDWaFmUgjv1qXTK90hdN2Ks2bbLNq9Zau5Qmg7LzZLIaIJGzRlyBnPbBnU9uNvgoF/O6ak4Gf37aNm+A+od6CVACYlxlxkwbDBLWunadXX7xxXZUttt2CcCe2rPb7jh6TFusmmRWTXbaEtptITUS8WfanUBPrG51RFYbrjaBSwCEqd5YHsVFaVmXctYnm1E3XbDJXtHZr/P2pNGt06Ij0k9iFYtNxjlNMTixJqgwZ83NsNinFd/z8VsV4UILDVXennMLaJ1z4Fpknp5DbPWLcxFndfynul+O6Z8iT5XzCAKWkJoJg3zx+LQNaGvWqqYWu/aiS+0S7XT48sMPaCuUBn3NG+PaHpXXtqgp2UKLaGW5Fl1NUgX3pJX0ik1yKSCu0RaT977scrtQU/OWiePacyadJK1+sd2kKC4spj13YS1hTWr6GArMv2L2vCtgIfZndmTPx29VWCdDr3o+3Vth+7m34rnIPC2Yh3MR54IJzvq4HNM/RZ6C0kSmvUiLuGJ6G6DTqmtRfSuiswoukTHI/je9xT79k3tsb1qbsdTP2LQfXkB9ZVapvuCPNQlcAS0jofdTyE7bJikTfmDry2y9BK2Z6UFZF5B9dCk0Yv43IPAKVdb9ZY0zKHtUMY0UsLfnj0OCQDvCVbe/6nv/HT/+3nFcvKi7egmcogTg6t3CiQZ8JiQMlgXJKEpcWtBo097R0NC4/frWq+3TD9xtj0yNObPgSamZZGoSIVh2qUHH8VklnYvXq4Wht299ia2ToD46OirLllFLi53FRnpAgMVxXIgdwwCZVqGQF51vNpaqZVwelE63Ss75VPF0Can7q+0ScA2FUyy1KV6gBUeFpl5BXBVnAOS1LatbFj84ffx9V19hE3ffaQfZLsU0sSYRAiWZGnRh2YZKiY1908ZNdmGLNgWbrHCm4E10lPu49trlpQuux+CMgg9bOzgtmet8c7Dws6+58jDbD891Vy+B0ykBmYsEptzArm2k6j8sfMlirmYpzdPqS+WkTcqqRiySsVXljL1TsuSGdEbGJ2WRtkbdkuEpJyGjj+U6oP5U+qE0faQkF9DWlE068uq6VestOiJLmFoVURnLXLCsTOh4+KKE9W6qJBbFKyfC9qLO4w8srdHyfg5ZISmI5iQMLTa3Wlqb9YpRVYkypaMn3HYXAjhOUk0vIi3xkMomoZGzJHM1TlPd6WipzGSOGEWQqNjQad0ldaR9SSuuhC0qXFG6Ozmd+hyTFdSozLwww4bDwxBhULIMFkNyeoe2eTAiW11KP1LWaq2eC1qpZbN1sKjj7iVDdCpS0CVBbnhmzqrk5UdxaouP9uDYmDT0E5I9RiR35N+kdvLENSDFpmV6WaNOUd9lMMwmNXZ2ZKJSfUnLMmpE6iw6U1umluPYrpFt+XEdptGo1a6StgKx6Sic0N9cwGLBFv1SBnkra6qj4zukaCsa1UZi4sqxPhKS/KaADpnUAcoSiMKhk2vaDNu4mTfFdV9ShvIa9AI65gzlW857nJIN/phsjIUKMZnZmRDNiLiT4u7Vbt1ykDJ7njiG/EpHq9Q551P6gZCS4G2RRS6hWkgbXK9s7rKda9baFw7pOLukzHDPuLkswPhvi/itJLyIgARZcuCiY7HJo6KsTSfiVJasXbf5AktOZnTPvLxsWYFXWUiX1zIupy3PducbYHn6i8pTqbHd7k1nLSO9tKnGuE6p0UgnFMnGdSS9Wh15Q3kzwGrE6LB16Oiu9R1t1jo8rnJjS4z+6VNJyFHMZi3WolUhbTYWBMhSgzYpazWpKGQJaCsMp05joE9G2LXsjdKmkspxPmJc/qROogY8LvAJJrVReWpKunGiL6x9gAK3BH41OpQcPQI2gVlZoBKSKei0NgWyLtIkUAmqc8dkT549JqFpwRZgKBPRufGsNQWbbTQvPTwdliH8tJxMNQ/9QvlY3WARWWDNpKecrS846rxALxgXrVGBRT4hA4GC+fwRi8VbLD2Rt4aGsE2U9wv8ZKNLy/kctxabVh4mS9akMkwPTcmaq6zBqg2FVQZsYSpJQZUdAFwJLfpY9rj0liI2Eh0Xso4qPZ1qlElr0NDAIe6+RcAVVrisABAsRaYKp8/9+eKYDsIoPNeBHWoKGiBgItQKBP46bmQya1f39tv3BvfYCLJkheWqJbdkwDVXMfCOkb9flgIu0FJtcELKcGpjOUZh9bCwEAtZ1nJyY9ocvE9mWIZXDtjqa7fJzLA6mDiHiDpasxQGaVCgOtYdODk6ODFiz9z1Y+3GSdsVel9wK0ZaWRVbDycUnlJDE+BPaXGjrPMGp9Uoo9i3kfHAJlhXWX04qg7PAKGDcMSVCQDaW21UgBcQ51IW4DVIoXdQgLQillSnFygIqAryH1JnBkipI6UiyxP6m0paRoqMY7KqOqGjydaKptSoTNSIGyoJ0PKyw6Zz0iw4nbeWRKsNjwxbqF0a+DnZfpfp6IHXvdryvXtkUrrf8g/tsOzjOx2ndUgmcpp72i0ue/THG2UOupiw4GTIktEugZdsyjfn7GBJR5q9JGkrNl9hkf5NjvMqjxy1qXsft6E7duqsD1lSFRBPKS9BlU9OoFUWCDFYhri3YzbadMQm+qXftGmDtQ5cp2PZemQDf9wmnnnYxncctaE9B6xBFmWjZaUfkqkgKXKWYS9hWZaJ46g5+p4T4itbrNp3NzbbQKrZhlR23ubWWc7uXBBw2kkw9Mx2MthtH9R1WqCGUcAmjXanQuSIzKw067h0j95FNXogyE2D1AjUN3TPiqHZy9u77PK2NjHmsqeu6U1BI25JHTkkEy3olTgbU7OpPk+fA8GUDZaSlt10iR3WtpcjGu0mRiZsRBZIC8OaYo3oeVQHVYzlZWdKHGe6oAOiNQU6eNRWa8oMe4+bECBFe7tk/VPcjMoW7uO4pkuxFT02KG3pFPM4ddaCjgYb3dhv0+KCYoo7KfA4lBFn0tthGY2qIQHMUa3mNqxcZRlN05l/j2ogSbS1WCk9Jj4FyMJos7YOJZI2LFM5k6mYNa1daZMCumEdKCt+z5owoaNpLXbw05r2Jzt6bPjoUUvqEA1n0ysWl4HAuO168glBQcCOPrnL9h3TadmpFitIvlJc12OH1AJzAtGxBgGubM23Jlts9NiwyXqNHQ8fsu4rGq3rhldYqL9PzF1KR6HJQmpiyppWdtpjX/uxhY/mbeLwbstoCh7RMW0lgZhKRtSLG9UCD4eEFNbpiLMbLrPkxZsk+NHhIqkmCyfF9a5ttKZ+TWvHjlhpLKFTlHRCdnBS7VSAV9L0ye+JPE/bXTXZcGQhcZGIW7AqkmQblcpqRHl8aHjEefX92//68L4/p9Npy2QyJ/r37O/+md+xsTFQH+A6XfD6hPxquHzWLR1wqUMAXFDOFIUbZCBhTQ+uX7/B1mgqU0CWpcZbOXF5huOS1zLClWXimHIcEIu+r7XdhrW/OD8+aTe9/CqLTqTtFyNDNiLOakKmWwans3ZMdq4GJ3T8lqZ8CR3Guk6mkXNS/3DTKsFF17vfZnlZHS2OpN30yNo7rOt1r7fs4QOa0k1pVTZhw81Ntu49b7OG/hU28tROyZXCNiLzLyvftM2VaG5oTEAUt/6b32HHH35YDVjTPWlaN65fa9l9smaaV9wKkxdI7Rc3dsE7brDGzesdGHSt2GR9L9tqu57R+Yri7uJaS08nUtb/nndadOsW27/jaUuIjrjALNfbYx0vv9pyO/ZZ6KjymQvZpbe+22LdrVbef9Tar7jEWpVm20svs86NGy3V0iLt7lad1L1fFlxHLLE2Zy1Xd9hEWJyduMWyznksynhiuTxu2fQh69DBuD//2Y+tWYKt/o0XSj7FaUZY31U7EthLAGGFrgPWclW/DuRIWPnoLoUVKIXGLSwz0EVplQdbNeXVBCp3VIJtbX8J6yRwON+C9mxiaWO5uKL6XwTxgdpiSIqpIanZ5yUrDOt09O/s2yt7/gIyOZiU2e4MgOu5kc2O/Nnn5wCXYGFpHJwWbubHWcs0yVxC0jxtSmqKIrQvOUBTg6kwFSpXvXs+2a0kUdN/pyUJHlG+jmn+NSnuMiH5XkonlV59zUZrHV9p9977kO09LM4n3qhtTxoNpWgbxryybK+XEKyrBGPC8YysjZqmfLslwu5MpTS1KmtJu2A9mnKnNY1r1z7OEU0bWy+7zMae2m3NG1bp1J9mmx7JSJ4jobQAakpxY200o3v1UstqukmTLep9TtPIgrg4RmJJvXXMWNFWv/wKm9LiwlPfv0urvbIVPx2zda+72ta/7GU2dsf9FozpIAvRVBDIjYyO6ci0jZa/7z7p24VsSOaYGzSVDErhMSXZ3QVv2aY4M1bWadlj05Ja3X6XDeqEjs4bXysuoGS777lT5y/mbUWD7Mjnh613RcgeuvtLtncyaW1NIbvqmjdq4EvZnj077bE7H7HV5WlrvaZobelWycDG1SEb3eIOwkDtsZDQXrK09sMWmOi2zDcOCZxGbG/ubrv4XTfaji/ebs2xFmteI3tmKc0CWiRjHZZ/HSSiuae4OzVI33BrunWdPnEhceklgXrOLcaI+1I7bEHWeu4cJQjfsii3ZMBVTS3TRHashzTdQfYSSsRlDE4jqVhYLYJZVB2TM/FyEhSj+oCcZbk48oboPasRLic2Pc4URl1rSmCyWSdEb3zDy23nvmP2vXsetvzUpCU01YvK6F5Zq4pMmSmXmHZqR7SCBwub6+0WlIwLUHRoRHOnjM/L7LLY1jimmwVmbZsvth3//h8WbND0bqDPMsO/qJSnyjSE0N0tuQmgxFkwNYclL4gTZhWO54LqqCxZVklTwcb1A7bj8Yd0SGza1mgaOxWZtqE7f2hR+UtpOntQFdV+0YBNbX/ajh3VeY8XrrcDj9wvG/R5N/UvISsSEGbag5boarFdP3vYVm8RuCkHTeJqVsoIok7FkHVT6RnJa5OW63OS8cVTmo6qzWxZu8muaB8QsCI32yt9pIKt18nYm978KiH5ISuNP2KTD2hKelSrj1PTKltNOcVN5BRPViZ7WsuS7f1w1Ao7dPjs9KhNNT9h9oZLLf3zu6x/6mIbDKetZWujRTvjNsmKZ3GTVlFjAncB7OlJUs6LZgp6UPcF1X1B3Ghe4B5U/cYqy7DnKg8ku2hXE8D1LPVCKU0hWfnRQpAb1KIyFuQEwvqk7qplfbYinFGen02uBu6apyLWoE7QMCnQUOMJSiAf0QoeDFQ2M2wJnW6zdk2XvXvlG+wr/99PbPTosE1NTVg0ro6UYcojwapkYyWtBBbUKS997Wsstn9IgFW0NV1rzLTzH9PNEQFXj1bwtOZvwSPjdujp7bZRenKDD+k8Q2beurQ1TUqHlLcKXxfljmpAXuXNahpiHVYf80qLU3qYOeQk2+gQ3UmduJNtGrNGTamaS03CG8nYGvK27pJ1tvvL37LCiIDv8ist2NIgs9ATsoyq034EqGmtcva++8325J33W490XiKDOiNReZsWl5XKaUVUrHlwMi+9I5VLTt9ERKaYFiiN2eDIYfvuZ+60Y/L76vcmbePa19iXP/mvFpdM6pWv7Le23mFxcCVL6+ShDnXEkNQfEMkXGAQl3wo07LDDPx+1gSPrpX4xZn1dymDwmPU0KJ8PZ61Nm/sPBn5ha2/cpHBplQmnYMtcUFmrjssMuOhR7hKXnVNZNyTFbYn7qlVXM8BVMbyvzqHpyIlCVKfxTn3DOWaPy8lNp/KWFqeQL2ubhfZd5qYlmBb3EtHKX1kgIeiwEcmsvvuTB23f3gMOTFqZtklIr3FSx20VrVGjZEocEhvO933my9ay75COsW+UcH7aNv7mLVKHmpYwWytnN1yjxjhia9//LkWrsl7VZumfPmQ9u0bFzjSonUoVReoEgSYJ26QeMCZQKescRNNkK18aF5YlleKEBLAhnceopiMAa07oLMSAvosDbG7st6FecVCS1409vtd6uvp1kGyvrXnHTWbD6vh9miRuvNyGjj9pLbmkjZUkd3vtSyW3ylr7Qw9advNWTYmbrEUcW7AwJE5KcilxeOGEQDKisyKlHBmPjGmFMm7B4+3Woanh296YtmR3syUkGytOD9v/9X9vs8Je6f4dk7/D7XZEKhjxcKtWTKMCW2VfHH1EnHyxELF0rtE2/8pL7ch9j9gd9263N76iy2zoqAVf22nffewh23J5o61/+QUCUS3Kyg5/oGFcskVZeMUCyTLi+l2f8v1Kv5jn1lig9qeRqUZdzQAXekLo+xR0oSzIFFFtzAnuc2JZUZqD0apY9qzdkeD51nOgoCpQRick6B4SaMebijas5f9CVAogOprrjh8+ZA88+JhNqjNPaUrYJoXA4bFR69DeDVb3UJfQ/8pwqVUdzeWsQUL4Ca0oBmh4EqyGdETXlI4Z621tsXv/4z8tODgh+VJCgvurLbW624YPD1lSYYaSAUvL3hmKsCbOqSDurCghO2oQEQHYqBQ5W5VUVqoOcQnwxw4dsI6XbLSf7XrGBvqTdnR6yC6+UosBWhEdV1wbr9hqx77zAzvy2HZLsSK6qt02vXGbPXLfw5aKC/Q6G6y9q9V2/cNXJadKWVq6UgGJA8a08BCX/K6gkT+po8oKk1qdnBK4iR6dImstAa1wPnPcYs3KWxtCc3Gr49pdofBxbV0JSj4V1KKAk0cdlC5WVOc5jrGSqFcC+ICAR+uyNjZxwBrlr+vaLrv5xgtUXiizxqxrzZX2xo+/ipUSK08es/Hd45IZdojrS0psoVKnY3sh7fOt8Br075gCzxGo3ahUtYIt2ad2rZxDR7OdYUeefypLBlzVVFNmABcyLRY3MlpZi0mfiamhFqAkk1D+mK5oasE0ZiGLmM+/CJY2BPKjoFYJ4VgQjmbUYWMCqcNP7LKvfP8Bm1BZxDVdzAmU3Gk9k6MyrjhhCQmI1U+d7g0zu7yAT0hjjQK/jNQZwuIoIo0SDE2mJXcvW/e61ZY/ctySY1J1UHk2CwzKjz5t3VoFfObhJwUIBRu4aLOV2ldZG7pXUkfZ/JbXW0ytOt/bZJNDQzYqzqdxNCN5WkL4mLXDjz5qa1e+1l7xzrfa8O69dmFXm/u287F7LM9SZ0vKwnfsso5xpan1uQP7DkumFbXOjmbJT0oWl/HHwe/eaf2SYY2qtsOawgVUDkxB4zIcmdFiQozDO8R5xTU1S0rnLZQdt6HDUhXJDQlkxq25tceOyijeE/ufsUhTrzTnD9qWrkusRbpXhWPSxpfmsvR1NU1UOTvQkvKF4mfhZ3Tfak0RyzY4qcNjnxy3xp4WyVoFeuKqBkcO2WWbNf3s2mLpYxHrCrRpSi0OWP4lpV/aRnMOUoeBLKvtuYN+JW9FbJGW6OEcukWDFjQhe53tevTig7pOC9QWq8dVmFGHcByDGlGRRiugYil2nVYVN7a3S24jLW/x5EWNqCV1yKDkJiihLid1iILA+qg4nIMSrmfEfZVkXnn3o3tsx8+esWGOqFEDKjIKSihf1tQtkpcgWac9d+UnbT0cimRcbilbYDIxMmL5Y0esQUCYlL5bVkLWzNiI5Q4ek2DabHDfHmubZAEkZ43S1M9LvSItSwDBMW28kQrCuFYvJ48OSn3iqEDpEelBDVrh8GFLH9hvmZ17LCEAimi6FhP4lLVSGZVW/oGnnraiVhTLAuD08Kil73jYGiVQL4lzCh+WlrvijWoLD63JcdEHDlpAumZZbZovHjhkmWf2aV8qHURnCKm+s4ODltRWpVJGMi1xepNaYQwcOKY8i26phIjnsiNHDznba8OTE9ZxSbulelu08CBg17ctKy+y8af3W3kPSqSH1W563GpgVKuKaKChOKoi111YB8/q9OzRI9a+QmAmDf3h8WHLiK6ktjZtGOi01R0b7fgvprQwsNHieckHlXpODRbLCjN8rn7Pf8fOC/Ghypd+WUkWk5BRf9updnLHocMn1CD8OYrVOT4DdQiA63TB6xPye9Lk/LTAqZrQs3Xvpn3UvxwcFw2hKK4LsxtPDx6xawZW6UhQcQ0il+OU2FMHS1vxi//l4cIyK9Ite0iJ0ePW2Jqww5o27pS2uCZoFtBGcze9UVaL0jQPq1EhJJ+emLLmDnEHmj5hAojqZ3WxsPuguDWtSUqtIiCuDelUfvch6xQnVhoe1ibagDWI+5iSugV7Gsva6jOOZrjexYalEsBxXlJ9AGBiKmgUgktM2wWASXEaCO/zUhxF/hHR97A09KNq4Pmdh6VDdVhhpXE9yeKJIpDcLSfgapQ/9kCOy558THsyEzsPObriSj+w/aADopzQKIElAq385eEWhdesYJKx4rERTdMq+UNxtKCp42rpeG3f/YQlVrTagYPSgpsOWUejlFBVPtk9khUemrSR3Uest2HAjowVrbW/yR1jxkoZp3oHnAwiaA3FduVznU3u3209saj1rdsoGeOktUiXqTSlVdhdYesNXCil3GYRoHxqFRexBe3QGQddHk1QAxmLFsx2KmKapNRdsprqPHp0n0BemaUm9MusyD+fhaxXIl5kREsGXLPpdQUixKfjPD1y3I6qEfarhSTUB5B5MY1yOjgzBTk7/Pn6XNAUb1Wq0bYoj/tHDoqzqmilSzKqVbXKQoXrwMp3TvIrdUFb1yR9KW2zkUzcscxMFemLSXFudG6a4bRkZMIJcTO6BARTAoeoPMmr3ikufUPxt0HfInC/+pAUIE3pXsEd94KfgPS5ImjJMo0VFxUTwBSEpmiQh2SMLiFuEPvkjNZ5yd0cM6JvccnD8Dc908JopU7RWPGzUknn11/3CwejcUn0iatS4o67lliA73EplqLZDbfExpSC2kVJ3N6K1dJ0V0crFqSxfzAtBVZNYSXPS0rlYUXrxTaRHrXt2w/Z2utWSjdJU7ukTuIuzWh2K/GgaC1mGzU0rre2wEqlI7AbHRSXP20ZLYqw0yAZ6ZAirxYtREmRgTMoro7zJlU3y8mFmMVotZbzD1CLyGhAjPf22Z13flMLEkg1K8B1FkGLKNUKVKGLdEsGXDAK3nFbKRRGs4ANqaHdvWOH3dy/TlMjNRjJOnKSiZQR6iwzF0atYWLCrtZ2k1c2x6UEKh0aVSns+oR6uLBFTlAkwUxB4JYSZ9UsoXHxyEGhS7PjyJygWO/j4qwqHUwHjQioUCmJgGjqeYIIxSMBuWo8NR2xUVBKBR+XPKMgtjavtAAjpmdJIR6c3aQ4MNkL0Eb3yiburDg2FBWpC/Tp0LhGfqZurXv91XR3XOIxuGSAiTp2gClgi87s7SbOiLg3tpfkBHaAZoPEcxisgxuCWMx1I2MJiJ6wOMgcOmo4JRUUoHHMVlh0xuSvlI5Lv6tdemllywiYcuIKpgUuzZt7rGXDJif7k9aY/mmTtGRTAGMY/TB11HJqXBrxDRaYktUHrei2aS9lMNUhvTHZptIKaj6vKaSm4CWxgOXAmNLU3lGVjUrBAX+FqPP/L1XoN2GXxVGaOM5HpXs3rCk+CxFeY/4sA9cZFdySAVeM+YYcwnhcpYNKK0mynKlYu313727bdsFGCw9NSpjM6K4GG9CGXjVKLDPSaSoWHBmhK52SeNA9qjVXDdKzaXM28pW/VH5MSDPmpk7ej0RIczokf0EdalCWwuoJfSJNxWQdZsYl1UHFHaksJtCo4HKDm6wcKM5iEs5MTsUmVSm5mUqAf3NgQ3kCIjwWVB/u1v2R8QfnhFty1AMvKuG5c4spvk6dD3FjipYtMmxsJn7AEwffQtkAdhXHG3Ft+luaOa2pqFXCE446d4MXPKXWExCUy+yNy4gjV/sJIULXpFYmcVqjqHI60VtPlTan6XJGOmnKn5BaQSQ/NU0JxaFqayxQrA4scJMiL1EKKiXG0OXiU/lRROeJg9RKdQmE9BAU6DtFb3FaPDO2lSUfCEptRlsM7Hhzg33xvidtuKndutwgQgxn3VGsi3bzdI1Fx3dWAsbEZeWzBfvq/ffYrVdeLlmKwEvANY2wWp2onGPKI85EAKaqcNMMGjvNeUpTpFpzlYb//KmSmOgkV13TFaA/6fNJD36blH85X9OrjtP5nc/jTETV/md79fRW+yEY/ma/4/1c72bHiT/cYv1SDvPFWYn59P6elP7ZiPD0kj0rvlDh4MIyBtNt/jP15olFFThRVpUj2nA/roWxrz3+iB3TlrLOpKaJiA5q0NUmcGmaUBK7eq82urbs2mM3r7nAwiPjlpRBqElpMGfElSETYeRg6gI761qnk4sI3GrM0aGZOs3l5nuP32rucS6gQrY1n3sOcM2R/lxxQs9CNHlwIt35/M0X72xa5/I3X7xz+T3T9OcLDw0VjvJZimenv1DZPxuqVu7gv9m2VRkJES0gz0KswFQn5hYswjL22Gg/PXTIvrl7hwWkRBzRXtGC7KjVoqtJ4MpK/hBMyvKk1AO+JR2jBu1be/PGdZZKa0+arAuUNT0oIqRlBU0drTLdqbDD0YXmZUtQA+DF7E5QTQbTqPncbPCp9kenW2gwXCjN6nhm31N8C3XKxcY7O51aeF6o7AFo6m4ud6qynyvMUr5jj29G094AU/UZhx4b03pscbGqOBYP22OjI/bFu++1QI/2u6pvxfWuMsf0oc7qL6zcfEV8yoRqErhYlg+pVyYkoAhqr90XH7xfB1iO2i2XXmLJQZ1KUkpoKilzvhLEanHILcOXMHmsighmZspCxeJLxY+sjuk98fLZ73BrvK7+Xn1fPdr6ex8nJXzifiYeV+rV954kn7bzoDTlB6bpRJy8r/JLw5rPkSafq+n0+SDMQmF9nD5dTxZxwby6eKte+lsX/8yDS5d0Tnz0sZ7erws2E4mPYr44Z/v1/khpdvqz87TQOOaKV5GdSH/mxsWvjz4dHyfpOS/648KS/omXlQcflw9T/f0ErXrp/fk6O8nf84mzyq9P08fJJ9J06eoj6i1liWBiGJ9kYUPPWa3QjjUl7dt7dtk3frHdcjJ7RB/CqOSo5IkpJ4kkptpyNQlcjAuuYrWcnhenGu7rth+qYHft2m3vufZVdoXQKhlv1laQSYlMxQaj4atKwHRwQVYmas3RoHyjdQ3U54+HEy34uVT7hYvnfjlLb5T+XMl7WudKBXm4d96ffzVXXCf86obvJ2V5jvRdXDMRndLvafjzNHo6TvfXr1/7PJ0AhZkIFhvv6aZ/Nv0xUUwwK5ScOKAtTQURj427omzyH5aS8ecfv9ceHD5ik9qvGpWuG/p6khzr34nVnrNJzlmJqyaBS+chqGBln0qcFwqQKUyK6HzFveKwPvb92+0yGYl72YUX20v6+q1bwkSULrGnDgcW1m9NObEwzzLoz6WMUXA+V+k0CFGfddz7EFIQefbDrDs0oasB038mBKoL8zk+cWblfI5tWfM5zUhOcifRqoeFyuGkgGfxYTZN4jkoGZfCQuW3UD4JvVAZnUXyz0pUbIrP6AKwslKrGdIe0EcP7rYH9+y2fYNH7ZDOOmhp7ZQAX+cXSBaQFsMQ0ep9kwRh+XOnglSphEXmsCaBq9K5pLckzKfQC7I/ldA2k6y2hDR39dj2nKyDPvqQRR55yKLSeE7KBEeTlnFj2POSPatacq4vL1BFs0fyatoX+nYm8S4kwyL9BdNdIC/IeudzcChLwaWciqaF6J3vG+8XKqOFwi3Ft5wKAWVg9AAnZX1kWqv2GSk657UAZivaxWXFbUS6er2ygJHTlrKszi9Dpa6o86Rq1dUkcGnV1g3PEVYJae3aSpIXNxWQZnRJQouS2N1IkzbPqqOMS0g/JOWdkPayJaREiLGFmnOzOJGzRt9i410AYE5J22LTJNxiw56SqEV6WIiehb4tMrmlCsZBJ2UN6FHJtSKyjlvUrgYU3thCBv+JkncmmtDmfE0T9Y9N/yXp3S08Vzjj3FDCCwyDC8dfi91cms3Mr7GsgmkVKaXyq1GDY8qQsWBTCctQUalDaLuvcq/hQSNKUYfkFeV/ztGQEX+OspjL71ycwVz+iO50/c7pz0XwXKLm9DsX8Wea/hnGSbObHYVribNfPg86XZHM0ZznKv9zUU5Lnv45KFNUenXMjBOzM00Osa1MfaooDoGdKlEVZLO2WbgdEXqWxTG3e6FVXJiwrCZdTQIX+xJLWhZjb5uKVht6Bc1ir7AMykRwWgJE7J8HpVTHgaQc6lnQlhVssEcwz1t39RKol8CJEmD7FvsR6T3I9djWA9Ot7uN4Hs1drFvmtA9oG9i4EAHT2xGFcQzBiVjO+g2pL9r5Vd1FR3AuAuqwY7GuMssioXuooPP1ZMJlS0vcNsukyuqxQYvLqkFYrG9Qc/Sglnalhio7UNr8K/aW/QuMzlwhcV9s0HabtFl51DcunvnuqkZTTO4LOW2kmQnr/fiwPrz358Pijzj47u99HP4dfrmf/evfQSOqHYQriUbe+wua+E5YvpGW9+PfE46L5yKmX6ro4dmnwz1l5N/x3oed/cs30uE9aVdfPn2++bT49bRBB34I4+Plnjjx4+nkHc8ZWYTgHrrIL78+b94P8fAO/7yrvkjLx0saPl8+LHnGD3Hjz+eJd54+/87HzzPfoYV4eA5roOSX+Kvbik/bf+M7tOZkmod3XNQv/ogPeniXlawJv8TPPRd+SJfw+CNNnrnwF5WFW+LgIg7v37cfnn2chOEefxyky75Vd6K5bIKznxM9LjbJsxd0RaxsV6Yi1jpxTPa41LfEaYW1O4UZTq26mgSuoCwSFDTPTukkmOj0iDWnj9tA7rjdsnWNXRWdsG6d/dcoZbq87AWpht1x66hFhFTRVBYVSJnnaTwzDYlGwHv2ZnGPCgV+eObefdN33yBcpROXvtOQYjL96xqUwuLHNzDC8d7H7+pa3/HD5RsP/vFLg8S/D0PaNEjC4Zd7DuTEbxytZcUBDdzTQPGHHSxPvw/nQE5hePbp4A8TLp5GaCBunz5+vX/8eH/+O7/ej//19LvnmbihEb/QyHsfvppGyocO5wBEfnimbhpTDTapwza452Rs8u+AQc/EBU2UCxfx8utp8WkRL+/JmwcG0vbvCJeUxVdfH+690qJs/MW36nihwZcdIETZQ5sHJNKsBiTS4BvthPfkLyETQMRJONIkL6M6p5B22dKos0j1rah0oBX/PPs6J33/jnqDPsCN/BEPfgF90sMf6XvwYxu4a2/ywzds3enwNndgLvNFeK+k3iXEADQp7hXTh+yNL+m2N/S12IqJMW1tpc0o3EIrG0uMaDU5VWSnfgMVL2Bq0L6XgUjBrliZsMu6zToubrE9j5jt0pLuuObqx6Tte8s7brb161fbsE5qMe0C/o+vfMWyOmsvpNGF04NK2ozsFO400rDTvSBTLFFtG+Lo+Vhc9qX0zIZlDKXhN6gwYU6rwM2oBnBQZlnhiYdfwmcxlSyHf29kze+g97+kh5oGJleIA7q45zuG8/jFD994D23eOQVbvScPxP/+979f9udHbFTG7u6++24XL+8Jz29ceSFO4ijol7xFFJZ3fCc+aPU08U4fXJ58mvz69+63+oPuiQMeH7qxnwbN+JuWUULihVbKhjR9uRAF5UqZUtbyyCtXP6tWrbKrrrrKPd911122f/9+F4d7MUNzTOovhJuakhFFqb9QVtXO1QUnceulcuTSh6Y89sZ0tba2Ovo+8IEP2IEDBywnv9/7/vcdvT4ewla7i3WW5Nvf/nablK4g+Tp48KB9Re3qrW99q23atMnRSF0Oy87ZN7/5TdeWPvrRj7qyoGyeeuop+/a3v2033nij8w/N0EkZfPrTn5Zh2rRddNFF9oY3vMGF5dsPf/ADe+TnP3f5e9vb3qY2vd7+9m//1lISqBMP+Vi5cqXt27fP0fV95eGGG26wT//v/+3S5WDmm266yb74xS+6NvDe977XVqxYIbGKLK6kJ+yzn/qUNetIuLBmJhgHaWagkSmkV/XGbGBAJ0HJPtnI8YzdoZFyQgCn81YYOWrS1SRwJTS/jsja5RWXbLJ9D91hl1zYbxev7bZwbMpaGyatK9Rp+2T3PCgTw63dXTawboP9r//5Z9aiAxMuu+xy61u10nbs2OE62dr16+ywrHjSyGIaAelQa9YOOADQOG7T2o9Fw1inDrRnzx73nc5IQ6Lx0+C4CIdNqriOTuM+I9PFK1evcn6OHTvmOjBhaOSc6EsYGjCdjvfQwH2DbG/RyemISR1fD11RvU8oL9BBx/XARhys/LiOq/sv/fuXXYPs6tKhDtAiLoKOAFjgVycEuzQ3XLDR5e+oTo4GzPA7sGbADmkfGrSHFKdIkwPMBFxVvRaagxo0yDt5Id5qhyVW0vQgC5gwAPT19zn/R44csSmZX4amFtm4J7y/8Lt69WoXN7RQLk8+/ZQdPOzpEueg6X+DTq/2oE1YyigUjdgFqjcAhHMJvIMe4gUAKffxcR0IK/8dHR3W09Nje1SnaR3rRlqf/dfPOVrcyesqg0Z1dO9Ij3oB+NylMnn08cfsG9/4hvNCucC5pBob7Kv/+X9cvNQL+WQg+b3f+z27/fbbpSS9y6V/22232U/u+qmj9f/8v//pwIa6AFiYUWh64H5/dMeP7bHHHrMNGzY4UKZNJtQuqEPKuVPte0hms//1C5938X784x+3v/1//s6VEeUOPdhMmxLAxtQ2m1owehiwV73m1Taqk5D+/T++IhFK2S5/2ZX2nne/3f7zS1+2ZqkYrZO9raxOBe/tXmHXDujMyeaCrcDWm5S7f6q8Tmklv0ljDJZIatHVJJ6OxLB/FLbMfp19p53rFzXp6Pa2/RoiZMZXByY0Znbr0FitLGpHe042yP/XZz9pawa6bENfv23evNmeeeYZ15B/53d+x3XU973vfTKh3uaAgXd0uje/+c3u3cDAgBtZJ2QTC390mte+9rV24YUXusZER/jIRz7iRjhGx4997GMuzne+852uEzJqbt261QCTSy+91MXLSAlAfPKTn7TLL7/crrzySnvFK17hOiyjMhzR6173OvvgBz/oGuPv//7vu7iIg4bd3d1t119/vesU/DY3Nzsa6ST4gXMA6KD9mmuucekyEpMv8tDb2+vSJB/Q8aEPfcgBwS233OKA7MMf/rCjhc5IGDog94AVfnm3ZcsWo/MBHHzD8fv/s/fewZVf153neRnvIccGutE5R3YzN1MzSKSYlGjKVLK1ltclaaSy94+tWs/6n5naWdeWxy7XeFaekl0r27LL8ihYJJUoiWIQc2aTbHbO6EZo5Pjyfr734Qc8vH5AA+hu4jWFi3r4pRvPvffcc8499xxNeuW7du1ae+SRR1z9BYvrr7/eGhsbHVWoNKqn4m3bts2+9KUvOYQiCqYGj9TK4+GHH3Z104QX4lE7vv71r7t6qv5f+MIXHMUhikH1U529tuidKC/BQ0hK6bw2qy9EESm//fv3u3Squ9quvlW9NAYEUy0Uiqt2rFmzRtEcshDCFiWlOKq/xoJHzao8wV3xBQPlKaSovDTuFE8I52/+5m8cItW9fmqDEI1gqHYIySqo7RoPiiO4qU1qn5DVj370I9u9e7d7r+8qS7BTXoqreqpswf6WW24xUYl6LwSs8S1EqrJCsKr7XnnZtq5bgZOUMMqoUF3dcRscPmh3x3osuQ5EGj6Dp/NznFvssVWwqa0sPulQqaItcLODXon9kzpEBXbVb17mt43lfiuvbkFRbjWIq5Ft3Dq7a90Sux5fgVXIDeTjL4ZfwSzeaAawnqqBoA4UxSUSftWqVW6Sa9BUVlbaq6++akeOHLEnIctvwOOyBq7YBw18sQWKr8EncvxtyHYNDE1oUQoaVEJwiqfB9eyzz9oTTzzhVtkubKW/9tprjtLZixMJBVE8Gnwi6VUnb+BqkIsi8NhC1fm5556zV155xZYtW2ZK39ra6vISEjxx4oQbpBr0Dz30kB04cMCGYDW0UovaOw5VIdZEddWEEgWg+t97770uP1Fxev7+97/v0qk9YjEefPBBl6+oDQXlr2+aGEK4qm9hUP3ee+89V0fF14RSHUUJaTIqrdqmNqrMp556amIyixVUGuWhhUTlamKpX0Qx6V5lqo80+d566y0H240bNzqKVWkEJ5WnflDZ6ishGMHuZz/7met39f3LeMzWgqG8FU/5iSK9++67HUJTPdRn6mN9U98pqA1C3B4C0FXpv/KVr7h2Ka6+1+MTQW3UT+PCY0fVl155Klvf8oP6SGXrp+/KXz/BQm1R+7XIqU+Vj5CjvutbflB85aFvKlNtE1JVUB8oH73XPQ6ckKXBZgda2C2sxcXbiO2oG7Gb6pNWVoNHpSaQ6PKN+MUM2s41CfvYkqBF+0eQSV7WHXoRtvMOJYm4otjUqgx1WbNvn338lqjVbEQGtYIB0JK0io2j1hQdsKU4NPBjM2hty0q7fut11t7bZ/sPHXSyn1UgH628QjYa/EJM6nhNDA0wdag3ONXZQjBi5TR4Repr8OqnwaGfBsftt99uv/nNb9xg0qqnQaUVT/mIihAy01Xx9U0/ISZ9173yUz5CDKKYfvCDH0w8a3BrQKuOqo8m/r59+2B7dzkEofRKq1VUyFjxNOg1cL12KA/9zp075yak2ixEpVVag1jfNMn0rLwee+wxe+aZZxy1oTrp500oyVMkJ1HehUGUnRCh4uu7rspf8BMyeemllxwiEDLx2GPBROV/5jOfcfUTUlE6vVdbRH39+7//u7vXe7VNFJVgobwFW8FP5apdBw8edLBXP6otykf36luVK2pWVOvrr7/ukL/apXzULiEqr28dNUIbhIwEf68++q6+FCy1IGgMeQhLfSPE+uabbzoErjaK+hUSFPJVO1WWKESv7/NhqDJVjtopWEpmqXGiOuq92q42CHEpjtos5KU884PiKyiN2qTFWAhb7VC5arsQt6MecZiybgPIvw1nIDiJ9KEWvyYwbA9urLWWHWTSErMMYgffuiiLyilbMthudViAnemAuiv84v7lGjDPPEpSxpXB3og/gL+/jjHbtj1itr7JMsu3mW+wG+XUkxY+2UCHtmMbvN5GuX7xEw9b84YVFsRbTfOqlfa9733PDVx1uDpSso5VIDMhJa3WojbEAnz72992K5IoD3W82My/Q4CpQaEJpeBNJE1IsY0aCJpIGlhi64QMhNBUjr55QQNKg/xrX/uae6XJqjiiGr773e+6AacBpgGpSSrBuwa1qClNWFEWf/EXf+FYU8VTnZS/2J89e/ZMIE5lLkQq6kbpNYFuu+02V5YGsuLfcccdLr4Q+T/8wz+4NqvuaqMoJW8iaQKpPaLkNFGFDLwJogkpZCD4aXLpWUhQk1WUlITWmoSCjfIU1fCP//iPDrGojYKjylE/6Ccko7IED1GkarPgq3yFONQWlaM6iQVTG8QSqd+Ur+ql/JRO5Qn5Cd6i8PRO9RRLJ/bVk6spTy1OouBUd1GtWhQEf5UjBKS6Cs56L/ZLY0hlKU9RsqJsxHYKPkKaau8Pf/hDx5apDoK/qDEtPIKH4ig/tV9t82SZGzZscPE8uaTaqiBxg8ap+lNBZWsRPnbsmKujylU+yld1VL3VLi9v1V9w0cIkKlGUfhJE1VRZjYz02xatxtkK3kt6sLKyayuu2HaCAlZsx9VdpUUxHJg4EbKeeMhGOO7jD7LZlbmsvhVdG+fzrxjW20lGL/EDY1w4CGBiawRgdU5+0LP3U4dphVEn6ieATxfSHPasGwrYXZUJBIrolzx0jfmbbgeIeIFp/569+61+++bT++1MaCUHrGusD1+Dtcub8I7st0O9eCImf3WmJprqpZVTg1Mrmd6JwtKgUhzVQxNIg16rm9KKvPZWZdVf6TTAFMRSapXVAFK7lYcGtQaL7oUMlIcG0De+8Q0n69Ak0aBX0Hfdq0whHAmz//RP/9T+np0hlatnXZXnn/3Zn9lf/dVfuToK6QmR6CqYqyzVTe3ShNcg9t5rsmpgizrRvSghsbZiK5VG9VHQvdKqHprEHvL1ylH7JEQXDFWeEIfaJVZRbbzqqqtcOlEkiiuYKR+Vrbx0r/YKfroXrFeBtFQv1VUwUzzVT2WoDwQbpRdiEyxUV8Fa5Wn8CBErT9VZ5QlWqq9kVypD9wpCHmqf8lZZ+im+goe0tCCoXYKdyle5XlA+ykNB6dQfKkv5KChv1V8IR8hL8TXGhYg92Kue6mcPmaitiqf2aDHVvb6pLeoHpffmRT5S0neVoTJFCUr+pXroWbBVHqqfgspUXMFLbRNcUmF8GbR34J8UTkN2oJAf34+/zY/fHbW1X2g2//ovkhKVlJ437exjb9p/+5tOO1yzGQfFB81nORi4zAv+qXzBTWWpr9R3etbPC4XPSoPoQ4CeykN7CaZevb0SURFTBG4lSXFFUIgbxfD3O3i1WdrRa9cnQtacxZFBstL2nonYz4/ErT3SbAN4NbZ0P7tXERyWdtExUddx6kh1oJCNrhow6kwNRE0OURTe5NJA0U/ITQNZneAFb5AqP71XPpp0HrWgAah89F1x9V1BA0Z5aYApbw1QbyB6g02TQZNKiEfplUY/PWuwiy3U9rsmh9Lop4GtMjUYNIkUNGBVN00gIWPVQfmqfLVXFICQtSaG0njlKD/lI/h47VN+Su9NYA1Gle+1XQhL9VYdVQ+xbIKx6qW26r3i6qd7xVVear+C4okK1GRT/QRzBcXRs+KpXt7k8/LQs36irLRwqL4eIvFg5pWhflBeiuO9E+wVT/XSd71Xu9Q3Hky8vnYV4p9XttIqP68PdFVc5af3HmL04micCTbe4iAYKA+Vp3uVqbao/nonmKj9iqOgvFWGV1fde32g/tKz+sirh9qpoPIVT0F5qhzloTKH2YEvB5ekQjooB96ijL0cN6kejllVx3pbsq6ZzTDY/RMn7dE3k3aoOmr9AahRqK2ZLIm4whbo3yRqnKzAglNcPra7h9j5qEzhNj500KKwimsb9ljy7Kg9ffppLKHWW3+wHFKWCYT7mFBi0BohEE/jMSc2lqP6NBjUkRoQ+nmDTauyVi11qDpbg0GDUB2tDteAUFpd9dPA1jelV37eNz3r3vuufPSsq+LpqjgqQ4NUeemdNyAVx8tXiEqDXXH03aPI9OwNSNXRG9y6es/K2ytbk0V56pvSKp4GuZ5VN9VHz5q4uldQeV5eqpOC0uq74mmSKY6+eXl68fRePyEv1VP3gqGC0np1Vz30TW1UfppsqqdHjQiG3ncPRroqjsr02uBNeMXVd+Wjb4qnOik/BbVHz4qnoO+K7/WHvnlpvXoqrtcupVHdvToonde3yktBdVZ71F71gdqobypb9VS99VNQPvrpm9Jp7Omb4nllqGy9U1nKR4hTz6qn917f9JzfNqX36q2rNx5Vfy/+WChhTSz+XbB/lRguGPP1oCLBQsNJlOuqllnD1mY7g/C+/9126+getZ4G5KCsNdW+Rovjkm26oHqpfMFuHhSXOqcY/skvblqKq1jCBUdc+TUvdj+GXV3ZWdLPx9EfudVKwcfrWpa6rDshxaqz+G4RAiUNAR2FyxmlBFfIw4+rrXBCbqHyZ2YlFTqvjQuJuEqSVTwPQgUv/BlYXg5X69iD+6G0J/+BOpSdomMWwyIEFiGQBwHmyRRfAY5cEdIS8lKYlO3lnj+Q/8WIplkXfGUiLvhxncfygbx0bstRnNxrNZGW8GJYhMAiBCYhwExxc0WISh6rObPGlIE1BWFp9shn5AIETdR5I68FqfHFAimURUBNJn6QlHhsvGU4GAREbWFqVgitMDj374UvlWqWcYvFU3bF8i0Wt1g8pZ9t3OLxXA30b0ooHrcIQqft50NqYdukhhSD1azbRIuKrV2zz9PVYAo89TDb8ovFczkWqVSxuMXqefHlI9TnELa8d+eoq3FZJs8+zEdNOBVWQR9cKDb0Zl36FYG4JGT0hJdqWWaMM3qIshIcwk6lQVTIvNDhdry7c/9erPlF5q2iFX098VI3OfiyVk3cK10u8K5oXO973nUiXt47bou+nnipmxnKRzg6GfLjTr6duJuIOhlPpYP6J6LkbvheJO7Eq/zYRV/mJS8aV4lmaJMKn8g3P25+ZuP3E/Hyv82lTZP5T+QwBaYTbyerNPlqmnoWyfOi25RX1DzKlx4B2xcgKTYsWOh9zoM1ytP+bWbubwAAQABJREFUSo4DlXGecjg/1yvi/opAXN6uiSAqgaA/XI7Rf3S6IrLaKLtBnAFDzSMLBeFPT9UwviJ6YbGSixC4jBBIcqA7w85fgI2sAPoNFb4YMmF2d1PoC+KKjNNyV1y4IhCXkJWCh8AGAqesrCFkTaurrG5Fk0UqUTuIobkOq4gdBxc3t6brfy5t7r/7NOXfJL1xftzCtXMybi6L/Dzz407GOz/PKYXzUBh3Sp58zOcwcnGL5znb8vPjqS6T5edqNl35k/GKl59Lnfs/U9w5lZ9Xv/nlmavrdG1SbXP55uLpeUrcie9ePMU4P+7laJNKyg+5eupNkfJ5df44mUztLApjqiaDPa9Eb8I6j56z4Y4eG+qS+k7YKrI5heTJFB/IXSHY5lRoSSIuv186RGgSY0gwGK6GyI3YQLoP+1sHrHldyjbeshPzHZWWgUVM+zgeQow4q4mC7GUthkUIzBYCmvAQ7Q4daD/a22uTxdDpAiiATaCcRVXJqdBmI6oMAebQy3Ryqunyu9zvtfDLfJEfpdNgHSZtNrZidBDdxXjWXnvrRYu/jp/S7GqO1EUtglgmEMTKKvPJ748gx79sO46TuHgeAChJxJXGKmNlRSVeRwaw5Dhk/fEeiywL2o03X2Ord1TaYayhjgZy2tgeNcbS4Zqfk0XNAxKLSX4rIeDhJy3/mqLuylDSVb/pgjfahPK0P5eLLSQm5DdTyulyvIzvJddiQVetEKiAlDg5gCGDDEY6b7nnNgss99urTx9CuRUrFP4K2oDgBZeAWZCYZGKXKag6887c67fLVLf5ZRvE0FmG1cDnD9lYcMBqN6fthgdXW+3GKmsbzh10Vs75Avv5lbSY6rcdArKAwBFX94tzHYPs0vOFUE/QCbhFo2nt58runPKSl6lSD9L0VwgGgjYwyHxa7rM9D+9Eg95vvdmzFmfDK1pW5TxoXca2zBtpubpfxorNO+ucSIttWsZDCHPN196/zobKO20AOj6F08oybeNevpVg3vVeTHhlQkAIRwd5xOIJYfkd+zcTOSAqC7v/SjPe5AwIS/kohL2XuceS+1+GJeAUR8Ay7DJGMN88mOHgd1mvbf3ocksCiIGDnHNM8K2E51hJLg8SFcjLWDwyZrvvvc6Goz04fh3A864WttyBV1FbQl6LCKzk5sUVUyE5IVEQ0pLXGzmkgHtyiGdmcgDEBVeQlpd1xmBCagZkFiKPDF6pSj3IV4CbN9RdCGzQn7Th8KgNhntt++2brHwZB/9xr1FyLG8eYEsScfmxDtGTarfr7t5i/vq4DUsIj+8R7RqG8f6zGBYhcCkgIKG8gpg9UVkhfuWQTeWoDHhIzUUo+Kdk+gVAdNIblM2VAIef/XGsMIAIrrQgOKAogfNXxDO1Sdt0E/boY0Mgr8valnHozw9aJYm4BhK91rKpyqpXJa3HOljZUC7NVFoEW0IRBIuLYREClwICoqqEoPQTe4dRHotgCrwKT6m5o2TFS0EPHSQHK8WvHOqlHARWxS5cbSpj1WwsXWkBOgEEjCkckFd34ow1rMY66ybs3CUHLmdTZiZqL1DygiEuYDUlwPRNPKcCCVu9dalloxztCWMShmEU5AS7VkSMckzEK5Ubx9qOSzsm26EW6khS7ppboydrLI11SVSm7oJOPgsaLoYT+OWeXH7KIg94ufR6Afmvby54tfAi6up9zb1jT2k8ri4TpXHnxVMxuTiTb7wkymOyrt5bpc06tZTx8mBFXP1og5eHvrhcHVy8/HPDMFczDy6qlVLBsLj0PCEA9+qag4HSj5fhjrPw6AKWQtyfN7xz9dWnXBm5XPRVz2nqokPIEToyxpGMeHePBcaSFgYHqZqKk19/3YvaCoO0whlseiHsXhqtsc1LV9nOFRuQDWHDi7ycmoXrv/FeAh6lGCKoPATwXi10HUalqyd+yjbuXGFlVaWrmbpg6hBxZFVICMD0HJjOhvD/h+XGJDpZfnzVLRu1gVXs7uAQI5GARQyArDBbk3ADVzaXSotdHEEzOZqNs+piAniE6RDBkB2jvcyPPalML/K5BAMZ5Jtoou7IQIhv2TosU/baYLYfu0es9AnM6JaNofqBnSbGt8zzxDGSmMThZwZfeMmRUVtWUWMDGAsMIlwdTGKHC3dtPnRtysIYfBses2ioyXrjZ5C7hJxfyiAekrJoR4dwODIwIkEspwriKevFJ6W/ImtL0o2oleDlOSmfeth6CvRYWbza/Hjt6o6jqxQ+jtOSRkddpFFUjKteIKYU50FlDljt8LEVV5Ytsyja2ANsx0XCGAcMweqP0G8V6OFZu8WGsXfvxwYazR6kLbgbscBolzWFSJdegmJkrQWr+60fFi06nLZz0RPAr9xWJdZasuwsrjKDWLfFkihOUdoi2GfDwN2SYCO0+HH8Ya7gCMs5K4tgJXWs2YKhiPVnDsPmVDG+cPkVaGdSongJkomjIp6kHWLnYhn0sBh7nfB5fZGgbcHibjI5hHf0gF3TPmgb30lZV0XI3tpebW/WZKwuhd0y2j8CNgqmdXrDbHN3B/bAknb3DZ+z1upNeMWpdXKyzyTPWmf3afvxy49ZO6RcV1nMegNYag2EyGchTnYwGN1CURxtYtEO5Kxjc0kbws9iNFJjkTqMN67CJtj7k6anAeG4jTOhfIWFoy4XDHE5GQIDwQsZKbq5FSlj6zHRGwwhAM1iKI6Jm2R7tpRDRTxmlQxMw4/d9tXXWkP1FhRig9bVfsSOnnrTMrWDeDPmmEWy1pbV19jKljVWlmmy9468bKeTpx3y3rrmWozVjbCrg9MMtqmD+JYcCQftvdP45st2WoRzZUtCO2zbhhZ7++gvLRBD9pcqs4bwJltStdbK62WMEMcSyUMYdTtjiYpRlHZCtqZ1q53seAfkwuAdwW585UbbUdNge0++bltX7MRgr1yuYzk2G7VM5ZAFOHjw+tHnbfu6G+x4lwz0RTguMgiiwpopgz+bKmf3TcqWDGLyD4JMasvqbX3rBouBOAfH2mzf4XdtCScZRvxnQQ5jVh1aartW7rH9p4/gx/IsbFncmspWW/ZcBuusK/CHKW83y6GwcWA7PGr7ehAHBOttV+MtNhrssn4M4fnO9dq5fhCF/xzqyHW2tGaHdbVhQRavpb7UEmuoWGZlFX473d5mO9Z8yvYffc8iuN4aQFheNhazDa3r7XTfWevL9IF48aHoD9oI1xhqDY0grTFkp8MoPF/bm7UW7LFvqV9tHWc7bOBAl3XtWmLdSO01XTVkg1B3dfhqXI5F0frmJnvmJ4/ZhuWn7No772Iqx+3V539hZzoP2tWbVtqB7rMYBuR4Ggh1DGRZioGunAiimP1B6gkR0bKq1s681+2+ldpGmIc6Jyr+Qd2IwBfyctIEyGrEA25VwFOiVdeWg8PGsXkhhnOE+wdVy9mVgy9si8Zr7P5bvmBXr73B+qGKIpm43XPDR+yeXZ+16FgNpEbEbtxxm330hrtttC9j/T0Ddt+tD9ptW3BsgRuoqkg9q/Yau6b1Rrt1/UesIbocN+1LLRapBjnAjoBYdq2/1fZsesDqw81YqMS6aarFNjZtsevWbLMqFAzXNrTabevusUeu/w+4ZIe1Ssfsnq33Ew/WhX3u8nSF3Xvt5+xjmx+0ymS91UWWWGv5Stuz4Q67YdVuqw82W1NVk1VB/d6+Zo8tD6/HRnm16GK36yY7Am7LH+rRJ4scyYDVxRrsd/Z8BoqtyhKDbbaqZqk9cMvHLdHXAzWFhdhEo62tv9OuXXmf3bjpdouMZaxGFOdQyG676tN22867bUVTjS3H9dySUKN97KqPW0NmlTX5V9hHdtxN3GprjDbZdur30J1/jPzpNqvN7LY7tj9i1ZlmK8NU+o7m6+3+6z9Le5E34UPwquU327WrbrLQIO65QBi14RV2244HrDxVwSLBRg/U4hiU1wCebao4s7ety29rOjN21XDU1rzZYRsjjRZrrLbV0TpbdqjTGofRJB+nWMLaeWRyLxs5Z7tXrLJGxu4jv/MQzovXQkWfBknts91Xb7EHHrzPWqB4r8FBa93oIAsDnoRgK0sx5BCXUIG8VqHmwdnfkXS3VTblvEt5St6ltIO/YBSXVq6cLq/uQGNQBFDSUBxxq2+ogi2R6ynIajrb55T6iKfB42QGpdX9fg3klu2Q2K326K//xdL4dwx0jdjBI8/al+75M9t36i1LwpbsWLHV/vnJ/+68rKRZ7d944Sn75M13m//4qL16/CkLDGdsa8sNtnblGnvl/WesKwwM/Dg+CDRbRXCZtdTV2q+ee8yu23mb/eTFc2g/MzGtHyR2yN7b/2PM/S6zTG/Kvvbwn1lqL+gGaigJkolCJYVht9av3mHD5/A6jcv1tJ8yDz3DdxwuQA364ONeeP8pwAuVG8AWPaxaJQjXh8duP5M/CrLJ4F08I5JMXeH6LG07tm+2tw68Ycf3n8DSQKedPHrc7n/wYWARtGGcikaSzXbNujvt9Vfftxt377SfZn9go0ns+0O1rWzaaK+8+ivrHn4RpAPrG2iyPau3UFeZo8abDWz26+/81OKwbMeH6uwTD/yRBcfwDO6nbogRwlCoW1beYNduvcUeffJRa0/ug1ow3Ik9a/fvvt9OvPuW9WQ6cHV/lQ12AMsxxlM0R/VI76oW9jiOgH0Epeb6Cmz994zajkH8Pm5dArvaZw2RuC1hJ7t+KGn1tVU2KARUDgsFFdma7bMaTnU0LG+xw6/8DPYd117Xb0Bps9P2vd5mlXVRa4EtXoJBgHrG9CnGcowFYYposUSGsVRB/FLpEAEBG5yAIvZjijpSnbOMKoSl+enHQ7aHxC5B1YUvNfnnFRaM4goAKLCQA5bqHwxK4iUD/6xo2uZACI/dh/FG5bWPCV9qgQ10W7Nqu+1v22t9/tM4ph2xcwH8G/qP2P7et21LwyZrqWi0tt42G8jC+oT7rSN23E6HjtpjT/zcOuN9sCIcbYIdSzKI0gyUYeRd2Qq8CwOLLFTLulVX4bLqHevq2G/N1csMhWcbS+MdJ15lFbYZNm27jcCy7sDzTncHsh8O2/mQl8V85VgaR2aBN6Sd266xp176GdDGTrrKCOHIIxR3sps0iEDP8WjWRtjaR7AE5VVhSysbrDJYAbWCB+pRyeioDzKuDLLIEGzaL5593A6feNuiVQz8UIOtXHGNDQxlrCJWDQKstPIQnp7LgEfnfusf6bPmurUgJmyyw6QOs2tVVVkLghNL2oqXnnJGADb+00EbHMpa5yj5VGyEfW2yxjo2a5COBUAMhkwrw7e1TdfZx67+vP340R8j8zsGOzZoifCwneh5DXkbOkkrb7VGf4utbVxur7z9G5ypgHg9yonFpmk4aSPMze4m7MfXBGwUmWJtQ6UlomPWU42tdxyj+lDOXDXss6WwpY1ZbONrEsfHoFgxH0570j2Hrf0k/jYTXZbtOWGZvjY7d3IvsrdBy0KVBTvboXTFeiHIz9lPLrXhy/TzKEHQASS1aIM0srx0MCePu0yUVt6knjtIFpTi8gGh3M4RbWCVdwhY2zsMLoww557VJgnlSxBhqWoK2fiQ1VZW4Ty020JleCgKMhF4H0Bw3jGQsmZkN7hitQHcNwW1EyUKkkGcwSGILwRLDL8zgBypMYpw14+nmwBeiUOogWS6zZcsY5cqaluXbLbXf/MLVEIYWJgi2bh6lx3ofx05+HJbUXeD3XxbJdMaN2RQrX0noFJBXFHYR3HaowN+23PX/fbiay9ZPHgWmCOv8g8wkYeYYDqTNiiBFcercE7KaYWG8iorY3dtG77/VpRlbDCIf8JIkx1+6ZQ9P/pTkAx9w8aDTKVEa0LWN9RjS5Y12rblN0C9LeeZTYfeMaimKtu+4Xob7DxDHU7Y6SMnbEvrbus5dJB6DdiLLzxhD9/zWdu1s8XGEhyRwAFKKNvIPkwDwyFmDbH1ductv0Ntx2wJyHZoEM8zfrbtR3BZF1tuN+66x3phB6+6epv97MB+GySNqPiaqM9efPnX9sitX7X2X5yGQktZ99gRG64YYoMA1JgJ4/0cxxUslo1jUPjHzlgL1Fd2+KwFKlst3Byx1qZqs1NtsNNsOuzrtOYY/XbzBnsXBxMB2Ex/us7iQ7iFQ67WUhu0c21vW/PqpZQfhXqrtnfefclWX3e7jZ7E5R1OMkBzyAeR6IdKcVdcCApnJ+wssvpwz6wUdYWcuVTDgiGuQprZacKzG6VrBlIV2E0TJPtidpVQCMMijXSfQ0DcaqcH37EMMimNgUS80hpqd9nJvY+bL+az1U3rLXwQeRPON+GD3Rb0to232bPtoDk8GkmeFORbIFPBqgd7xw/K3Zrr8W5cGbO7b74PVoTT++Equ3rrdnv3+WfMH+m0d8/80B5797vWjfC3MVFl3/jI/4Fbt1Yb6RhAdhi3FUvXci4N78hnTloq0sUUwi19BiE78p4MO4JBZF/BNLuAvEujAxAHxKIs9uIN+dhwl42hjLit6Qa777pP24tPPwtVDOKDpRxldzIDZiyHzdp//IC1HzkFa1lnn/rYp23lklY7dXLEdq29x8oSffbJez9mmdAKi9autWf3/jMebhCAb1yB38azUEPPWVf6GPk22tfu+zLld5q/vAI27pB9/+n/AgWKjtRonX3s9s/ZqhWbrffYINRRpz3x5LeAUbN99IG7rLq71c4OgByAQRnU/OlzZ61t6Jjdu/tee+Pok+aPjdgwyDrJjmuUhTAMxXUOin7zaMhu2D9sN4aXWBfsa1UrW6psdMRGMa4XDlk1TlJX9aSsraPbEjewv4EQvxzWeaAPKqoOv4yJftjIWvwkbgIZs8HBruiSxhVWj2B+tK8D5B2yVAIbo2EQLru3Hg9RQsOXquSIBUhpxgCSTBYknQzIiu8u0bCgNYMpFHPoKCxOVVuK1TMKGxEc8VkYe1txqJIytqezrM5swiNQZTDD9lRkoRBKKGTCS+3Fw7+yu254yE6ebbdofxvWJRm8VattacWgPT18gMlbbpvKttny6A3W29kL4qqxDWvW2ZqmNfbC4TKE8GOs4gwcKJ8Uk0pqDqOGYDq7xK6p22jvHnrZfnnsl7BA/bY+stH+lzu/aOvjzbBVOKqNRqxiqAGaDuIN91ftgWMWLQfjwSaV+ZbYtdfeao+/9i+wgTggjVcwYVERgKLqgbKtZrIJuWXZSYpASJXxLgqVN4DKQjqK0zec7Ybj7Pbx/Wz6CKxlF4gVdYtUFGFztd1z00et61SvvX/mfZARk5Q6BNEVCNC++gY/Zb1tj/3m30EYIGfY2i/c/yXbsnQjbG/Iyqua7UgvrFb4ODIusWVD1hXopu7LHMU4gEA7NrgGikkLGQfuQZblqWo7lWyzU9lTds43bAOxNyxwIG1f2vW79t2ff9dOBE+AnJMclE7ayy8+a7fdcbftZTdzgHGEH2mn9iF9MB2o1uCvHshaHJYvUB6whhXsoWzC0N6SevMNbLYzO17Ald0Ji/5Lr1UlVtpbrJeDUFzL0Qs5hczrVF/Ujj79vnWizhONHTVfTbsN1iIfPJO0/T3I+nY2of7AbmKEjQs2DoJOPFJCA3e8Kj7qLzmtvGT5/egDijZAtucXp6P56X6Mp4lwScQ1ynBa8mSiqGluFhRxuTqNs4CitOSfTRRGHB2iUUj4MpT6kmwl+4gj2UQA1hLXmtM0ZeFeD431WybRYW8dftVu/8hNlhzsgkqIW0PVSnv6pV8if+m3AeK8fuA1u+nWG214CIetCMsjCG+ffPaXxIVyASX7oKQyYag1tv+H4tLhgm0EYbSuXWY/ef4nCNLjsGYxazt10rpGBq2pZZeNgUAa6rYjuN5AHmh040/v9NnTdvTsMWuKbID167ejXXvtdPcBi9Xj89CBj2UAyiQogTsTSpNcglkfTnhH07BCqHYMWq9tu26TbfOtABnhkTpaYc88/2uArL5AGslPAvWX33zFPnrbR23FiuVMzhGLhSvtWN8xO3RmP2aIbrDDHYdpGV6Y0RcLlCXthfeehX1ci4rGYagq5Fgn8ZSNrlMGvTSqT13whg0yjpWVI/fM2t0P3gEiRO8qUG6dsKTvHnsLJUl029ixSwal35e1vfvetGuXXmVX3XCVHXsTBMLmyJ67brP1/rX28sGXrB/461yO5qOmoQLDiTN6OBZuabRUXdh2snDUrW41/8qVlqpage5bubWMoCN2+ri112bsLBRTTwXHggRj+OlsFCqz86zt3L7D2oDBe+iRHa2DZawM2+6jo3bH0mZbC4X5i7737N01q6w3GrCakdJjE3PQwBoLANHR8SR6ZmHaCmlJ/84br+Synfn/RWVeLPEH5FdxKgJKMnAD6Mokk6O25KO1Vr+7xlJM2pGxXgwKMqkYaLhnBbRRBh+DvIRCVRnKm2MMafSFYjgoDbLzFo0GrY+drCAykpEwmtjUHWUo5BxmS+qbbQydqxEk7OEojjp9Hdaf7UE7qc5CfWEE3TFrR4jvK2f1Q41mVeVaa+tHTlQJ+wI7XZuqtygUnDjmDCynDC9KJiEzJQnYt5RkKuXDVjaKhdhYnXWPtNlQGYgQkEegWBvQIevEPehAEE/fyHvqUOVg5FofCrE9fImh4LmqYquBb0CqIE8QaX//ELpo5dZb1Q7lgINd9LuCqRCKrymrqWiwKHK9aFXA2jthwFJZPGvXgKjIc4gGcKBvFKe9adQ+osjrGqtjdq6bfCK1NgK1F0J+lJFZAlatusoWG+oDblBYFTHUYqC6w4yLwbGc63qpo4s9rYLiOdvVhlwuaUtQ68igE1bfVActCSwR+m9t3GS+Xp+1QzGmQCpxDhHr7J2fukdpK1UCIfdD3VbabT3suL78hN38sestc8seEOr1LJYwdedeM3t/n73ym6P2zupN9uO6PuzEBa2Wndt4edJWo1u25Win7Vxzjf08ccqOLauwqvo6ix05aPclllnb6VP2ysZme4k0PrRXK9RPUDelGSRXZkFjcYywaxsZLbPRswk7/E/oIDKmc7uKjF9J7seD7iW4n6dDWEbvBYMK049lZyqXXSxxM5H+kN+sqDFVuqqqyjWANNMGUQJyva6G6ieLjPkhjVwogD6ETqsPh0Zt5dYVsEqs1Ox0pSFfsZjNisAAEOCExUooDIz0uG34DDS2j13AUSbOCDKrcAx7R8h3Umyrx2FzpG3uC2PzaLjHURZjCOJHoAYyEXncltE2sypW+mF2pALIxIYyeEqOSEsM6iaG5jaTNBDkmkCOw+o/FjhDnA5W/2F2Cbsx+YNQ39/DYdkeqI5aRDQB6xk6Y1YRR9kSYTnwTSYoB0QwnIUVhNKLgVhFdUWgYkbYQRN+DZIuOTZEuciyoGwGkrCLlUEQAiwu38VSSEVFVgbK2HiIQ6XhctSO9xyxWDU7hmw4jMDm9bNbmmVnKsm9D/8AcSgyIbMhNjP8IOykn28BehlYZaF+yivLrW+4F610xgjliOoMoX3fOdzOJif1B07xIJRplJMAsMzlVVE4yJyb+QpkgCNp2kTZ2p1mBoLghHtRntXuGHDX2ArD/riRxBCqElxD1ei4IXgf6rByqKLqZTs4tLiJRQEqeLTT3jz0lr1C3OONdTbMbnEWdi8eq7JD5JuOwH5WVNmRvnN2aAQl4comWMwRO9NxAqrRZ20tS+y9qgqQfBlqFUHrxvhlWGZPSi0AE5n0SbNxoU0ybezUsUnSua/Dho5DpTLHFTRvC4M3n+UhXh66vWcvXuGz3uP5XBPYI369qDNd/y8+TqF0ZoWcZspx3t/EPzs2UW2Qm/XcbkYwEIV9QKjZi0SiBvfq7DJmFZcOFzsjxJsRqVFCIczk12TxBTUJOU6D5UitEzqyk4nKDC5qH1AQUiMYY+BrsqW1otNkv5Z+tQgNbjn76M0gv+OdKKmo0pDvMKya8tOEE8XmZ7enP8EgydY6BMUpHhcgtHIBMI1xUJhtMgtGoXYkP9QRK1b7MAvIkK8fnTlkWeyOpahvGp2dOAjUB+sYZMs/DUJJozqQ0kYJpWaYoNA7TFLVVX2gnzSsaSMyH0g9ajnExC2zIXSbJsZkUFY9FF3tgcWK+Hgeos6oHACTLDpqmjARjkrJkcMQyBKZMJQRbKC2N8m3m82BrFyWAwDtzWVB/mlYVGaIDdNwCZEDIMUB5KCIZYAPY4l3gykOFmXZNaWOYm3ZRKQlGu5EolUS+HezUmjHMlmJ4uU2juyMDdrpE2ctVtGOTKuPncE37f1Etw2u3WL9qZRVUddB8hgAKW1OomsYgFWuw+jecjTrO7SZMmZxFputS9fZ+2Vl1kF/auxW0ZYM7auiXlNmHzUpjSBhPAif9oG22CRC5oos8eirZ6wi0EobcpSVZ0qqFOq8cIhrYgKoK3MDU4PKT+dWoIV95JWjtvYOzvOxMo8wCRn2LpaGfqmFLJOdZcpNEAhnEBVgZRBkQACo87GDF6PKWmC0Wa/VbbwFxEkiCwunYX9BzFJKFdKQrEFIOsr7FPKnNBNPds0j6RDZoiQoVQTKc77ypgOGBMFgOfYN+UEFiUVhEmlfa4iVPwSLGCI/yTbSlKE6SegeZJcxDSvl2HEtLCAVYQQhWrFPAawI6Guux/gvSS4/ITi9zwWQM5+EInJtpl9BzBmpetAWSDK+g6hBYkFNCuoiBUj3DaRFBO6VB5UCQWk6uQxdftzqGRij5gtsQaHAYwz2XDBTu7QzJu146cSFaIurmcNqSuMkOY7C62eR7AslrZPuiyNDa+eE8etdJ6znpe/YUvBvQmdJa5qg1DSxJX/T2INzYHdQP8RAmFwKWX/fmG3ShsLoiI2h23YO5NkLnMMsYBVwFiMsvsOM6xrkkRzbLMGQQ+Q6HZDhTOaSSIMdQYkWwRzULRCjD/S7xEFDaN6ZLizicgNT4GCgci+SVPIaeR05/vZRW3NtFWQ+qytWT31MbPHg827pJYZ6fnZ+JjvTjG7QpNNxGGQ/PCL25pl2adIwfYS0cgiMFqshTNY08ho5sg2lIxxylo8iTQ6oEPQpgkxCZSvqQJM4zHOA6xjPCcrySW1kmiBBvQToQnjQIMRFZ4zyArBtmtSYHEdcRj35S4EsRaDpILhkQMx84unwOyiDd3IY6vIBmQR5VqVI7togyYjaor4RcvKCmhfQbFcDaHtGyAp2T/pM6kshHpWlhcgPghbN5pAU0bX6Z8Xn8dbBS5NGOJInBSEfIT4hQEFVzGGSNOyN8QXqiQoJXSdoVwzqQVRPCsShMoUQXX2Jv4Td0jg6bAkQXxAF3aFQlQ34eoDWCTvJ2ccMMrYgdtirEsgrEVm0se3aRJc2xjmHWpbLsxxqakkCbX6qPwSiGoQaDvgqoWbZIRc1mhi2MY4WjUCeBmk3ZgdzjSih/5PLjYhczmJ0j9rbrxywRswPZ7SAjCMuXUV1XaLgdee8siuW+AMSzucm6NRa56oTwqZRBnnCSNOg3fjFm6w7hKY0bIdf5jcYdaz9/DGAmVhCFjpbheSHNxrwKHF+4EG10QLiLSKiEhRy73K1dS/454HciyH5XQ4ZeG+UbryFLg8hB4VJisahC/du+n9enXLlTeYI28bKKkTo1cvLLb9MrwUqVWlzbRF6nvxyftle23Jf8p9yeXiDPtde9VcuCOEoeBDIXSdhlYuV+69vXs652rj66xWf9CZ3y1tucuIFL2cvnfJgGQGhSX6qp4BDyKKGRemJemb/GkQf1KQFiQpmSYe4QUwgzSTplIdyVO3FTqk8ZyKH79AuvFfNcrGEw1WXmaBH1MsSaCXl5mCdMw3E4gTSD/ITMu8LHMc6S4xtL1RYsi32/P981UZRManDekhaGHma4LGQCyGcX0CKS11aGHLvRJpnEWh2n+qyvc/st613rkPwCvGNzCYlIQ+rAnQKiRkIDBIfukhZ9L9ywVs/CvO+3M/epMiVk3ua+m5qDbwYGt75Ifd+8t1kHsXe5aec6d7LRXBjDrowmd8kKsh9KTa9lENhXXOxp/ufn38ujvosP0w+5+J6tcyPM/N9DnVQ/7zCvFu98+6n5pIrR8jEjR8+5uIpgdouClX0Hu3NvSIGbKhDbqII9TQ5zhw6Hq+6bHApiEEdv+VeiNG9XpB/bq64RV66WhAAmkNc5WJNoTpWY8N9KY52Vdq+Vw9Yx8Euaw4td21fkArPotAFRFzT1y7Dtr62r+qDrXb85TYnbN18ywpLIj8YBoHFYTk0rNgqYnCI9dBWlwYZspjAsBsoGjXjY8kNIK2GCt748b6563hc940XXhzvqjj58SbyGY+bH0/f8uPOlKeLSwQXn4dicb1v+XkqneIuVJtc+VTI1Xe8LnpXrK7F2uTFLdYmZeLapkiEuebp0pCBl65Y+d43dy2IO1uYXl4VJ9f0S/ZPu/G5vVTkpyCsSQ7FQceSfbW2MrLCDr9wwg4/dRilnAbYd3gYZJ+SD5ZiKEnElYUt1PANIztqyiy10893Wv/ZXtt5z2arqK1AFnQOcGplhKVkaZRMJEcNgMzGV5ELwTvXZeNdcoG+mYg723jKdrZxZxtvLnnOJe6HsfzL3CaNBye+E5yvgCDZpuSrbOuAwsQKCx3px3/Y4LJUkz37k5ds8P0Rq08u4cB+NdwNUkKRiRODv7QaWpqIC90eCa79GMqLsSPmR6ek70iP/fI7GL+7bqs1XdeMjhQmSVIoVbJR70dPR3JhySQCnLdTyGcd6J8J+Hv94I1td/Vekq5wxXV56R9hStwieU7EUdy8PGdd/oXyVAFevkXiuvopDiG//AVtE3VZ0PKLwMnBR/8IDmazgGlhPDYOr5ggg5xMDnbsobzYrAjDzUQ49o/in50+2W5v/PwJLM9GrRJF5PIApoxQlvWhLuPUlbzOu/StFQTzh+ycSihJxCVayo8g1FFP7DLG2KbOYBEhzVGgw0+dtGefP2tbNq+wLdtXcuylGfiidsDq4P6wPaXJnYOI/udGmP4XukZ32+0T0MvFzcWeGlcr02Sek/B1cd0nLxXljne0csvVIieg1VOx8nPxCuIW5jmlfKXIlZcrfzJ/l8ssyz+/Tbl8i7ZphvJn3aaC9p9fvmo/3rLC9s+6TcrA5ezymgL/wjxnaJOw/mSPFu9TJ5oXyIipuO52Sp7jVXAxvAhT404uLkqdKzHX1PzSlQ9tmnh1ftzi5U/G86MHF0JHT7vbGXZBe88O26GDZ+3owTOYSeqxNVUtWCAp4/C+dplVEyg05tNlUIFQY7yQA5n3NMdrSSIupytEs7KoCmiXZni0P6dBjtJlGUeJq22jDb7RY0+88Cx7+FKXQFSKY0uRv2Oc4VsMixC43BC4kmRcssMf4EhWFtWONEfNpL9XCTtYjxml1spllnLmQEBUqHgMJdCtQyE5gOqGJGNCYqUYShJxSRnT6Q+h6iCFzArO7sWx8R1ia9qx3TjQCGO0rhJDdxm2pWXNUtu6Ken+hDhgtxgWIXDREMjtGuZoJP6zkIrq0VvRdFK5zaC3FeBEgFNpFcVPJOm5Se6qLTltH2kzL8BmUwrNe1kQzWhn0ulGKd34M1d4Of6Ldrr0wScWEeViH3qC2TL03eBiQpJzwRJKsywlzX6OSum8cAiFbx0B88FaSha2iLjm0B+oDuZiq9Pp6BTuwANBbB3xNof/pVsiLXC8kDAy9FPgOBrfczIu92Lx3yIE5gmBLIunFklp5Wvzx1FYIKYou21CSHo/gFJpDCl9CEXTHia+jqVVjOGtCg4gwtlblM4dzZJGmVjILMwRpkHOmfql5U/8MSidoA+DhU5QzhEuhOc5nb55VnqaZJ4CKWettBE/cfZQs0YqwbIdKNE99kr0YiLg52nivtRuSpLimguQ1CneIVCly7+fSz6LcRchkA8BndvTeqhD2e7IExeZ1E7rEDKK0NXcl3M4e5BzpynOfFayO6QTE9hwZYOIw+cgA3MOVTmVALUVCaFryAIciZS7s6wj2PIPYY/eOR2BrpHOmIc+8utxqe7z5VXeQn+p8l6IfK54xCWgfRg6YiE6f7HM6SEgEYWYuShnEqVKIF4RkStmdHBpBl1/E27mhlDReQsTS4OYsihjhw49e/GRCLqx1DHabitXVWB/7WrbdjV200bi9soLr9pTzx2zgUEMImIKKM0RrqRIOW1EQfk4VlFk0GUIH7Y5IjS/GOYBAZ3ZkikPXfUTpSe7RUlMNGiQ6L6hocHdp7H0EJDFCEIZVgMUV/G0CiqerspDcWT+R9/1PDw8zAqNtYbx9Fq5o1FWbvLXvdLG8aKsPHXv1UHlOAEr+akc5aM4epeTtUhZl1We77oqncpV3ipX9wreVe/0zWuD6hOL4SeSeEovc0Ve2bqqnJERDnKPf9ezQjkmexI44vDeK0/VT+/VDtWtp6fHtVfPgovK8tqiMpW/fl585as6VFQg76Se+ulZZejnwdTLZ3AQTz3jsNB3vffgrKvKFMyjZcAW2/qVlCkzQmHkU3J6Uc2B6VBvu+1C0fmzG5faDmyJlcMqZnCekUCHcBSTTKmBHrvnzpvtv/4/f2YPf/YjtuvalXbTHVfb1/74C/Zf//Mf2IolEGODPZyBl1xLSEuH3LWftxhmC4HcbJoau5nHP+Q3K2pMg2h+9rhy0qqpRV8ZT5oYW7ZssT/8wz+09evX20c/+lE3mY4cOeImmTd5R0dH3UTShFMa/bwJpJZqkniTSxNRyEcTT0GTurKy0tk40r0mvODci89GpVFcvdNVdpCUr/JT2UIGN954oz344IN2zTXX2LvvvjvxXmlVD8VV8NLoXnnou4fchHy8uPqmshSURvVVXLVN8YQMdBUCGRgYcM/6LqSjPHSveum7rl69dRUyUZ6q7yOPPIKp6Wvt7rvvdvmcPHlyAnmrDipHZesqWAnJeO9UhuIIBgq699qpb6qfgr6rXOVz++23O1jddNNN2L8/4+rm+gAZVRZ+LzAIMiRtBkOG69gkGu46ZvdtXG63rwzZytY6840M2YGz/TaI1dCMhGJ4tt7VOmK/+/DNdvrMCziBbcPOGHlx4uPYoTcxzvembVq7wd7GUmuSg/VgVnbPZRFEQUqhVxYtIfjq99tjj8t11JX5T6v4xz/+cfvmN79pfXju0UD/oz/6I3v++efdhFiJ+V9REZqwr776qgyn2Y4dO/DajGG5996zU6dOuQm/fft2l1b5vfbaaw4RXX311dg5r7F33nnH5a081qxZ46gaxdGz8tq/f79LW19f78o8fvy4m5BCOsvxBN7c3Gw/+clP7Ny5cy6N6iBk29raaq/jBEPPmsBbt261Wpw9vPzyyw4BrFu3znWKkKKH8NQeIVGF7u5uO3jwoN16662unkIsR48etaamJofENYA34B1I7Vb73njjDYe0VB+1XW1RmaqX8hFiE0IWHIWE/umf/sk6Ojocgvu93/s9l4/quW3bNgdT1V2IWm05exbP1KQTDIS8BANNIsUXUlI9BPO9e/c6JKjnFStWuPz1TguC4Oj14dKlS126tWvXYiMsY+fKgjb2fpudGcaUNDbJlqP07Oe42e7GCmtYA4WEZdlV2N+qw5xNF96d0iDkQHbE7rwqYOtbMBbJ/ZnBQ+AyqLBRKNHRg3bVhjrr6w3a2uaoHeyQuZu426EE3bq/YpSEA/zivykQuLLQ+5SqL9yDVmtNPE1iTTz9/vmf/3mCArr55pvd90984hNuBRcy0kQ7dOiQfeELX3ATUJSHVnmxRqIuhPyuwieiKIO3337bPv/5zztKRFSIqBqVJ2pEiOn++++32267zQFAZezatcul06TVdyEMIQEhRJUnpCMKTO+FLL7yla84ZHHvvfe6b11dXfbZz37W3QspigISclm2bJlDAtdff73LS/XQ5BdyEzJ78803XV2ENIRM9LsdCkaIQMjjU5/6lGu/4LN7926HKFSOELeQg96J+hJlKnZTSFkU4i233GKf/vSn7dlnn3VphCT1TYjp61//umubYKb0en/fffe5NIKhfkJae/bssU2bNll7e7s9/PDDLo3gfeDAAdu8ebNrg2CtOqq9f/AHf+AoZ9VHlNnDH38Yj+RYT4UAHuNQcgrjhX6oq2pOaqzD03q6ASslDXg4wvprA9tyUWzQx1DHieLRpxXLt2nKTXX14sL+sPn6UTXoHLLT+/aB7J7H+NcpW7m0GrUDUYBQadoIoO8knP8tChclzfutgtSlHBRCXpo0Gvz6tbS0uMkt5HH69Gk3OTWJhDw0GXUvhCb2RbIvxevs7HQTSxNek1+ITdSDWE9RISpDk1MUkVhSUSuiElSeKCddxd4oL9VFk07lKO2TTz7pJunPf/5zhzSvu+46h1jvuusuh2CEKDZu3OgoLVEfjz76qKtbW1ubo6BUJ01g1VfUoxCeKCshoRMnTjjkdOeddzoEprii4NRuISF9Vz1U18985jOOOhUcRNEcO3bMwea5555zyFFtELJVHmqHynn//fftlVdecUhR30S1qf133HGHa5v6UeUJjipH5apegoHXJ0JSP/zhD115//qv/+pgo7LVflGkii+qUkhOiFnUrMpSm1WH45FuG9r/no31dFgsG7dtKGd+qjZqd4d6rGx9rwUbUdKsi1hVLGr/24py2z2Ir0gssQ5jiysRxw1aYMz6cEqLKVrz92BDfwjkXIfqA458sZ/oBPQ44cypWUj8D4WHZPNSDtFSzyvHHc+zlouIax6AE4LQpNREFHUhoa7YGg18USOiDDQRNSk1YUUtie36u7/7O/vxj3/sWBy9F1uliSMEJbmQ2E+xWN/61rccAtDEEjv0s5/9zH7wgx/Y9773PTcBJecS5SBEKFZHeWnS66qJrEntlS3Ky0Ms+1jx//Zv/9bVQXXyyhcIJFfSZFYe+imNkKnyEzJQPT35lMpVfb/97W87NkwUneAhJKefJr/yELWjOj/xxBMONspDSFBx9F1wE/yEmJVG8FO+qpsQid6r/p/73OccYhX8RMkJXspHcBY1JsQrJJZff7VNZaivtHCor4Ss//Iv/9KENPVNeSmNKF0hR48ddsh6CEON4JGBiPAMRhUTo1aP3f2rbt9qiSYoqBYso+IBO7IF6hs3aUE8NNnYMGoSAXvrbIP1nA3ZoZcOWevGq20vHsaP4O8yO+K3d/aOWnuPz0629+AEVweeESWj6AnZ9duFtuYx7/KTzEoAn59g8Z49ICaDJqOoCU0+TQrJZkR13XPPPW7Si2qS3OcjH/mIm4Ri6W6HjdJk/PM//3P73d/9XTcZNQlFpWjia/KKwtJkFOJRXFFMX/3qVx3yEIX21ltvuQknNu1P/uRP7KWXXnLyH01CITohl2eeecYhUsmfVq9e7ZCVKLOHHnrIsVXqw7/+6792yFSsl5CFqCkhFiErIUEFISyxpUJeCmKpVEchTlEuaq8oRLGPksl5SEkspSc3UloPQQjRKHzta19zCOPxxx93bLDapfarzWJtHeIAMantgo3a8clPftIhG9VNZQqxCrkJBkI6ohT1zYOD2MxvfOMbrq5ChGqjWN/f//3fd1eVqfronZCZ0imO8lCdY2iaS7sqw1GyFEfNUpyHbcPT9dbbllpmY6ulGjZYtgrEsxmb9O9X22C/aACOn2Ha+un9Abt6A0jXX29//6MXrHtVi7VGG6wZj9m3XP2APfN6p3VAjEWrGnBEgja7g4pQ10URIeO5/HZcikHqA7KAWrpauRfqem9wa4KJItCE1zshKsmzRCFpEgspPfDAA/ad73zHTQy9EzWkqxCevmvCKIgC0GT0EJje6V6TSVSBEIoQpthAXTWhRaF43/TsIQjdqz51dbjrguqRsF9IR0hNSEVyNeUjBKQ6KD/lpe9eW4RENLkbGxtdvdROtVfIUXVRfVW28lD+iq826ao4uqo+QgJKpzK89gshiyJSHfVNdVJeSq94eqdn5atn5SEkqvxUD8FE94qnPAU3tUN1VzqVrTiqp+Kr7YonpKs2CpEpbwW9U1AdvD4QLKoqynFQjGVdHKEk8UDUMHjKHljNpsxntljVTbg+a74F6wr7LLH3JXv8f5yxH7w+bOeqpNpCPplTtr0hY3fcvtyOVh6zToT56+vWW9k7bdZ1uM6eenGfDeGsF/oMYoszhFKFQJXCTxtl4fdKCeoT9aH6QuNMi5We9fNC4bPSIEqQ7ocG/mREL8HUq3C6fue5JyuWcBFxTQXeBZ/UcQqaQJoYuqrDNOE0cTQRFsOVBYEhju1Ux7FMkgARYx/OF+m31rI+u3f3SrvnUx+36NpbrK/rDXvux/9mP//haTs8WGvDNUFc02VtKYL89AiewcGNvpY+237HOkvjkevQ022W7Y/YEFRWKsgChAFMHKMBGChazI9LCXURcU0ZJ9MirkVWcQqc5v8gROUhKY/q0AouqmAxXHkQkN15OTJJ4MeS44k2DAJLYuvtOz8/Yr9+HQq69tfW3XOCnWV8WGYaLIVDjHQCiglrJaPSzwrVsIChlnE6aJ0/POWc5waHsSyKeqT8TibZqczAfrK1A3BQCBaV4uzY/9YoRBQjmmY9UBYR16xBNX1Ekb9iU8SuKIhVEssiSkwIbZHimh52pfqlKpXEA5CcE2McHDMwUTxP+fDePYhd9gMDIJrBcxBKWOPFK3hQDiXQso+ldII5Zb0oolZDPYXBeMFgtfWOcGoBN2ZRtO77MY6J+i4+N4cthoMKP9rzzvkG+MqTdZUqTEqpXouI6xL0hhCXqCxd9ROlpesitXUJgLtAWeCOF4TF8RFHMEMdYSkhZ64ZQgFTNjkRDbJJjvjQ1TlpDcqkuC0HaeUCTsOR1yesUqIcxAUyuBTECa44wxhu7oWp0jiVnaDJ3e7ieOLFy4wQyAlnZoyy+HERAosQWITAJYfARRGYi4jrkvfHYoaLEFiEwOWGwCLiutwQXsx/EQKLELjkEFhEXJccpIsZLkJgEQKzgMBF7SouIq5ZQHgxyiIEFiFQWhBYRFyl1R+LtVmEwCIEZgGBD0QdQhrkCp6GuXSbCkPhO6kTTBfy484UrzB9frrCb4XPc8m3MO18nwvrV1iHwu/55RTGzf+m+/y0F4qbnzY/nd7PJW1+Ph/m+1KA0ULVQeXq540L76r+9uqU/y7//mLGxCVBXDpTVyx4DRLikkKmztDp7Jl0ngp1nLxGFsun8J30pLwwV0DMtpy55qv6KM1c03nIvFj6wrxmqnth3MJnD1665peZ/77YvbfoeN/mktZL82G/lgKMCuswl7Fysf2jslVeIR4oVoeZxuVc6nHRiEuVk8b4dJXUQPcGuw60CmFJy9x7N11li+Xnxb1QWi9e4XWmPAvjzgfASqPfXMopLLdwABZ+n+55pjK9ek2XdvH9hw8C8xm/c4WCxqrmok6GqDwdVM8PxcZysXf5aWZ7f9GISwV5QCqcPHrWN6+Bugor6xByYShERl6eileYb37amb7lx7tQPoVx5/LstdNLU9gW732xa2FH5qct/JYPk8K88qlQfcuPm5+nvhXmq3fThYtJO12eH7b3pQCjwjoUcjSXA+Ya9x4H5Vn1uFA5eePSO29woSRFv18SxOXl7FXKA6L3rKsAqYZ6ZlO8NN7Vi+s966r4HlLw7vW+WFy9LxaUbj6hMN1MZXrfvDp6z165hc/ee10LvxU+58ed6V4DZ6ZQ2J78uDOVWYjk8uMW5pn/Tfnnf5/pm+Lmf89PV/hNz/nf89MVfitMm5+u8Fth2rnkeyEkkV9uYb4qd74hP9+FOA+rtmiuq/3enJ+uLZey3SpjcVdxOkgvvr8gBC71YPQKvFz5evkvXq98CFxSiuvKB8eHtwUzIYOZvhVCZKa48/2mMuabdi7pFDefSplL2nw4zJQuP16x+4tJWyy/K/jd/Fih8QYvIq4ruOfnUvWZJsxM3wrLyI8rJJD/nH8/U7rCb3rOT3ul5VusPdO9y2/ndHF+S96XjozrUgFcrs7l8cSH5xMFDWS5N8d4zLgbJ1mLLB7yV9TiMeb3tjDfuQxAbyIqTWE+M9XmQmXM9H2mb4VlzqVOM8X1BUFk1mu+VA3+B9lNlh0Y56FZZmFwmOqb3hLsjPnOIKecKZ3aOdP3mb75cRkmi6SqfyAtD9/l1J9R6cdJLuZrIpncDloajxrsJfNuEqpZfC86TxsqP4tNNn6BTJArzjsCM8siJ3P54O6GsRVWlvZZLI091iT244D3WJC64vjDH8CWv3xDZzAznsXBLw0NI9MKMDdHA/g5CEw2vBCehfLRghZNJiz4MJvHkqS4EPmxAueq5tOI8MtdJoNftozkgw6Tt4uh9CAQ1/KSarBIGlvvwXOWoZ9S6XpLBrHrjk31KykISWERn3oz7gJ4LcKzNf6PIA21I56kTdjeAiHJ+7SCc3ThsJcQtgxKMobdF6LR9jTeqpUX7rHH35bOJZKRrXs20JhqI2E8NVHPtF91xgpZuilHRKi6PI+BqFNhIANSKwM8oRxt4Rozl8WSBB8+ikvOA7SOaYUKuhULiPLOIS8GQdYWEZcbKSX2L61JzYD3sdOUDeJsgqHp8w8yv3lPX7rJXWJ1nq46FWMY+nNIK2gpkE4ay6YZJm3GJ/+NokAYgzzLwYVPYxNTzLIQKDLCD3XmTDFDoYjo1AzNOkSAmzOQeqmFcqy9pnzY1vcHobJQXxIVSZt82MEPQymyb8g8xCEM1xRERAaklfDjAclRpR56po3AYg5BoJp3KLldRTU+5RtlXIxiu3vAApHx9jFY/AyWAO6frrTgdaiu3r3akE9KF367UBvz87lQ3A/qeyzDEhzMWCcnGyq3fMI6ylbacFgs/whMft7S/EFV6CLKkTFSfChZPIv3IFyVpZisGRCyD4opk8WVHOaZ47BKaSY7zBMUC+aZYSnDaVjIMNSKXJUx8X2azCy+aajPdAlSWwJReTJlETxwh5ljgTSe2VMxiySrLBavtlE8HCVsjCYIOaetgp6sTvqtFnv6YRyDXES4qMQlwypqEutYkPRRMgFcVSXjVlkRs9EEXoDB7n6A6mPA+KFnfYHpZVwXAcg5Jc3n5y+ERKTjkq9no7R61okDXfORVn6+hRWaqZyZvs0ln8K4c3mO+HE7Fm+2G+75vFUsu9rKl91sb//iH5gYx80XhPJytMdccly4uKORTtgkpoecXuCVB08ClkknnX34EIgqiiefQFBITSiK/mXsQqOQBmkepGY0FCa+PFRDiUF2+UF+BhLMggQWOhSOsbba621J63ILhMuwix+BSsaRseRyoJaaUCVOQEYsAjWdGu6wtiNvWFlwmPYMWxb2OefM9oNvUckgLk08TWL5vzs9kLRY7Urr7TtrdVH53kM+gjBQqx3c+AcPpYssUW2TZrsGjBxp6F7ITMq4hRrvF1nUgibvzVTZ9Tc9YgdODdtVrUE7jQPUjz/wZXv6R39r6cwhBPZXDtXlC/TiCLbGRmEZ65ffbGs2brVoeY1lEjEb6e629vf/zdo6e62qeZWt336dhaqWWKYMCoW53HvwJ3bi0DtW6R+1KijObBLnKSy8GbGOJcDjFC5yt3zyqzbQf8YGe05bKj4IAQHGAsnKdVo81WOBGBRmNmarttxiFSuX2qtP/tAqiFIOss4iH1uIUDKISxNZDjzlYHTXrodsy8477LFH/9YSyZMWQiifY6e1ClwUa7wQMHYUlVyXbdq0yY4fP+4crQpBq8357OKCVO4SFlq9eRusYZcdPfyM3XXtBju19yf2rjXblj07be9zJ6FERi9haZc3q8HkCotUrLf77vxf8Y/YaC++9SvrPfkKlJTfmpasta1f/gsLvLfXtq1baScPv2ftJ96zkYFuq4yV2c4bv2Q33TRgTz3+Pyzef8DC7EL6oLxEwZQC6i5EXOXBkP348e9ZXagbdrEbAgJjCLCEKV+Z1fuHbAgWuC1ZYYfeXWmfuu8+PB7hEATuJ52E6po/HXFRE3nBEBdiTRsODCJzr33n6joAABuISURBVLMKSOlg9oQNjq6ziqv+2Fbv3mq/euxbUOc9FobsDvrwuOKEuzm36NrVWehQ2PkXqo/OZx45csRaW1sdwpKX6HxqS0hbHpdlOWO2YS51mCluIeswU/nhsRXIO/AdGOmySKjHehNL2TKP2VkmZ+jQYWs7/obVZRM2ynZTNnbcut4/Y2+8H7BwrAYvN2VWg6fnlvQBdh5j1h9Cbplstup0D/GRCZVQCKX8VrPtHjvn77bD//afLJ3CSzdTzQfJdOLoU3bqvdfsrk//7/abx/+b+bregZJK4PEaOVBnxF46/rytueELtu4jv28nH/2/8ceYQm1AsjAE+fx90EEsek6ziNJ144TvAWR4+P2EqhqsiFrVyBmQ7kE7F2i2QdtiwSqfjVQguxsahFKMWQ3SrRT9l8QF20j5UssEW6wrNWjL4getogd2uarewsAKBQkbgWILgeykBjJDEKk2b2AsGOJKs11em15hSfjnbHTAetPrbM3mh23DNZvtqZ9+x4Y7z1iUrVm/nGRKqC1g09KFIUxnAP8sPomy0k9u4dva2mz58uUuVVdXl5NzieoS2yiqrNQpsGTsDQYksjkE09lkmS1v3WM7t91h/rpqNLUq7M3Xf2pH971qfhBaLL3SGsubYRc/z6QeM7Zb7K0XX7fBQ6ctgo/BrA+5FyMw7cdHodXPApIfYBTYeiF09Yt+qDVp+rsKqC+1yIyOjtjg0LBVubcOPbi7NP0pSyhliAUyjpXyUl7ETL2Ipmt/YKbg9w8gV05BDlRYMLLOPv/x/yh6y1KhgPVnO6ASyxzSQnvLkMnbxx7+D8i3MtY91mbv/tt/ZlGKWD8y6XBEfaqS9G/eOGmmqk58K4a4JD2cNUU734mWTo8Z8j0rL/PbOV/YWq96xFZt3GMv/uqbluo+6MjrAOQ1588ZLhmnY5KrlgSlV1YQteMJ48UKS861Zs2a8yivmaiiy9niufShFtEgekppX26XbOu1e2zvm+/Y/hNvIdjtQmiLzhOreBhl1FiZD0/P79j3f/Sf6LoG5EAr7OGHft++f/BnyLwGwQRSexll1dcEn/WQu5ygmMhblFEIATvaTbyDVqJ+arvqmYVq0bU8hlIqwnn128QPRCXEJlwR9Hxt8pRDXYzkHOkzUc4HcjOBTIqXFk7Ug6Rr3Q5i65Ztdurcu/bsT79pNVVBC41iO4/2S8dLSCkDMpc37kCmzK5/+MvW3LrZxo7uM18ZMjHkOSzR9KkUJ1BlmhmNuFoVr9GUt5IvnDc4iiEutAUpeZbB67C5sBvKOuJrtHRlxtrZdt266/dsy4477Hv/8hdQYactNDJgoRg6JclR1CG0Ize+ZOQEXbOv3CzbcDmjCT7ebqmQlwa8VvDDhw875CW4nT171g32y1mPS5Z3shX1JgYugllp9kQqyuzwiedhG4+x8wQyYgfYsrX8UrRziLW6D4H2SRaiHuvsare+sfssVFlhmSGoGDTs08TIpitJV1qsYhKBuvpKfUZjHCLK/eNpvE9lkUN9ywtFcVfFVBr1q98vsy+oRLhZ5iJcsm6YU0bj02e6NH6pHwWgiNGCz5RXW0dfh1WXj1plRvpraMujz5ZFtUNKqT4o7QyUtkFnDvQNs3MawykuWvW0EZTOwiX2sDj68HAFES5Qo4maKt6sEdcQkVX6rIJXGXWU7hXU4QozITM/O4QdDNblN3zRWtbdb49+979Ybfw92A122zjz40uNWYCOzw0cAAostI5Jk358GF2wDBfhEv3z2lYsu5m+CQa5NuRSytiaEJfSnDlzxlpaWtwH3Us9It9ESmG+HowL38+1TrNJXyxPvfP5hmAjWIlRAh5j0CbSASitmEXV5cklbKYMIweBQs6g1Ih+UJkPN/VjAeRD6HKFURyIBKwvOWL16k/kllqdM06/qbQQV4i+EOJxJlvQop8SGOYOhvRtIpE0VFWnBMkpR4ahPPnp6NNsZ+mUTD7Ah9HUCHwN+IFdz2SSRQeE7BuLWXIsZPFKSd+hOl07ICKS2t2PgMiQdyH7ykB9+5BppR1npD4NgbxYrKdp9Lj5peKY7fw2KxdWw/NBWIzikqdw1VaJZlWAOjdf0Ey6C4ZsetAisWZb2rzSurrbmNx9tDpgIylWYsmnJfwQhcjglnzLn0GFgNoEYC84TnXFBpmv1sAWMhsaGnITQIL5YuasS7OR6PgwTKVVnUTgnHALbBphewIkNmaRMP3FVPb5ZTAyZf4QglriRllwQgzyTLYaSo0Bj3zLpzN9/n56WSv4FdyppdlRs65VTjFW22XaSxxGftnDsZ6TIKaUVaaanZDdD5/sg6xKoZvGvgX9mMod92FusjSTFj1M5qiPPg5xyiAIS5wq0qVatAkFK8G0VVUOXcW+FstA2p09/M7jK4tloHeS2XhU1nRxCt/rLFh0uN/2/fy/oxNy2m5kB6Yz0mydTAALMpDB3DqTqIPVutfADkKGTofJC/Mv1WcheLEXEsRLSC9EduLECfcun9oq1fprZykNpaED0/EAu0lQx0meRQmHaJsorXQGhBREWbOCoyFi99lty7KKhxjMOq6V5HiJ+lOH6bM6VlNkgJdq+z+M9dJxpmxgFDp6jCM+GYgDOgb+Ngk7GA+OWSLIooTMMqF3yO8SbJglOKuZEesoaoyYosp8EBvSvJdER0iwMIhjGB5GtUk3swsinoSLzgvFEJciH+Cn66zCeGVmFdeL5EOuEYbqiKUP28u//palR+N2x91ftkDtFhtAhhJHMxkoMKgZ9CIAAYrTTB5nR718Lobt8fK4nFfVT8JaIXbde5SphPNCVCdPnnTvPEpsprqURFt10J0dQskyJIDNZssZ0FhPyFZYErYxy8ocCI3YrxDIh8ZOWzaCjAdE5QsylDk2o0HllyhBuk2ZSiYJrEVBn84Egw/q2yjHdvxpqAe29bOc0wuDXSE2aINaXQnVyATHqkKANgVCkv34nMA6wO5aAFbZgjEr8zdAdYDQmcx+iTho88IGb0qDNzQuRSWBYOpTUMe2lDZV8pozmNkqiASdUqkFUWVR5WCRCo6CrPSLQ5El3PnNDAtSgjkaIGUUuVcW1jGN9Qshs8Q0uElcxhyCsF9bsfjFEJfiCXHNKgh5apt/NkjUm3iKK4GfjhakJdzjTOLbv/kHS51rt/s/+kdI7ldYOoIWMlsZEdgoEX86+JkkTYLVoTAo3+l+hXFn+1yY32zTKV5+Wj2LLXRCXD0QNmzY4LbTJddSXI/VvhDV6uWby2Xqf++bdxWMvZ9ieu91vaiAOZcMyCqYqnAsRAQk5OcgtY8t9SwqDkHYiTJWZn/XPlui0wIqTDtvOois3UZYjDBUtdhE6QflrIBcZJ0uqkHFE4tiiMX9FhlBnqc6g4wiUBp+ji5F4mwpcAzINzBs1eyIp+J9UJlpKE6QFYYhMr4Bi5eBwCA9shFEH0xkJ5/lunBBMKY+rkO0ZyqxC/2VTVrv6IDt/ux/tLU3f9YqW9eDpJAlZ3pBvEPMTUwUpZHiscCIS8oiutF54RDIDpQM1a1xxlnHFDIvYJFEyK9y4sztYkHc2RzGoAD2bLF8ism4FO9dfsJ2031XHBdUCVELcw3ZQD8rL6foweoh/pKBs/bKa/+f7bzqi/aRe75kv37qf1pmpM3iY31gdKgVrEJou1XbshGdzJ9lmAOQZpnj3KOJ4lKHSTC/efNmpzl/+vRpR4nNPbfZpbhc7Y6z4oaQMwoRBen7MhBXKAXVBAU9wRy4yTG1nn5RLQzDsuwoyEv9KQsDDDINcGaTZCKlFMpsAMcu/TaaDNgg1OQIbRxBWB0CaVcm4RaYnNVRKWRCeabqrS8DxYkeVCVjmWPmVs9Rn3QYZA6Vot24LIq3ElyXHoo2e/Lx/9OqW260Rj8UZiRJn6pNLLhQVpJtZVmIfPR5Th8EpOYOlmuLJscJpfiegNLKCrGrH3WmWLojkyPCde2FFuYi/S+q5Zki76dFTE8SucjwK5ZF7p1HNUwfY+oXH0jLGWnz9QOYCAOBreORMXvr5cdsx82ftt133W9P/ehbFgMQoDUGA7I0qIi5ykPmO4FnQ0FObVHxJ5Uv9lAyrXXr1tmpU6fs3Llz7lna9EJqlyPMt90XqovkH1lWXlHBDkYsKPFA2PqCWBNIndRiS0+eP3QS/mob9nNgVwrF+g7rry126epFQX45udeFSv/gvg9yxKUjjlJmeYs1XvM5u27L7ZYIcQgZ+V3o3Kj94v1n7bh24Jbstj3XPWLpqmY2ImqtbGzY2va9YGcDtSywsJTDUSgXMWU5GW1agr0SCzUjJ6xvfy8bRDXWGFlidewYdqeasDnWy34ZrB0UlWxlOI17EJj6zA8yRofeCehFaSWQacp8URDKNIspnDQbbZq3XtB47OnpcWNmDmNT8vZTXh751+koqv3jkQTl80dhfg6KAEJpb2+3ZcuWTfkiDJvPrnjIQBVnuJIxlFqw37EcPgBVnq1jAPfYiy/+qy1bsYlDrig0RjiYALL3sVrpoCpnDcD4ak8uFAKh8Nkr04s/0zU/bmE+hc/5+cz0TXkKDkJQBw4cYKc515m6CqEVps1/zr/PL8+7z/+ef+99vxTXfJgov6rMKH0RhRWQbAOGiuM90Za11nkuaUsG0MeaZrQMM+lTdU0W5yhQAEQn1sPHIeQolFglsr5edIFKKfjH6txmwlVXr7Uje8/a47/4ezs10mGVLLRb61bbFtgqq0nbLXfvtHeffdTe6zgNRRa3FWVB27nhPtu1eYudO7Uf5MWYhXJJg+vDUCESeVyOcKH+z/9e2KcBWPYIPO5YKmFtI+22/YZP2v5T99hQgCM8OiEhabtjc9MI1+NWV93Ac9CWYlHi8JsjUKXQVogJArD/PnYWs1AX7Cme10wt2Pn1OC/C1BcCVDe/ogCbZpi5HN7m/zZ+syYJduzY4RJ6/1RJAckDlHfV95TIShkpQ/lN8o4gvHSarfJMAPmA098KWjlHCAIowQUBhI6ZSJTIeg/MJoFyIUDkl+nVa7prftzCfAuf8/OY6ZsXz8u7cOewMG3+c/69l0/+Nf97/n1+nIu99+rt5ROxDgZmjdtdGglWWnnTbXbNrQ+YH3MB6VS1F+28a8A3jAh3wE4e3GdHnvqh1USGbZR+jKFC0Tiati4sK5RSKGdsDkVrLFDFzlr7KZB02kajyKo4n1cB+9gZXGe1K5fZ6InDFhvjNASsoXRRK+Nj1u5vtFBzi4Xio1Y+dAbTN6M2AhtWLgE2lMrlCBfq//zvhX1ak+yxwQBtxfTOGeR0m299xNY0bOEsKZbGxqlrD3+EMOUzNISOJRsTbfues3P7f8bB8wQbalqQeq0OdjqZrbGeaNwqtJsxHjTu9+/fPxfEJerkUX6f8fLIv86EuP5fIn6Z36wFShs3bnQskFeAgCUgeYDyrvo+yhZrQDof8NOyWRRkBZfOlpCTDgv4JM9iC3YEJcYAek8BlNqwgs0XFBvzqpSfp1du/vVC3/PjztS5+d/y0+h+pm9eXC+Ody18X+y5MK4Xx7vmf8+/975fiut5+bLr5JNQ3Y/sEVI4ganmFAhsJJS/W1Q4rBjA7NBph8qHvt4KFqYx1CR6kGBEkBE1jWZtCH2vUgroy1q2JmRDo+y3pStAWMZERqCBTK5sDNPN1H0ohJAedrKCXfCxCCMTsUZFImODsTOc8QtYUxny24ExG2YBHuawY3lSVOXlQVwXGuf5/Zh/L5j7saOfhAOqRg6bLk/aYKLS6hIN1s8Z0izyPQWvR2XGRnbIOKoIombepjlnGsBAgB9dRA5Z142GOTpUYZ3l/VabkIgqF2RUQL85BFFa2/ntK5ZmOlZRcf8rv68WSzTdOx0g1jb/bAJ7NAwCNpWhpMR6sJ45APrT1Qxmdi7Y2RgUYRViq5aBHmIrWYLdNBQaW46zKaKk4miw6HehAVZSlS5SmREmYABkE9JOUnoE9j6KZjyC+jjHtBC4Txf8WIIYHMPWE5r3kUA5lkVRdXR6QVErd3K+gemSLsz7uiXW6euwQHncxgah+kFSY2FYvVA5CApFWxbcLGM0AaUxhLRnOBrhmnSWEcqHKq2hqtp8w7KSgFAeMzFpFmqxT7NmXz7AVnez8MgmvjZdRkG84WiTjcUhHCpASiiET6It3XGyBawVCWOxBeoywhyWAmuGDRapyMjcumRh2pBgK2aiFTqjO8cgUBVFWsrHQ6TT5alltPBEw3Rx3aTcunWrk+d4kTRRvcmaj+m9d/nxvHvvmp/We1d4Lcyn8Pt8ny+U70w7JPntVPmFz/l1ulT5zFRGfnnF7vPbqnwuJq9i+S++++AhkN+nKn0h+1S6W9JXnEMdRG1pJWP3o3i40ALwIsmKCseKZ2dOSD+HCk6Xzazfq6z836wTXiBifp7F7i+Q/Ir6nN++K6rii5UteQhobMl8k65zCMiNbM9M8S+EuIoKxqbLUJXTlmehAHq6+Jfiff6kmyNwZiy+MN/C5xkTX2EfP8xtu8K64kNXXdklm4eep/DSezMBYyYZl9KJMRU7KXR5IbaSKJBnbP1LNUKWPhWmQyaF7/P1mbxv3tVlNM2/hSKJ8+s2lzrkpyvWpAt9z08zl7iXIt3/394Zu1ZSxHH8dYfkQrgkYLrkOOOJihYWioLYHRYemsLiKivtRDsri0g48A8QsTksJHCCnYKCIlgoQUW0CF4whjs0XhFICsXS72fz5jJv3svuzuzsc9/L7wfftzM7M7+Z+e7O783Mzs76OszdbQZS742ctaIM7u2QCL0svf9IoNd1qlT1uEh4VYh6yYpeV/8t8CJjKpBKZGq6IuMO/jgu3LGDRbQiGQONGeD+Pjo6urduMUIhNul6Vfw6husLKanqmQ3lw0Z5MeIaMsdJE7/sk1j+SePbytt9BhiFsMogsj3wKPIn4beqGp7+/PokJV02rWopJstqGzAKzLDx/Hm9jKtKIO5YeEp+wnih309aFubHa9Mdc3Fi4rZZZtNtDLTJwK1bt2KNFsXhQSBrR3fxlEmteSspYFnzvsDCjLppFLXXW11dLT475gwMRxA2YBdeJAp+wjDf77tJFuoNVJV6Q11+5FCv7w/T5Vri4OeP289zlD+Mf5o/1HNaPDtvDKQwwELTyNd7yAajxbotFp1WSp0eF0qY4yLus0Kd4aWiHQsLz+bn57XK+jgrGnnY0Ik56lxfRWmYi9P20Rp72wyb/klngDbC3nz7+/tDf7I16obhuibs1Ygb1Xs6J4V3BN6wjOp1sS0xrwPx5NAZrtAQxBguP22YLvTXIcHF8fW6c+4Yhvn5hGEuzahjGDf0j0rjzoVxQ7+LV3VMTVel18LPNgM8kNvd3S2miBKY2Faah+umq9vjQh9zXb8LLwijdwlTwChh6MQThsXFxXuGK4znG4ImYWV6Qr1N/OPKp0kZLa0xMC4GWLu5s7OT0tOiiPS2Lgkn275wtkSiek59Pb/quCpEp+WjEMx50ejLGn5ZGGXw13zh9yWmN1GVj683dMfMY/lpY8oXxg39vt4Ydy49MXla3OlkgHsJbG9vF2064d7iKz7vCW/FMBRtfKScV9yZ86q9KNUVCEPBFsZsXexvZezC3bHKoFSFOz1tHssuUGpYWN5QT+gP49f159JTNz+LN50McB+xrxw9Ldpkwn3FKE5vcveWYhmKGSo63WT2s7AmRKenp3JwcNCbm5srJuxHGaFR51zmVceYtAlEV2Vv4cbAmWGAl6f39vaa1JeO06PCUaySaMPTz4AdUh8XHhSinjKSHoPB6no+gMrwEYkxOGFc/A7jMkZl+biyuHKWxS0q7/34cX23FyXJmVNXUgEs0VQxwGt9d+9qU0m15URh06o3BRa4R0vKUNFlwlPGPwXWeKUawN7s7GxvZWXlnuFxysuOziAQx3fjb0AkyWtLWT5hmcrmw8IMfb2+O4wX68+pKzZviz89DHAv81aM24I8sWYYrc+EF4Uky9fEcFHmeeEvgRX1Sbpo5DQqNiDEiOEOG750D0hV+EDkljwxhiA1bky6qmrm1FWVl4VPJwMsLGWLmobCE8TbwsUmepKMTZDhU/LT3ZsNzkd7GTrS+2L4WGac/LCwQfph0QWISBDm6yctC/PjjXL7aX33qLgx53LqisnX4k42A9w3zGXxOT3cDe8jelcsecBWNNrDOofh4sq8LHwonOzVytlE4fuDS0tLxXuOqMi1/KGqODFGr2z4F+opu9h+GG7qypoYdGDI+ZgsC/twhxLmE4abP44Brincu2swMzPT4xNyXJezxjV15kPPzGVxD2YQelrMhzNKY7usRpLLcFGIa8INYbiFEZogNNYLFy70FhYW9M23qDWvCbnFJfENTlzKwdihHvw0HBoR/3Qs2uWbjBgvf16BhuReoxrUeOw7aw1tFAex5zBaGCsaLNcBDjnHtSj784zNp2vxXV0pF4aaB2csGOc8yCCsRMBo8TAvbtuYUzLPabjIglX1HwtM3GcR1wAxXMyBYcjokbUhmS7SwMV25T+tvH6exMU40UjYVWNzc7NYNuKMtt/rIh2NyiQvA/QumMdZX1/vbW1tFdcC/qeNa3dfch/xB3l4eNhjt1Lq6d+TGdilu8bDu8vCTgZ9hYrchgulzwhfCXSRsun3iXZuGjJGDDAvRg+kyT9jrgsW6nHlFR9D4sel7DQc4vONyo2NjcLNPz7nQj1+2iHFdiKJAcc/Q6S1tbXe8vJysfXwpHPNPUTd6LljoOhZ4Xf14t5y7iTiRieiu4bhul9oPDz0s8hmWHylcj8g/CIwbIxe56U0JsaAMTDZDDD5/odwSWB+K6u0ZVQYxzIJx4b3jZ4eZK2tKTMGjIG2GaCX9a/wpXBRyG60pLPV3pC+3Np7THhfoCKtVEB6TYwBY6AbDDDpSjt/TXi+zSK1NVQMy8xar68FFqomr7JXWhNjwBjoJgPMZTHSuiLcbruIbQ0Vw3J/pxO8GnRToCtpj8NEgokxMAUMMJqiTb8qPCK0brSUR76nfiirKaya3RPuE1iwOq5en7IyMQaMgUwM0MNiWPiD8HQmnbXVjKvH5ReIjcMWBLqUrOtgby+b/xIJJsbABDDAaIn56x8FNhQdu9FSnq1OzqO/TL5R4GXhJeF7wU3smRETGSbGQIcYoE06fCL3E8KTwliGhcpnSLo0THtIpWOc/IYASSyjsHVgIsHEGPgfGHBtkAdqjJKuCzeExttDSEdj6ZLhcpU5Jwdf+3hbuNo/+beOvOdjTyT7hNjBGGiBAXZuALQ1ppHeFT4Q9oROPVDrouESRwPCJP4rwjuC27TQ7XnvemSTUA8V38QY6AQDbtiHkeLVPHpVTLbfEV4XPu/7deimTGKD559gRXhOuCIsCzydpKfGRSDcxBgwBgYZwDAx/YKx+kfYFj4VvhXuCixpMDEGjAFjwBhoi4H/AC/Qt9eBRev1AAAAAElFTkSuQmCC" alt="...">
                    <div data-aos="fade-left">

                        {{-- <div class="mobile-device iphone-x">
                            <div class="screen">

                            </div>
                            <div class="notch"></div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- ./App bl2 -->

    
    <section id="bl2" class="section features">
        <div class="container" style="padding-bottom: 0">
            <div class="section-heading text-center">
                
                <div class="row gap-y align-items-center">
                <div class="col-md-6 col-lg-5 mr-lg-auto">
                    <div class="center-xy op-1">
                        <div class="shape shape-background rounded-circle shadow-lg bg-info d-none d-sm-block" style="width: 600px; height: 600px;" data-aos="zoom-in"></div>
                    </div>


                    <div class="device-twin align-items-center">
                        
                    <a
           href="https://www.youtube.com/watch?v=M1nVdDYKcKw" 
           data-youtube-id="M1nVdDYKcKw" 
           class="video-thumb js-trigger-video-modal"
        >
          <img 
            class="video-banner-img" 
            src="http://i3.ytimg.com/vi/M1nVdDYKcKw/maxresdefault.jpg" 
            alt="" 
            style="width:100%"
          />
          <img 
           class="video-banner-icon-play" 
           alt="Play Video" 
           style="width: 100%;height: 100%;max-width: 100px;"
           src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiPz4KPHN2ZyAKCXZlcnNpb249IjEuMSIgCgl4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIAoJeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiCgl4PSIwIgoJeT0iMCIKCXZpZXdCb3g9IjAgMCA3MiA3MiIgCglzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA3MiA3MjsiIAoJeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgCgl3aWR0aD0iIiAKCWhlaWdodD0iIiAKPgogICAgICAgIDxwYXRoIGQ9Ik0zNiw3MiBDNTUuOSw3MiA3Miw1NS45IDcyLDM2IEM3MiwxNi4xIDU1LjksMCAzNiwwIEMxNi4xLDAgMCwxNi4xIDAsMzYgQzAsNTUuOSAxNi4xLDcyIDM2LDcyIFogTTM2LDggQzUxLjQsOCA2NCwyMC42IDY0LDM2IEM2NCw1MS40IDUxLjQsNjQgMzYsNjQgQzIwLjYsNjQgOCw1MS40IDgsMzYgQzgsMjAuNiAyMC42LDggMzYsOCBaIiBpZD0iU2hhcGUiIGZpbGw9IiNGRkZGRkYiIGZpbGwtcnVsZT0ibm9uemVybyI+PC9wYXRoPgogICAgICAgIDxwYXRoIGQ9Ik0yNiw1MS43IEMyNi4zLDUxLjkgMjYuNyw1MiAyNyw1MiBDMjcuMyw1MiAyNy43LDUxLjkgMjgsNTEuNyBMNTIsMzcuNyBDNTIuNiwzNy4zIDUzLDM2LjcgNTMsMzYgQzUzLDM1LjMgNTIuNiwzNC42IDUyLDM0LjMgTDI4LDIwLjMgQzI3LjQsMTkuOSAyNi42LDE5LjkgMjYsMjAuMyBDMjUuNCwyMC43IDI1LDIxLjMgMjUsMjIgTDI1LDUwIEMyNSw1MC43IDI1LjQsNTEuNCAyNiw1MS43IFoiIGlkPSJTaGFwZSIgZmlsbD0iI0ZGRkZGRiIgZmlsbC1ydWxlPSJub256ZXJvIj48L3BhdGg+Cjwvc3ZnPg==" 
      />
        </a>
        


                        
                    </div>
                


                </div>
                <div class="col-md-6 text-center text-md-left">
                    <div class="section-heading">
                        <h2 class="bold font-md">Что это такое</h2>
                        <p class="lead text-secondary" style="text-align: left;">Sendme — сервис мультиссылок и микролендингов. Наш сервис позволяет указать любое количество мессенджеров, соцсетей и ссылок на сайты. А с помощью интеграций вы можете добавить виджеты «Яндекс.Карт» и Google Maps, встроить пиксели Facebook, «Вконтакте», «Яндекс» и Google. И вы получите сайт - мультиссылка.</p>
                    </div>

                        <p style="text-align: left;">
                Это полноценный инструмент для ведения бизнеса через Instagram и другие соцсети. Зачем выбирать, какой контакт дать в шапке аккаунта, когда можно добавить все удобные способы связи с вами👌
                </p>

                </div>
            </div>


            </div>
        </div>
    </section>
    <!-- ./App features -->
    {{-- <section id="features" class="section features">
        <div class="container">
            <div class="section-heading text-center">
                <h2 class="bold">5 причин выбрать наш сервис</h2>
                <p class="lead text-secondary">Сотни пользователей уже оценили преимущества Sendme. Примите верное решение и вы.</p>
            </div>
            <div class="row gap-y text-center text-md-left">
                <div class="col-md-6 py-4 rounded shadow-hover"><i data-feather="shield" width="36" height="36" class="mb-3 stroke-primary"></i>
                    <h5 class="bold">Не просим данные для входа</h5>
                    <p class="">Мы прошли модерацию в Instagram. При регистрации в сервисе вы заполняете данные входа на стороне Facebook, а не передаёте их нам.</p>
                </div> <div class="col-md-6 py-4 rounded shadow-hover"><i data-feather="star" width="36" height="36" class="mb-3 stroke-primary"></i>
                    <h5 class="bold">Настраиваем визитку за вас</h5>
                    <p class="">После регистрации сервис автоматически оформит страницу с учётом вашего профиля в Instagram. Скопируйте ссылку и делитесь визиткой с подписчиками!</p>
                </div>
                <div class="col-md-4 py-4 rounded shadow-hover"><i data-feather="activity" width="36" height="36" class="mb-3 stroke-primary"></i>
                    <h5 class="bold">Показываем статистику кликов</h5>
                    <p class="">Вам не придётся ничего высчитывать — все посещения отобразятся рядом с каждой из кнопок на визитке. Всё просто :)</p>
                </div>
                <div class="col-md-4 py-4 rounded shadow-hover"><i data-feather="help-circle" width="36" height="36" class="mb-3 stroke-primary"></i>
                    <h5 class="bold">Поддерживаем и днём и ночью</h5>
                    <p class="">Мы круглосуточно онлайн. Задайте вопрос консультанту и получите ответ в течение 10 минут.</p>
                </div>
                <div class="col-md-4 py-4 rounded shadow-hover"><i data-feather="dollar-sign" width="36" height="36" class="mb-3 stroke-primary"></i>
                    <h5 class="bold">Хотите сделать ссылки для instagram?</h5>
                    <p class="">Регистрируйтесь и вы сможете создать мультиссылку всего за минуту</p>
                </div>
            </div>
        </div>
    </section> --}}
    <!-- ./Tons of benefits -->
    <section class="section overflow-hidden">
        <div class="container bring-to-front">
            <div class="section-heading text-center">
                <h2 class="bold">Возможности Sendme</h2>
            </div>
            <div class="row gap-y align-items-center">
                <div class="col-md-6 col-lg-5 mr-lg-auto">
                    <div class="center-xy op-1">
                        <div class="shape shape-background rounded-circle shadow-lg bg-info" style="width: 600px; height: 600px;" data-aos="zoom-in"></div>
                    </div>
                    <div class="device-twin align-items-center">
                        <div class="mockup absolute" data-aos="fade-left">
                            <div class="screen"><img src="/land_assets/img/screens/app/mess.jpg" alt="..."></div><span class="button"></span></div>
                        <div class="iphone-x front mr-0">
                            <div class="screen shadow-box"><img src="/land_assets/img/screens/app/main-screen.png" alt="..."></div>
                            <div class="notch"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center text-md-left">
                    <div class="section-heading"><i class="far fa-check-circle fa-2x text-danger mb-3"></i>
                        <h2 class="bold font-md">Мессенджеры</h2>
                        <p>Не каждый клиент готов к разговору по телефону. Поэтому не забудьте добавить мессенджеры для связи с вами.</p>
                    </div>
                    <div class="row gap-y hiiden-list-m">
                        <div class="col-md-6">
                            <div class="media flex-column flex-lg-row align-items-center align-items-md-start"><i class="fab fa-whatsapp"></i>
                                <div class="media-body mt-3 mt-md-0">
                                    <h5 class="bold mt-0 mb-1">Whatsapp</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="media flex-column flex-lg-row align-items-center align-items-md-start"><i class="fab fa-telegram"></i>
                                <div class="media-body mt-3 mt-md-0">
                                    <h5 class="bold mt-0 mb-1">Telegram</h5>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="media flex-column flex-lg-row align-items-center align-items-md-start"><i class="fas fa-phone-alt"></i>
                                <div class="media-body mt-3 mt-md-0">
                                    <h5 class="bold mt-0 mb-1">Телефон</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="media flex-column flex-lg-row align-items-center align-items-md-start"><i class="fab fa-line"></i>
                                <div class="media-body mt-3 mt-md-0">
                                    <h5 class="bold mt-0 mb-1">Line</h5>
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-6">
                            <div class="media flex-column flex-lg-row align-items-center align-items-md-start"><i class="fab fa-viber"></i>
                                <div class="media-body mt-3 mt-md-0">
                                    <h5 class="bold mt-0 mb-1">Viber</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="media flex-column flex-lg-row align-items-center align-items-md-start"><i class="fab fa-skype"></i>
                                <div class="media-body mt-3 mt-md-0">
                                    <h5 class="bold mt-0 mb-1">Skype</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="media flex-column flex-lg-row align-items-center align-items-md-start">
                                <div class="media-body mt-3 mt-md-0">
                                    <h5 class="bold mt-0 mb-1">👊И много других</h5>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ./Path to success -->
    <section class="section path-success">
        <div class="shape-wrapper">
            <div class="shape-ring absolute right top" data-aos="fade-up"></div>
        </div>
        <div class="container bring-to-front">
            <div class="row gap-y align-items-center text-center text-lg-left">
                <div class="col-md-6 col-lg-5 ml-lg-auto order-md-2">
                    <div class="device-twin align-items-center">
                        <div class="mockup absolute right" data-aos="fade-right">
                            <div class="screen"><img src="/land_assets/img/screens/app/mess.jpg" alt="..."></div><span class="button"></span></div>
                        <div class="iphone-x front ml-0">
                            <div class="screen shadow-box"><img src="/land_assets/img/screens/app/main-screen.png" alt="..."></div>
                            <div class="notch"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center text-md-left">
                    <div class="section-heading"><i class="fas fa-trophy fa-2x text-danger mb-3"></i>
                        <h2 class="bold font-md">Социальные сети</h2>
                        <p>Они не только служат каналом связи и знакомят клиентов с продуктом, но и повышают конверсию в заказы. Дайте в своей визитке ссылки на все возможные соцсети.<br>Бесплатная мультиссылка за пару минут с поддержкой:</p>
                    </div>
                    <ul class="list-unstyled hiiden-list-m">
                        <li class="media flex-column flex-md-row"><i class="fab fa-youtube"></i>
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="bold mt-0 mb-1">Youtube</h5>
                            </div>
                        </li>
                        <li class="media flex-column flex-md-row mt-3"><i class="fab fa-facebook"></i>
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="bold mt-0 mb-1">Facebook</h5>
                            </div>
                        </li>
                        <li class="media flex-column flex-md-row mt-3"><i class="fab fa-vk"></i>
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="bold mt-0 mb-1">Vkontakte</h5>
                            </div>
                        </li>
                        <li class="media flex-column flex-md-row mt-3"><i class="fab fa-instagram"></i>
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="bold mt-0 mb-1">Instagram</h5>
                            </div>
                        </li>
                        <li class="media flex-column flex-md-row mt-3">
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="bold mt-0 mb-1">👊И куча других</h5>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="section overflow-hidden">
        <div class="container bring-to-front">
            <div class="row gap-y align-items-center">
                <div class="col-md-6 col-lg-5 mr-lg-auto">
                    <div class="center-xy op-1">
                        <div class="shape shape-background rounded-circle shadow-lg bg-info" style="width: 600px; height: 600px;" data-aos="zoom-in"></div>
                    </div>
                    <div class="device-twin align-items-center">
                        <div class="mockup absolute" data-aos="fade-left">
                            <div class="screen"><img src="/land_assets/img/screens/app/custombtn.jpg" alt="..."></div><span class="button"></span></div>
                        <div class="iphone-x front mr-0">
                            <div class="screen shadow-box"><img src="/land_assets/img/screens/app/main-screen.png" alt="..."></div>
                            <div class="notch"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center text-md-left">
                    <div class="section-heading"><i class="far fa-check-circle fa-2x text-danger mb-3"></i>
                        <h2 class="bold font-md">Конструктор кнопки</h2>
                        <p>Выручает, когда недостаточно элементов, которые есть в системе. Благодаря конструктору вы добавите кнопки с нужными ссылками, цвету и размеру, подстроив их под свой проект👌.</p>
                    </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section path-success">
        <div class="shape-wrapper">
            <div class="shape-ring absolute right top" data-aos="fade-up"></div>
        </div>
        <div class="container bring-to-front">
            <div class="row gap-y align-items-center text-center text-lg-left">
                <div class="col-md-6 col-lg-5 ml-lg-auto order-md-2">
                    <div class="device-twin align-items-center">
                        <div class="mockup absolute right" data-aos="fade-right">
                            <div class="screen"><img src="/land_assets/img/screens/app/product_ins.jpg" alt="..."></div><span class="button"></span></div>
                        <div class="iphone-x front ml-0">
                            <div class="screen shadow-box"><img src="/land_assets/img/screens/app/product_main.jpg" alt="..."></div>
                            <div class="notch"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-center text-md-left">
                    <div class="section-heading"><i class="fas fa-trophy fa-2x text-danger mb-3"></i>
                        <h2 class="bold font-md">Бесплатный поддомен🤑</h2>
                        <p>После регистрации, вы сразу получаете ваш именной поддомен с поддержкой шифрования - https, чтобы Ваши клиенты находили вас через поисковую систему google и yandex🤑</p>
                        <p>Для подключения Вашего домена - обратитесь в нашу поддержку.</p>
                    </div>
                </div>
                {{-- <div class="col-md-6 text-center text-md-left">
                    <div class="section-heading"><i class="fas fa-trophy fa-2x text-danger mb-3"></i>
                        <h2 class="bold font-md">Микро магазин</h2>
                        <p>Создайте интернет магазин товаров или оказывайте услуги прямо сейчас!<br>Все, что вам нужно- это загрузить фотографию, цену и описания!<br>
                            Неограниченные возможности в количестве товаров и располжении элементов!<br></p>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
    <!-- ./Counters -->
    {{-- <section class="section counters gradient gradient-primary-dark text-contrast shadow-lg">
        <div class="container">
            <div class="section-heading text-center">
                <h2 class="text-contrast">Присоединяйтесь к лучшим</h2>
                <p>Сотни компаний, бизнесменов и блогеров уже пользуются нашим сервисом. Начните и вы превращать подписчиков в клиентов. </p>
            </div>
            <div class="row hiiden-list-m">
                <div class="col-xs-4 col-md-4 text-center lis-item"><i data-feather="box" width="36" height="36"></i>
                    <p class="counter bold font-md mt-0">1360+</p>
                    <p class="m-0">умных ссылок сделано через сервис</p>
                </div>
                <div class="col-xs-4 col-md-4 text-center lis-item"><i data-feather="download-cloud" width="36" height="36"></i>
                    <p class="counter bold font-md mt-0">15</p>
                    <p class="m-0">секунд на создание визитки</p>
                </div>
                <div class="col-xs-4 col-md-4 text-center lis-item"><i data-feather="anchor" width="36" height="36"></i>
                    <p class="counter bold font-md mt-0">100+</p>
                    <p class="m-0">элементов можно поместить на страницу</p>
                </div>
            </div>
        </div>
    </section> --}}

    {{-- <section class="pos_developer_product_area sec_pad d-none d-sm-block">
        <div class="container">
            <div class="hosting_title text-center">
                <h2 class="wow fadeInUp" data-wow-delay="0.3s">Возможности системы</h2>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <div class="tab_img_info">
                        <div class="tab_img active" id="tab_one" style="margin-left: 0">
                            <img class="img-fluid wow fadeInRight" data-wow-delay="0.4s" src="/land_assets/img/screens/app/proto.png" alt="" data-pagespeed-url-hash="2843418726" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                            <div class="bg_circle"></div>
                            <div data-parallax='{"x": 0, "y": 100}' class="tab_round"></div>
                            <div data-parallax='{"x": 50, "y": 5}' class="tab_triangle"></div>
                            <div data-parallax='{"x": 0, "y": 100}'><div class="pattern_shap" style="background: url(/land_assets/img/pos/tab_pattern.png);"></div></div>
                        </div>
                        <div class="tab_img" id="tab_two">
                            <div class="square"></div>
                            <div class="bg_circle green"></div>
                            <div data-parallax='{"x": 0, "y": 100}' class="tab_round"></div>
                            <div data-parallax='{"x": 50, "y": 5}' class="tab_triangle"></div>
                            <div data-parallax='{"x": 0, "y": 100}'><div class="pattern_shap" style="background: url(/land_assets/img/pos/tab_pattern.png);"></div></div>
                        </div>
                        <div class="tab_img" id="tab_three">
                            <div class="square"></div>
                            <div class="bg_circle pink"></div>
                            <div data-parallax='{"x": 0, "y": 100}' class="tab_round"></div>
                            <div data-parallax='{"x": 50, "y": 5}' class="tab_triangle"></div>
                            <div data-parallax='{"x": 0, "y": 100}'><div class="pattern_shap" style="background: url(/land_assets/img/pos/tab_pattern.png);"></div></div>
                        </div>
                        <div class="tab_img" id="tab_four">
                            <div class="square"></div>
                            <div class="bg_circle yellow"></div>
                            <div data-parallax='{"x": 0, "y": 100}' class="tab_round"></div>
                            <div data-parallax='{"x": 50, "y": 5}' class="tab_triangle"></div>
                            <div data-parallax='{"x": 0, "y": 100}'><div class="pattern_shap" style="background: url(/land_assets/img/pos/tab_pattern.png);"></div></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 d-flex align-items-center">
                    <div class="developer_product_content">
                        <ul class="nav nav-tabs develor_tab mb-30" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" data-tab="tab_one" id="ruby-tab" data-toggle="tab" href="#ruby" role="tab" aria-controls="ruby" aria-selected="true">HTML-блок</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-tab="tab_two" id="curl-tab" data-toggle="tab" href="#curl" role="tab" aria-controls="curl" aria-selected="false">Текстовый блок</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-tab="tab_three" data-toggle="tab" href="#resize" role="tab" aria-controls="resize" aria-selected="false">Баннер</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-tab="tab_four" data-toggle="tab" href="#stat" role="tab" aria-controls="stat" aria-selected="false">Статистика</a>
                            </li>
                        </ul>
                        <div class="tab-content developer_tab_content">
                            <div class="tab-pane fade active show" id="ruby" role="tabpanel" aria-labelledby="ruby-tab">
                                <h4>HTML-блок</h4>
                                <p>Наши разработчики называют этот инструмент волшебным, и ведь не шутят. Именно в HTML-блок можно встроить пиксели ретаргета, виджеты чата, онлайн-записи или карт. Блок не ограничен длиной и вмещает любое количество виджетов. Смотрите видеоинструкцию и используйте возможности визитки по максимуму.
                                    Инструмент доступен на тарифе PRO.</p>
                            </div>
                            <div class="tab-pane fade" id="curl" role="tabpanel" aria-labelledby="curl-tab">
                                <h4>Текстовый блок</h4>
                                <p>Здесь вы можете указать всё, что пожелаете: рассказать о себе, описать продукт или услугу, добавить информацию о доставке и т. д. Пишите так, чтобы людей заинтересовало ваше предложение. Будьте полезны и искренни. Доступен на тарифе PRO.</p>
                            </div>
                            <div class="tab-pane fade" id="resize" role="tabpanel" aria-labelledby="resize-tab">
                                <h4>Баннер</h4>
                                <p>Используйте этот блок, чтобы публиковать фото товаров и примеры их использования, размещать своё портфолио, показывать корпоративную жизнь компании и ещё много чего. Думаем, посыл вы поняли.
                                Доступен на тарифе PRO.</p>
                            </div>
                        <div class="tab-pane fade" id="stat" role="tabpanel" aria-labelledby="stat-tab">
                                <h4>Статистика</h4>
                                <p>Чтобы узнать, какой канал связи наиболее удобен и популярен среди клиентов, пользуйтесь этим инструментом. Он понятен и прост: рядом с каждой кнопкой есть маленькая круглая иконка с цифрой — это и есть счётчик посещений. Какое число он показывает, столько людей и кликнуло на кнопку.
                                Доступен на тарифе PRO.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}

    {{-- <section class="pos_action_area">
        <div class="container">
            <div class="pos_action_content text-center">
                <h2>Хотите увеличить продажи?</h2>
                <p>Создайте свою умную ссылку сейчас и больше не теряйте клиентов.</p>
                <a href="/signup" class="pos_btn">Сделать визитку</a>
            </div>
        </div>
    </section> --}}


<!-- video modal -->
<section class="video-modal">

    <!-- Modal Content Wrapper -->
    <div 
         id="video-modal-content" class="video-modal-content"
         style="height:80%"
     >
      
       <!-- iframe -->
       <iframe 
          id="youtube" 
          width="100%" 
          height="100%" 
          frameborder="0" 
          allow="autoplay" 
          allowfullscreen 
          src=
        ></iframe>

        <a 
        	href="#" 
        	class="close-video-modal" 
        >
        	<!-- X close video icon -->
          <svg 
            version="1.1" 
            xmlns="http://www.w3.org/2000/svg" 
            xmlns:xlink="http://www.w3.org/1999/xlink"
            x="0"
            y="0"
            viewBox="0 0 32 32" 
            style="enable-background:new 0 0 32 32;" 
            xml:space="preserve" 
            width="24" 
            height="24" 
          >

            <g id="icon-x-close">
                <path fill="#ffffff" d="M30.3448276,31.4576271 C29.9059965,31.4572473 29.4852797,31.2855701 29.1751724,30.980339 L0.485517241,2.77694915 C-0.122171278,2.13584324 -0.104240278,1.13679247 0.52607603,0.517159487 C1.15639234,-0.102473494 2.17266813,-0.120100579 2.82482759,0.477288136 L31.5144828,28.680678 C31.9872448,29.1460053 32.1285698,29.8453523 31.8726333,30.4529866 C31.6166968,31.0606209 31.0138299,31.4570487 30.3448276,31.4576271 Z" id="Shape"></path>
                <path fill="#ffffff" d="M1.65517241,31.4576271 C0.986170142,31.4570487 0.383303157,31.0606209 0.127366673,30.4529866 C-0.12856981,29.8453523 0.0127551942,29.1460053 0.485517241,28.680678 L29.1751724,0.477288136 C29.8273319,-0.120100579 30.8436077,-0.102473494 31.473924,0.517159487 C32.1042403,1.13679247 32.1221713,2.13584324 31.5144828,2.77694915 L2.82482759,30.980339 C2.51472031,31.2855701 2.09400353,31.4572473 1.65517241,31.4576271 Z" id="Shape"></path>
            </g>

          </svg>
        </a>

    </div><!-- end modal content wrapper -->


    <!-- clickable overlay element -->
    <div class="overlay"></div>


</section>
<!-- end video modal -->
@endsection