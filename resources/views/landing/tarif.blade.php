@extends('layouts.landing.app')

@section('content')

<div class="body_wrapper">



    <section class="pricing_area_two sec_pad" style="padding-top:0;">
        <div class="container custom_container p0">
<!--             <div class="sec_title text-center">
                <h2 class="f_p f_size_30 l_height50 f_600 t_color2">Simple Pricing for Your Team</h2>
                <p class="f_400 f_size_16 l_height30 mb-0">Why I say old chap that is spiffing lavatory chip shop gosh off his nut,<br> smashing boot are you taking the piss posh.!</p>
            </div> -->
<!--             <ul class="nav nav-tabs price_tab price_tab_two mt_70" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active active_hover" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Персональный</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Бизнес</a>
                </li>
            </ul> -->
            <div class="tab-content price_content price_content_two">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <div class="row">
                        {{-- <div class="col-lg-6 col-sm-6">
                            <img src="https://static.thenounproject.com/png/240633-200.png" style="
                            margin-left: auto;
                            display: block;
                            margin-right: auto;" />
                            Пользуйтесь платформой бесплатно сколько хотите.
                            Здесь почти нет ограничений
                            <div>0/month</div>
                            <p>Подписка доступна в личном кабинете</p>
                        </div>  --}}
                        <div class="col-lg-6 col-sm-6">
                            <div class="s_pricing-item">
                                <img class="shape_img" src="/land_assets/img/price_line2.png" alt="">
                                <div class="tag_label">БЕСПЛАТНО</div>
                                <div class="s_price_icon p_icon2">
                                    <i class="flaticon-mortarboard"></i>
                                </div>
                                <h5 class="f_p f_600 f_size_20 t_color mb-0 mt_40">FREE навсегда 🤩</h5>
                                
                                <div class="price f_size_40 f_p f_700" style="line-height: initial;">
                                    <span style="color:#36a9e1">0 Руб <sub class="f_400 f_size_16">/ mo</sub></span>
                                </div>
                                <div style="text-align:left">
                                Пользуйтесь и изучайте платформу бесплатно сколько хотите.<br />
                                Здесь почти нет ограничений.
                                </div>
                                <a href="/signup" class="price_btn btn_hover mt_30" style="display:block" disable>Подключить</a>
                                <div style="text-align:left">Подключается автоматически.</div>
                            </div>
                        </div>
                   
                        
                        <div class="col-lg-6 col-sm-6">
                            <div class="s_pricing-item">
                                <img class="shape_img" src="/land_assets/img/price_line2.png" alt="">
                                <div class="tag_label">Без рекламы</div>
                                <div class="s_price_icon p_icon2">
                                    <i class="flaticon-idea"></i>
                                </div>
                                <h5 class="f_p f_600 f_size_20 t_color mb-0 mt_40">PRO 🤩😎</h5>
                                
                                <div class="price f_size_40 f_p f_700" style="line-height: initial;">
                                    <span style="color:#36a9e1">90 Руб <sub class="f_400 f_size_16">/ mo</sub></span>
                                    <div style="font-style: italic;
                                    font-weight: normal;
                                    color: #727c9c;
                                    font-size: 10px;">*При оплате за год.</div>
                                </div>
                                <div style="text-align:left">
                                    Все возможности без ограничений.<br />
                                    Личный помощник, который может все сделать за Вас.<br />
                                    Красивая ссылка на вашу страницу, которую можно менять.<br />
                                    И никакой рекламы.<br />
                                </div>
                                <a href="/signup" class="price_btn btn_hover mt_30" style="display:block">Подключить</a>
                                <div style="text-align:left">Подписка доступна в личном кабинете.</div>
                            </div>
                        </div>    
                        {{-- <div class="col-lg-6 col-sm-6">
                            <div class="s_pricing-item">
                                <img class="shape_img" src="/land_assets/img/price_line2.png" alt="">
                                <div class="tag_label">Popular</div>
                                <div class="s_price_icon p_icon2">
                                    <i class="flaticon-idea"></i>
                                </div>
                                <h5 class="f_p f_600 f_size_20 t_color mb-0 mt_40">PRO3 (на 3 месяца)</h5>
                                <p class="f_p f_400">За три месяца цена 349 Руб</p>
                                <p class="f_p f_400" style="text-align:left">Отсутствует помесячное продление, вы платите 1 раз в 3 месяцев</p>
                                
                                <div class="price f_size_40 f_p f_700" style="line-height: initial;">
                                    <span style="display:block;text-decoration: line-through;">447 Руб <sub class="f_400 f_size_16">/ 3 mo</sub></span>
                                    <span style="color: white;background-color: green;display:block;">Скидка 22%</span>
                                    <span style="color:green">349 Руб <sub class="f_400 f_size_16">/ 3 mo</sub></span>
                                </div>
                                        <ul class="list-unstyled mt_30" style="text-align:left;white-space: nowrap;">
                                            <li>Все преимущества бесплатной версии</li>
                                            <li>Работа с изображениями и анимацией</li>
                                            <li>Встраивайте Яндекс карту</li>
                                            <li>Google Tag и Facebook pixel</li>
                                            <li>Создавайте дизайн своих кнопок</li>
                                            <li>Интернет магазин</li>
                                            <li>Изменение ссылки страницы</li>
                                            <li>Никакой рекламы на странице</li>
                                        </ul>
                                <a href="/payment/11" class="price_btn btn_hover mt_30">Подключить</a>
                            </div>
                        </div> --}}
                    </div>
                    </div>




                    </div>
                </div>

            </div>
        </div>
    </section>
    @endsection