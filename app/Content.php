<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'content';

    protected $fillable = [
        'user_id', 'type_id', 'data','weight'
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function User()
    {
        return $this->belongsTo('App\User');
    }

    public function Type()
    {
        return $this->hasOne('App\TypeContent','id','type_id');
    }
}
