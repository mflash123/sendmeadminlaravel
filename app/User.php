<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Orchid\Metrics\Chartable;
use Orchid\Filters\Filterable;

//use Laravel\Scout\Searchable;

/**
 * App\User
 *
 * @property int $id
 * @property string|null $instagram_user_id
 * @property string|null $ava
 * @property string|null $ip_address
 * @property string|null $username
 * @property string $password
 * @property string|null $email
 * @property string|null $activation_selector
 * @property string|null $activation_code
 * @property string|null $forgotten_password_selector
 * @property string|null $forgotten_password_code
 * @property int|null $forgotten_password_time
 * @property string|null $remember_selector
 * @property string|null $remember_code
 * @property string|null $created_on
 * @property int|null $last_login
 * @property int|null $active
 * @property string|null $phone
 * @property string|null $instagram_token
 * @property int $has_added
 * @property string $cron_checked
 * @property int $tarif_id
 * @property int $is_checked
 * @property string|null $comment
 * @property string|null $domain
 * @property int $is_deleted
 * @property int $reg_type
 * @property string|null $forcelogin
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Tarif $Tarif
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereActivationCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereActivationSelector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCronChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDomain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereForcelogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereForgottenPasswordCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereForgottenPasswordSelector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereForgottenPasswordTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereHasAdded($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInstagramToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereInstagramUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsChecked($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereIsDeleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLastLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRegType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberSelector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTarifId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUsername($value)
 * @mixin \Eloquent
 */
//Change "extends Authenticatable" to "extends \Orchid\Platform\Models\User" in your User model
// class User extends Authenticatable implements MustVerifyEmail
//class User extends \Orchid\Platform\Models\User implements MustVerifyEmail
use Orchid\Platform\Models\User as Authenticatable;

class User extends Authenticatable

{
    use Notifiable, Chartable, Filterable;

    const CREATED_AT = 'created_on';
    //  use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'is_mobile', 'permissions', 'tarif_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'permissions'          => 'array',
        'email_verified_at' => 'datetime',
    ];

    protected $allowedFilters = [
        'username',
        'email',
    ];

    /**
     * @var array
     */
    protected $allowedSorts = [
        'username',
        'email',
        'updated_at',
        'created_on',
    ];

    public function Tarif()
    {
        return $this->belongsTo('App\Tarif');
    }

    public function Content()
    {
        return $this->hasMany('App\Content');
    }

    public function Order()
    {
        return $this->hasMany('App\Order', 'userId');
    }

    public function Category()
    {
        return $this->belongsToMany('App\Category','user_category','user_id','category_id');
    }
}
