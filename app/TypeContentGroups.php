<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeContentGroups extends Model
{
    protected $table = 'type_content_groups';

    public function typeContent()
    {
        return $this->hasMany('App\TypeContent','group_id');
    }
}
