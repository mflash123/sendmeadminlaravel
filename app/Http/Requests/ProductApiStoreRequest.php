<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductApiStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          //  'data.type' => 'regex:/^product_preview$/',
            'data.base64' => 'regex:/^data\:image.*/',

            'api_token' => 'required|exists:users,api_token',
            'data.name' => 'exclude_if:data.type,product_preview|required|min:5',
            'data.description' => 'exclude_if:data.type,product_preview|required|min:5',

            'data.price' => 'numeric',
            'data.currency' => 'string',

            'data.action.type' => 'numeric',
            'data.action.url' => 'url',
            'data.action.button' => 'exists:content,id',

            'data.images.*' => 'regex:/^\/storage\//',
        ];
    }
}
