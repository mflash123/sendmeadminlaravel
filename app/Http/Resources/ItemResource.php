<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Content;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        $enabled = true;
        /**
         * Я перешел на полностью бесплатную модель
         */
        // if(Content::find($this->id)->user->tarif_id > 1 ){
        //     $enabled = true;
        // } else {
        //     $enabled = (Content::find($this->id)->user->tarif_id == Content::find($this->id)->type->TypeContentGroup->tarif_id);
        // }

        return [
            'id'        =>  $this->id,
            'type_id'   =>  $this->type_id,
            'data'      =>  $this->data,
            'weight'    =>  $this->weight,
            'options'   =>  [],
            'enabled'    => $enabled ,
            // 'enabled'    =>  true,
            // 'secret' => $this->when( (Content::find($this->id)->type->TypeContentGroup->tarif_id==Content::find($this->id)->user->tarif_id) , function () {
            //     return 'secret-value';
            // }),
        //    'email' => $this->email,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
        ];
    }
}
