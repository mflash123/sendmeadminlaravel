<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\ContentTypeResource;

class ContentTypesResources extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       //  return parent::toArray($request);
       ContentTypeResource::withoutWrapping();

        return [
            ContentTypeResource::collection($this->collection),
          //  'meta' => ['song_count' => $this->collection->count()],

        ];
    }
}
