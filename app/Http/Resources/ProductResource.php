<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'data'          =>  json_decode($this->data),
            'active'        =>  $this->active,
            'content_id'    =>  $this->content_id,
            'root_image'    =>  env('APP_URL'),
        ];
    }
}
