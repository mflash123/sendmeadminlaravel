<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ItemResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       // return parent::toArray($request);
        $items = ItemResource::collection($this->content)->sortBy('weight')->filter(function ($value, $key) {
            /**
             * я убрал из items интеграцию jivo + fbpixel
             * + статичный БЕСПЛАТНЫЙ аватар
             * */
            if( ($value->type_id==33) or ($value->type_id==32) or ($value->type_id==1) ){
                return false;
            }
            return $value;
        });

        /**
         * Вывожу из контента сущность аватара для сингла. Несмотря на то, что он в content->это другая сущность
         * и уникальная в контексте пользователя
         */
        $avatar = ItemResource::collection($this->content)->first(function ($value, $key) {
            if($value->type_id==1){
                $value->data=json_decode($value->data);
                return $value;
            }
         });

        return [
            'id'            =>  $this->id,
            'username'      =>  $this->username,
            'items'         =>  $items->values()->all(),
            'isAdmin'       =>  $this->isAdmin,
            'tarif'         =>  $this->tarif,
            'isActive'      =>  $this->active,
            'avatar'        =>  $avatar,
            'theme'         =>  [
                'guest'     =>  $this->guest_theme,
                'dashboard' =>  $this->dashboard_theme,
            ],
            'isDeleted'     =>  $this->is_deleted,
        //    'email' => $this->email,
            // 'created_at' => $this->created_at,
            // 'updated_at' => $this->updated_at,
        ];
    }
}
