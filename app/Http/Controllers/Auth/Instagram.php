<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Http;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\InstagramUser;
use App\User;

class Instagram extends Controller
{
    /**
     * route:list выдает ошибку
     * Похоже надо валидировать https://laravel.com/docs/8.x/validation#form-request-validation
     * Если в в api.php закомментить Auth\Instagram@ ->то листинг заработает
     */
    use RegistersUsers;

    public function __construct(Request $request)
    {
        $request->validate([
            'api_token' => 'required',
        ]);

        $this->user = User::where('api_token',$request->input('api_token'))->first();

        if( !$this->user ){
            abort(403, 'Unauthorized action.');
        }
    }

    public function getRedirectUrl()
    {
        return response()->json(Socialite::driver('instagrambasic')->stateless()->redirect()->getTargetUrl());
    }

    public function callback(Request $request)
    {
        $request->validate([
            'code' => 'required',
        ]);
        //привязать к токену auth sendme

        $user = Socialite::driver('instagrambasic')->stateless()->user($request->input('code'));
        
        if ( InstagramUser::where('instagram_id',$user->user['id'] )->first() ){
            $ins = InstagramUser::where('instagram_id',$user->user['id'] )->first();
        } else {
            $ins = new InstagramUser();
        }

        
        $ins->user_id=$this->user->id;
        $ins->token=$user->token;
        $ins->instagram_id=$user->user['id'];
        $ins->account_type=$user->user['account_type'];
        //$ins->username=str_replace([".","_"],"-",$user->user['username']); //denyed symbols in subdomains
        $ins->username=$user->user['username'];
        $ins->save();

        //return response($ins);
    }

    public function isConnect()
    {

        return response()->json(( InstagramUser::where('user_id',$this->user->id )->exists() ) ? true : false);
    }
}