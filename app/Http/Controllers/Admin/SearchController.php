<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\User;

class SearchController extends Controller
{
    public function index(Request $request)
    {
        // $user = User::findOrFail($search);
        // if( $user ){
        //     return $user;
        // }
        //dd($request->query('search'));
        if(is_null($request->query('search'))){
            return redirect('/admin/users');
        }
        $search = $request->query('search');

        $data['users'] = DB::table('users')
        ->select('users.*','tarif.name as tarifName')
        ->leftJoin('tarif','users.tarif_id','=','tarif.id')
        ->where('users.username','LIKE','%'.$search.'%')
        ->orWhere('users.email','LIKE','%'.$search.'%')
        ->orWhere('users.id','=',$search)
        ->get();
        
        return view('users.list',$data);

       // dd($users);
    }
}
