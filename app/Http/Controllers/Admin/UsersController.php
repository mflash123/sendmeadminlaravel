<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Tarif;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

use App\Charts\UserChart;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd(Str::random(80));
        if($request->get('users')){
            $data['users'] = User::where('username','like',"%{$request->get('users')}%")
            ->orWhere('email','like',"%{$request->get('users')}%")
            ->get();
        } else{
            $data['users'] = User::select('users.*','tarif.name as tarifName')
            ->leftJoin('tarif','users.tarif_id','=','tarif.id')
            ->orderBy('id','desc')
            ->paginate(100);    
        }
       


        $data['users_count'] = User::all()->count();

        // ChartJS
        $data['charts_data'] = DB::table('users')
        ->select([DB::raw('count(*) as count'), DB::raw('DATE_FORMAT(created_on,\'%Y-%m\') as date')])
        ->where('created_on','>','2020-00-00')
        ->groupBy('date')
        ->get();

       //dd($data['charts_data']);
        
        $data['charts_data'] = $data['charts_data']->reject(function($user){
            if(is_null($user->date)) return true;
        });

        $labels = $data['charts_data'] ->map(function($user) {
           return $user->date;
        });
        $datasets = $data['charts_data'] ->map(function($user) {
            return $user->count;
        });
        
        $Chart = new UserChart;
        $Chart->labels($labels->values()->toArray());
        $Chart->dataset('Users by trimester', 'line', ($datasets->values()->toArray()) );
       // dd($Chart);
        $data['Chart'] = $Chart;

        $data['search']='users';

        return view('admin.users.list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // dd(User::find($id)->content);
        $data['user'] = User::findOrFail($id);
        $data['user']->tarif=Tarif::find($data['user']->tarif_id);
        $data['tarif'] = Tarif::all();
        $data['tarif']->map(function($itm) use ($data) {
            if($itm->id == $data['user']->tarif->id){
                return $itm->selected=TRUE;
            } else {
                return $itm->selected=FALSE;
            }
        });
       // dd($data['tarif']);
        //dd($data['user']->tarif);
        return view('admin.users.show',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $inputs = $request->except('_token','_method');
      //  dd($inputs);
      // $user = User::findOrFail($id);
      // $user->
        if( User::where('id',$id)
        ->update($inputs) ){
            return redirect()->back()->with('message', 'Saved');
        }

        
        //$user->fill($request);
        //$user->
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $user = User::findOrFail($id);
         $user->content->each->delete();
         $user->order->each->delete();
         $user->delete();
         return redirect('admin/users');
    }
}
