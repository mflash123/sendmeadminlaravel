<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Order;
use App\Charts\UserChart;
use Carbon\Carbon;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // dd(Carbon::now()->addMonth());
        $data['orders'] = Order::orderBy('id','desc')->paginate(15);
        $data['count'] = Order::all()->count();



        // foreach ($data['orders'] as $key => $value) {
        //     dd($value->statusorder);
        // }
        // dd($data['orders']);



        // ChartJS
        $data['charts_data'] = DB::table('orders')
        ->select([DB::raw('count(*) as count'), DB::raw('DATE_FORMAT(date,\'%Y-%m\') as date')])
        ->groupBy(DB::raw(' DATE_FORMAT(date,\'%Y-%m\') '))
        //->groupBy('date')
        ->get();
        
        
        $data['charts_data'] = $data['charts_data']->reject(function($user){
            if(is_null($user->date)) return true;
        });

        $labels = $data['charts_data'] ->map(function($user) {
            return $user->date;
        });
        $datasets = $data['charts_data'] ->map(function($user) {
            return $user->count;
        });
        
        $Chart = new UserChart;
        $Chart->labels($labels->values()->toArray());
        $Chart->dataset('Users by trimester', 'line', ($datasets->values()->toArray()) );
        $data['Chart'] = $Chart;

        return view('admin.orders.list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['order']=Order::findOrFail($id);
        return view('admin.orders.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->status = $request->input('status');
        $order->subscribe_until=Carbon::now()->addMonth(); //month-only PRO
        $order->save();

        //update tarif user
        $order->user->tarif_id=$order->tarifId;
        $order->user->active=1;
        $order->user->save();

       return back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
