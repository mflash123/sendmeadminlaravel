<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Content;
//use App\TypeContent;

class ContentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->get('content')){
            $data['contents'] = Content::where('data','like',"%{$request->get('content')}%")
           // ->get();
            ->paginate(15);
        } else{
            $data['contents'] = Content::orderby('id','desc')->paginate(15);  
        }

      //  $data['contents'] = Content::orderby('id','desc')->paginate(15);
        $data['count'] = Content::all()->count();
       // dd(Content::find(98)->user);
        $data['contents']->map(function($content){
          //  dd($content);
        });

        $data['search']='content';

       // dd($data['contents']);
        return view('admin.content.list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['content']=Content::findOrFail($id);
        return view('admin.content.show',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $content = Content::findOrFail($id);
        $content->data = $request->input('data');
        $content->save();
       // dd($request->input('data'));
       return back()->withInput();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Content::destroy($id);
        return redirect('admin/content');
        //return back();
    }
}
