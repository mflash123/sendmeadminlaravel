<?php

namespace App\Http\Controllers\Api\v2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Content;
use App\User;
use App\Category;
use Illuminate\Support\Facades\DB;

class ContentApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd(123);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::where('api_token', $request->post('api_token'))->firstOrFail();

        switch ($request->post('typeId')) {
            case 1: //avatar
                if (preg_match('/^data:image\/(\w+);base64,/', $request->post('data')['name'])) {

                    $data = substr($request->post('data')['name'], strpos($request->post('data')['name'], ',') + 1);

                    $data = base64_decode($data);
                    $filename = 'ava_' . $user->username . '.png';

                    // if (Storage::disk('public')->exists($filename)) {
                    //     Storage::disk('public')->delete($filename);
                    // }

                    Storage::disk('public')->put($filename, $data);

                    $content = new Content;


                    $str = ['fileName' => $filename, 'type' => $request->post('data')['type']];
                    // $content->data = json_encode($str);
                    // $content->type_id = $request->post('typeId');
                    // $content->user_id=$user->id;
                    // $content->weight=99999; //get weight
                    //return response($content);

                    $content = Content::updateOrCreate(
                        ['type_id' => 1, 'user_id' => $user->id],
                        ['data' => json_encode($str), 'weight' => 99999]
                    );



                    //$content->save();
                    return $content;
                }

                break;

            case 27:
                /** Avatar + banner */
                if (preg_match('/^data:image\/(\w+);base64,/', $request->post('data')['name'])) {

                    $data = substr($request->post('data')['name'], strpos($request->post('data')['name'], ',') + 1);

                    $data = base64_decode($data);
                    $filename = md5(time()) . '.png';
                    Storage::disk('public')->put($filename, $data);

                    $content = new Content;

                    $str = ['fileName' => $filename, 'type' => $request->post('data')['type']];
                    $content->data = json_encode($str);
                    $content->type_id = $request->post('typeId');
                    $content->user_id = $user->id;
                    $content->weight = 99999; //get weight
                    //return response($content);
                    $content->save();
                }
                break;

            case 28:
                /**
                 * Product moved to ProductApiController
                 */
                return false;

            case 21:
                //Custom Button
                /** Detect type */
                $content = new Content;
                $content->type_id = $request->post('typeId');
                $content->user_id = $user->id;
                $content->weight = 99999; //get weight


                // $button = $this::customButton($request);


                $data = $request->post('data');
                /** DETECT HTTP/ INSERT IF DOESNT EXIST */
                if ($data['type'] == 'link') {
                    $link = parse_url($data['link']);
                    if (!array_key_exists('scheme', $link)) {
                        //set default scheme
                        $link['scheme'] = 'http';
                    }
                    if (!array_key_exists('path', $link)) {
                        //set default scheme
                        $link['path'] = '';
                    }
                    $data['link'] = "{$link['scheme']}://{$link['host']}{$link['path']}";
                    //return $data;
                }
                //return $data;
                $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                $content->save();

                return response($content);
                break;

            case 26:
                //text block
                $content = new Content;
                $content->data = json_encode($request->post('data'), JSON_UNESCAPED_UNICODE);
                $content->type_id = $request->post('typeId');
                $content->user_id = $user->id;
                $content->weight = 99999; //get weight

                $content->save();
                break;

            case 34: //ya_map
            case 32: //fbpixel
            case 33: //gtm
                $content = new Content;
                $content->data = json_encode($request->post('data'), JSON_UNESCAPED_UNICODE);
                $content->type_id = $request->post('typeId');
                $content->user_id = $user->id;
                $content->weight = 99999; //get weight

                $content->save();
                break;

            case 35: //question-ask
                $content = new Content;
                $content->data = json_encode($request->post('data'), JSON_UNESCAPED_UNICODE);
                $content->type_id = $request->post('typeId');
                $content->user_id = $user->id;
                $content->weight = 99999; //get weight

                $content->save();
                break;

            case 36: //youtube
                $content = new Content;
                $content->data = json_encode($request->post('data'), JSON_UNESCAPED_UNICODE);
                $content->type_id = $request->post('typeId');
                $content->user_id = $user->id;
                $content->weight = 99999; //get weight

                $content->save();
                break;

            case 37: //customButtonv2
                $content = new Content;
                $content->data = json_encode($request->post('data'), JSON_UNESCAPED_UNICODE);
                $content->type_id = $request->post('typeId');
                $content->user_id = $user->id;
                $content->weight = 99999; //get weight

                //return $content;
                $content->save();
                return response($content);
                break;

            case 39:
                //text block Draftjs
                $content = new Content;
                $content->data = json_encode($request->post('data'), JSON_UNESCAPED_UNICODE);
                //$content->data = $request->post('data');
                $content->type_id = $request->post('typeId');
                $content->user_id = $user->id;
                $content->weight = 99999; //get weight

                $content->save();
                //return $content;
                break;

            case 99: //wizard
                if ($request->has('data.isavatar')) {
                    if ($request->post('data')['isavatar']) {
                        /**
                         * avatar
                         */
                        Content::where([
                            ['user_id', '=', $user->id],
                            ['type_id', '=', 1],
                        ])->delete();

                        $data = [
                            'fileName' => str_replace('/storage/', '', $request->post('data')['avatar']),
                            'type' => 'avatarSingle',
                        ];

                        $content = new Content;
                        $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                        $content->type_id = 1;
                        $content->user_id = $user->id;
                        $content->save();
                    }
                }
                if ($request->has('data.isdescription')) {
                    if ($request->post('data')['isdescription']) {
                        /**
                         * description
                         */
                        $data = [
                            'name' => $request->post('data')['description'],
                        ];
                        $content = new Content;
                        $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                        $content->type_id = 26;
                        $content->user_id = $user->id;
                        $content->save();
                    }
                }
                if ($request->post('data')['isemail']) {
                    /**
                     * email
                     */
                    $data = [
                        'name' => '📩' . $user->email,
                    ];
                    $content = new Content;
                    $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                    $content->type_id = 26;
                    $content->user_id = $user->id;
                    $content->save();
                }
                if ($request->post('data')['isphone']) {
                    /**
                     * email
                     */
                    $data = [
                        'name' => '📱Позвонить',
                        'destination' => $request->post('data')['phone'],
                    ];
                    $content = new Content;
                    $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                    $content->type_id = 16;
                    $content->user_id = $user->id;
                    $content->save();
                }
                if ($request->has('data.isinstagram')) {
                    if ($request->post('data')['isinstagram']) {
                        /**
                         * instagram
                         */
                        $data = [
                            'name' => 'Instagram',
                            'destination' => 'https://instagram.com/' . str_replace('@', '', $request->post('data')['username']),
                        ];
                        $content = new Content;
                        $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                        $content->type_id = 5;
                        $content->user_id = $user->id;
                        $content->save();
                    }
                }
                if ($request->post('data')['iswhatsapp']) {
                    /**
                     * whatsapp
                     */
                    $data = [
                        'name' => 'Whatsapp',
                        'destination' => $request->post('data')['phone'],
                    ];
                    $content = new Content;
                    $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                    $content->type_id = 6;
                    $content->user_id = $user->id;
                    $content->save();
                }

                if ($request->has('data.categories')) {
                    if ($request->post('data')['categories']) {
                        /**
                         * Categories
                         */
                        foreach ($request->post('data')['categories'] as $value) {
                            DB::table('user_category')
                                ->updateOrInsert(
                                    ['user_id' => $user->id, 'category_id' => $value['id']],
                                    ['user_id' => $user->id, 'category_id' => $value['id']]
                                );
                        }
                    }
                }
                //$request->post('data')['isemail']
                break;

            default:

                /** Для интеграции костыль, я не передаю тип, а пытаюсь его узнать здесь */
                if (isset($request->post('data')['type']) and ($request->post('data')['type'] == 'integration')) {
                    //return $request->post('data')['data']['fbpixel'];

                    foreach ($request->post('data')['data'] as $key => $value) {

                        $data = $request->post('data')['data'];
                        if ($key == 'fbpixel') {
                            $content = Content::where([['user_id', $user->id], ['type_id', 33]])->first();
                            if (!$content) {
                                $content = new Content;
                            }
                            $content->data = json_encode([$key => $value], JSON_UNESCAPED_UNICODE);
                            $content->type_id = 33;
                            $content->user_id = $user->id;
                            $content->weight = 99999; //get weight
                            $content->save();
                        }
                    }
                    return response()->json(['msg' => 'saved', 'request' => $content]);
                }
                // return $request->post('data')['type'];

                if ($request->post('typeId') == 11) {
                    //11 site
                    $data = array(
                        'destination' => $request->post('data')['destination'],
                        'name'  =>  $request->post('data')['name'],
                    );
                } else {
                    //$url = parse_url($request->post('data')['destination']);
                    $data = array(
                        //'destination' => preg_replace('/\//','',$url['path']),
                        'destination' => $request->post('data')['destination'],
                        'name'  =>  $request->post('data')['name'],
                    );
                }

                //return $data;

                $content = new Content;
                $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                $content->type_id = $request->post('typeId');
                $content->user_id = $user->id;
                $content->weight = 99999; //get weight

                $content->save();
                break;
        }


        // var_dump($content);
        return response()->json(['msg' => 'saved', 'request' => $content]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $content = Content::find($id);
        $content->type;

        // dd($content->type);
        return response()->json($content);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** 
         * TODO :   Token validation
         *          Check name/destination values
         */
        $content = Content::findOrFail($id);

        // return response($request->input('data'));

        // if($content->type_id==21){
        //     $this::customButton($request);
        // } else {
        //     $content->data = $request->input('data');
        // }
        $content->data = $request->input('data');
        $content->save();
        return response()->json($request->input('data'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        /** 
         * TODO :   Token validation
         */
        $content = Content::findOrFail($id);
        $content->delete();
        return response()->json('removed');
    }

    private function customButton($request)
    {
        return response()->json($request->post('data'));
        /** Button prepare */
        $app = app();
        $button = $app->make('stdClass');
        $button->type = $request->post('data')['type'];


        $button->design = $app->make('stdClass');
        $button->design->type = $request->post('data')['variant'];

        $button->name = $request->post('data')['data']['name'];
        $button->description = $request->post('data')['data']['isDescription'] ? $request->post('data')['data']['description'] : false;


        /** Destination */
        switch ($button->type) {
            case 'link':
                $button->link = $request->post('data')['data']['linkUrl'];
                break;

            case 'call':
                $button->call = $request->post('data')['data']['callNumber'];
                break;

            case 'email':
                $button->email = $request->post('data')['data']['email'];
                $button->emailsubject = $request->post('data')['data']['emailSubject'];
                break;

            default:
                return response('TYPE BTN: ' . $button->type);
                break;
        }

        /** Design */
        switch ($button->design->type) {
            case 'contained':
                $button->design->bkgcolor = $request->post('data')['data']['bkgColor'];
                $button->design->fontcolor = $request->post('data')['data']['fontColor'];
                $button->design->radius = $request->post('data')['data']['slider'];
                break;

            case 'outlined':
                $button->design->brdcolor = $request->post('data')['data']['brdColor'];
                $button->design->fontcolor = $request->post('data')['data']['fontColor'];
                $button->design->radius = $request->post('data')['data']['slider'];
                break;

            case 'text':
                $button->design->fontcolor = $request->post('data')['data']['fontColor'];
                break;

            default:
                return response('Design unknow: ' . $button->design);
                break;
        }

        return $button;
    }
}
