<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests\ApitokenRequest;
use App\Http\Resources\ContentCollectionResource;

use App\Content;
use App\User;
use App\TypeContent;

class ActionApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(ApitokenRequest $request)
    {
        $validated = $request->validated();
        $request->validate([
            'type_id' => 'exists:type_content,id',
        ]);

        $user = User::where('api_token', $validated['api_token'])->firstOrFail();

        $type_ids = TypeContent::
            //where([['active', 1]]) //я решил, что кнопку можно скрыть на дашборде, а на продукте вывести
            whereIn('group_id', [1, 2, 9])
            ->whereNotIn('id', [1])
            ->pluck('id');


        //   return response($type_ids);

        $data = ContentCollectionResource::collection(Content::where([
            ['user_id', $user->id],
        ])
            ->whereIn('type_id', $type_ids)
            ->get());
        return response($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(ApitokenRequest $request, $id)
    {
        return new ContentCollectionResource(Content::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
