<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Tarif;
use App\Order;
use Illuminate\Support\Facades\Log;
use Telegram\Bot\Api;
use Carbon\Carbon;

class PaymentApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->testmode=FALSE;

        if($this->testmode){
            $this->password1 = 'OrsnGid47EBO16oDrDM9';
            $this->password2 = 'yaHt989q3ED1XYQsmoTF';
        } else {
            $this->password1 = 'iUdA94Hl1qn1B9JOyZUe';
            $this->password2 = 'j5c5nAbhd3uwH25EKqCw';
        }
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $token = $request->input('api_token');
        $tarif_id = $request->input('tarif_id');

        $user = User::where('api_token',$token)->first();
        if(!$user){
            $msg = [
                'error'=>1,
                'msg'=>'User doesnt exist'
            ];
            return response()->json($msg,403);
        }

        $tarif = Tarif::find($tarif_id);

        if(!$tarif){
            $msg = [
                'error'=>1,
                'msg'=>'Tarif doesnt exist'
            ];
            return response()->json($msg,403);
        }


        $order = new Order;
        $order->tarifId = $tarif->id;
        $order->userId = $user->id;

        if($order->tarifId==10){
            /** Pro1 Archive */
            $order->subscribe_until=Carbon::now()->addMonth(1); //month-only PRO
        } elseif ($order->tarifId==11) {
            /** Pro3 Archive */
            $order->subscribe_until=Carbon::now()->addMonth(3);
        } elseif ($order->tarifId==12) {
            /** Pro12 Archive */
            $order->subscribe_until=Carbon::now()->addMonth(12);
        } elseif ($order->tarifId==13) {
            /** Pro1 */
            $order->subscribe_until=Carbon::now()->addMonth(1);
        } elseif ($order->tarifId==14) {
            /** Pro3 */
            $order->subscribe_until=Carbon::now()->addMonth(3);
        } elseif ($order->tarifId==15) {
            /** Pro12 */
            $order->subscribe_until=Carbon::now()->addMonth(12);
        } elseif ($order->tarifId==2) {
            /** TRIAL Archive */
            return response()->json($this::trial($order,$tarif));
            
        }
        //return response()->json($order);
        $order->save();
       // $order->url = $payment->getPaymentUrl();

       $payment = new \Idma\Robokassa\Payment(
        'sendmecc', $this->password1, $this->password2, $this->testmode
        );
        
        $payment
            ->setInvoiceId($order->id)
            ->setSum($tarif->priceRUB)
            ->setDescription('Sendmecc: '.$tarif->name);


        $msg = [
            'error'=>0,
            'msg'=>$payment->getPaymentUrl()
        ];
        return response()->json($msg);
    }

    private function trial(Order $order, Tarif $tarif)
    {
        /** Check if user created trial */
        if(Order::where([
                ['userId',$order->userId],
                ['tarifId',$order->tarifId],
            ])->exists()){

                $msg = [
                    'error'=>1,
                    'msg'=>'Trial expired '
                ];
                return $msg;
        }
        /** Check if user innactive or deleted */
        if(User::where([
            ['id',$order->userId],
            ['active',0]
            ])
            ->orWhere([
                ['id',$order->userId],
                ['is_deleted',1]
                ])
            ->exists()){
                $msg = [
                    'error'=>1,
                    'msg'=>'User innactive or deleted'
                ];
                return $msg;
        }

        $days = $tarif->data;
        $order->subscribe_until=Carbon::now()->addDays($days);
        $order->status=2;
        $order->save();

        User::where('id',$order->userId)
        ->update(['tarif_id'=>$order->tarifId]);

        /** Email notification */
        $user = User::find($order->userId);
        \Illuminate\Support\Facades\Mail::to($user->email)
        ->send(new \App\Mail\ThanksTrial($user,$order));

        return [
            'error'=>0,
            'msg'=>'https://sendme.cc/dashboard',
            'status'=>'Trial activated'
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function payment_result(Request $request)
    {
        Log::info('Robokassa payment result');
        Log::info(json_encode($_GET));

        $payment = new \Idma\Robokassa\Payment(
            'sendmecc', $this->password1, $this->password2, $this->testmode
        );
        

        $order = Order::find( $request->input('inv_id') );
        $user = User::find( $order->userId );
        
        
        if ($order) {
            $tarif = Tarif::find( $order->tarifId);
            if($tarif->priceRUB==$request->input('OutSum')){
                $order->status=2;
                $order->save();

                $user->tarif_id = $order->tarifId;
                $user->save();
                //Send email
                \Mail::to($order->user->email)
                ->send(new \App\Mail\ThanksPayment($tarif));
 
            }
        };


        //Send telegram
        $str = "Sendme@Оплата\nЗаказ: $order->id\n";
        $str .="Сумма: $tarif->priceRUB";
        $telegram = new Api('1060361306:AAFMaK7KT1FpALbNKdEwJjv_qbfOvzFmaq0');
        $response = $telegram->sendMessage([
            'chat_id' => '253455673', //'-318481081', 
            'text' => $str,
        ]);



        // send answer
        return response('OK'.$order->id); // "OK1254487\n"
        

    }

}
