<?php

namespace App\Http\Controllers\Api\v2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Resources\ProductResource;
use Image;

use App\Product;
use App\Content;
use App\User;

use App\Http\Requests\ProductApiStoreRequest;

class ProductApiController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function products($content_id)
    {
        $products = Product::where('content_id', $content_id)
            ->orderBy('weight')
            ->get();
        return response()->json($products);
    }

    public function product($product_id)
    {
        $product = new ProductResource(Product::find($product_id));
        return $product;
    }


    public function store(ProductApiStoreRequest $request)
    {
        
        
        $user = User::where('api_token',$request->post('api_token'))->firstOrFail();
        /** PRODUCT */
        /** Product img */
        /**
         * сюда захожу в 2х вариантах
         * 1 - product_preview - сохраняю картинку для товара, просто превью, без всяких записей в бд
         * 2 - уже сохраняю товар в базе
         */
        if ((array_key_exists('type', $request->post('data'))) && ($request->post('data')['type'] == 'product_preview')) {
            if (isset($request->post('data')['base64'])) {
                return response()->json($this::getSavedImageFromBase64($request));
            }
        }

        $validated = $request->validated();

        if (count($validated['data']['images']) < 1) {
            $app = app();
            $product = $app->make('stdClass');
            $product->status = false;
            $product->message = "Images required";
            return response()->json($product,422);
        }

        /** Search widget or create new
         * In current ver widget exist only one
         */
        $widget = Content::where([['user_id', $user->id], ['type_id', 29]])->first();
        if (!isset($widget->id)) {
            /** Create widget */
            $widget = new Content;
            $widget->user_id = $user->id;
            $widget->type_id = 29;
            $widget->data = json_encode(['name' => 'product_widget']);
            $widget->weight = 99999; //get weight
            $widget->save();
        }
        /** End search/create widget in content */

        $app = app();
        $product = $app->make('stdClass');
        $product->name = $validated['data']['name'];
        $product->description = $validated['data']['description'];

        if (array_key_exists('price', $validated['data'])) {
            //цена
            $product->price = floatval($validated['data']['price']);
            $product->currency = $validated['data']['currency'];
        }
        if (array_key_exists('action', $validated['data'])) {
            //действие
            $product->action = $app->make('stdClass');
            $product->action->type = $validated['data']['action']['type'];

            if ($product->action->type == 1) {
                $product->action->url = $validated['data']['action']['url'];
            }
        }


        $product->attach = $app->make('stdClass');
        $product->attach->images = $validated['data']['images'];

        $json = json_encode($product, JSON_UNESCAPED_UNICODE);

        $product = new Product;
        $product->data = $json;
        $product->user_id = $user->id;
        $product->content_id = $widget->id;
        $product->weight = 99999; //get weight

        $product->status = $product->save();

        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductApiStoreRequest $request, $id)
    {
        $validated = $request->validated();
        //return response($validated);

        $app = app();
        $product = $app->make('stdClass');

        $product->name = $validated['data']['name'];
        $product->description = $validated['data']['description'];



        if (array_key_exists('price', $validated['data'])) {
            //цена
            $product->price = floatval($validated['data']['price']);
            $product->currency = $validated['data']['currency'];
        }
        if (array_key_exists('action', $validated['data'])) {
            //действие
            $product->action = $app->make('stdClass');
            $product->action->type = $validated['data']['action']['type'];

            if ($product->action->type == 1) {
                $product->action->url  = $validated['data']['action']['url'];
            }
            if ($product->action->type == 5) {
                //button action
                $product->action->button  = $validated['data']['action']['button'];
            }
        }

        $product->attach = $app->make('stdClass');
        $product->attach->images = $validated['data']['images'];

        $json = json_encode($product, JSON_UNESCAPED_UNICODE);

        $product = Product::find($id);
        $product->data = $json;
        //  $product->user_id=$user->id;
        //  $product->content_id=$widget->id;    
        //   $product->weight=99999; //get weight

        $product->save();




        // return response($request->input('data')['data']['name']);
        return response()->json([['msg' => 'update ok'], ['product' => $product]]);
    }

    public function weightUpdate(Request $request)
    {
        if ($request->data) {
            foreach ($request->data as $key => $value) {
                //  $value->id
                $product = Product::find($value['id']);
                $product->weight = $key;
                $product->save();
                //     dd($product);

                # code...
            }
            // return $product;
            return response()->json('zaebis');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return response()->json('delete');
    }

    private function getSavedImageFromBase64(Request $request)
    {
        $request->validate([
            'data.base64' => 'required|starts_with:data:image',
        ]);

        if (preg_match('/^data:image\/(\w+);base64,/', $request->post('data')['base64'])) {
                        
            $data = substr($request->post('data')['base64'], strpos($request->post('data')['base64'], ',') + 1);
        
            $data = base64_decode($data);
            $filename=md5(time()).'.png';
            $data = Image::make($data)->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->stream('png', 100);
            


            Storage::disk('public')->put($filename, $data);

            return Storage::url($filename);


            // use jpg format and quality of 100

// then use Illuminate\Support\Facades\Storage
//Storage::disk('your_disk')->put('path/to/image.jpg', $resized_image); // check return for success and failure



            // if( $request->post('data')['type']=='product_preview' ){
            //     return response()->json($filename);
            // }

            // return response()->json('NOT READY');
            

            // $content = new Content;
            // $str = ['fileName'=>$filename,'type'=>$request->post('data')['type']];
            // $content->data = json_encode($str);
            // $content->type_id = $request->post('typeId');
            // $content->user_id=$user->id;
            // $content->weight=99999;
            // $content->save();

        }

        return response($request);

    }
}
