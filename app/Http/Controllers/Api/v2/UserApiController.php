<?php

namespace App\Http\Controllers\Api\v2;


use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;


use App\User;
use App\InstagramUser;
use App\Content;
use App\Order;
use App\Product;
use App\TypeContent;
use Carbon\Carbon;
use InstagramScraper\Instagram;
use Illuminate\Support\Facades\Storage;

class UserApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($username)
    {
        //
        // UserResource::withoutWrapping();
        // $user = User::find(432);
        // $user->isAdmin=true;
        // return new UserResource($user);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * token example - aHOsbuagjjBjozfp7H0KaTeMHsXwqas1qanR7QrNBtFOXOWdH9kO0YaSdeuhna9OgrjLRvH7W5YYFdip
     */
    public function show($username, Request $request)
    {
        //dd($request->query('token'));
        UserResource::withoutWrapping();
        $user = User::where('username', $username)->firstOrFail();
        if ($user->is_deleted) return response('Account deleted'); //надо вывести вьюху что удален, сделано на скорость

        $user->isAdmin = FALSE;
        if ($user->api_token == $request->query('api_token')) {
            User::where('id', $user->id)->update(['last_login' => Carbon::now()->toDateTimeString()]);
            $user->isAdmin = TRUE;
            $user->tarif = (object) ['id' => $user->tarif->id, 'name' => $user->tarif->name];
        }
        //  dd($user->content);
        return new UserResource($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 'edit';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $username)
    {
        $validated = $request->validate([
            'username' => 'required|unique:users|max:255|min:5|alpha_num', //проблема что если русский будет
            'api_token' => 'required',
        ]);

        $user = User::where('username', $username)->firstOrFail();

        if ($user) {
            if ($user->api_token == $validated['api_token']) {
                if ($user->tarif->id > 2) {
                    $user->username = $validated['username'];
                    $user->save();
                    return response()->json(['error' => false], 200);
                }
                return response()->json(['error' => 'Subscribe error.'], 403);
            }
            return response()->json(['error' => 'User or token error.'], 403);
        }
        return response()->json(['error' => 'User doesnt exist.'], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $validated = $request->validate([
            'api_token' => 'required',
        ]);

        $user = User::where('api_token', $validated['api_token'])->first();

        if ($user->id == $id) {
            Content::where('user_id', $user->id)->delete();
            Product::where('user_id', $user->id)->delete();
            Order::where('userId', $user->id)->delete();
            User::where('id', $user->id)->delete();
            return response()->json(['error' => false], 200);
        }

        return response()->json(['error' => 'User doesnt exist.'], 403);
    }

    public function getinstagram(Request $request)
    {
        $user = User::where('api_token', $request->query('api_token'))->firstOrFail();
        $inst = InstagramUser::where('user_id', $user->id)->first();

        /**
         * https://rapidapi.com/restyler/api/instagram28
         * 0 - 20/d
         * 1 - 50/d
         * 10 - 500/d
         */
        $response = \Unirest\Request::get(
            "https://instagram28.p.rapidapi.com/user_info?user_name=" . $inst->username,
            array(
                "X-RapidAPI-Host" => "instagram28.p.rapidapi.com",
                "X-RapidAPI-Key" => "3aa499cbbfmsh4e6a99ff5abe261p110a64jsna59c7b0f83b9"
            )
        );

        if ($response->code != 200) {
            return response()->json([
                'response' => $response->body,
                'error' => 'response error.'
            ], 403);
        }

        if(!isset($response->body->data)){
            return response()->json([
                'response' => $response->body,
                'error' => 'response empty.'
            ], 403);
        }
        // $res = $instagram->getAccount($inst->username);

        $avatar = file_get_contents($response->body->data->user->profile_pic_url_hd);
        // Image::make($avatar)->save(public_path('/upload/cover/' . $filename));
        Storage::disk('public')->put('ava_' . $inst->username . '.png', $avatar);

        $account = [
            'Biography' => $response->body->data->user->biography,
            'Username'  =>  $response->body->data->user->username,
            'externalUrl'   =>  $response->body->data->user->external_url,
            'profilePicUrlHd'   => Storage::url('ava_' . $inst->username . '.png'),
        ];


        return response()->json($account);
    }
}
