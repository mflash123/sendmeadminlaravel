<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Tarif
 *
 * @property int $id
 * @property string $name
 * @property int $priceRUB
 * @property string $data json(id types) NOT SURE
 * @property string $date
 * @property int $active
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $user
 * @property-read int|null $user_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tarif newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tarif newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tarif query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tarif whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tarif whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tarif whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tarif whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tarif whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tarif wherePriceRUB($value)
 * @mixin \Eloquent
 */
class Tarif extends Model
{
    protected $table = 'tarif';

    public function user()
    {
        return $this->hasMany('App\User');
    }

    // public function name()
    // {
    //     //Tarif name
    //     return $this->
    // }
}
