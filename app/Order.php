<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Orchid\Filters\Filterable;

/**
 * App\Order
 *
 * @property int $id
 * @property int $tarifId
 * @property int $userId
 * @property int $status 1-new,2-ok
 * @property string $date
 * @property int $expired order life cycle
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereTarifId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUserId($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
    use Filterable;

    protected $fillable = [
        'status', 'expired', 'subscribe_until',
    ];

    protected $allowedSorts = [
        'id',
        'username',
        'date',
        'subscribe_until',
    ];

    public function User()
    {
        return $this->belongsTo('App\User','userId');
    }

    public function StatusOrder()
    {
        return $this->belongsTo('App\Status','status');
    }

    public function Tarif()
    {
        return $this->belongsTo('App\Tarif','tarifId');
    }
}
