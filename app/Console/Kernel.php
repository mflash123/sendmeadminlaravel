<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('order:check')->daily();

        /** Send order notification expire */
        $schedule->command('order:notification_expired 3')->daily();
        $schedule->command('order:notification_expired 1')->daily();
        $schedule->command('content:zero 2')->daily();

        $schedule->command('telescope:prune --hours=48')->daily();

        //update disposable emails db
        $schedule->command('disposable:update')->weekly();

        //remove users active=0
        $schedule->command('users:remove 7')->weekly();

        //sitemap
        $schedule->command('sitemap:generate')->weekly();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
