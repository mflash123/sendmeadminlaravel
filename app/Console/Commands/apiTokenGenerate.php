<?php

namespace App\Console\Commands;


use Illuminate\Support\Str;
use Illuminate\Console\Command;
use App\User;

class apiTokenGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apiGenerate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate tokens for API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "\nStart\n";
        $users = User::all();
        foreach ($users as $user) {
            $user->api_token = Str::random(80);
            if($user->id==1947) $user->api_token = 'aHOsbuagjjBjozfp7H0KaTeMHsXwqas1qanR7QrNBtFOXOWdH9kO0YaSdeuhna9OgrjLRvH7W5YYFdip'; //mflash123 token
            $user->save();
        }

        echo "\nDone\n";
    }
}
