<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\User;

class OrderGenerateUntil extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:makeExpired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute once. Make orders to Expired and user to tarif = 1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /** Переношу все ордера в expired, кроме 7 */
        $orders = Order::whereNotIn('tarifId',[7])->where('id','!=',1259)->get();
        foreach ($orders as $order) {
            $order->expired=1;
            $order->save();
        }

        /** Обнуляю тариф всем пользователям, кроме тем, у кого 1 и 7 */
        $users = User::whereNotIn('tarif_id',[1,7])->get();
        foreach ($users as $user) {
            $user->tarif_id=1;
            $user->save();
        }

        $this->info('Done. Never do it again.');
        
    }
}



