<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Content;
use App\Order;
use App\Product;
use Carbon\Carbon;

class removeInnactiveUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:remove {days}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove users where active=0 after {N days} after registration';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $days = $this->argument('days');
        $this->info('Innactive users deleting after days: '.$days);
        $users = User::where('active',0)->get();
        
        $this->info('Fetching users where active=0. Count: '.count($users));

        foreach ($users as $user) {
            $date = Carbon::parse($user->created_on);
            if($date->diffInDays(Carbon::now()) > $days){
                $this->info('Expired user. Removing...');
                $this->info('User:'.$user->id);
                $this->info('Created_on:'.$user->created_on);
                $this->info('Diff:'.$date->diffInDays(Carbon::now()));

                $this->info('Removing conent...');
                Content::where('user_id',$user->id)->delete();

                $this->info('Removing products...');
                Product::where('user_id',$user->id)->delete();

                $this->info('Removing orders...');
                Order::where('userId',$user->id)->delete();

                $this->info('Removing users...');
                User::where('id',$user->id)->delete();
            }
        }

        $this->info('Finish');
    }
}
