<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Content;

class ConvertSpecificContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:content {type_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert spicific content';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type_id = $this->argument('type_id');

        $contents = Content::where([
            ['type_id',$type_id],
            //['user_id',1434],
            ])->get();

        //dd($contents);

        switch ($type_id) {
            
            case 26: //mail
                foreach ($contents as $content) {
                    $data = json_decode($content->data);

                    $data->name = preg_replace("/&#?[a-z0-9]+;/i","",$data->name);
                    $data->name = str_replace('\n', '', $data->name);
                    $data->name = trim($data->name);

                    $content->data = json_encode($data,JSON_UNESCAPED_UNICODE);
                    $content->save();
                }
                return;

            case 17: //mail
                foreach ($contents as $content) {
                    $content->delete();
                }
                return;
            
            case 6:
            case 8:
            case 9:
            case 10:
            case 15:
            case 16:
            case 19:
            case 20:
            case 24:
            case 5: //instagram  {"name":"Instagram","destination":"\"dimasiberian\""}
            case 7: //telegram
            case 11: //web
                foreach ($contents as $content) {
                    $data = json_decode($content->data);
                    $data->destination = json_decode($data->destination);
                    $content->data = json_encode($data,JSON_UNESCAPED_UNICODE);
                    $content->save();
                }
                return;
            

        }
    }
}
