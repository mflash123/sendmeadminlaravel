<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

class OrderNotificationExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:notification_expired {days}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'REQ* days. Send emails to users who has soon expired orders. days expired = {days}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days');
        $this->info('Soon expired start... Days: '.$days);

        $this->info('Check paid orders start');
        $orders = Order::where([
            ['expired',0],
            ['status',2]
        ])->get();

        $this->info('Orders to proceed: '.count($orders));

        foreach ($orders as $order) {
            $this->info('Order : '.$order->id.' Expire: '.$order->subscribe_until);
            $date = Carbon::parse($order->subscribe_until);
            
            if($date->diffInDays(Carbon::now())==$days){
                $this->info('Send email to notify: '.$order->id);
                $user = User::find($order->userId);
                $this->sendEmail($user,$order);
            }
           
        }


        $this->info('Soon expired finish');
    }

    private function sendEmail(User $user,Order $order)
    {
        Mail::to($user->email)
        ->send(new \App\Mail\SubscribeExpiredSoon($user,$order));
        $this->info('Email finished');
    }
}
