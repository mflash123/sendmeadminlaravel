<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Content;

class ConverContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ConvertContent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert content from entity to humanable format';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

       
        //dd(1);

        //Banner and parallax convert
        $content=Content::where([
            ['type_id',27],
            ])->get();

        foreach ($content as $value) {
            $value->data = str_replace(['parallax'],'bannerTop',$value->data);
            echo "\nStart\n";
            echo $value->data;
            echo "\n====\n";
            $value->save();
        }


        $content=Content::where([
            ['type_id',26],
          //  ['user_id',1947],
            ])->get();
        
        foreach ($content as $value) {
            echo "\nStart\n";
            echo $value->data;
            echo "\n====\n";
           // Remove \n, etc symbols...
            $result=stripslashes(trim(html_entity_decode($value->data),'"'));
            $value->data=strip_tags($result);
            $value->save();
        }

        /**
         * Deleting old unused types
         */
        Content::whereIn('type_id',array(1,2,3,4,22,25,28,29))->delete();
        
        //dd($content->data);

        /**
         * convert data column to json
         * data: { name:'%name%',destination:'' }
         */

        $content=Content::whereIn('type_id',[5,6,7,8,9,10,11,15,16,19,20,24])->get();

        foreach ($content as $value) {
            echo "\nStart\n";
            echo $value->type->humanName;
            echo "\n====\n";

            $obj = app()->make('stdClass');
            $obj->name=$value->type->humanName;
            $obj->destination=$value->data;
            echo(json_encode($obj));
            $value->data=json_encode($obj,JSON_UNESCAPED_UNICODE);
            $value->save();


        }

        $content=Content::whereNotIn('type_id',[5,6,7,8,9,10,11,15,16,19,20,24,27])->get();

        foreach ($content as $value) {

            $obj = app()->make('stdClass');
            $obj->name=$value->data;

            $value->data=json_encode($obj,JSON_UNESCAPED_UNICODE);
            $value->save();

        }


        //Custom button convert
        $content=Content::where([
            ['type_id',21],
            ])->get();

        foreach ($content as $value) {
            //максимально примтивно, только название и урл, остальное в топку

            // dd(json_decode(json_decode($value->data)->name));

            dump($value->data);
            $tmp = json_decode($value->data);
            
            $tmp = json_decode($tmp->name);

            if(!isset($tmp->name)){
                $tmp = json_decode($tmp);
                //dd($tmp);
                $name = $tmp->name;
                $link = $tmp->link;
            } else {
                $name = $tmp->name;
                $link = $tmp->link;
            }
            
            //dd($tmp);
            // $name = json_decode(json_decode($value->data)->name)->name;
            // if(!isset($name)){
            //     dd($value);
            // }
            // if(is_nul(json_decode(json_decode($value->data)->link))) {
            //     dd($value->data);
            // }
            // $link = json_decode(json_decode($value->data)->link);

            echo "\n$name\n";
            echo "\n$link\n";
            // $value->data = str_replace(['avatarTop','parallax'],'banner',$value->data);
            // echo "\nStart\n";
            // echo $value->data;
            // echo "\n====\n";
            // $value->save();
            $str = '{"type":"link","name":"%NAME%","description":false,"design":{"fontColor":"#d9e3f0","brdColor":"#22194D","bkgColor":"#22194D","type":"contained","radius":10},"link":"%LINK%"}';
            //$str = '{"type":"link","name":"%NAME%","description":false,"design":{"fontColor":"#000","brdColor":"#22194D","bkgColor":"#22194D","type":"text","radius":10},"link":"%LINK%"}';
            $value->data = str_replace(['%NAME%','%LINK%'],[$name,$link],$str);
            //$value->data = str_replace('%LINK%',$link,$str);
            //dd($value->data);
            $value->save();
        }



    }
    
}
