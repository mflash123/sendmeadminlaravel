<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Content;

class ConvertTmp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'convert:contentfull';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Convert content to full path, del after execute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //SELECT * FROM `ci_content` WHERE type_id in(5,7,9,10,20,24)
        $contents = Content::whereIn('type_id', [5,7,9,10,20,24])->get();
        foreach ($contents as $content) {
            if($content->type_id==5){
                $data = json_decode($content->data);
                $data->destination = trim($data->destination);

                //определяю если есть http(s) -> continue
                $scheme = parse_url($data->destination, PHP_URL_SCHEME);
                if(!is_null($scheme)) continue;

                $data->destination = preg_replace('/@/', '', $data->destination);
                $data->destination = "https://instagram.com/".$data->destination;

                $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                $content->save();
            } elseif ($content->type_id==7) {
                $data = json_decode($content->data);
                $data->destination = trim($data->destination);

                //определяю если есть http(s) -> continue
                $scheme = parse_url($data->destination, PHP_URL_SCHEME);
                if(!is_null($scheme)) continue;

                $data->destination = "https://t.me/".$data->destination;
                $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                $content->save();
            } elseif ($content->type_id==9) {
                $data = json_decode($content->data);
                $data->destination = trim($data->destination);

                //определяю если есть http(s) -> continue
                $scheme = parse_url($data->destination, PHP_URL_SCHEME);
                if(!is_null($scheme)) continue;

                $data->destination = preg_replace('/"/', '', $data->destination);
                

                //dd($data->destination);
                //echo "$data->destination\n";

                $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                $content->save();
            } elseif ($content->type_id==10) {
                $data = json_decode($content->data);
                $data->destination = trim($data->destination);

                //echo "$data->destination\n";

                //определяю если есть http(s) -> continue
                $scheme = parse_url($data->destination, PHP_URL_SCHEME);
                if(!is_null($scheme)) continue;

                $data->destination = preg_replace('/"/', '', $data->destination);
                

                //dd($data->destination);
                //echo "$data->destination\n";

                $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                $content->save();
            } elseif ($content->type_id==20) {
                $data = json_decode($content->data);
                $data->destination = trim($data->destination);

                //echo "$data->destination\n";

                //определяю если есть http(s) -> continue
                $scheme = parse_url($data->destination, PHP_URL_SCHEME);
                if(!is_null($scheme)) continue;

                $data->destination = preg_replace('/"/', '', $data->destination);
                

                //dd($data->destination);
                //echo "$data->destination\n";

                $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                $content->save();
            } elseif ($content->type_id==24) {
                $data = json_decode($content->data);
                $data->destination = trim($data->destination);

                //echo "$data->destination\n";

                //определяю если есть http(s) -> continue
                $scheme = parse_url($data->destination, PHP_URL_SCHEME);
                if(!is_null($scheme)) continue;

                $data->destination = preg_replace('/"/', '', $data->destination);
                

                //dd($data->destination);
                //echo "$data->destination\n";

                $content->data = json_encode($data, JSON_UNESCAPED_UNICODE);
                $content->save();
            }

            //dd($content);
        }
    }
}
