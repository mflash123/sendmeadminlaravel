<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Order;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Mail;

class OrderCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make unpaid orders expired. And check active paid orders and update user tarif';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Unpaid + expired
         */
        $this->info('Expired unpaid start');
        Order::where([
            ['expired',0],
            ['status',1],
            ['created_at','<',Carbon::yesterday()]
        ])->update(['expired'=>1]);
        $this->info('Expired unpaid finish');
        /**
         * Paid
         */
        $this->info('Check paid orders start');
        $orders = Order::where([
            ['expired',0],
            ['status',2]
        ])->get();
        //dump(Carbon::yesterday());
        //dump($orders);
        foreach ($orders as $order) {
            if($order->subscribe_until < Carbon::yesterday() ){ //даю фору 1 день
                $this->expired($order);
            }   
        }
        $this->info('Check paid orders finish');
    }

    private function expired(Order $order)
    {
        $order->expired=1;
        $order->save();
        $this->info('Expired order : '.$order->id);


        /**
         * Email
         */
        $user = User::find($order->user->id);
        if($user->is_deleted){
            $this->info('User deleted. Stop sending :'.$user->email);
            return true;
        }

        $this->info('Email sending... :'.$user->email);
  
        Mail::to($user->email)
        ->send(new \App\Mail\SubscribeExpired($user,$order));
        $this->info('Email finished');


        $this->info('Updating user->id : '.$order->user->id.' to FREE');
        User::where('id',$order->user->id)->update(['tarif_id'=>1]);


    }
}
