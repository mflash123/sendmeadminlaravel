<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Mail;

class ContentZeroNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'content:zero {days}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send users notifications if content zero after reg = {days}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $days = $this->argument('days');

        $users = User::where('created_on','>',DB::raw('DATE_SUB(CURDATE(), INTERVAL '.$days.' DAY)'))->get();
        
        $this->info('Users proceed count : '.count($users));
        /** Checking content count */
        foreach ($users as $user) {
            if(count($user->content)<2){

                $this->info('User content count < 2 : '.$user->id);
                $this->info('Email send : '.$user->email);

                Mail::to($user->email)
                ->send(new \App\Mail\ContentNotify($user));

                $this->info('Email finished');
            }
        }
        
    }
}
