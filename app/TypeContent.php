<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeContent extends Model
{
    protected $table = 'type_content';

    public function Content()
    {
       // return $this->belongsTo('App\Content','type_id');
        return $this->hasMany('App\Content','type_id');
    }

    public function TypeContentGroup()
    {
        return $this->hasOne('App\TypeContentGroups','id','group_id');
    }
}
