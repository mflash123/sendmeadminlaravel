<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'order_status';

    public function Orders()
    {
        return $this->hasMany('App\Order','status');
    }
}
