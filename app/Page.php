<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;
use Orchid\Filters\Filterable;

class Page extends Model
{
    use HasFactory, AsSource, Filterable;

    protected $fillable = [
        'title',
        'description',
        'data',
        'url',
        'active',
    ];

    protected $allowedSorts = [
        'id',
        'title',
        'created_at',
        'updated_at'
    ];

    protected $allowedFilters = [
        'title',
        'url',
    ];
}
