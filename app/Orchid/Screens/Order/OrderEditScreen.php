<?php

namespace App\Orchid\Screens\Order;

use Orchid\Screen\Screen;
use App\Order;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Color;
use App\Orchid\Layouts\Order\OrderEditLayout;
use Illuminate\Http\Request;
use Orchid\Support\Facades\Toast;

class OrderEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'OrderEditScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'OrderEditScreen';

    public $permission = 'platform.systems.users';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Order $order): array
    {
        $this->order = $order;

        if (! $order->exists) {
            $this->name = 'Create order';
        }


        return [
            'order'       => $order,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make(__('Remove'))
            ->icon('trash')
            ->confirm(__('Once the account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.'))
            ->method('remove')
            ->canSee($this->order->exists),

            Button::make(__('Save'))
            ->icon('check')
            ->method('save'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::block(OrderEditLayout::class)
            ->title(__('Order Information'))
            ->commands(
                Button::make(__('Save'))
                    ->type(Color::DEFAULT())
                    ->icon('check')
                    ->canSee($this->order->exists)
                    ->method('save')
            ),
        ];
    }

    public function save(Order $order, Request $request)
    {
        $userData = $request->get('order');
        $order
            ->fill($userData)
            ->save();

        Toast::info(__('Order was saved.'));

        return redirect()->route('platform.systems.orders');
    }
}
