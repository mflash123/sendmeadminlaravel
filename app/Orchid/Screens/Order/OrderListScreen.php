<?php

namespace App\Orchid\Screens\Order;

use Orchid\Screen\Screen;
use App\Orchid\Layouts\Order\OrderListLayout;
use App\Order;

class OrderListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'OrderListScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'OrderListScreen';

    public $permission = 'platform.systems.users';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
     // dd(Order::find(1280));
        return [
            'orders' => Order::filters()->orderBy('id','desc')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            OrderListLayout::class
        ];
    }
}
