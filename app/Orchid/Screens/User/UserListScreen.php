<?php

declare(strict_types=1);

namespace App\Orchid\Screens\User;

use App\Orchid\Layouts\User\UserEditLayout;
use App\Orchid\Layouts\User\UserFiltersLayout;
use App\Orchid\Layouts\User\UserListLayout;
use App\Orchid\Layouts\UsersChartMonthLayout;
use App\Orchid\Layouts\UsersChartDayLayout;
use Illuminate\Http\Request;
//use Orchid\Platform\Models\User;
use App\User;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;
use Illuminate\Support\Facades\DB;

class UserListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'User';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'All registered users';

    /**
     * @var string
     */
    public $permission = 'platform.systems.users';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        $users = User::selectRaw(DB::raw('count(id) as count,DATE_FORMAT(created_on,\'%Y-%m\') as label'))
            ->orderByDesc('created_on')
            ->groupBy(DB::raw('DATE_FORMAT(created_on,\'%Y-%m\')'))
            ->limit(6)
            ->get();

        $user_chart_month[0]['labels'] = [];
        $user_chart_month[0]['values'] = [];
        foreach ($users as $user) {
            array_push($user_chart_month[0]['labels'], $user->label);
            array_push($user_chart_month[0]['values'], $user->count);
        }
        $user_chart_month[0]['labels'] = array_reverse($user_chart_month[0]['labels']);
        $user_chart_month[0]['values'] = array_reverse($user_chart_month[0]['values']);


        $users = User::selectRaw(DB::raw('count(id) as count,DATE_FORMAT(created_on,\'%Y-%m-%d\') as label'))
            ->orderByDesc('created_on')
            ->groupBy(DB::raw('DATE_FORMAT(created_on,\'%Y-%m-%d\')'))
            ->limit(30)
            ->get();


        $user_chart_day[0]['labels'] = [];
        $user_chart_day[0]['values'] = [];
        foreach ($users as $user) {
            array_push($user_chart_day[0]['labels'], $user->label);
            array_push($user_chart_day[0]['values'], $user->count);
        }
        $user_chart_day[0]['labels'] = array_reverse($user_chart_day[0]['labels']);
        $user_chart_day[0]['values'] = array_reverse($user_chart_day[0]['values']);

        return [
            'users' => User::with('roles')
                ->filters()
                ->filtersApplySelection(UserFiltersLayout::class)
                ->defaultSort('id', 'desc')
                ->paginate(),
            'chart_month' => $user_chart_month,
            'chart_day' => $user_chart_day,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make(__('Add'))
                ->icon('plus')
                ->route('platform.systems.users.create'),
        ];
    }

    /**
     * Views.
     *
     * @return string[]|\Orchid\Screen\Layout[]
     */
    public function layout(): array
    {
        return [
            UsersChartMonthLayout::class,
            UsersChartDayLayout::class,
            UserFiltersLayout::class,
            UserListLayout::class,

            Layout::modal('oneAsyncModal', UserEditLayout::class)
                ->async('asyncGetUser'),
        ];
    }

    /**
     * @param User $user
     *
     * @return array
     */
    public function asyncGetUser(User $user): array
    {
        return [
            'user' => $user,
        ];
    }

    /**
     * @param User    $user
     * @param Request $request
     */
    public function saveUser(User $user, Request $request): void
    {
        $request->validate([
            'user.email' => 'required|unique:users,email,' . $user->id,
        ]);

        $user->fill($request->input('user'))
            ->save();

        Toast::info(__('User was saved.'));
    }

    /**
     * @param Request $request
     */
    public function remove(Request $request): void
    {
        User::findOrFail($request->get('id'))
            ->delete();

        Toast::info(__('User was removed'));
    }
}
