<?php

namespace App\Orchid\Screens\Page;

use Orchid\Screen\Screen;
use Orchid\Screen\Actions\Link;
use App\Page;
use App\Orchid\Layouts\Page\PageListLayout;

class PageListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'PageListScreen';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'PageListScreen';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'pages' =>  Page::filters()->defaultSort('id','desc')->paginate()
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create page')
                ->icon('pencil')
                ->route('platform.page.edit'),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            PageListLayout::class
        ];
    }
}
