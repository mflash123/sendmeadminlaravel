<?php

namespace App\Orchid\Screens\Page;

use Orchid\Screen\Screen;
use App\Page;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Quill;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Alert;
use Illuminate\Http\Request;
use Orchid\Support\Color;
use Illuminate\Support\Str;
use Orchid\Screen\Fields\CheckBox;

class PageEditScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'Page create';

    /**
     * Display header description.
     *
     * @var string|null
     */
    public $description = 'Page Edit';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(Page $page): array
    {
        $this->exists = $page->exists;

        if ($this->exists) {
            $this->name = 'Edit page';
        }

        return [
            'page' => $page,
        ];
    }

    /**
     * Button commands.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): array
    {
        return [
            Button::make('Create page')
                ->icon('pencil')
                ->method('createOrUpdate')
                ->canSee(!$this->exists),

            Button::make('Update')
                ->icon('note')
                ->method('createOrUpdate')
                ->canSee($this->exists),

            Button::make('Remove')
                ->icon('trash')
                ->method('remove')
                ->canSee($this->exists),
        ];
    }

    /**
     * Views.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('page.title')
                    ->title('Title')
                    ->placeholder('Attractive but mysterious title')
                    ->required()
                    ->help('Specify a short descriptive title for this page.'),

                Input::make('page.url')
                    ->title('Url')
                    ->required()
                    ->canSee($this->exists),

                TextArea::make('page.description')
                    ->title('Description')
                    ->rows(3)
                    ->maxlength(200)
                    ->required()
                    ->placeholder('Brief description for preview'),

                CheckBox::make('page.active')
                    ->sendTrueOrFalse()
                    ->placeholder('Show'),

                Quill::make('page.data')
                    //->toolbar(["text", "color", "header", "list", "format"])
                    ->required()
                    ->title('Main text'),
            ])
        ];
    }

    public function createOrUpdate(Page $page, Request $request)
    {
        $page
            ->fill($request->get('page'))
            ->fill([
                'url' => Str::slug($page->title),
            ])
            ->save();


        Alert::info('You have successfully created an post.');

        return redirect()->route('platform.page.list');
    }

    /**
     * @param Page $page
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Page $page)
    {
        $page->delete();

        Alert::info('You have successfully deleted the post.');

        return redirect()->route('platform.page.list');
    }
}
