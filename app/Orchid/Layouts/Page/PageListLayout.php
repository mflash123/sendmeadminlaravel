<?php

namespace App\Orchid\Layouts\Page;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use App\Page;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Actions\Button;
use Orchid\Support\Color;

class PageListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'pages';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('id', 'ID')
                ->sort(),

            TD::make('title', 'Title')
                ->sort()
                ->filter(TD::FILTER_TEXT)
                ->render(function (Page $page) {
                    return Link::make($page->title)
                        ->route('platform.page.edit', $page);
                }),

            TD::make('url', 'Url')
                ->filter(TD::FILTER_TEXT)
                ->render(function (Page $page) {
                    return Link::make($page->url)
                        ->href('https://sendme.cc/page/' . $page->url);
                }),

            TD::make('edit', 'Edit')
                ->render(function (Page $page) {
                    return Link::make('Edit')
                        ->icon('pencil')
                        ->route('platform.page.edit', $page) . '<li> Active:' . $page->active . '</li>';
                }),

            TD::make('created_at', 'Date of publication'),

        ];
    }
}
