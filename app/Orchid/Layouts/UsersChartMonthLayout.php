<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Chart;

class UsersChartMonthLayout extends Chart
{
    /**
     * Add a title to the Chart.
     *
     * @var string
     */
    protected $title = 'Month registration (limit 6)';

    /**
     * Available options:
     * 'bar', 'line',
     * 'pie', 'percentage'.
     *
     * @var string
     */
    protected $type = 'line';

    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the chart.
     *
     * @var string
     */
    protected $target = 'chart_month';

    /**
     * Determines whether to display the export button.
     *
     * @var bool
     */
    protected $export = false;
}
