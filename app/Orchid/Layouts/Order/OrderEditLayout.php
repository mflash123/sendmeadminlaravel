<?php

namespace App\Orchid\Layouts\Order;

use Orchid\Screen\Concerns\Makeable;
use Orchid\Screen\Field;
use Orchid\Screen\Layouts\Rows;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\DateTimer;

class OrderEditLayout extends Rows
{
    /**
     * Used to create the title of a group of form elements.
     *
     * @var string|null
     */
    protected $title;

    /**
     * Get the fields elements to be displayed.
     *
     * @return Field[]
     */
    protected function fields(): array
    {
        return [
            Input::make('order.id')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('id'))
                ->disabled(),

            Input::make('order.user.username')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Username'))
                ->disabled(),

            Input::make('order.tarif.name')
                ->required()
                ->title(__('Tarif'))
                ->disabled(),

            Input::make('order.tarif.data')
                ->title(__('Description'))
                ->disabled(),

            Input::make('order.status')
                ->required()
                ->title(__('Status')),

            Input::make('order.expired')
                ->required()
                ->title(__('Expired')),

            DateTimer::make('order.date')
            ->title(__('Created'))
            ->required()
            ->disabled(),

            DateTimer::make('order.subscribe_until')
            ->title(__('Expire'))
            ->required(),

            // TextArea::make('user.utm')
            //     ->type('text')
            //     ->disabled()
            //     ->title(__('Utm'))
            //     ->placeholder(__('Utm')),
        ];
    }
}
