<?php

namespace App\Orchid\Layouts\Order;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use App\Order;
use Orchid\Screen\Actions\Link;
use Illuminate\Support\Carbon;

class OrderListLayout extends Table
{
    /**
     * Data source.
     *
     * The name of the key to fetch it from the query.
     * The results of which will be elements of the table.
     *
     * @var string
     */
    protected $target = 'orders';

    /**
     * Get the table cells to be displayed.
     *
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::make('id', __('Id'))
            ->sort()
            ->render(function (Order $order) {
                return Link::make($order->id)
                ->route('platform.systems.orders.edit', $order);
            }),

            TD::make('User', __('User'))
            //->sort()
            ->render(function (Order $order) {
                return (Link::make($order->user['username'].'  -- '.$order->user['id'])
                ->route('platform.systems.users.edit', $order->user['id'])).
                ('<span style="padding:.25rem .5rem">user tarif:'.$order->user->tarif->id.'</span>');
            }),

            TD::make('Tarif', __('Tarif'))
            //->sort()
            ->render(function (Order $order) {
                $txt = $order->status==1?'Unpaid':'Paid';
                $color = $order->status==1?'red':'green';
              return (
                  '<ul style="list-style: none;padding: 0;">'.
                  '<li>'.$order->tarif['name'].'('.$order->tarif['id'].')'.'</li>'.
                  '<li style="color:'.$color.'">'.$txt.' ('.$order->status.')</li>'.
                  '</ul>'
              );
            }),

             TD::make('date', __('Created'))
             ->sort()
             ->render(function (Order $order) {
                 return $order->date;
             }),

             TD::make('updated_at', __('Last edit'))
             ->render(function (Order $order) {
                 return $order->updated_at;
             }),

             TD::make('subscribe_until', __('Deadline'))
             ->sort()
             ->render(function (Order $order) {
                 $now = Carbon::now();
                 $expired = $now>$order->subscribe_until?'EXPIRED':'';
               //  dump($expired);
                 return (
                    '<ul style="list-style: none;padding: 0;">'.
                    '<li>'.$order->subscribe_until.'</li>'.
                    '<li>'.$expired .'</li>'.
                    '</ul>'
                    );
             }),


        ];
    }
}

// TD::make('underlyingSubType', __('underlyingSubType'))
// ->render(function (Pair $pair) {
//     return view('Pair.Component.UnderlyingSubType', [
//         'pairs' => $pair->underlyingSubType,
//         'is_subscribe' => true,
//     ]);
// }),



