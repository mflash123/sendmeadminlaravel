<?php

declare(strict_types=1);

namespace App\Orchid\Layouts\User;

use Orchid\Screen\Field;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Layouts\Rows;

class UserEditLayout extends Rows
{
    /**
     * Views.
     *
     * @return Field[]
     */
    public function fields(): array
    {
        return [
            Input::make('user.username')
                ->type('text')
                ->max(255)
                ->required()
                ->title(__('Username'))
                ->placeholder(__('Username')),

            Input::make('user.email')
                ->type('email')
                ->required()
                ->title(__('Email'))
                ->placeholder(__('Email')),

            Input::make('user.tarif_id')
                ->type('text')
                ->required()
                ->title(__('Tarif'))
                ->placeholder(__('Tarif')),

            TextArea::make('user.utm')
                ->type('text')
                ->disabled()
                ->title(__('Utm'))
                ->placeholder(__('Utm')),
        ];
    }
}
