<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InstagramUser extends Model
{
    use HasFactory;

    protected $table = 'instagram_user';
    
    protected $fillable = [
        'user_id',
        'token',
        'instagram_id',
        'account_type',
        'username',
    ];


}
