<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Order;
use Carbon\Carbon;

class SubscribeExpiredSoon extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /**
         * разделяю TRIAL от обычных тарифов, поскольку разный текст
        */
        //dd( $this->user->order->where('expired',0)->where('status',2) );
        if($this->user->tarif->id==2){
            $data = [
                'trial'     =>  true,
                'subject'   =>  'Напоминание о скором конце пробного периода'
            ];
        } else {
            $data = [
                'trial'     =>  false,
                'subject'   =>  'Напоминание о скором конце подписки'
            ];
        }

        /**
         * Собираю контент, который скрою на бесплатном тарифе
         */
        $contents = $this->user->content;
        foreach ($contents as $content) {
            $content->enabled=false;
            if( $content->type->TypeContentGroup->tarif_id==1 ){
                $content->enabled=true;
            }
        }

        return $this->view('emails.SubscribePayment.expiredPaymentSoon')
        ->with([
            'tarif_id'  =>  $this->user->tarif->id,
            'username'  =>  $this->user->username,
            'login'     =>  $this->user->email,
            'contents'  =>  $contents,
            'date_end'  =>  $this->order->subscribe_until,
            'trial'     =>  $data['trial'],
        ])
        ->subject($data['subject']);
    }
}
