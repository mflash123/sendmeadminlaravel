<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Order;
use Carbon\Carbon;

class ContentNotify extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.Content.ContentZeroNotification')
        ->with([
            'username'  =>  $this->user->username,
            'login'     =>  $this->user->email,
        ])
        ->subject('Нам грустно что не используете Sendme =( Давайте мы сделаем Вам сайт бесплатно?');
    }
}
