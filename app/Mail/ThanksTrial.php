<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Order;
use Carbon\Carbon;

class ThanksTrial extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Order $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.thanksTrial')
        ->with([
            'username'          =>  $this->user->username,
            'login'             =>  $this->user->email,
            'subscribe_until'   =>  $this->order->subscribe_until,
        ])
        ->subject('У Вас подключен пробный тариф');
    }
}
