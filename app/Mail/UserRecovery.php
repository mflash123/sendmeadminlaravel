<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRecovery extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($forgotten_password_code)
    {
        $this->forgotten_password_code = $forgotten_password_code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.userRecovery')
        ->with([
            'forgotten_password_code'  =>  $this->forgotten_password_code,
            'app_url_front'   =>  env('APP_URL_FRONT')
        ])
        ->subject('Восстановления доступа')
        ;
    }
}
