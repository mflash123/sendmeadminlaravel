<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertTarif extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tarif')->whereNotIn('id',[1])->update(['active'=>0]);
        
        DB::table('tarif')->insertOrIgnore([
            ['id' => 10, 'name' => 'Pro1', 'priceRUB' => '149' ,'data' => '1 month'],
            ['id' => 11, 'name' => 'Pro3', 'priceRUB' => '349' ,'data' => '3 months'],
            ['id' => 12, 'name' => 'Pro12', 'priceRUB' => '999' ,'data' => '12 months'],
        ]);

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('tarif')->whereIn('id',[10,11,12])->delete();
        DB::table('tarif')->update(['active'=>1]);
    }
}
