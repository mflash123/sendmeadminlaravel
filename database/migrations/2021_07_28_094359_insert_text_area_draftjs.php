<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class InsertTextAreaDraftjs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('type_content_groups')->insertOrIgnore([
            ['id' => 15, 'tarif_id' => 1, 'humanName' => 'Текстовый блок', 'active' => 1, 'classIcon' => 'far fa-font', 'weight' => 4],
        ]);
        DB::table('type_content')->insertOrIgnore([
            ['id' => 39, 'group_id' => 15, 'humanName' => 'Text field', 'name' => 'Textfield', 'active' => 1],
        ]);

        DB::table('type_content')->where('id', 26)->update(['active' => 0]);
        DB::table('type_content_groups')->where('id', 5)->update(['active' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('type_content_groups')->where('id', 15)->delete();
        DB::table('type_content')->where('id', 39)->delete();
    }
}
