<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;

class InsertNewTypeContentYandex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('type_content')->insertOrIgnore([
            ['id' => 34, 'group_id' => 13, 'name' => 'ya_map' ,'humanName' => 'Яндекс карта'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('type_content')->whereIn('id',[34])->delete();
    }
}
