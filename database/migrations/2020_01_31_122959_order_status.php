<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;

class OrderStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('order_status')){
            Schema::create('order_status', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name',100);
                $table->timestamps(0);
            });
    
            DB::table('order_status')->insert([
                [
                    'id'    =>  1,
                    'name'  =>  'new',
                ],
                [
                    'id'    =>  2,
                    'name'  =>  'paid',
                ]
            ]);
    
            Schema::table('orders', function (Blueprint $table) {
                $table->foreign('status')->references('id')->on('order_status');
            });
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_status');
    }
}
