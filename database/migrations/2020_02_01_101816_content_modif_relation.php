<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContentModifRelation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_content', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('group_id')->change();
        });

        Schema::table('content', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('user_id')->change();
            $table->unsignedBigInteger('type_id')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_content', function (Blueprint $table) {
            //
        });
    }
}
