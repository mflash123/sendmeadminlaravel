<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;

class AddTypeContentRecord extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('type_content_groups')->insertOrIgnore([
            ['id' => 13, 'tarif_id' => 3, 'humanName' => 'Интеграции'],
        ]);

        DB::table('type_content')->insertOrIgnore([
            ['id' => 32, 'group_id' => 13, 'name' => 'fbpixel' ,'humanName' => 'Facebook Pixel'],
            ['id' => 33, 'group_id' => 13, 'name' => 'gtm' ,'humanName' => 'Google Tag Manager'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('type_content')->whereIn('id',[32,33])->delete();
        DB::table('type_content_groups')->where('id', 13)->delete();
    }
}
