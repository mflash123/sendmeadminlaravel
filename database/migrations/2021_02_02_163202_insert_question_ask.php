<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertQuestionAsk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('type_content_groups')->insertOrIgnore([
            [
                'id'        =>  14,
                'tarif_id'  =>  1,
                'humanName' =>  'Вопрос - Ответ',
                'active'    =>  1,
                'weight'    =>  4
            ],
        ]);

        DB::table('type_content')->insertOrIgnore([
            [
                'id'        =>  35,
                'group_id'  =>  14,
                'name'      =>  'question_ask',
                'humanName' =>  'Вопрос - Ответ',
                'active'    =>  1
            ],
        ]);

        DB::table('type_content_groups')
        ->where('id', 13)
        ->update(['weight' => 10]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('type_content_groups')->where('id', 14)->delete();
        DB::table('type_content')->where('id', 35)->delete();
    }
}
