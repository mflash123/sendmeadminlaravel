<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\User;

class ModifyUserCreatedon extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * просто указываю левую дату, потому что я уже не найду реальную
         */
        $users = User::where('created_on',NULL)->get(); //ok
        foreach ($users as $user) {
            $user->created_on = date("2018-m-d H:i:s");
            $user->save();         
        }

        $users = User::where('created_on','not like','%:%')->get(); //ok
        foreach ($users as $user) {
            $date = new DateTime($user->created_on);
            $user->created_on = $date->format('Y-m-d H:i:s');
            DB::table('users')
                ->where('id', $user->id)
                ->update(['created_on' => $date->format('Y-m-d H:i:s')]);  
        }
        
        DB::statement('ALTER TABLE `ci_users` CHANGE `created_on` `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
