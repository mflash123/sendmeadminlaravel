<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class InsertNewCustomButton extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('type_content')->insertOrIgnore([
            ['id' => 37, 'group_id' => 9, 'name' => 'CustomButtonV2' ,'humanName' => 'Custom Button'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('type_content')->where('id', 37)->delete();
    }
}
