<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateNewPricesSeed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('tarif')->where('id', 10)->update(['name' => 'Pro1.Archieve', 'active' => 0]);
        DB::table('tarif')->where('id', 11)->update(['name' => 'Pro3.Archieve', 'active' => 0]);
        DB::table('tarif')->where('id', 12)->update(['name' => 'Pro12.Archieve', 'active' => 0]);

        DB::table('tarif')->insert([
            ['name' => 'Pro1', 'priceRUB' => 90, 'data' => '1 month', 'active' => 1],
            ['name' => 'Pro3', 'priceRUB' => 252, 'data' => '3 months', 'active' => 1],
            ['name' => 'Pro12', 'priceRUB' => 799, 'data' => '12 months', 'active' => 1],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
