<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\Hash;
use \Illuminate\Support\Facades\DB;

class CreateAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('ip_address')->nullable()->change();
            $table->text('created_on')->nullable()->change();
            $table->text('forcelogin')->nullable()->change();
        });

        DB::table('users')->insert([
            [
                'email'     =>  'admin@laravel',
                'password'  =>  Hash::make('882815'),
                'username'  =>  'admin@laravel',
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
