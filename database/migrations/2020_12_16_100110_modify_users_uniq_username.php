<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyUsersUniqUsername extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $duplicates = DB::table('users')
        ->select('username', DB::raw('COUNT(*) as `count`'))
        ->groupBy('username')
        ->havingRaw('COUNT(*) > 1')
        ->get();

        foreach ($duplicates as $value) {
            $users = DB::table('users')
            ->select('id','username')
            ->where('username',$value->username)
            ->get();
            foreach ($users as $user) {
                DB::table('users')
                ->where('id', $user->id)
                ->update(['username' => $user->username.'_'.$user->id]);
            }
        }

        Schema::table('users', function (Blueprint $table) {
            $table->string('username')->unique()->change();
            $table->string('email')->unique()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
