<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });

        DB::table('categories')->insert([
            ['name' => 'Личный бренд'],
            ['name' => 'Блогер'],
            ['name' => 'Искусство'],
            ['name' => 'АвтоМото'],
            ['name' => 'Литература'],
            ['name' => 'Бизнес'],
            ['name' => 'Образование'],
            ['name' => 'Финансы'],
            ['name' => 'Фитнес'],
            ['name' => 'Еда'],
            ['name' => 'Игры'],
            ['name' => 'Правительство'],
            ['name' => 'Здоровье'],
            ['name' => 'Красота'],
            ['name' => 'Продажа товара/услуг'],
            ['name' => 'Путешествие'],
            ['name' => 'Инфобизнес'],
            ['name' => 'Взрослые 18+'],
            ['name' => 'Азартные игры 18+'],
            ['name' => 'Другое'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
