<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContentTypeColumnsRemover extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('type_content', function (Blueprint $table) {
            $table->dropColumn(['placeholder']);
            $table->dropColumn(['backgroundColor']);
            $table->dropColumn(['classIcon']);
        });
    }
}
