<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertYoutubeContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('type_content')->insertOrIgnore([
            [
                'id'        =>  36,
                'group_id'  =>  7,
                'name'      =>  'youtube_embed',
                'humanName' =>  'Youtube',
                'active'    =>  1
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('type_content')->where('id', 36)->delete();
    }
}
