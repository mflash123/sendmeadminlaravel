<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;

class ContentTypeRelationContentTypeGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Make custom button relate to group */
        DB::table('type_content')->where('id', 21) 
        ->update(['group_id' => 4]);

        /** disable old types content */
        DB::table('type_content')->whereIn('id', [12,13,22]) 
        ->update(['active' => 0]);
        DB::table('type_content_groups')->whereIn('id', [4,6,11])
        ->update(['active' => 0]);

        /** modify for new properties */
        DB::table('type_content_groups')->whereIn('id', [7])
        ->update(['humanName' => 'Images']);

        DB::table('type_content')->whereIn('id', [27,30]) 
        ->update(['group_id' => 7]);
     
        /** Prepare for relations */
        Schema::table('type_content_groups', function (Blueprint $table) {
            $table->bigIncrements('id')->change();
        });


        Schema::table('type_content', function (Blueprint $table) {
            $table->unsignedBigInteger('group_id')->change();
            $table->foreign('group_id')->references('id')->on('type_content_groups');
        });
    }
}
