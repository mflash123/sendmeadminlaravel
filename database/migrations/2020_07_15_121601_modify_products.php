<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;

class ModifyProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** Create column v2(boolean) means convert product for new frontend */

        /** Default v2-true, BUT old records=false */
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('date');
            $table->bigIncrements('id')->change();
            $table->unsignedBigInteger('content_id');
            
            $table->boolean('v2')->after('active')
            ->default(true);
            $table->timestamps(0);
        });

        DB::table('products')->update(['v2' => false]);

        /** Cant make before upgrade all products and relate to content */
        // Schema::table('products', function (Blueprint $table) {
        //     $table->foreign('content_id')->references('id')->on('content');
        // });

        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('v2');
            $table->dropColumn('created_at');
            $table->dropColumn('updated_at');
        });
    }
}
