<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use League\HTMLToMarkdown\HtmlConverter;
use PHPMailer\PHPMailer\PHPMailer;

//use Unsplash;

Route::get('/sitemap.xml', function () {
   return response(Storage::disk('local')->get('sitemap.xml'))->header('Content-Type', 'text/xml');
});




Route::get('blog/p/{url_name}', function ($url_name) {
   if ($url_name == 'tarif') {
      return Redirect::to('/tarif');
   }
   $content = Illuminate\Support\Facades\DB::table('pages')
      ->where('url', $url_name)
      ->first();
   if (!$content) abort(404);
   return view('landing.content', [
      'content' => $content->data,
      'title' => $content->title,
      'description' => $content->description,
   ]);
})->name('blog.page.show');


Route::get('/tarif', function () {
   return view('landing.tarif');
});


Route::get('/blog', function () {
   $content = Illuminate\Support\Facades\DB::table('pages')
      ->where([['type_id', 1],['active',1]])
      ->get();
   return view('landing.blog', ['posts' => $content]);
});
/**
 * Statics routes finish
 */




//Auth::routes();

// Route::get('auth/facebook', [SocialController::class, 'facebookRedirect']);
// Route::get('auth/facebook/callback', [SocialController::class, 'loginWithFacebook']);

Route::get('/auth/fb/redirect', function () {
   return Socialite::driver('facebook')
      ->redirect();
});

Route::get('/auth/fb/callback', function () {
   // ./ngrok http 127.0.0.1:8000
   $user = Socialite::driver('facebook')->user();


   $fb = new \Facebook\Facebook([
      'app_id' => '2522856534684017',
      'app_secret' => '76f9baed49f6944d732d0671b16cce72',
      // 'default_graph_version' => 'v2.10',
      // 'default_access_token' => 'IGQVJXb0dhT09YUGM5S1J5TWlyUVVqbXZA5NnRleDJCVjltOWtDLUZAGWEgzOGJrMjAzLVQxczAzYkRWX3MyZAVZAKaGpsODgxTFhYMjZAnOVJuc1JmNTVIQWJyNUpWRHhDSGlOd2NJNDZA4eDliOVd1TzB2QklRQnR0Q1d1YlVV', // optional
   ]);

   $response = $fb->get(
      '/me',
      // '/17841400765276283',
      $user->token,
   );
   dd($response);

   // $user->token EAAj2hkZBRsXEBAJteNsQOqHqu6uBZCg07VeH1xDGoxTNCRQPZB5ATBZClfh2jyS0bnUkjvSrhZAvWnIfIZCsc0Gv7M000RYNzQFuIe3FZCZAExDUFNvvxIJ4VZCZAjtXmQUBCw6GFsopLesbmuHjdAlzC4Suwx5Qq2bAHfPPvsYkspTLXJEA6rB87V
});




Route::get('/testunsplash', function () {
   //я запаузил задачу по выбору шаблона-медиа. Я не особо представляю задачу. И у меня лимиты на 50 запросов в час
   // $response = Unsplash::randomPhoto()
   //  ->orientation('squarish')

   //  //->color() https://unsplash.com/documentation#search-photos
   //  ->term('people')
   //  ->count(2)
   //  ->toJson();
   $response = '[{"id":"hnP3sBOPzK4","created_at":"2021-02-09T15:27:28-05:00","updated_at":"2021-05-08T23:17:37-04:00","promoted_at":null,"width":8192,"height":8192,"color":"#26260c","blur_hash":"L56@f%%2IUIn0fIp%2%2aKbHt7WV","description":null,"alt_description":"person in black pants wearing blue and white nike sneakers","urls":{"raw":"https:\/\/images.unsplash.com\/photo-1612902377801-b577274469a7?ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1","full":"https:\/\/images.unsplash.com\/photo-1612902377801-b577274469a7?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1&q=85","regular":"https:\/\/images.unsplash.com\/photo-1612902377801-b577274469a7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1&q=80&w=1080","small":"https:\/\/images.unsplash.com\/photo-1612902377801-b577274469a7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1&q=80&w=400","thumb":"https:\/\/images.unsplash.com\/photo-1612902377801-b577274469a7?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1&q=80&w=200"},"links":{"self":"https:\/\/api.unsplash.com\/photos\/hnP3sBOPzK4","html":"https:\/\/unsplash.com\/photos\/hnP3sBOPzK4","download":"https:\/\/unsplash.com\/photos\/hnP3sBOPzK4\/download","download_location":"https:\/\/api.unsplash.com\/photos\/hnP3sBOPzK4\/download?ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU"},"categories":[],"likes":4,"liked_by_user":false,"current_user_collections":[],"sponsorship":null,"user":{"id":"5tFd-NLJFEI","updated_at":"2021-05-09T04:06:13-04:00","username":"solesavy","name":"SoleSavy","first_name":"SoleSavy","last_name":null,"twitter_username":"","portfolio_url":"https:\/\/solesavy.com\/","bio":"Step into a genuine collective of sneaker culture. Access tools to level-up your sneaker collection & tap into a community you can trust","location":null,"links":{"self":"https:\/\/api.unsplash.com\/users\/solesavy","html":"https:\/\/unsplash.com\/@solesavy","photos":"https:\/\/api.unsplash.com\/users\/solesavy\/photos","likes":"https:\/\/api.unsplash.com\/users\/solesavy\/likes","portfolio":"https:\/\/api.unsplash.com\/users\/solesavy\/portfolio","following":"https:\/\/api.unsplash.com\/users\/solesavy\/following","followers":"https:\/\/api.unsplash.com\/users\/solesavy\/followers"},"profile_image":{"small":"https:\/\/images.unsplash.com\/profile-1612902236183-771ce83e316dimage?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=32&w=32","medium":"https:\/\/images.unsplash.com\/profile-1612902236183-771ce83e316dimage?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=64&w=64","large":"https:\/\/images.unsplash.com\/profile-1612902236183-771ce83e316dimage?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=128&w=128"},"instagram_username":"solesavyinc","total_collections":0,"total_likes":0,"total_photos":12,"accepted_tos":true,"for_hire":false},"exif":{"make":"SONY","model":"ILCE-7M2","exposure_time":"1\/80","aperture":"1.8","focal_length":"55.0","iso":800},"location":{"title":null,"name":null,"city":null,"country":null,"position":{"latitude":null,"longitude":null}},"views":77842,"downloads":309},{"id":"_Eo9ew6-0k4","created_at":"2018-09-19T13:51:49-04:00","updated_at":"2021-05-09T07:04:33-04:00","promoted_at":null,"width":3519,"height":3519,"color":"#260c0c","blur_hash":"LIC6GdtRbHRj~Vt7IoayShR%n%NG","description":"Hayamongst","alt_description":"man wearing black crew-neck T-shirt standing beside plant","urls":{"raw":"https:\/\/images.unsplash.com\/photo-1537379221007-95a05e5b2dd3?ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1","full":"https:\/\/images.unsplash.com\/photo-1537379221007-95a05e5b2dd3?crop=entropy&cs=srgb&fm=jpg&ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1&q=85","regular":"https:\/\/images.unsplash.com\/photo-1537379221007-95a05e5b2dd3?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1&q=80&w=1080","small":"https:\/\/images.unsplash.com\/photo-1537379221007-95a05e5b2dd3?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1&q=80&w=400","thumb":"https:\/\/images.unsplash.com\/photo-1537379221007-95a05e5b2dd3?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU&ixlib=rb-1.2.1&q=80&w=200"},"links":{"self":"https:\/\/api.unsplash.com\/photos\/_Eo9ew6-0k4","html":"https:\/\/unsplash.com\/photos\/_Eo9ew6-0k4","download":"https:\/\/unsplash.com\/photos\/_Eo9ew6-0k4\/download","download_location":"https:\/\/api.unsplash.com\/photos\/_Eo9ew6-0k4\/download?ixid=MnwyMjk1NjV8MHwxfHJhbmRvbXx8fHx8fHx8fDE2MjA1NjYxMTU"},"categories":[],"likes":7,"liked_by_user":false,"current_user_collections":[],"sponsorship":null,"user":{"id":"246i-NyfOSs","updated_at":"2021-05-09T06:23:12-04:00","username":"tothnorex","name":"Norbert T\u00f3th","first_name":"Norbert","last_name":"T\u00f3th","twitter_username":null,"portfolio_url":null,"bio":"https:\/\/www.instagram.com\/norex_77\r\n https:\/\/www.instagram.com\/tothnorex","location":"Hungary","links":{"self":"https:\/\/api.unsplash.com\/users\/tothnorex","html":"https:\/\/unsplash.com\/@tothnorex","photos":"https:\/\/api.unsplash.com\/users\/tothnorex\/photos","likes":"https:\/\/api.unsplash.com\/users\/tothnorex\/likes","portfolio":"https:\/\/api.unsplash.com\/users\/tothnorex\/portfolio","following":"https:\/\/api.unsplash.com\/users\/tothnorex\/following","followers":"https:\/\/api.unsplash.com\/users\/tothnorex\/followers"},"profile_image":{"small":"https:\/\/images.unsplash.com\/profile-1580408292210-6590abc9d2f2image?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=32&w=32","medium":"https:\/\/images.unsplash.com\/profile-1580408292210-6590abc9d2f2image?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=64&w=64","large":"https:\/\/images.unsplash.com\/profile-1580408292210-6590abc9d2f2image?ixlib=rb-1.2.1&q=80&fm=jpg&crop=faces&cs=tinysrgb&fit=crop&h=128&w=128"},"instagram_username":"tothnorex","total_collections":0,"total_likes":71,"total_photos":158,"accepted_tos":true,"for_hire":false},"exif":{"make":"NIKON CORPORATION","model":"NIKON D5300","exposure_time":"1\/500","aperture":"1.8","focal_length":"50.0","iso":100},"location":{"title":null,"name":null,"city":null,"country":null,"position":{"latitude":null,"longitude":null}},"views":27049,"downloads":116}]';

   foreach (json_decode($response) as $key) {
      //dd($key->urls->small);
      echo "<li><a href='" . $key->urls->small . "' target='_blank'>sd</a></li>";
   }

   //return $response;


});
