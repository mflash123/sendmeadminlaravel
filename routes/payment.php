<?php

/*
|--------------------------------------------------------------------------
| Payment Web Routes
|--------------------------------------------------------------------------
|
|
*/

/**
 * Yana kostil
 */
Route::get('/yana_sha_braid', 'BraidsController@index');
Route::post('/yana_sha_braid/postback', 'BraidsController@postback');
Route::post('/yana_sha_braid/postbacktilda', 'BraidsController@postbacktilda');

/**
 * Robokassa callback result
 */
Route::get('/robokassa/result', 'Api\v2\PaymentApiController@payment_result');