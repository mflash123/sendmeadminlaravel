<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Api\v2\ActionApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::prefix('v2')->group(function () {
    Route::post('auth',function(Request $request){
        /**
         * SIGNIN (LOGIN)
         */
        if(empty($request->input('data')['email'])){
            return 'Error. Email required';
        } elseif (empty($request->input('data')['password'])) {
            return 'Error. Password required';
        }
        $user = [
            'email' =>  $request->input('data')['email'],
            //'email' =>  'mflash123@gmail.com',
            'password'  =>  $request->input('data')['password'],
        ];

        
       if( App\User::where([
        ['email',$user['email']],
        ])->exists() ){
            $user = App\User::where([
                ['email',$user['email']],
                ])->first();

                //return response()->json(['error' => $user],403);
            //return DB::enableQueryLog();
            
            if (Hash::check($request->input('data')['password'], $user->password)) {
                return response()->json(['token'=>$user->api_token,'username'=>$user->username]);
            }

            
        }

        return response()->json(['error' => 'Not authorized.'],403);



        


    });

    Route::post('signup',function(Request $request){

        /**
         * SIGNUP (REGISTRATION)
         */        
       // return response()->json($request->input('isMobile'));

       $validatedData = $request->validate([
        'email' => ['email','indisposable','required', 'unique:users'],
        'password' => ['required','min:4'],
        'privacy' => ['accepted'],
        ]);

        $user = [
            'email' =>  $validatedData['email'],
            'password'  =>  $validatedData['password'],
        ];
       // return $user;
        // $user = App\User::where([
        //     ['email',$user['email']],
        //     ])->first();

        // if( App\User::where([['email',$user['email']]])->exists() ){

            
        //     return response()->json('User exist');

        // }
        /**
         * Gen username
         */
        $username = explode('@',$user['email']);
        $username = str_replace([".","_"],"-",$username[0]); //denyed symbols in subdomains
        $i=0;
        while( App\User::where('username',$username)->exists() ){
            $i++;
            $username = $username.$i;
        }
       
        $userModel = new App\User;
        $userModel->username=$username;
        $userModel->password=Hash::make($user['password']);
        $userModel->email=$user['email'];
        $userModel->activation_code=bin2hex(random_bytes(8));
        $userModel->active=0;
        $userModel->api_token=Str::random(80);
        if ($request->has('utm')){
            $userModel->utm = $request->input('utm');
        }
        $userModel->is_mobile = $request->input('isMobile');

        $userModel->save();
        
        //email preview for debug
        //return new App\Mail\UserApprove($userModel);

        // return response()->json('ok');
        
        Mail::to($userModel->email)
        ->send(new App\Mail\UserApprove($userModel));
        
        /** Generate avatar */
        $filename = 'ava_'.$username.'.jpg';
        Avatar::create($userModel->email)->save('storage/'.$filename, 100); // quality = 100
        App\Content::insert([
            [
                'user_id'   =>  $userModel->id,
                'type_id'   =>  1,
                'data'      =>  json_encode([
                    'fileName'  =>  $filename,
                    'type'      =>  'avatarSingle'
                ]),
                'weight'    =>  0
        ],
        ]);

        // {"fileName":"2dd2729f1d34d17a8c54b6926682c44d.png","type":"avatar"}
        
        return response()->json('ok');
        
    });
    Route::post('verify',function(Request $request){
        /**
         * Verify email
         * Deprecated. Transfer to back
         */
        if(empty($request->input('code'))){
            return 'Error. Code required';
        }
        
        $code = $request->input('code');
        if(App\User::where([['activation_code',$code],['active',0]])->exists()){
            App\User::where('activation_code',$code)
            ->update(['active'=>1]);
            return 'ok';
        }
        return 'Error. Code doesnt exist or activated early';
     
    });

    Route::post('recover',function(Request $request){
        /**
         * Recover 
         */
        if(empty($request->input('email'))){
            return 'Error. Email required';
        }
        
        $email = $request->input('email');
        if(App\User::where('email',$email)->exists()){
            $forgotten_password_code = Str::random(40);
            App\User::where('email',$email)
            ->update(['forgotten_password_code'=>$forgotten_password_code]);

            Mail::to($email)
            ->send(new App\Mail\UserRecovery($forgotten_password_code));
            // return new App\Mail\UserRecovery($forgotten_password_code);
            return 'ok';
        }
        return 'Error. Email doesnt exist';
     
    });
    Route::get('code/{code}',function($code){
        /**
         * Recover 
         */
        if(empty($code)){
            return 'Error. Code required';
        }
        
        if(App\User::where('forgotten_password_code',$code)->exists()){
            $password = Str::random(6);
           
            $user = App\User::where('forgotten_password_code',$code)->first();
           // $user->password = Hash::make($password);

            //dd($user);
            App\User::where('forgotten_password_code',$code)
            ->update(['password'=>Hash::make($password)]);

            Mail::to($user->email)
            ->send(new App\Mail\UserGeneratorEmail($password));
            // return new App\Mail\UserGeneratorEmail($password);
            return 'ok';
        }
        return 'Error. Code doesnt exist';
     
    });

    Route::get('/auth/instagram/geturl', 'Auth\Instagram@getRedirectUrl')->name('instagram.redirect');
    Route::get('/auth/instagram/callback', 'Auth\Instagram@callback')->name('instagram.callback');
    Route::get('/auth/instagram/isconnect', 'Auth\Instagram@isConnect')->name('instagram.isconnect');
    Route::get('/auth/getinstagram', 'Api\v2\UserApiController@getinstagram')->name('instagram.getuser');

    /** V2 API */
    Route::resource('user', 'Api\v2\UserApiController')->only([
        'index', 'show', 'update', 'destroy'
    ]);
    
    Route::middleware('auth:api')->resource('user', 'Api\v2\UserApiController')->only([
        'edit'
    ]);

    Route::post('content/weight',function(Request $request){
        $responce = [];
        if($request->input('contentIds')){
            foreach ($request->input('contentIds') as $key => $value) {
                $content = App\Content::findOrFail($value['id']);
                $content->weight = $key;
                $responce[]=['weight'=>$key,'id'=>$value['id']];
                $content->save();
            }
            return response()->json($responce);
        } else {
            return response()->json('Error. contentIds required');
        }
       
    });

    Route::post('content/pin',function(Request $request){
        $content_id = $request->input('contentId');

        $content = App\Content::findOrFail($content_id);
        /** searching if pin exist and disable it */
        $content = App\User::findOrFail($content->user_id)->content;
        foreach ($content as $key => $value) {
            //if($value->type_id==27){
            if($value->id==$content_id){
                //получаю все 27 типы,а тут и баннер и ава, Надо както разделить, иначе пинет только 1 последнее
                $tmp = json_decode($value->data);
  
               // return response()->json($tmp);

                if (strpos($tmp->type, 'avatar') !== false) {
                    if($tmp->type=='avatarTop'){
                        if($value->id==$content_id){
                            //unpin
                            $content = App\Content::findOrFail($content_id);
            
                            $content->data = ['fileName'=>json_decode($content->data)->fileName,'type'=>'avatar'];
                            $content->weight=-1;
                            $content->save();
                            return response()->json($content);
                        }
                        $tmp->type='avatar';
                        DB::table('content')
                            ->where('id', $value->id)
                            ->update(['data'=>json_encode($tmp)]);
                    }
                } elseif (strpos($tmp->type, 'banner') !== false) {
                    
                    if($tmp->type=='bannerTop'){
                        if($value->id==$content_id){
                            //unpin
                            $content = App\Content::findOrFail($content_id);
            
                            $content->data = ['fileName'=>json_decode($content->data)->fileName,'type'=>'banner'];
                            $content->weight=-1;
                            $content->save();
                            return response()->json($content);
                        }
                        $tmp->type='banner';
                        DB::table('content')
                            ->where('id', $value->id)
                            ->update(['data'=>json_encode($tmp)]);
                    }
                }
            }  
        }
        /** searching if pin exist and disable it */

        $content = App\Content::findOrFail($content_id);
        
        if (strpos($tmp->type, 'avatar') !== false){
            $content->data = ['fileName'=>json_decode($content->data)->fileName,'type'=>'avatarTop'];
            $content->weight=-1;
        } elseif (strpos($tmp->type, 'banner') !== false){
            $content->data = ['fileName'=>json_decode($content->data)->fileName,'type'=>'bannerTop'];
            $content->weight=-2;
        }

        $content->save();
     return response()->json($content);


       
    });

    Route::resource('content', 'Api\v2\ContentApiController');

    Route::resource('contenttype', 'Api\v2\ContentTypeApiController');

    Route::get('products/{content_id}', 'Api\v2\ProductApiController@products');
    Route::put('product/weight', 'Api\v2\ProductApiController@weightUpdate');
    
    Route::get('product/{product_id}', 'Api\v2\ProductApiController@product');
    Route::post('product', 'Api\v2\ProductApiController@store');
    Route::put('product/{product_id}', 'Api\v2\ProductApiController@update');
    Route::delete('product/{product_id}', 'Api\v2\ProductApiController@destroy');

    Route::resource('payment', 'Api\v2\PaymentApiController');



    Route::get('typesgroup/{group_id}',function($group_id){
        /** Resource fix required */
        $group = App\TypeContentGroups::findOrFail($group_id);

        $group = collect($group->typeContent)->filter(function($value,$key){
            return $value->active==1;
        });
       return response()->json($group->values());
       
    });

    Route::get('typegroups',function(){
        /** Resource fix required */
        return response()->json(App\TypeContentGroups::where('active',1)->orderBy('weight')->get());
    });

    Route::get('category',function(){
        return response()->json(App\Category::where('is_active',1)->orderBy('name')->get());
    });

    Route::get('integrations/{username?}', function(Request $request, $username = null){
        /**
         * по Username смотри Guest
         * по token смотрит админ (dashboard/...)
         */
        if(is_null($username)){
            if($request->has('api_token')){

                $contents = DB::table('users')
                ->join('content','users.id','=','content.user_id')
                ->where([
                    ['users.api_token',$request->input('api_token')],
                    ['content.type_id',$request->input('type_id')],
                ])
                //->where('content.type_id',$request->input('api_token'))
                ->select('content.id','content.type_id','content.data')
                ->first();
                return response()->json($contents);

                // $contents = DB::table('users')
                // ->join('content','users.id','=','content.user_id')
                // ->where([
                //     ['users.api_token',$request->input('api_token')]
                // ])
                // ->whereIn('content.type_id',[32,33])
                // ->select('content.id','content.type_id','content.data')
                // ->get();
                // return $contents;

            }

            return 'error';
        }

        $contents = DB::table('users')
        ->join('content','users.id','=','content.user_id')
        ->where([
            ['users.username',$username]
        ])
        ->whereIn('content.type_id',[32,33])
        ->select('content.id','content.type_id','content.data')
        ->get();
        return $contents;
    });

    Route::put('theme',function(Request $request){
        if($request->has('api_token')){
            $user = App\User::where('api_token',$request->input('api_token'))->first();
            if($request->input('type')=='guest'){
                $user->guest_theme=$request->input('data');
            } elseif ($request->input('type')=='dashboard') {
                $user->dashboard_theme=$request->input('data');
            }
            $user->save();
            return 'ok';
        }
        return 'error';
    });
    /**
     * Reports
     */
    Route::put('report',function(Request $request){
        if(App\User::where('username',$request['username'])->exists()){
            App\User::where('username',$request['username'])
            ->update(['comment'=>$request['message'].' / '.$request['email']]);
            return response(null);
        } else {
            return response(null,403);
        }
    });

    Route::get('theme',function(Request $request){
        if($request->has('api_token')){
            $user = App\User::where('api_token',$request->input('api_token'))->first();
            $result = [
                'dashboard_theme'   =>  $user->dashboard_theme,
                'guest_theme'       =>  $user->guest_theme,
            ];
            // if($request->input('type')=='guest'){
            //     $user->guest_theme=$request->input('data');
            // } elseif ($request->input('type')=='dashboard') {
            //     $user->dashboard_theme=$request->input('data');
            // }
            // $user->save();
            return response()->json($result);
        }
        return 'error';
    });

    Route::apiResource('actions', ActionApiController::class);

    /** V2 API */


});