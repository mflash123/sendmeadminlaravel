<?php

/*
|--------------------------------------------------------------------------
| Guests Web Routes
|--------------------------------------------------------------------------
|
|
*/
use Illuminate\Http\Request;

Route::domain('{username}.sendme' . (App::environment('local') ? '.loc' : '.cc'))->group(function () {
   Route::get('/', function ($username) {
      $user = App\User::where('username', $username)->firstOrFail();
      $data['seo']['title'] = $user->username;
      $data['user']['username'] = $user->username;
      return view('react.react', $data);
   });
});

Route::get('/verify/{code}', function ($code) {
   if (App\User::where([['activation_code', $code], ['active', 0]])->exists()) {
      App\User::where('activation_code', $code)
         ->update(['active' => 1]);
      return redirect("/signin?verify=1");
   } else {
      return response('Something goes wrong');
   }
});

Route::get('/pwdtest', function () {
   $password = '123456';
   return Hash::make($password);
});

Route::get('/it_sochi_community', function () {
      return redirect()->away('https://it-sochi-community.sendme.cc');
});

Route::get('/{username}', function ($username) {
   //  dd('guest');

   $user = App\User::where('username', $username)->first();
   if ($user) {
      $url = 'https://' . $username . '.sendme' . (App::environment('local') ? '.loc' : '.cc');
      //dump('user exist->redirect');
      return redirect()->away($url);
   } else {
      //dump('user doesnt exist->not redirect');
      $data['seo']['title'] = 'Dashboard';
      $data['user']['username'] = 'null';
      return view('react.react', $data);
   }

   // return redirect()->away($url);
});


Route::get('/{username}/dashboard', function ($username) {
   //fix for dummies who posted bad link like sendme.cc/mobifreshclub/dashboard	
   $url = 'https://' . $username . '.sendme' . (App::environment('local') ? '.loc' : '.cc');
   return redirect()->away($url);
});

// Route::get('/{username}/{reacturl}', function ($username) {
//    $data['seo']['title'] = $username;
//    return view('react.react', $data);
// });
// Route::get('/{username}/product/{product}', function ($username) {
//    $data['seo']['title'] = $username;
//    return view('react.react', $data);
// });

// Route::get('/{username}/p/{product}', function ($username) {
//    $data['seo']['title'] = $username;
//    return view('react.react', $data);
// });

Route::get('/dashboard/{all}', function () {
   $data['seo']['title'] = 'Dashboard';
   $data['user']['username'] = 'null';
   return view('react.react', $data);
})->where('all', '.*');

// Route::get('/r/{code}', function () {
//    $data['seo']['title'] = 'Dashboard';
//    $data['user']['username'] = 'null';
//    return view('react.react', $data);
// });

Route::get('/r/{code}', function () {
   //restore
   $data['seo']['title'] = 'Dashboard';
   $data['user']['username'] = 'null';
   return view('react.react', $data);
});

Route::get('/callback/instagram', function () {
   $data['seo']['title'] = 'Instagram';
   $data['user']['username'] = 'null';
   return view('react.react', $data);
});

Route::get('/payment/{any}', function () {
   $data['seo']['title'] = 'Payment';
   $data['user']['username'] = 'null';
   return view('react.react', $data);
});

Route::get('/', function (Request $request) {
   if ($request->has('utm_source')) {
      Cookie::queue('utm', json_encode($request->all(), JSON_UNESCAPED_UNICODE), 3600, null, null, false, false);
   }
   return view('landing.index');
});